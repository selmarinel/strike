<?php

namespace Logger;

use KronikarzClient\Domain\Preparator\TypeInterface;
use Logger\Message\StrikeLogMessage;
use KronikarzClient\Infrastructure\Logger\Scripter;

class Loggger
{
    public function __construct(string $url)
    {
        $this->scripter = new Scripter($url);
    }

    public function log(string $service, string $type, $data, string $message = '')
    {
        $logMessage = new StrikeLogMessage();
        $logMessage->setService($service);
        $logMessage->setType($type);
        $logMessage->setData(self::transformData($data));
        $logMessage->setMessage($message);
        $this->scripter->write($logMessage);
        unset($logMessage);
    }

    public function logTyped(TypeInterface $type)
    {
        $type->setData(self::transformData($type->getData()));
        $this->scripter->write($type);
    }

    /**
     * @param $data
     * @return string
     */
    private static function transformData($data): string
    {
        if (is_string($data)) {
            return $data;
        } elseif (is_array($data)) {
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        } elseif ($data instanceof \Exception) {
            $dataToWrite = [
                'message' => $data->getMessage(),
                'code' => $data->getCode(),
                'file' => $data->getFile(),
                'line' => $data->getLine(),
                'trace' => $data->getTrace(),
            ];
            return json_encode($dataToWrite);
        }
        return 'Unsupported Data type';
    }
}