<?php

use KronikarzClient\Domain\Preparator\TypeInterface;
use Logger\Loggger;

if (!function_exists('kronikarz')) {
    function kronikarz(string $service, string $type, $data, string $message = '')
    {
        $env = getenv('APP_ENV');
        if ($env === 'dev') {
            return 0;
        }

        static $log;

        if ($log === null) {
            $log = new Loggger(getenv('LOGGER_URL'));
        }
        try {
            $log->log($service, $type, $data, $message);
        } catch (Exception $exception) {

        }
    }
}

if (!function_exists('chronicle')) {
    function chronicle(TypeInterface $type)
    {
        $env = getenv('APP_ENV');

        if ($env === 'dev') {
            return 0;
        }
        static $log;

        if ($log === null) {
            $log = new Loggger(getenv('LOGGER_URL'));
        }
        try {
            $log->logTyped($type);
        } catch (Exception $exception) {
        }
    }
}