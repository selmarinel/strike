<?php

namespace Logger\Message;


use KronikarzClient\Domain\Preparator\TypeInterface;

class StrikeLogMessage implements TypeInterface
{
    /** @var string  */
    protected $message = '';
    /** @var string  */
    private $app = 'STRIKE';
    /** @var string  */
    protected $service = '';
    /** @var string  */
    protected $data = '';
    /** @var string  */
    protected $type = '';

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getApp(): string
    {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @param string $service
     */
    public function setService(string $service)
    {
        $this->service = $service;
    }

    /**
     * @param string $data
     */
    public function setData(string $data)
    {
        $this->data = $data;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }
}