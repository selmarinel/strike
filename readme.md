проект 2017 года. залит сюда намного позже


![](https://www.cupe3902.org/wp-content/uploads/2017/11/o-STRIKE-facebook-2000x1000.jpg)

# STRIKE PROCESSOR Assistant

Input Point for strike processor is **Handle** and its method **process**
Process has only one entry parameter -  *InputRequest*. It's divided for to interfaces - **InputGetRequestInterface** and **InputSetRequestInterface**.

Processor in global works with get interface, who includes 

 - scheme
 - hostname
 - slug
 - geo
 - request uri
 - identifier if user is bot
 And paginate data
 - page,
 - limit
 - offset

Ok. We got this. What next?

## Search Host

For search domain, host, and make redirects use **urlParsingService**. It's process request and make actions with it.
First you need to find host by *hostname*. If host not found, you get exception **HostNotFoundException**

Next step - is geo redirect. **HostGeoRedirectService** make redirect if domains stuck together to one domain. Can return Boolean or **RedirectException**. The exception automatically make redirect in listener.

And then, if domain has not geo redirect make regular redirect by **redirectedService**.  This service make all redirects, such as direct redirect by **redirectRepository** or domain redirect by domain data viz field redirect.

## Process Slug
After searching host and make all redirects (if need), Processor must understands, what it will do next. It's need to identify slug.
Using **uriSlugParser** it'll can do this. In this stage processor works with entity `Host`

Parsing host data, processor make array with all slugs. Each element of this array is instance of **Slug** who includes pattern, type and identifier that is static resource. 

> (somebody, kill me)

Next part - is text generate settings.  This settings need to generate seo text. Configuration loads from entity, transfrom into **TextGenerateTypeVO** and set to **HostDAO**.

From this moment, processor works with **HostDAO**.  This class is maintenance.

> ![](https://image.flaticon.com/icons/png/128/1435/1435557.png)
> HostDAO  - maintenance 
> Includes:
> - host id
> - hostname
> - entity slug
> - entity id
> - parent id
> - all slug data
> - entity type
> - robots txt
> - host type
> - template driver
> - abuse level
> - seo data
> - text generate configuration (for seo text)
> - static flag
> - geo redirect flag (for widgets)

Okay. Let's go to next step - fill **HostDAO**. Adding all slugs to *host* and check if this slug matches with *request slug*. If it's match, fulfill host with *host* data (id,hostname, parent id e.t.c) and set *type slug*, *entity slug* and *entity id* (if exists)

## HOST Type process

In general, receiver receive DTO by **hostDAO** and **Request**

### Search
As receiver appears **SearchResultDTOReceiverService**. 
First we need to search **entityToHost** entity. And next search **searchEntity** by finded *entityToHost*

Fill Search DTO with results (youtube results from search entity and elastic search results entity [for each make DTO])
So `Search` - is special DTO entity like collection, but includes different entity DTO + youtube results

### Static
As receiver appears **CollectionDTOReceiverService**. 
First stage - prepare collection. Provided by **PrepareCollectionReceiveUrl**
For root all simple. just set name, link and type as root parameters;

For collection (entity list) makes more complicated actions:
- search **entityToHost** by **CollectionParserService** (in host or parent host). And prepare **EntityToHostDAO**. Collection DAO prepare from **entityToHost**, Validate it by type and fill ( *link* by **entityLinkGenerator**) and sets to *entityToHostDAO*

>    ![](https://image.flaticon.com/icons/png/128/159/159469.png)
>      In this stage makes **entityToHostDAO**
>      It's fill with **slug**, **hostDAO** and **collectionDAO**
>      From this moment processor works with *DAO*
- Create Collection DTO Class from *entityToHost* entity type and fill it. 
> In other words create blank Collection DTO from entityToHost
> collection entity. 
- Search related data. Get data from DB by **CollectionParserService** (with paginate). For each entity from base convert to **entityDAO**
- Set total child count to DTO
- identify Parent and sets to **PrepareCollectionReceiveUrl** service
- Each *entityDAO* transform into **entityDTO** due to `MAPS` and identify children. Children sets to **PrepareCollectionReceiveUrl** service

In other words we prepare entity to host data with it's relations to process. 
Service has parent entity - collection entity
and Children entities - Entity DTOs (Every DTO know, from where it's must be filled, from what API)

So, next step - build collection DTO entity. Initial set  - parent from *PrepareCollectionReceiveUrl*
#### And more interest - identify api hosts and fetch data
For every child DTO entity get **entityType**. By this **entityType** through **ReceiverMap** identify api parameters such hostname, username and password for auth.

And fetch all data by multi curl through **FetcherService**
After fetching, For each entityDTO in collection fill with fetched data.
`Collection DTO` - is DTO who includes collection with DTO entities

### Regular (or dynamic)
As receiver appears **EntityDTOReceiverService**. 
First stage - prepare entity. Provided by **PrepareEntityReceiveUrl**
Make DTO by **entityType** and make initial fill
For this Entity search Children from **EntityNestedTree** and morph each into EntityDAO
Squash for DTO related entities and add navigation

> In other words prepare EntityDTO

For all child entityDAO transform its to EntityDTO and include into parent EntityDTO

> Fill EntityDTO with child DTOs

As in the past type identify api data due to **ReceiverMap**, after this fetch data and fill DTO's
It's Very simple, isn't it?
## Got it
Finally processor makes **DTO** and **exception**

In next stage, DTO passed on **Template service**, exception need to custom status of response while generate view;

![](https://cdn0.iconfinder.com/data/icons/flat-security-icons/128/tick.png)
