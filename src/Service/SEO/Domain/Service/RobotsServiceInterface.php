<?php

namespace App\Service\SEO\Domain\Service;

interface RobotsServiceInterface
{
    public function generate(string $hostname): string;
}