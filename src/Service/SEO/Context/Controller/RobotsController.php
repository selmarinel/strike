<?php

namespace App\Service\SEO\Context\Controller;

use App\Service\SEO\Domain\Service\RobotsServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RobotsController extends Controller
{
    public function index(
        Request $request,
        RobotsServiceInterface $robotsService
    ) {
        return new Response($robotsService->generate($request->getHost()));
    }
}