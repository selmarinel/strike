<?php

namespace App\Service\SEO\Infrastructure\Service;

use App\Service\SEO\Domain\Service\RobotsServiceInterface;

class RobotsService implements RobotsServiceInterface
{
    const HOST_ROBOTS_HEADER = "Host: %s";

    public function generate(string $hostname): string
    {
        return sprintf(self::HOST_ROBOTS_HEADER, $hostname);
    }
}