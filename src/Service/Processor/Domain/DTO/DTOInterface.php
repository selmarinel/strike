<?php

namespace App\Service\Processor\Domain\DTO;


interface DTOInterface extends SetDTOInterface, GetDTOInterface
{
    //todo move
    public function setRelations(array $relations);

    public function getRelations(): array;

    public function setText(string $text);

    public function getText(): string;

    public function setCreated(int $created);

    public function getCreated(): int;
}