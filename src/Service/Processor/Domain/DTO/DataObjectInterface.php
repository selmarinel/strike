<?php

namespace App\Service\Processor\Domain\DTO;

interface DataObjectInterface
{
    public function getChildren(): array;

    public function getLink(): string;

    public function getTotalChildren(): int;

    public function toArray(): array;

    public function getEntityType(): int;

    public function getTemplateName(): string;
}