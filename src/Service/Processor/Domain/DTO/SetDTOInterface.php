<?php

namespace App\Service\Processor\Domain\DTO;


use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface SetDTOInterface
{
    public function setEncryption(EncryptionInterface $encryption);

    public function setLink(string $link);

    public function setSourceId(string $id);

    public function fill(array $arguments);

    public function appendChild(DTOInterface $DTO);

    public function setTotalChildren(int $totalChildren);

    public function setEntityType(int $entityType);

    public function setRequest(InputGetRequestInterface $request);

    public function setTags(array $tags);
}