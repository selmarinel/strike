<?php

namespace App\Service\Processor\Domain\DTO;


interface GetDTOInterface extends DataObjectInterface
{
    public function getEntityURI(): string;

    public function getSourceId(): string;

    public function getTags():array;
}