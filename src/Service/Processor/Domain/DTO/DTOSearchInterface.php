<?php

namespace App\Service\Processor\Domain\DTO;


interface DTOSearchInterface extends DataObjectInterface
{
    public function appendResult(DTOInterface $DTO);

    public function appendChild(DTOInterface $DTO);
}