<?php

namespace App\Service\Processor\Domain\DTO;

interface DTOFieldsInterface
{
    public static function getAvailableFields(): array;
}