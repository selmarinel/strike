<?php

namespace App\Service\Processor\Domain\DTO\Collection;


interface DTOHasTags
{
    public function setTags(array $tags);
}