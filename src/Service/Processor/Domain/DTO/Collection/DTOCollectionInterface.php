<?php

namespace App\Service\Processor\Domain\DTO\Collection;

interface DTOCollectionInterface extends SetDTOCollectionInterface, GetDTOCollectionInterface
{

}