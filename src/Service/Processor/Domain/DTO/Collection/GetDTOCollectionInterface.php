<?php

namespace App\Service\Processor\Domain\DTO\Collection;

use App\Service\Processor\Domain\DTO\DataObjectInterface;

interface GetDTOCollectionInterface extends DataObjectInterface
{
    public function getName(): string;
}