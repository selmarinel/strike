<?php

namespace App\Service\Processor\Domain\DTO\Collection;

use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface SetDTOCollectionInterface
{
    public function setLink(string $link);

    public function setTotalChildren(int $totalChildren);

    public function setEntityType(int $entityType);

    public function setName(string $name);

    public function appendChild(DTOInterface $DTO);

    public function setRequest(InputGetRequestInterface $request);

}