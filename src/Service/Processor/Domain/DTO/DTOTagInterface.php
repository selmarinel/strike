<?php

namespace App\Service\Processor\Domain\DTO;


interface DTOTagInterface
{
    public function getName(): string;

    public function setName(string $name);

    public function getLink(): string;

    public function setLink(string $link);
}