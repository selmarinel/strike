<?php

namespace App\Service\Processor\Domain\DTO;


interface DTOHasNavigationInterface
{
    public function setNextLink(string $next);

    public function setPrevLink(string $prev);
}