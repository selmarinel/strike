<?php

namespace App\Service\Processor\Domain\VO\Receiver;


interface GetReceiverParametersInterface
{
    public function getUsername(): string;

    public function getPassword(): string;

    public function getHost(): string;

}