<?php

namespace App\Service\Processor\Domain\VO\Receiver;


interface SetReceiverParametersInterface
{
    public function setUsername(string $username);

    public function setPassword(string $password);

    public function setHost(string $host);
}