<?php

namespace App\Service\Processor\Domain\VO\Slug;


interface GetSlugInterface
{
    public function getPattern(): string;

    public function isStatic(): bool;

    public function getSlugType(): int;

    public function getSlug(): string;

    public function getOrder(): array;

    public function getSeparator(): string;
}