<?php

namespace App\Service\Processor\Domain\VO\Slug;


interface SetSlugInterface
{
    public function setPattern(array $pattern);

    public function setStatic(bool $static);

    public function setSlugType(int $slugType);
}