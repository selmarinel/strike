<?php

namespace App\Service\Processor\Domain\VO\Encryption;

use App\Service\Encryption\Domain\Service\EncryptionInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface SetEncryptionInterface
{
    public function setHost(GetHostInterface $host);
}