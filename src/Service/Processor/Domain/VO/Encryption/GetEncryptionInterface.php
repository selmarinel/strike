<?php

namespace App\Service\Processor\Domain\VO\Encryption;


use App\Service\Encryption\Domain\Service\EncryptionInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface GetEncryptionInterface
{
    public function getEncryption(): EncryptionInterface;

    public function getHost(): GetHostInterface;
}