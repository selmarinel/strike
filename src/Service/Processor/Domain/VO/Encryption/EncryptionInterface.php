<?php

namespace App\Service\Processor\Domain\VO\Encryption;

interface EncryptionInterface extends GetEncryptionInterface, SetEncryptionInterface
{

}