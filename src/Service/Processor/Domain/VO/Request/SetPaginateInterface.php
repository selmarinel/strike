<?php

namespace App\Service\Processor\Domain\VO\Request;


interface SetPaginateInterface
{
    public function setPage(int $page);

    public function setLimit(int $limit);
}