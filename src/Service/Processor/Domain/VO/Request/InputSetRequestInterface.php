<?php

namespace App\Service\Processor\Domain\VO\Request;


interface InputSetRequestInterface extends SetPaginateInterface, SetRequestInterface
{

}