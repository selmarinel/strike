<?php

namespace App\Service\Processor\Domain\VO\Request;


interface GetRequestInterface
{
    public function getScheme(): string;

    public function getHostName(): string;

    public function getSlug(): string;

    public function getGeo(): string;

    public function getIsBot(): bool;

    public function getRequestUri(): string;
}