<?php

namespace App\Service\Processor\Domain\VO\Request;


interface InputGetRequestInterface extends GetPaginateInterface, GetRequestInterface
{

}