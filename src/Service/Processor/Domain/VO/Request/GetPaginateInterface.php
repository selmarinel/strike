<?php

namespace App\Service\Processor\Domain\VO\Request;


interface GetPaginateInterface
{
    public function getPage(): int;

    public function getLimit(): int;

    public function getOffset(): int;
}