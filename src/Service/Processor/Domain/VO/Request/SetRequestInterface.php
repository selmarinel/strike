<?php

namespace App\Service\Processor\Domain\VO\Request;


interface SetRequestInterface
{
    public function setScheme(string $scheme);

    public function setHostname(string $hostname);

    public function setSlug(string $slug);

    public function setGeo(string $geo);

    public function setIsBot(bool $isBot);

    public function setRequestUri(string $requestUri);
}