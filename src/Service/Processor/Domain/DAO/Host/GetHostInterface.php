<?php

namespace App\Service\Processor\Domain\DAO\Host;


use App\Service\EntityTextGenerator\Infrastructure\VO\TextGenerateTypeVO;
use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;

interface GetHostInterface
{
    public function getHostId(): int;

    public function getHostname(): string;

    public function getEntitySlug(): string;

    public function getTypeSlug(): string;

    public function getEntityId(): string;

    public function isStatic(): bool;

    public function getParentId(): ?int;

    public function getEntityType(): int;

    /** @return GetSlugInterface[] */
    public function getSlugData(): array;

    public function getRobotsTxt(): string;

    public function getHostTypeDriver(): string;

    public function getTemplateDriver(): string;

    public function getAbuseLevel(): int;

    public function getSeoData(): array;

    public function isSearch(): bool;

    public function isArticles(): bool;

    public function getHostType(): int;

    /** @return TextGenerateTypeVO[] */
    public function getTextGenerateConfiguration(): array;

    public function hasGeoRedirect(): bool;
}