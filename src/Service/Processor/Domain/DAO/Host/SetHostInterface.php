<?php

namespace App\Service\Processor\Domain\DAO\Host;


use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;

interface SetHostInterface
{
    public function setHostId(int $hostId);

    public function setHostname(string $hostname);

    public function setEntitySlug(string $entitySlug);

    public function setTypeSlug(string $typeSlug);

    public function setEntityId(string $entityId);

    public function setEntityType(int $entityType);

    public function setStatic(bool $static);

    public function setParentId(int $parentId = null);

    public function setSlugData(array $slugData);

    public function appendSlug(GetSlugInterface $slug);

    public function setRobotsTxt(string $robots);

    public function setHostType(int $hostType);

    public function setTemplateDriver(string $templateDriver);

    public function setAbuseLevel(int $abuseLevel);

    public function setSeoData(array $seoData);

    public function setTextGenerateConfiguration(array $configuration);

    public function setHasGeoRedirect(bool $marked); //todo change name
}