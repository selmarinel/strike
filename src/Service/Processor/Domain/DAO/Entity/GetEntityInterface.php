<?php

namespace App\Service\Processor\Domain\DAO\Entity;


interface GetEntityInterface
{
    public function getRelatedId(): int;

    public function getEntityId(): int;

    public function getSourceId(): string;

    public function getName(): string;

    public function getEntityType(): int;

    public function getLink(): string;

    public function getEntitySlug(): string;

    public function getTags(): array;

    public function getRelations(): array;

    public function getCreated() :string;
}