<?php

namespace App\Service\Processor\Domain\DAO\Entity;


interface SetEntityInterface
{
    public function setRelatedId(int $id);

    public function setEntityId(int $entityId);

    public function setSourceId(string $sourceId);

    public function setName(string $name);

    public function setEntityType(int $entityType);

    public function setLink(string $link);

    public function setEntitySlug(string $entitySlug);

    public function setCreated(string $created);
}