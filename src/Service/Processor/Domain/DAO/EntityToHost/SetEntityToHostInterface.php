<?php

namespace App\Service\Processor\Domain\DAO\EntityToHost;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface SetEntityToHostInterface
{
    public function setEntity(GetEntityInterface $entity);

    public function setHost(GetHostInterface $host);

    public function setSlug(string $slug);

    public function setId(int $id);
}