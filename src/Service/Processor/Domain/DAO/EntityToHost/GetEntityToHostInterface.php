<?php

namespace App\Service\Processor\Domain\DAO\EntityToHost;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface GetEntityToHostInterface
{
    public function getEntity(): GetEntityInterface;

    public function getHost(): GetHostInterface;

    public function getSlug(): string;

    public function getId(): int;
}