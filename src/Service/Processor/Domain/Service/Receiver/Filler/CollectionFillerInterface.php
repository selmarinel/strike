<?php

namespace App\Service\Processor\Domain\Service\Receiver\Filler;


use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;

interface CollectionFillerInterface
{
    /**
     * @param DTOCollectionInterface $DTOCollection
     * @param DTOInterface[] $DTOCollectionToPrepare
     * @return DTOCollectionInterface
     */
    public function fill(
        DTOCollectionInterface $DTOCollection,
        array $DTOCollectionToPrepare): DTOCollectionInterface;
}