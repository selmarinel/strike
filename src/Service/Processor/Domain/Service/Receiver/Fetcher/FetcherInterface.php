<?php

namespace App\Service\Processor\Domain\Service\Receiver\Fetcher;


use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;

interface FetcherInterface
{
    /**
     * @param GetReceiverParametersInterface[] $uriCollection
     * @return array
     */
    public function fetchMany(array $uriCollection);

    /**
     * @return bool
     */
    public function hasException(): bool;
}