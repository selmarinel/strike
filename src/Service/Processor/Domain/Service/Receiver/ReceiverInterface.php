<?php

namespace App\Service\Processor\Domain\Service\Receiver;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface ReceiverInterface
{
    public function receive(GetHostInterface $host, InputGetRequestInterface $request);

    public function getException(): ?\Exception;
}