<?php

namespace App\Service\Processor\Domain\Service\Receiver;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface SlugDTOReceiverInterface extends ReceiverInterface
{
}