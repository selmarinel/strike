<?php

namespace App\Service\Processor\Domain\Service\Receiver;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface CollectionDTOReceiverInterface extends ReceiverInterface
{
    public function receive(GetHostInterface $host, InputGetRequestInterface $request): DTOCollectionInterface;
}