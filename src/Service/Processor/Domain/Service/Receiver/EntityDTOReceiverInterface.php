<?php

namespace App\Service\Processor\Domain\Service\Receiver;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\DTO\ReceiverCountException;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;

interface EntityDTOReceiverInterface extends ReceiverInterface
{
    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface
     * @throws ReceiverCountException
     * @throws EntityNotFoundException
     */
    public function receive(GetHostInterface $host, InputGetRequestInterface $request): DTOInterface;
}