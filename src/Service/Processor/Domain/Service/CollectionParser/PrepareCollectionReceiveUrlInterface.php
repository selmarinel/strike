<?php

namespace App\Service\Processor\Domain\Service\CollectionParser;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface PrepareCollectionReceiveUrlInterface
{
    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request);

    /**
     * @deprecated
     * @param GetHostInterface $host
     */
    public function prepareRoot(GetHostInterface $host);

    /**
     * @return DTOCollectionInterface
     */
    public function getParent(): DTOCollectionInterface;

    /**
     * @return DTOInterface[]
     */
    public function getChildren(): array;
}