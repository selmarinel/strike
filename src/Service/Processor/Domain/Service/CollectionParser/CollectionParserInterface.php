<?php

namespace App\Service\Processor\Domain\Service\CollectionParser;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface CollectionParserInterface
{
    public function parse(GetHostInterface $host): GetEntityToHostInterface;

    /**
     * @param GetEntityToHostInterface $entityToHost
     * @param InputGetRequestInterface $request
     * @return GetEntityInterface[]
     */
    public function getCollection(GetEntityToHostInterface $entityToHost, InputGetRequestInterface $request): array;

    public function getTotalCollectionCount(): int;
}