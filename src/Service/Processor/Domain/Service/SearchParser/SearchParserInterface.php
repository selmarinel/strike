<?php

namespace App\Service\Processor\Domain\Service\SearchParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;

interface SearchParserInterface
{
    public function makeDTO(EntityToHost $entityToHost, GetHostInterface $host): ?DTOInterface;
}