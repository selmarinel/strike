<?php

namespace App\Service\Processor\Domain\Service\SearchParser;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;

interface SearchPreparationInterface
{
    /**
     * @param GetHostInterface $host
     * @param string $query
     * @return DTOInterface[]
     */
    public function prepare(GetHostInterface $host, string $query): array;
}