<?php

namespace App\Service\Processor\Domain\Service\DTOReceiverMap;


use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;

interface ReceiveMapInterface
{
    public function map(DTOInterface $DTO): GetReceiverParametersInterface;
}