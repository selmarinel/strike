<?php

namespace App\Service\Processor\Domain\Service\TagParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface TagParserInterface
{
    public function parseTags(GetEntityInterface $entity, EntityToHost $entityToHost, GetHostInterface $host);

}