<?php

namespace App\Service\Processor\Domain\Service\EntityParser;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;

interface EntityChildrenPreparationInterface
{
    /**
     * @param GetHostInterface $host
     * @param GetEntityInterface $entity
     * @param GetPaginateInterface $paginate
     * @return GetEntityInterface|SetEntityInterface[]
     */
    public function prepare(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    ): array;

    /**
     * @return int
     */
    public function getTotalChildren(): int;

    public function findNext(): ?GetEntityInterface;

    public function findPrev(): ?GetEntityInterface;
}