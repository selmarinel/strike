<?php

namespace App\Service\Processor\Domain\Service\EntityParser;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;

interface EntityParserInterface
{
    /**
     * @param GetHostInterface $host
     * @return GetEntityToHostInterface
     * @throws EntityNotFoundException
     */
    public function parse(GetHostInterface $host): GetEntityToHostInterface;

    /**
     * @param GetHostInterface $host
     * @param GetEntityInterface $entity
     * @param GetPaginateInterface $paginate
     * @return GetEntityInterface[]
     */
    public function getChildren(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    ): array;

    /**
     * @return int
     */
    public function getCountOfChildren(): int;

    public function getPreviousLink(GetHostInterface $host): string;

    public function getNextLink(GetHostInterface $host): string;
}