<?php
/**
 * Created by PhpStorm.
 * User: selmarinel
 * Date: 14.06.18
 * Time: 17:12
 */

namespace App\Service\Processor\Domain\Service\EntityParser\Tag;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;

interface TagChildrenPreparationInterface
{
    public function getChildren(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    );

    public function getCountOfChildren(): int;
}