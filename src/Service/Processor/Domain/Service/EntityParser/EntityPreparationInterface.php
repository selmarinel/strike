<?php

namespace App\Service\Processor\Domain\Service\EntityParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;

interface EntityPreparationInterface
{
    public function prepareEntityFromEntityToHost(EntityToHost $entityToHost): GetEntityInterface;
}