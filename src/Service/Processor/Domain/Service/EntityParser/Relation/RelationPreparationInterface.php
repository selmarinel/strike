<?php

namespace App\Service\Processor\Domain\Service\EntityParser\Relation;


use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface RelationPreparationInterface
{
    public function prepare(
        GetEntityToHostInterface $entityToHost,
        InputGetRequestInterface $request
    ): array;
}