<?php

namespace App\Service\Processor\Domain\Service\EntityParser\Relation;


use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface RelationSquashInterface
{
    /**
     * @param DTOInterface $DTOEntity
     * @param GetEntityToHostInterface $entityToHost
     * @param InputGetRequestInterface $request
     * @param EncryptionInterface $encryption
     * @return mixed
     */
    public function squash(
        DTOInterface $DTOEntity,
        GetEntityToHostInterface $entityToHost,
        InputGetRequestInterface $request,
        EncryptionInterface $encryption
    );
}