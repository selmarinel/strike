<?php

namespace App\Service\Processor\Domain\Service\EntityParser;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface PrepareEntityReceiveUrlInterface
{
    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface[]
     * @throws \App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request): array;
}