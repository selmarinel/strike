<?php

namespace App\Service\Processor\Domain\Service\UrilParser;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;
use App\Service\Processor\Infrastructure\Exception\Redirect\RedirectException;

interface UrlParserServiceInterface
{
    /**
     * @param GetRequestInterface $request
     * @param bool $ignoreRedirect
     * @return GetHostInterface
     */
    public function processRequest(GetRequestInterface $request, bool $ignoreRedirect = false): GetHostInterface;
}