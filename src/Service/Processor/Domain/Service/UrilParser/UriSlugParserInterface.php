<?php

namespace App\Service\Processor\Domain\Service\UrilParser;


use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;

interface UriSlugParserInterface
{
    /**
     * @param array $slugDataFromHost
     * @return GetSlugInterface[]
     * @throws HostSlugException
     */
    public function parseSlug(array $slugDataFromHost);
}