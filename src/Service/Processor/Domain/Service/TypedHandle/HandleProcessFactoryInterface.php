<?php

namespace App\Service\Processor\Domain\Service\TypedHandle;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\ReceiverInterface;

interface HandleProcessFactoryInterface
{
    public function getReceiver(GetHostInterface $host): ReceiverInterface;
}