<?php

namespace App\Service\Processor\Domain\Service\EntityLinkGenerator;

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface EntityLinkGeneratorInterface
{
    public function generateLink(GetEntityInterface $entity, GetHostInterface $host): string;
}