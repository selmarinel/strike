<?php

namespace App\Service\Processor\Domain\Service\EntityLinkGenerator;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface PrepareHostAndEntityInterface
{
    public function prepareFromEntityToHost(EntityToHost $entityToHost);

    public function getHostDAO(): GetHostInterface;

    public function getEntityDAO(): GetEntityInterface;
}