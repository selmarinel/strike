<?php

namespace App\Service\Processor\Domain\Service\RequestParser;


use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use Symfony\Component\HttpFoundation\Request;

interface RequestPreparationInterface
{
    /**
     * @param Request $request
     * @return InputGetRequestInterface
     */
    public function prepareFromHttpRequest(Request $request): InputGetRequestInterface;

    public function prepareFromString(string $url): InputGetRequestInterface;
}