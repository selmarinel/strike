<?php

namespace App\Service\Processor\Domain\Service\DTOPrepare;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface PrepareBySlugInterface
{
    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface[]|array
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request): array;
}