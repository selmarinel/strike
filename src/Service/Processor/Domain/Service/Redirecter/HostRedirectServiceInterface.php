<?php

namespace App\Service\Processor\Domain\Service\Redirecter;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;

interface HostRedirectServiceInterface
{
    public function redirect(GetRequestInterface $request, Host $host): bool;

    public function hasRedirectRules(Host $host): bool;
}