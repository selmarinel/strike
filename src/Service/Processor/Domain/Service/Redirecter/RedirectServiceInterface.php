<?php
/**
 * Created by PhpStorm.
 * User: vladislavpistun
 * Date: 12.06.18
 * Time: 17:53
 */

namespace App\Service\Processor\Domain\Service\Redirecter;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;

interface RedirectServiceInterface
{
    public function redirect(GetRequestInterface $request, Host $host);
}