<?php

namespace App\Service\Processor\Domain\Service\EntityExtensionText;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;

interface EntityTextInterface
{
    public function setHost(GetHostInterface $host);

    public function extend(DTOInterface $dataObject);
}