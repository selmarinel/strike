<?php

namespace App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextFetcher;


use App\Service\Processor\Infrastructure\Service\EntityExtensionText\VO\EntityTextVO;

interface EntityTextGetInterface
{
    public function getTextDTO(int $entityId, int $hostId):? EntityTextVO;
}