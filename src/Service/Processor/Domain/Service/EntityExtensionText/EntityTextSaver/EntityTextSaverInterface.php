<?php

namespace App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextSaver;


interface EntityTextSaverInterface
{
    public function append(int $entityId, int $hostId, string $text): void;

    public function save(): void;
}