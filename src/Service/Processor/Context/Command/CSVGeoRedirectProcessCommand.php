<?php

namespace App\Service\Processor\Context\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class CSVGeoRedirectProcessCommand extends Command
{
    /** @var string */
    private $dir;

    public function __construct(KernelInterface $kernel)
    {
        $this->dir = $kernel->getRootDir() . '/../';
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('app:process:csv:geo:redirects')
            ->setAliases(['a:p:c:g:r'])
            ->addArgument('path', InputArgument::REQUIRED, 'Path to csv with hosts');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('path');

        if ($file && file_exists($file)) {
            $output->writeln(sprintf('Now we start parsing file: %s', $file));
            $fiO = new \SplFileObject($file);
            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }
                $domain = trim($line);
                $cmd = "php {$this->dir}bin/console app:rules:geo:redirect {$domain}";
                $process = new Process($cmd);
                $process->setTimeout(120);
                $output->writeln("<info>[START]</info> Processing [{$domain}]");
                $process->run();
                if(!$process->getExitCode()){
                    $output->writeln("<info>[FINISH]</info> Complete");
                } else {
                    $output->writeln("<error>[FINISH]</error> Complete with error");
                }

            }
        }
    }
}