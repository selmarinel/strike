<?php

namespace App\Service\Processor\Context\Command;


use App\Service\Base\Infrastructure\Entity\Redirect\HostGeoRedirect;
use App\Service\Base\Infrastructure\Repository\Redirect\HostGeoRedirectRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeoRedirectRulesCommand extends Command
{
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityManagerInterface */
    private $em;
    /** @var HostGeoRedirectRepository */
    private $geoRedirectRepository;

    public function __construct(HostRepository $hostRepository,
                                HostGeoRedirectRepository $geoRedirectRepository,
                                EntityManagerInterface $entityManager)
    {
        $this->hostRepository = $hostRepository;
        $this->geoRedirectRepository = $geoRedirectRepository;
        $this->em = $entityManager;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('app:rules:geo:redirect')
            ->setAliases(['a:r:g:r'])
            ->addArgument('host', InputArgument::REQUIRED, 'Host id to set rule');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $hostArgument = $input->getArgument('host');
        /** @var Host $host */
        if(is_numeric($hostArgument)) {
            $host = $this->hostRepository->find($hostArgument);
        } elseif (is_string($hostArgument)){
            $host = $this->hostRepository->findByHostName($hostArgument);
        } else {
            $output->writeln('<error>[ERROR]</error> Invalid host argument');
            return 1;
        }
        if (!$host) {
            $output->writeln('<error>[ERROR]</error> Host not found');
            return 1;
        }
        if ($host->isParent()) {
            return $this->processParent($host, $output);
        } else {
            return $this->processParent($host->getParent(), $output);
        }
    }

    private function processParent(Host $parentPost, OutputInterface $output)
    {
        /** @var Host $hostTo */
        $hostTo = $this->hostRepository->findOneBy([
            'parent' => $parentPost,
            'abuse_level' => 1,
            'geo' => ''
        ]);
        if (!$hostTo) {
            $output->writeln('<error>[ERROR]</error> Not exists global host');
            return 1;
        }

        $hostGeoRedirect = $this->geoRedirectRepository->findOneBy([
            'hostTo' => $hostTo,
            'hostFrom' => $parentPost]);
        if ($hostGeoRedirect) {
            $output->writeln('<info>[NOTICE]</info> Redirect already exists');
            return 1;
        }
        $output->writeln('<comment>[INFO]</comment> Creating Redirect');
        $hostGeoRedirect = new HostGeoRedirect();
        $hostGeoRedirect->setHostFrom($parentPost);
        $hostGeoRedirect->setHostTo($hostTo);
        $this->em->persist($hostGeoRedirect);
        $this->em->flush();
        return 0;
    }
}