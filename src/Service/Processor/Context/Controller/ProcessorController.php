<?php

namespace App\Service\Processor\Context\Controller;

use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Processor\Infrastructure\Service\Handle as Processor;
use App\Service\Templator\Infrastructure\Handle as Decorator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProcessorController extends Controller
{
    /**
     * @param Request $request
     * @param Processor $service
     * @param Decorator $decorator
     * @param RequestPreparationInterface $requestPreparation
     * @return Response
     * @throws \App\Service\Processor\Infrastructure\Exception\DTO\ReceiverCountException
     * @throws \App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostSlugException
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(
        Request $request,
        Processor $service,
        Decorator $decorator,
        RequestPreparationInterface $requestPreparation
    )
    {
        $processorRequest = $requestPreparation->prepareFromHttpRequest($request);
        $service->process($processorRequest);

        $response = new Response($decorator->draw($service->getHost(), $service->getDTO(), $processorRequest));
        $this->addNoIndexFollowForParamReqs($request, $response);

        if($service->getException()){
            $response->setStatusCode($service->getException()->getCode());
        }

        return $response;

    }

    private function addNoIndexFollowForParamReqs(Request $request, Response $response)
    {

        if ($request->getQueryString()) {
            $response->headers->set('X-Robots-Tag', 'noindex,follow');
        }

    }
}