<?php

namespace App\Service\Processor\Context\Service;

use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use App\Service\Processor\Domain\VO\Receiver\SetReceiverParametersInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Domain\VO\Request\InputSetRequestInterface;
use App\Service\Processor\Infrastructure\Service\Handle;
use Symfony\Component\HttpFoundation\Request;

class PrepareFromRequest
{
    /** @var Handle */
    private $service;
    /** @var InputSetRequestInterface|InputGetRequestInterface */
    private $request;

    /**
     * PrepareFromRequest constructor.
     * @param Handle $service
     * @param InputSetRequestInterface $request
     */
    public function __construct(
        Handle $service,
        InputSetRequestInterface $request
    ) {
        $this->service = $service;
        $this->request = $request;
    }


    /**
     * @param Request $request
     * @return void
     * @throws \App\Service\Processor\Infrastructure\Exception\DTO\ReceiverCountException
     * @throws \App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostSlugException
     */
    public function process(Request $request)
    {
        $this->request->setHostname($request->getHost());
        $this->request->setSlug($request->get('slug'));
        $this->service->process($this->request);
    }
}