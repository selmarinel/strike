<?php
/**
 * Created by PhpStorm.
 * User: selmarinel
 * Date: 14.05.18
 * Time: 15:57
 */

namespace App\Service\Processor\Infrastructure\Exception\Map;


class MapException extends \Exception
{
    protected $code = 404;

    protected $message = 'Map Exception';
}