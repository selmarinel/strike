<?php

namespace App\Service\Processor\Infrastructure\Exception\EntityToHost;


class EntityNotFoundException extends EntityException
{
    protected $code = 404;

    protected $message = 'Entity Not Found Exception';
}