<?php

namespace App\Service\Processor\Infrastructure\Exception\EntityToHost;


class EntityException extends EntityToHostException
{
    protected $code = 400;
    protected $message = 'Entity Exception';
}