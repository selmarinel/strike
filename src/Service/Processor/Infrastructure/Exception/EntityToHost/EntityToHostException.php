<?php

namespace App\Service\Processor\Infrastructure\Exception\EntityToHost;


class EntityToHostException extends \Exception
{
    protected $code = 400;

    protected $message = 'Entity to Host Exception';
}