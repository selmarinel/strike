<?php

namespace App\Service\Processor\Infrastructure\Exception\DTO;


class DTOException extends \Exception
{

    protected $code = 400;

    protected $message = 'DTO Exception';
}