<?php

namespace App\Service\Processor\Infrastructure\Exception\DTO;


class ReceiverCountException extends DTOException
{
    protected $message = "Count of returned requests is few";
}