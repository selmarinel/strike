<?php

namespace App\Service\Processor\Infrastructure\Exception\DTO;


class FillDTOException extends DTOException
{
    protected $code = 429;

    protected $message = 'Fill from API fail';
}