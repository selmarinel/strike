<?php

namespace App\Service\Processor\Infrastructure\Exception\Redirect;

class RedirectException extends \Exception
{
    protected $code = 302;
}