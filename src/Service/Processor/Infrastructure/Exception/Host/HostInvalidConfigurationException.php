<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class HostInvalidConfigurationException extends HostException
{
    protected $message = 'Invalid Host Configuration';

    protected $code = 302;
}