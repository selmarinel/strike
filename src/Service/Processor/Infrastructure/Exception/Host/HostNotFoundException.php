<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class HostNotFoundException extends HostException
{
    protected $code = 404;

    protected $message = 'Host Not Found';
}