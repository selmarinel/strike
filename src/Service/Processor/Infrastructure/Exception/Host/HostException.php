<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class HostException extends \Exception
{
    protected $code = 400;

    protected $message = 'Host Exception';
}