<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class HostRedirectGeoException extends \Exception
{
    protected $code = 302;
}