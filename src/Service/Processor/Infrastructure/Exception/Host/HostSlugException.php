<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class HostSlugException extends HostException
{
    protected $code = 400;

    protected $message = 'Slug Exception';
}