<?php

namespace App\Service\Processor\Infrastructure\Exception\Host;


class RootHostBotRedirectException extends \Exception
{
    protected $code = '200';

    /** @var string */
    protected $url = '';

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}