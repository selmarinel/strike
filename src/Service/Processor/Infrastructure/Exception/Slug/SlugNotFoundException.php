<?php

namespace App\Service\Processor\Infrastructure\Exception\Slug;


class SlugNotFoundException extends SlugException
{
    protected $code = 404;

    protected $message = 'Page Not Found';
}