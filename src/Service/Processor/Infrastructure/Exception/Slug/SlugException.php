<?php

namespace App\Service\Processor\Infrastructure\Exception\Slug;


class SlugException extends \Exception
{
    protected $code = 400;

    protected $message = 'Slug Exception';
}