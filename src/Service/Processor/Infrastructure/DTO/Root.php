<?php

namespace App\Service\Processor\Infrastructure\DTO;

use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;

class Root extends AbstractCollectionDTO implements DTOCollectionInterface
{
    protected $templateName = 'root';
}