<?php

namespace App\Service\Processor\Infrastructure\DTO\Search;

use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class YouTubeVideo extends AbstractDTO implements DTOInterface
{
    /** @var string */
    private $title = '';
    /** @var string */
    private $id = '';
    /** @var string */
    private $src = '';
    /** @var string */
    private $description = '';

    public function getChildren(): array
    {
        return [];
    }

    public function getLink(): string
    {
        return 'https://www.youtube.com/watch?v=' . $this->getId();
    }

    public function getTotalChildren(): int
    {
        return 0;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'src' => $this->getSrc(),
            'description' => $this->getDescription(),
            '__link' => $this->getLink()
        ];
    }

    public function getEntityType(): int
    {
        return 0;
    }

    public function getTemplateName(): string
    {
        return 'youtube';
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param mixed $src
     */
    public function setSrc($src): void
    {
        $this->src = $src;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function fill(array $arguments)
    {

    }
}