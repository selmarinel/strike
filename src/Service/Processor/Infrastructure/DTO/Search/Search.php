<?php

namespace App\Service\Processor\Infrastructure\DTO\Search;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOSearchInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractCollectionDTO;

class Search extends AbstractCollectionDTO implements DTOSearchInterface
{
    private $query = '';

    /** @var string */
    protected $templateName = 'search';

    protected $entityType = Type::SEARCH_TYPE;
    /**
     * @var array
     */
    private $results = [];

    public function toArray(): array
    {
        return [
            'query' => $this->getQuery(),
            'items' => $this->processItems(),
            'results' => array_map(function (DataObjectInterface $dataObject) {
                return $dataObject->toArray();
            }, $this->getChildren())
        ];
    }

    private function processItems()
    {
        $result = [];
        /** @var DataObjectInterface $item */
        foreach ($this->results as $item) {
            $result[$item->getEntityType()][] = $item->toArray();
        }
        return $result;
    }

    public function appendResult(DTOInterface $DTO)
    {
        $this->results[] = $DTO;
    }

    public function getEntityType(): int
    {
        return $this->entityType;
    }

    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query): void
    {
        $this->query = $query;
    }
}