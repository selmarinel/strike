<?php

namespace App\Service\Processor\Infrastructure\DTO\Video\Tag;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Genre extends AbstractDTO implements DTOInterface, DTOTagInterface
{
    /** @var string */
    private $name = '';

    /** @var int */
    protected $entityType = Type::VIDEO_SERIES_GENRE;

    protected $entityURL = 'series/genre';

    protected $templateName = 'genre';

    /**
     * @return array
     */
    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'name' => $this->getName(),
            'items' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['name'])) {
            $this->setName($arguments['name']);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}