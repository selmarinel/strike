<?php

namespace App\Service\Processor\Infrastructure\DTO\Video;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOHasTags;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;
use App\Service\Processor\Infrastructure\Traits\DTO\DescriptionCutterTrait;
use App\Service\Processor\Infrastructure\Traits\DTO\RatingCalculateTrait;

class Series extends AbstractDTO implements DTOInterface, DTOFieldsInterface, DTOHasTags
{
    use DescriptionCutterTrait, RatingCalculateTrait;

    /** @var int */
    private $id = 0;
    /** @var string */
    private $titleRu = '';
    /** @var string */
    private $titleEn = '';
    /** @var string */
    private $titleKk = '';
    /** @var string */
    private $titleAlt = '';
    /** @var string */
    private $description = '';
    /** @var string */
    private $descriptionEn = '';
    /** @var string */
    private $descriptionKk = '';
    /** @var string */
    private $posterSalt = '';
    /** @var string */
    private $type = '';
    /** @var string */
    private $imdbRating = '';
    /** @var string */
    private $imdbUrl = '';
    /** @var string */
    private $startYear = '';
    /** @var string */
    private $trailer = '';
    /** @var string */
    private $kinopoiskUrl = '';
    /** @var string */
    private $kinopoiskRating = '';

    /** @var string */
    protected $templateName = 'seria';

    protected $entityURL = 'video/series';

    protected $entityType = Type::VIDEO_SERIES;

    public function fill(array $arguments)
    {
        if (isset($arguments['id'])) {
            $this->setId($arguments['id']);
        }
        if (isset($arguments['title_ru'])) {
            $this->setTitleRu($arguments['title_ru']);
        }
        if (isset($arguments['title_en'])) {
            $this->setTitleEn($arguments['title_en']);
        }
        if (isset($arguments['title_kk'])) {
            $this->setTitleKk($arguments['title_kk']);
        }
        if (isset($arguments['title_alt'])) {
            $this->setTitleAlt($arguments['title_alt']);
        }
        if (isset($arguments['description'])) {
            $this->setDescription($arguments['description']);
        }
        if (isset($arguments['description_en'])) {
            $this->setDescriptionEn($arguments['description_en']);
        }
        if (isset($arguments['description_kk'])) {
            $this->setDescriptionKk($arguments['description_kk']);
        }
        if (isset($arguments['poster_salt'])) {
            $this->setPosterSalt($arguments['poster_salt']);
        }
        if (isset($arguments['type'])) {
            $this->setType($arguments['type']);
        }
        if (isset($arguments['imdb_url'])) {
            $this->setImdbUrl($arguments['imdb_url']);
        }
        if (isset($arguments['start_year'])) {
            $this->setStartYear($arguments['start_year']);
        }
        if (isset($arguments['trailer'])) {
            $this->setTrailer($arguments['trailer']);
        }
        if (isset($arguments['kinopoisk_url'])) {
            $this->setKinopoiskUrl($arguments['kinopoisk_url']);
        }
        $this->setImdbRating($this->calculateRating(
            $arguments,
            'imdb_rating',
            'title_en'));
        $this->setKinopoiskRating($this->calculateRating(
            $arguments,
            'kinopoisk_rating',
            'title_en'));
    }


    public function toArray(): array
    {
        $current = [
            'id' => $this->getId(),
            'titleRu' => $this->getTitleRu(),
            'titleEn' => $this->getTitleEn(),
            'titleKk' => $this->getTitleKk(),
            'titleAlt' => $this->getTitleAlt(),
            'description' => $this->cut($this->getDescription()),
            'descriptionEn' => $this->cut($this->getDescriptionEn()),
            'descriptionKk' => $this->cut($this->getDescriptionKk()),
            'posterSalt' => $this->getPosterSalt(),
            'type' => $this->getType(),
            'trailer' => $this->getTrailer(),
            'startYear' => $this->getStartYear(),
            'imdbRating' => $this->getImdbRating(),
            'imdbUrl' => $this->getImdbUrl(),
            'kinopoiskRating' => $this->getKinopoiskRating(),
            'kinopoiskUrl' => $this->getKinopoiskUrl(),
            'photo' => $this->getPhoto(),
            'seasons' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'domain',
            'id',
            'titleRu',
            'titleEn',
            'titleKk',
            'titleAlt',
            'description',
            'descriptionEn',
            'descriptionKk',
            'posterSalt',
            'type',
            'trailer',
            'startYear',
            'imdbRating',
            'imdbUrl',
            'kinopoiskRating',
            'kinopoiskUrl'
        ];
    }

    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_nur_' . $this->getPosterSalt()
            ) . '.jpg';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitleRu(): string
    {
        return $this->titleRu;
    }

    /**
     * @param string $titleRu
     */
    public function setTitleRu(string $titleRu): void
    {
        $this->titleRu = $titleRu;
    }

    /**
     * @return string
     */
    public function getTitleEn(): string
    {
        return $this->titleEn;
    }

    /**
     * @param string $titleEn
     */
    public function setTitleEn(string $titleEn): void
    {
        $this->titleEn = $titleEn;
    }

    /**
     * @return string
     */
    public function getTitleKk(): string
    {
        return $this->titleKk;
    }

    /**
     * @param string $titleKk
     */
    public function setTitleKk(string $titleKk): void
    {
        $this->titleKk = $titleKk;
    }

    /**
     * @return string
     */
    public function getTitleAlt(): string
    {
        return $this->titleAlt;
    }

    /**
     * @param string $titleAlt
     */
    public function setTitleAlt(string $titleAlt): void
    {
        $this->titleAlt = $titleAlt;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescriptionEn(): string
    {
        return $this->descriptionEn;
    }

    /**
     * @param string $descriptionEn
     */
    public function setDescriptionEn(string $descriptionEn): void
    {
        $this->descriptionEn = $descriptionEn;
    }

    /**
     * @return string
     */
    public function getDescriptionKk(): string
    {
        return $this->descriptionKk;
    }

    /**
     * @param string $descriptionKk
     */
    public function setDescriptionKk(string $descriptionKk): void
    {
        $this->descriptionKk = $descriptionKk;
    }

    /**
     * @return string
     */
    public function getPosterSalt(): string
    {
        return $this->posterSalt;
    }

    /**
     * @param string $posterSalt
     */
    public function setPosterSalt(string $posterSalt): void
    {
        $this->posterSalt = $posterSalt;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getImdbRating(): string
    {
        return $this->imdbRating;
    }

    /**
     * @param string $imdbRating
     */
    public function setImdbRating(string $imdbRating): void
    {
        $this->imdbRating = $imdbRating;
    }

    /**
     * @return string
     */
    public function getImdbUrl(): string
    {
        return $this->imdbUrl;
    }

    /**
     * @param string $imdbUrl
     */
    public function setImdbUrl(string $imdbUrl): void
    {
        $this->imdbUrl = $imdbUrl;
    }

    /**
     * @return string
     */
    public function getStartYear(): string
    {
        return $this->startYear;
    }

    /**
     * @param string $startYear
     */
    public function setStartYear(string $startYear): void
    {
        $this->startYear = $startYear;
    }

    /**
     * @return string
     */
    public function getTrailer(): string
    {
        return $this->trailer;
    }

    /**
     * @param string $trailer
     */
    public function setTrailer(string $trailer): void
    {
        $this->trailer = $trailer;
    }

    /**
     * @return string
     */
    public function getKinopoiskUrl(): string
    {
        return $this->kinopoiskUrl;
    }

    /**
     * @param string $kinopoiskUrl
     */
    public function setKinopoiskUrl(string $kinopoiskUrl): void
    {
        $this->kinopoiskUrl = $kinopoiskUrl;
    }

    /**
     * @return string
     */
    public function getKinopoiskRating(): string
    {
        return $this->kinopoiskRating;
    }

    /**
     * @param string $kinopoiskRating
     */
    public function setKinopoiskRating(string $kinopoiskRating): void
    {
        $this->kinopoiskRating = $kinopoiskRating;
    }

}