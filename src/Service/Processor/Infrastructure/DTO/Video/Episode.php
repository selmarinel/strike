<?php

namespace App\Service\Processor\Infrastructure\DTO\Video;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOHasNavigationInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Episode extends AbstractDTO implements DTOInterface, DTOFieldsInterface, DTOHasNavigationInterface
{
    /** @var string */
    private $seriesId = '';
    /** @var  string */
    private $videoId = '';
    /** @var  string|null */
    private $secondVideoId = '';
    /** @var string */
    private $season = '';
    /** @var  string */
    private $episode = '';
    /** @var  string|null */
    private $titleEn;
    /** @var  string|null */
    private $titleRu = '';
    /** @var  string|null */
    private $onlineDate = '';
    /** @var  string|null */
    private $isActive;
    /** @var  string|null */
    private $isUniqueDescription;
    /** @var  string */
    private $seasonEpisodeOrder;
    /** @var string */
    private $nextLink = '';
    /** @var string */
    private $prevLink = '';

    /** @var string */
    protected $templateName = 'episode';

    public function setNextLink(string $next)
    {
        $this->nextLink = $next;
    }

    public function setPrevLink(string $prev)
    {
        $this->prevLink = $prev;
    }

    public function getText(): string
    {
        if (!$this->text) {
            return $this->getTitleRu();
        }
        return $this->text;
    }

    protected $entityURL = 'video/series/episode';

    protected $entityType = Type::VIDEO_EPISODE;

    public function fill(array $arguments)
    {
        if (isset($arguments['series_id'])) {
            $this->setSeriesId($arguments['series_id']);
        }
        if (isset($arguments['season'])) {
            $this->setSeason($arguments['season']);
        }
        if (isset($arguments['second_video_id'])) {
            $this->setSecondVideoId($arguments['second_video_id']);
        }
        if (isset($arguments['episode'])) {
            $this->setEpisode($arguments['episode']);
        }
        if (isset($arguments['title_en'])) {
            $this->setTitleEn($arguments['title_en']);
        }
        if (isset($arguments['title_ru'])) {
            $this->setTitleRu($arguments['title_ru']);
        }
        if (isset($arguments['online_date'])) {
            $this->setOnlineDate($arguments['online_date']);
        }
        if (isset($arguments['is_active'])) {
            $this->setIsActive($arguments['is_active']);
        }
        if (isset($arguments['is_unique_description'])) {
            $this->setIsUniqueDescription($arguments['is_unique_description']);
        }
        if (isset($arguments['season_episode_order'])) {
            $this->setSeasonEpisodeOrder($arguments['season_episode_order']);
        }
        if (isset($arguments['video_id'])) {
            $this->setVideoId($arguments['video_id']);
        }
    }

    protected function generateMeta()
    {
        $date = date('U');
        $result = [
            "@context" => "http://schema.org/",
            "@type" => "TVSeries",
            "@id" => $this->getLink(),
            "name" => $this->getTitleRu(),
            "url" => "//player{$date}.dzenkinos.kz/service/commercial/{$this->getVideoId()}?illegal=1",
            "description" => $this->getText(),
            "datePublished" => $this->getOnlineDate()
        ];
        return "<script type=\"application/ld+json\">" .
            json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            . "</script>";
    }

    public function toArray(): array
    {
        $current = [
            "series_id" => $this->getSeriesId(),
            "second_video_id" => $this->getSecondVideoId(),
            "season" => $this->getSeason(),
            "episode" => $this->getEpisode(),
            "titleEn" => $this->getTitleEn(),
            "titleRu" => $this->getTitleRu(),
            "online_date" => $this->getOnlineDate(),
            "is_active" => $this->getIsActive(),
            "is_unique_description" => $this->getIsUniqueDescription(),
            "season_episode_order" => $this->getSeasonEpisodeOrder(),
            'videoId' => $this->getVideoId(),

            '__prev' => $this->prevLink,
            '__next' => $this->nextLink

        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            "videoId",
            "series_id",
            "second_video_id",
            "season",
            "episode",
            "titleEn",
            "titleRu",
            "online_date",
            "is_active",
            "is_unique_description",
            "season_episode_order"
        ];
    }

    /**
     * @return string
     */
    public function getSeriesId(): string
    {
        return $this->seriesId;
    }

    /**
     * @param string $seriesId
     */
    public function setSeriesId(string $seriesId)
    {
        $this->seriesId = $seriesId;
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId(string $videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return null|string
     */
    public function getSecondVideoId()
    {
        return $this->secondVideoId;
    }

    /**
     * @param null|string $secondVideoId
     */
    public function setSecondVideoId($secondVideoId)
    {
        $this->secondVideoId = $secondVideoId;
    }

    /**
     * @return string
     */
    public function getSeason(): string
    {
        return $this->season;
    }

    /**
     * @param string $season
     */
    public function setSeason(string $season)
    {
        $this->season = $season;
    }

    /**
     * @return string
     */
    public function getEpisode(): string
    {
        return $this->episode;
    }

    /**
     * @param string $episode
     */
    public function setEpisode(string $episode)
    {
        $this->episode = $episode;
    }

    /**
     * @return null|string
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * @param null|string $titleEn
     */
    public function setTitleEn($titleEn)
    {
        $this->titleEn = $titleEn;
    }

    /**
     * @return null|string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * @param null|string $titleRu
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;
    }

    /**
     * @return null|string
     */
    public function getOnlineDate()
    {
        return $this->onlineDate;
    }

    /**
     * @param null|string $onlineDate
     */
    public function setOnlineDate($onlineDate)
    {
        $this->onlineDate = $onlineDate;
    }

    /**
     * @return null|string
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param null|string $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return null|string
     */
    public function getIsUniqueDescription()
    {
        return $this->isUniqueDescription;
    }

    /**
     * @param null|string $isUniqueDescription
     */
    public function setIsUniqueDescription($isUniqueDescription)
    {
        $this->isUniqueDescription = $isUniqueDescription;
    }

    /**
     * @return string
     */
    public function getSeasonEpisodeOrder(): ?string
    {
        return $this->seasonEpisodeOrder;
    }

    /**
     * @param string $seasonEpisodeOrder
     */
    public function setSeasonEpisodeOrder(string $seasonEpisodeOrder)
    {
        $this->seasonEpisodeOrder = $seasonEpisodeOrder;
    }
}
