<?php

namespace App\Service\Processor\Infrastructure\DTO\Video\Collection;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractCollectionDTO;

class TVSeries extends AbstractCollectionDTO implements DTOCollectionInterface, DTOFieldsInterface
{
    protected $name = 'Список Артистов';

    /** @var string */
    protected $templateName = 'collection/series';

    protected $entityType = Type::COLLECTION_VIDEO_SERIES;

    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'series' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            'total',
            'name',
            'domain',
            'series'
        ];
    }
}