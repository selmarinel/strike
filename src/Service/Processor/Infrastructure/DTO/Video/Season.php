<?php

namespace App\Service\Processor\Infrastructure\DTO\Video;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Season extends AbstractDTO implements DTOInterface, DTOFieldsInterface
{
    /** @var  int */
    private $seriesId = 0;
    /** @var  int */
    private $season = 0;
    /** @var string */
    private $name = '';

    /** @var string */
    protected $templateName = 'season';

    protected $entityURL = 'video/series/season';

    protected $entityType = Type::VIDEO_SEASON;

    public function fill(array $arguments)
    {
        if (isset($arguments['series_id'])) {
            $this->setSeriesId($arguments['series_id']);
        }
        if (isset($arguments['season'])) {
            $this->setSeason($arguments['season']);
        }
        if (isset($arguments['title_ru'])) {
            $this->setName($arguments['title_ru']);
        }
    }

    public function toArray(): array
    {
        $current = [
            'id' => $this->getComplicatedId(),
            'name' => $this->getName(),
            'description' => '',
            'seriesId' => $this->getSeriesId(),
            'season' => $this->getSeason(),
            'episodes' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'name',
            'seriesId',
            'season',
        ];
    }

    /**
     * @return string
     */
    private function getComplicatedId(): string
    {
        return "{$this->getSeriesId()}/{$this->getSeason()}";
    }

    /**
     * @return int
     */
    public function getSeriesId(): int
    {
        return $this->seriesId;
    }

    /**
     * @return int
     */
    public function getSeason(): int
    {
        return $this->season;
    }

    /**
     * @param int $season
     */
    public function setSeason(int $season)
    {
        $this->season = $season;
    }

    /**
     * @param int $seriesId
     */
    public function setSeriesId(int $seriesId)
    {
        $this->seriesId = $seriesId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}