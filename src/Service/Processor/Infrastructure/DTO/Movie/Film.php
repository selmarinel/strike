<?php

namespace App\Service\Processor\Infrastructure\DTO\Movie;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOHasTags;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;
use App\Service\Processor\Infrastructure\Traits\DTO\RatingCalculateTrait;

class Film extends AbstractDTO implements DTOInterface, DTOFieldsInterface, DTOHasTags
{
    use RatingCalculateTrait;
    /** @var string */
    private $videoId = '';
    /** @var string */
    private $titleRu = '';
    /** @var string */
    private $titleEn = '';
    /** @var string */
    private $titleKK = '';
    /** @var int */
    private $ratingIMDB = 0;
    /** @var string */
    private $urlIMDB = '';
    /** @var int */
    private $ratingKinopoisk = 0;
    /** @var string */
    private $urlKinopoisk = '';
    /** @var string */
    private $secondVideoId = '';
    /** @var string */
    private $trailer = '';
    /** @var string */
    private $description = '';
    /** @var string */
    private $descriptionKk = '';
    /** @var string|null */
    private $photoSalt = '';

    protected $templateName = 'film';

    protected $entityURL = 'video/movie';

    protected $entityType = Type::MOVIE_FILM;

    /**
     * @return string
     */
    protected function generateMeta()
    {
        $date = date('U');
        $result = [
            "@context" => "http://schema.org/",
            "@type" => "VideoObject",
            "@id" => $this->getLink(),
            "name" => $this->getTitleRu(),
            "url" => "//player{$date}.dzenkinos.kz/service/commercial/{$this->getResource()}?illegal=1",
            "description" => $this->getText(),
            "thumbnail" => $this->getPhoto(),
            "thumbnailUrl" => $this->getPhotoUrl(),
            "uploadDate" => date("Y-m-d", $this->getCreated())
        ];
        return "<script type=\"application/ld+json\">" .
            json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            . "</script>";
    }

    public function getText(): string
    {
        if (!$this->text) {
            return $this->getTitleRu();
        }
        return $this->text;
    }

    public function getPhotoUrl()
    {
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return 'http://' . $this->getEncryption()->getHost()->getHostname() . $this->getPhoto();
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['video_id'])) {
            $this->setVideoId($arguments['video_id']);
        }
        if (isset($arguments['title_ru'])) {
            $this->setTitleRu($arguments['title_ru']);
        }
        if (isset($arguments['title_en'])) {
            $this->setTitleEn($arguments['title_en']);
        }
        if (isset($arguments['title_kk'])) {
            $this->setTitleKK($arguments['title_kk']);
        }
        if (isset($arguments['second_video_id'])) {
            $this->setSecondVideoId($arguments['second_video_id']);
        }
        if (isset($arguments['url_imdb'])) {
            $this->setUrlIMDB($arguments['url_imdb']);
        }
        if (isset($arguments['url_kinopoisk'])) {
            $this->setUrlKinopoisk($arguments['url_kinopoisk']);
        }
        if (isset($arguments['trailer'])) {
            $this->setTrailer((string)$arguments['trailer']);
        }
        if (isset($arguments['description'])) {
            $this->setDescription($arguments['description']);
        }
        if (isset($arguments['description_kk'])) {
            $this->setDescriptionKk($arguments['description_kk']);
        }
        if (isset($arguments['photo_salt'])) {
            $this->setPhotoSalt($arguments['photo_salt']);
        }

        $this->setRatingIMDB($this->calculateRating(
            $arguments,
            'rating_imdb',
            'title_en'));
        $this->setRatingKinopoisk($this->calculateRating(
            $arguments,
            'rating_kinopoisk',
            'title_en'));
    }

    public function toArray(): array
    {
        $current = [
            'videoId' => $this->getVideoId(),
            'secondaryVideoId' => $this->getSecondVideoId(),
            'titleRu' => $this->getTitleRu(),
            'titleEn' => $this->getTitleEn(),
            'titleKk' => $this->getTitleKK(),
            'imdbRating' => $this->getRatingIMDB(),
            'kinopoiskRating' => $this->getUrlIMDB(),
            'imdbUrl' => $this->getUrlIMDB(),
            'kinopoiskUrl' => $this->getUrlKinopoisk(),
            'trailer' => $this->getTrailer(),
            'description' => $this->getDescription(),
            'descriptionKk' => $this->getDescriptionKk(),
            'photo' => $this->getPhoto(),
            'resource' => $this->getResource(),
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    /**
     * @return string
     */
    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_nur_' . $this->getPhotoSalt()
            ) . '.jpg';
    }

    /**
     * @return string
     */
    private function getResource()
    {
        return ($this->getSecondVideoId()) ?: $this->getVideoId();
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'videoId',
            'secondaryVideoId',
            'titleRu',
            'titleEn',
            'titleKk',
            'imdbRating',
            'kinopoiskRating',
            'imdbUrl',
            'kinopoiskUrl',
            'trailer',
            'description',
            'descriptionKk',
            'photo',
        ];
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId(string $videoId): void
    {
        $this->videoId = $videoId;
    }

    /**
     * @return string
     */
    public function getTitleRu(): string
    {
        return $this->titleRu;
    }

    /**
     * @param string $titleRu
     */
    public function setTitleRu(string $titleRu): void
    {
        $this->titleRu = $titleRu;
    }

    /**
     * @return string
     */
    public function getTitleEn(): string
    {
        return $this->titleEn;
    }

    /**
     * @param string $titleEn
     */
    public function setTitleEn(string $titleEn): void
    {
        $this->titleEn = $titleEn;
    }

    /**
     * @return string
     */
    public function getTitleKK(): string
    {
        return $this->titleKK;
    }

    /**
     * @param string $titleKK
     */
    public function setTitleKK(string $titleKK): void
    {
        $this->titleKK = $titleKK;
    }

    /**
     * @return int
     */
    public function getRatingIMDB(): int
    {
        return $this->ratingIMDB;
    }

    /**
     * @param int $ratingIMDB
     */
    public function setRatingIMDB(int $ratingIMDB): void
    {
        $this->ratingIMDB = $ratingIMDB;
    }

    /**
     * @return string
     */
    public function getUrlIMDB(): string
    {
        return $this->urlIMDB;
    }

    /**
     * @param string $urlIMDB
     */
    public function setUrlIMDB(string $urlIMDB): void
    {
        $this->urlIMDB = $urlIMDB;
    }

    /**
     * @return int
     */
    public function getRatingKinopoisk(): int
    {
        return $this->ratingKinopoisk;
    }

    /**
     * @param int $ratingKinopoisk
     */
    public function setRatingKinopoisk(int $ratingKinopoisk): void
    {
        $this->ratingKinopoisk = $ratingKinopoisk;
    }

    /**
     * @return string
     */
    public function getUrlKinopoisk(): string
    {
        return $this->urlKinopoisk;
    }

    /**
     * @param string $urlKinopoisk
     */
    public function setUrlKinopoisk(string $urlKinopoisk): void
    {
        $this->urlKinopoisk = $urlKinopoisk;
    }

    /**
     * @return string
     */
    public function getSecondVideoId(): string
    {
        return $this->secondVideoId;
    }

    /**
     * @param string $secondVideoId
     */
    public function setSecondVideoId(string $secondVideoId): void
    {
        $this->secondVideoId = $secondVideoId;
    }

    /**
     * @return string
     */
    public function getTrailer(): string
    {
        return $this->trailer;
    }

    /**
     * @param string $trailer
     */
    public function setTrailer(string $trailer): void
    {
        $this->trailer = $trailer;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescriptionKk(): string
    {
        return $this->descriptionKk;
    }

    /**
     * @param string $descriptionKk
     */
    public function setDescriptionKk(string $descriptionKk): void
    {
        $this->descriptionKk = $descriptionKk;
    }

    /**
     * @return null|string
     */
    public function getPhotoSalt(): ?string
    {
        return $this->photoSalt;
    }

    /**
     * @param null|string $photoSalt
     */
    public function setPhotoSalt(?string $photoSalt): void
    {
        $this->photoSalt = $photoSalt;
    }
}