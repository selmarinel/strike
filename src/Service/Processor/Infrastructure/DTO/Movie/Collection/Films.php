<?php

namespace App\Service\Processor\Infrastructure\DTO\Movie\Collection;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractCollectionDTO;

class Films extends AbstractCollectionDTO implements DTOCollectionInterface, DTOFieldsInterface
{
    protected $name = 'Список Фильмов';

    /** @var string */
    protected $templateName = 'collection/films';

    protected $entityType = Type::COLLECTION_MOVIE_FILMS;

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            'total',
            'name',
            'domain',
            'films'
        ];
    }

    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'films' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

}