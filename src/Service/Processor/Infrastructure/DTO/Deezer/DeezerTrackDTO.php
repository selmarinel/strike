<?php

namespace App\Service\Processor\Infrastructure\DTO\Deezer;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class DeezerTrackDTO extends AbstractDTO implements DTOInterface
{
    /** @var int */
    private $id = 0;
    /** @var string */
    private $title = '';
    /** @var string */
    private $titleShort = '';
    /** @var int */
    private $duration = 0;
    /** @var int */
    private $bpm = 100;
    /** @var int */
    private $rank = 0;
    /** @var string */
    private $picture = '';
    /** @var string */
    private $artistName = '';

    protected $entityURL = 'tracks';

    protected $entityType = Type::DEEZER_AUDIO_TRACK;

    protected $templateName = 'track';

    protected function generateMeta()
    {
        $result = [
            "@context" => "http://schema.org/",
            "@type" => "AudioObject",
            "@id" => $this->getLink(),
            "name" => $this->getTitle(),
            "contentUrl" => $this->getSourceUrl(),
            "description" => $this->getText(),
            "encodingFormat" => 'audio/mpeg',
            "uploadDate" => date("Y-m-d", $this->getCreated())
        ];
        return "<script type=\"application/ld+json\">" .
            json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            . "</script>";
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['title'])) {
            $this->setTitle($arguments['title']);
        }
        if (isset($arguments['title_short'])) {
            $this->setTitleShort($arguments['title_short']);
        }
        if (isset($arguments['duration'])) {
            $this->setDuration($arguments['duration']);
        }
        if (isset($arguments['bpm'])) {
            $this->setBpm($arguments['bpm']);
        }
        if (isset($arguments['rank'])) {
            $this->setRank($arguments['rank']);
        }
        if (isset($arguments['id'])) {
            $this->setId((int)$arguments['id']);
        }

        if (isset($arguments['artist_name'])) {
            $this->setArtistName($arguments['artist_name']);
        }

        ////
        if (isset($arguments['album']['cover'])) {
            $this->setPicture($arguments['album']['cover']);
        }

    }

    public function toArray(): array
    {
        $sourceUrl = $this->getSourceUrl();

        $data = [
            'artistName' => $this->getArtistName(),
            'totalListeners' => 0,
            'totalListening' => 0,
            'totalDownloads' => 0,
            'created' => '',
            'fileId' => '',
            'servers' => '',
            'description' => '',
            'source_url' => $sourceUrl,
            'url' => $sourceUrl,
            'name' => $this->getTitle(),
            'title_short' => $this->getTitleShort(),
            'duration' => $this->getDuration(),
            'bpm' => $this->getBpm(),
            'rank' => $this->getRank(),
            'photo' => $this->getPhoto(),
        ];
        return array_merge(parent::toArray(), $data);
    }

    private function getSourceUrl()
    {
        $listenUrl = getenv('API_RECEIVER_DEEZER_HOST') . 'tracks/' . $this->getId() . '/listen';

        return "/{$this->getEncryption()->getEncryption()->encrypt(
            Type::AUDIO_PROVIDER_DEEZER. '_'.
                $this->getEncryption()->getHost()->getHostname() . '_' . 
                $listenUrl .'_' .
                'easternEgg'
        )}.mp3";
    }

    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() .
                '_' .
                Type::AUDIO_PROVIDER_DEEZER . 'v2_' .
                $this->getPicture()
            ) . '.jpg';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitleShort(): string
    {
        return $this->titleShort;
    }

    /**
     * @param string $titleShort
     */
    public function setTitleShort(string $titleShort)
    {
        $this->titleShort = $titleShort;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return int
     */
    public function getBpm(): int
    {
        return $this->bpm;
    }

    /**
     * @param int $bpm
     */
    public function setBpm(int $bpm)
    {
        $this->bpm = $bpm;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank(int $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getArtistName(): string
    {
        return $this->artistName;
    }

    /**
     * @param string $artistName
     */
    public function setArtistName(string $artistName)
    {
        $this->artistName = $artistName;
    }
}