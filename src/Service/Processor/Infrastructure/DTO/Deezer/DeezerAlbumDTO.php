<?php

namespace App\Service\Processor\Infrastructure\DTO\Deezer;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class DeezerAlbumDTO extends AbstractDTO implements DTOInterface
{
    /** @var int */
    private $id = 0;
    /** @var string */
    private $title = '';
    /** @var string */
    private $picture = '';
    /** @var string */
    private $label = '';
    /** @var int */
    private $trackCount = 0;
    /** @var float */
    private $rating = .0;
    /** @var int */
    private $fansCount = 0;
    /** @var string  */
    private $imageHash = '';

    protected $entityURL = 'albums';

    protected $templateName = 'album';

    protected $entityType = Type::DEEZER_AUDIO_ALBUM;

    public function fill(array $arguments)
    {
        if (isset($arguments['title'])) {
            $this->setTitle($arguments['title']);
        }
        if (isset($arguments['picture'])) {
            $this->setPicture($arguments['picture']);
        }
        if (isset($arguments['nb_tracks'])) {
            $this->setTrackCount((int)$arguments['nb_tracks']);
        }
        if (isset($arguments['fans'])) {
            $this->setFansCount((int)$arguments['fans']);
        }
        if (isset($arguments['rating'])) {
            $this->setRating((float)$arguments['rating']);
        }
        if (isset($arguments['id'])) {
            $this->setId((int)$arguments['id']);
        }
        if (isset($arguments['image_hash'])) {
            $this->setImageHash($arguments['image_hash']);
        }
    }

    public function toArray(): array
    {
        $data = [
            'id' => $this->getId(),
            'name' => $this->getTitle(),
            'fans' => $this->getFansCount(),
            'rating' => $this->getRating(),
            'label' => $this->getLabel(),
            'year' => '',
            'description' => '',
            'photo' => $this->getPhoto(),
            'tracks' => array_map(function (DTOInterface $track) {
                return $track->toArray();
            }, $this->children)
        ];
        return array_merge(parent::toArray(), $data);
    }

    private function getPhoto()
    {
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        //todo remove string constant
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_' .
                Type::AUDIO_PROVIDER_DEEZER . '_' .
                $this->getImageHash()
            ) . '.jpg';
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getTrackCount(): int
    {
        return $this->trackCount;
    }

    /**
     * @param int $trackCount
     */
    public function setTrackCount(int $trackCount)
    {
        $this->trackCount = $trackCount;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating(float $rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function getFansCount(): int
    {
        return $this->fansCount;
    }

    /**
     * @param int $fansCount
     */
    public function setFansCount(int $fansCount)
    {
        $this->fansCount = $fansCount;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImageHash(): string
    {
        return $this->imageHash;
    }

    /**
     * @param string $imageHash
     */
    public function setImageHash(string $imageHash = '')
    {
        $this->imageHash = $imageHash;
    }
}