<?php

namespace App\Service\Processor\Infrastructure\DTO\Deezer\Tag;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class DeezerGenreDTO extends AbstractDTO implements DTOInterface, DTOTagInterface
{
    private $name = '';

    private $picture = '';
    private $pictureSmall = '';
    private $pictureMedium = '';
    private $pictureBig = '';
    private $pictureLarge = '';

    protected $entityType = Type::DEEZER_AUDIO_GENRE;

    protected $entityURL = 'genres';

    protected $templateName = 'genre';

    public function fill(array $arguments)
    {
        if (isset($arguments['name'])) {
            $this->setName($arguments['name']);
        }
        if (isset($arguments['picture'])) {
            $this->setPicture($arguments['picture']);
        }
    }

    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'name' => $this->getName(),
            'nameEn' => $this->getName(),
            'nameUa' => $this->getName(),
            'photo' => $this->getPhoto(),
            'items' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

    private function getPhoto()
    {
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        //todo remove string constant
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_' .
                Type::AUDIO_PROVIDER_DEEZER . 'v2_' .
                $this->getPicture()
            ) . '.jpg';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getPictureSmall(): string
    {
        return $this->pictureSmall;
    }

    /**
     * @param string $pictureSmall
     */
    public function setPictureSmall(string $pictureSmall)
    {
        $this->pictureSmall = $pictureSmall;
    }

    /**
     * @return string
     */
    public function getPictureMedium(): string
    {
        return $this->pictureMedium;
    }

    /**
     * @param string $pictureMedium
     */
    public function setPictureMedium(string $pictureMedium)
    {
        $this->pictureMedium = $pictureMedium;
    }

    /**
     * @return string
     */
    public function getPictureBig(): string
    {
        return $this->pictureBig;
    }

    /**
     * @param string $pictureBig
     */
    public function setPictureBig(string $pictureBig)
    {
        $this->pictureBig = $pictureBig;
    }

    /**
     * @return string
     */
    public function getPictureLarge(): string
    {
        return $this->pictureLarge;
    }

    /**
     * @param string $pictureLarge
     */
    public function setPictureLarge(string $pictureLarge)
    {
        $this->pictureLarge = $pictureLarge;
    }
}