<?php

namespace App\Service\Processor\Infrastructure\DTO\Deezer;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOHasTags;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class DeezerArtistDTO extends AbstractDTO implements DTOInterface, DTOHasTags
{
    /** @var int */
    private $id = 0;
    /** @var string */
    private $name = '';
    /** @var string */
    private $picture = '';
    /** @var int */
    private $albumsCount = 0;
    /** @var int */
    private $fansCount = 0;
    /** @var int */
    private $tracksCount = 0;
    /** @var string  */
    private $imageHash = '';

    protected $entityURL = 'artists';

    protected $templateName = 'artist';

    protected $entityType = Type::DEEZER_AUDIO_ARTIST;

    public function fill(array $arguments)
    {
        if (isset($arguments['id'])) {
            $this->setId($arguments['id']);
        }
        if (isset($arguments['name'])) {
            $this->setName($arguments['name']);
        }
        if (isset($arguments['picture'])) {
            $this->setPicture($arguments['picture']);
        }
        if (isset($arguments['nb_album'])) {
            $this->setAlbumsCount((int)$arguments['nb_album']);
        }
        if (isset($arguments['nb_fan'])) {
            $this->setFansCount((int)$arguments['nb_fan']);
        }
        if (isset($arguments['nb_tracks'])) {
            $this->setTracksCount((int)$arguments['nb_tracks']);
        }

        if (isset($arguments['nb_tracks'])) {
            $this->setTracksCount((int)$arguments['nb_tracks']);
        }

        if (isset($arguments['image_hash'])) {
            $this->setImageHash($arguments['image_hash']);
        }
    }

    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_' .
                Type::AUDIO_PROVIDER_DEEZER . '_' .
                $this->getImageHash()
            ) . '.jpg';
    }

    public function toArray(): array
    {
        $data = [
            'name' => $this->getName(),
            'fans' => $this->getFansCount(),
            'id' => $this->getId(),
            'country' => '',
            'photo' => $this->getPhoto(),
            'totalAlbums' => $this->getAlbumsCount(),
            'totalSongs' => $this->getTracksCount(),
            'totalListening' => 0,
            'listeners' => 0,
            'description' => '',
            'albums' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge(parent::toArray(), $data);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getAlbumsCount(): string
    {
        return $this->albumsCount;
    }

    /**
     * @param string $albumsCount
     */
    public function setAlbumsCount(string $albumsCount)
    {
        $this->albumsCount = $albumsCount;
    }

    /**
     * @return string
     */
    public function getFansCount(): string
    {
        return $this->fansCount;
    }

    /**
     * @param string $fansCount
     */
    public function setFansCount(string $fansCount)
    {
        $this->fansCount = $fansCount;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTracksCount(): int
    {
        return $this->tracksCount;
    }

    /**
     * @param int $tracksCount
     */
    public function setTracksCount(int $tracksCount)
    {
        $this->tracksCount = $tracksCount;
    }

    /**
     * @return string
     */
    public function getImageHash(): string
    {
        return $this->imageHash;
    }

    /**
     * @param string $imageHash
     */
    public function setImageHash(string $imageHash = '')
    {
        $this->imageHash = $imageHash;
    }
}