<?php

namespace App\Service\Processor\Infrastructure\DTO;

use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Encryption\GetEncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Traits\DTO\DescriptionCutterTrait;
use App\Service\Processor\Infrastructure\Traits\DTO\PaginateTrait;

/**
 * Class AbstractDTO
 * @package App\Service\Processor\Infrastructure\DTO
 * @mixin DTOInterface
 */
abstract class AbstractDTO implements DTOInterface
{
    use DescriptionCutterTrait, PaginateTrait;

    const LINK_FIELD = '__link';
    const TOTAL_CHILDREN_FIELD = '__total';
    const TAGS_FIELD = '__tags';
    const RELATIONS_FIELD = '__relations';
    const TYPE_FIELD = '__type';
    const TEXT_FIELD = '__text';
    const META_FIELD = '__meta';

    protected $entityURL = '';
    /** @var string */
    private $link = '';
    /** @var string */
    private $sourceId = '';
    /** @var array */
    protected $children = [];
    /** @var int */
    private $totalChildren = 0;
    /** @var EncryptionInterface */
    private $encryption;
    /** @var int */
    protected $entityType = 0;
    /** @var InputGetRequestInterface */
    private $request;
    /** @var string */
    protected $templateName = '';
    /** @var array */
    private $tags = [];
    /** @var array */
    private $relations = [];
    /** @var string */
    protected $text = '';
    /** @var string */
    private $created = '';

    protected function generateMeta()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return array
     */
    public function getRelations(): array
    {
        if ($this->relations) {
            return array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->relations);
        }
        return [];
    }

    /**
     * @param array $relations
     */
    public function setRelations(array $relations): void
    {
        $this->relations = $relations;
    }


    /**
     * @return array
     */
    public function getTags(): array
    {
        if ($this->tags) {
            return array_map(function (DTOTagInterface $DTO) {
                return [
                    'name' => $DTO->getName(),
                    self::LINK_FIELD => $DTO->getLink()
                ];
            }, $this->tags);
        }
        return [];
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return DTOInterface[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }


    public function setRequest(InputGetRequestInterface $request)
    {
        $this->request = $request;
    }

    public function setEncryption(EncryptionInterface $encryption)
    {
        $this->encryption = $encryption;
    }

    protected function getEncryption(): GetEncryptionInterface
    {
        return $this->encryption;
    }

    abstract public function fill(array $arguments);

    public function toArray(): array
    {
        return [
            static::LINK_FIELD => $this->getLink(),
            static::TOTAL_CHILDREN_FIELD => $this->getTotalChildren(),
            static::TAGS_FIELD => $this->getTags(),
            static::RELATIONS_FIELD => $this->getRelations(),
            static::TYPE_FIELD => $this->getEntityType(),
            static::TEXT_FIELD => $this->getText(),
            static::META_FIELD => $this->generateMeta(),
            'pages' => $this->preparePaginate(),
        ];
    }

    public function getEntityURI(): string
    {
        return $this->entityURL;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function setSourceId(string $id)
    {
        $this->sourceId = $id;
    }

    public function getSourceId(): string
    {
        return $this->sourceId;
    }

    public function appendChild(DTOInterface $DTO)
    {
        $this->children[] = $DTO;
    }

    /**
     * @return int
     */
    public function getTotalChildren(): int
    {
        return $this->totalChildren;
    }

    /**
     * @param int $totalChildren
     */
    public function setTotalChildren(int $totalChildren): void
    {
        $this->totalChildren = $totalChildren;
    }

    public function setEntityType(int $entityType)
    {
        $this->entityType = $entityType;
    }

    public function getEntityType(): int
    {
        return $this->entityType;
    }

    public function getCreated(): int
    {
        return $this->created;
    }

    public function setCreated(int $created)
    {
        $this->created = $created;
    }

}