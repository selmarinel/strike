<?php

namespace App\Service\Processor\Infrastructure\DTO\Article;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Article extends AbstractDTO implements DataObjectInterface, DTOFieldsInterface
{
    /** @var  string */
    private $title = '';
    /** @var string */
    private $header = '';
    /** @var string */
    protected $text = '';

    /** @var string */
    protected $templateName = 'article';
    /** @var string */
    protected $entityURL = 'article';
    /** @var int */
    protected $entityType = Type::NEWS_ARTICLE;

    public static function getAvailableFields(): array
    {
        return [];
    }

    public function toArray(): array
    {
        $current = [
            'title' => $this->getTitle(),
            'header' => $this->getHeader(),
            'relations' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['title'])) {
            $this->setTitle($arguments['title']);
        }
        if (isset($arguments['text'])) {
            $this->setText($arguments['text']);
        }
        if (isset($arguments['header'])) {
            $this->setHeader($arguments['header']);
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeader(): string
    {
        return $this->header;
    }

    /**
     * @param string $header
     */
    public function setHeader(string $header)
    {
        $this->header = $header;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }
}