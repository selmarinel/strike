<?php

namespace App\Service\Processor\Infrastructure\DTO\Article\Collection;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractCollectionDTO;

class Category extends AbstractCollectionDTO implements DTOCollectionInterface, DTOFieldsInterface
{
    protected $name = 'Category';

    /** @var string */
    protected $templateName = 'collection/category';

    protected $entityType = Type::COLLECTION_AUDIO_ARTISTS;

    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'news' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }


    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            'total',
            'name',
            'domain',
            'news'
        ];
    }
}