<?php

namespace App\Service\Processor\Infrastructure\DTO;


use App\Service\Processor\Domain\DTO\Collection\GetDTOCollectionInterface;
use App\Service\Processor\Domain\DTO\Collection\SetDTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Traits\DTO\PaginateTrait;

abstract class AbstractCollectionDTO implements GetDTOCollectionInterface, SetDTOCollectionInterface
{
    use PaginateTrait;

    protected const LINK_FIELD = '__link';

    private $link = '';

    private $totalChildren = 0;

    protected $entityType = 0;

    protected $children = [];

    protected $name = '';

    protected $templateName = '';

    /**
     * @return DTOInterface[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getTemplateName():string
    {
        return $this->templateName;
    }

    /** @var InputGetRequestInterface */
    private $request;

    public function setRequest(InputGetRequestInterface $request)
    {
        $this->request = $request;
    }

    public function setLink(string $link)
    {
        $this->link = $link;
    }

    public function setTotalChildren(int $totalChildren)
    {
        $this->totalChildren = $totalChildren;
    }

    public function setEntityType(int $entityType)
    {
        $this->entityType = $entityType;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function appendChild(DTOInterface $DTO)
    {
        $this->children[] = $DTO;
    }

    public function getTotalChildren(): int
    {
        return $this->totalChildren;
    }

    public function toArray(): array
    {
        return [
            self::LINK_FIELD => $this->getLink(),
            'name' => $this->getName(),
            'total' => $this->getTotalChildren(),

            'pages' => $this->preparePaginate()
        ];
    }

    public function getEntityType(): int
    {
        return $this->entityType;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }


}