<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOHasTags;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Artist extends AbstractDTO implements DTOInterface, DTOFieldsInterface, DTOHasTags
{
    /** @var string */
    private $id = '';
    /** @var string */
    private $name = "";
    /** @var string */
    private $photoSalt = "";
    /** @var string */
    private $info = "";
    /** @var int */
    private $totalAlbums = 0;
    /** @var int */
    private $totalSongs = 0;
    /** @var int */
    private $totalListenings = 0;
    /** @var int */
    private $listeners = 0;
    /** @var string */
    private $country = "";
    /** @var string */
    protected $templateName = 'artist';
    /** @var string */
    protected $entityURL = 'audio/artist';
    /** @var int */
    protected $entityType = Type::AUDIO_ARTIST;

    public function fill(array $arguments)
    {
        if (isset($arguments['id'])) {
            $this->setId($arguments['id']);
        }

        if (isset($arguments['name'])) {
            $this->setName($arguments['name']);
        }

        if (isset($arguments['photo_salt'])) {
            $this->setPhotoSalt($arguments['photo_salt']);
        }

        if (isset($arguments['info'])) {
            $this->setInfo($arguments['info']);
        }

        if (isset($arguments['total_albums'])) {
            $this->setTotalAlbums($arguments['total_albums']);
        }

        if (isset($arguments['total_songs'])) {
            $this->setTotalSongs($arguments['total_songs']);
        }

        if (isset($arguments['total_listenings'])) {
            $this->setTotalListenings($arguments['total_listenings']);
        }

        if (isset($arguments['listeners'])) {
            $this->setListeners($arguments['listeners']);
        }

        if (isset($arguments['country'])) {
            $this->setCountry($arguments['country']);
        }
    }

    public function toArray(): array
    {
        $current = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->cut($this->getInfo()),
            'country' => $this->getCountry(),
            'photo' => $this->getPhoto(),
            'totalAlbums' => $this->getTotalAlbums(),
            'totalSongs' => $this->getTotalSongs(),
            'totalListening' => $this->getTotalListenings(),
            'listeners' => $this->getListeners(),
            'albums' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'domain',
            'name',
            'description',
            'country',
            'photo',
            'totalAlbums',
            'totalSongs',
            'totalListening',
            'listeners',
            'albums'
        ];
    }

    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_nur_' . $this->getPhotoSalt()
            ) . '.jpg';
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhotoSalt(): string
    {
        return $this->photoSalt;
    }

    /**
     * @param string $photoSalt
     */
    public function setPhotoSalt(string $photoSalt): void
    {
        $this->photoSalt = $photoSalt;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }

    /**
     * @return int
     */
    public function getTotalAlbums(): int
    {
        return $this->totalAlbums;
    }

    /**
     * @param int $totalAlbums
     */
    public function setTotalAlbums(int $totalAlbums): void
    {
        $this->totalAlbums = $totalAlbums;
    }

    /**
     * @return int
     */
    public function getTotalSongs(): int
    {
        return $this->totalSongs;
    }

    /**
     * @param int $totalSongs
     */
    public function setTotalSongs(int $totalSongs): void
    {
        $this->totalSongs = $totalSongs;
    }

    /**
     * @return int
     */
    public function getTotalListenings(): int
    {
        return $this->totalListenings;
    }

    /**
     * @param int $totalListenings
     */
    public function setTotalListenings(int $totalListenings): void
    {
        $this->totalListenings = $totalListenings;
    }

    /**
     * @return int
     */
    public function getListeners(): int
    {
        return $this->listeners;
    }

    /**
     * @param int $listeners
     */
    public function setListeners(int $listeners): void
    {
        $this->listeners = $listeners;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }
}