<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio\Tag;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Genre extends AbstractDTO implements DTOInterface, DTOTagInterface
{
    /** @var string */
    private $name = '';

    private $nameEn = '';

    private $nameUa = '';

    private $photoSalt = '';

    /** @var int */
    protected $entityType = Type::AUDIO_GENRE;

    protected $entityURL = 'audio/genre';

    protected $templateName = 'genre';

    /**
     * @return array
     */
    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'name' => $this->getName(),
            'nameEn' => $this->getNameEn(),
            'nameUa' => $this->getNameUa(),
            'photo' => $this->getPhoto(),
            'items' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['name_ru'])) {
            $this->setName($arguments['name_ru']);
        }
        if (isset($arguments['name_en'])) {
            $this->setNameEn($arguments['name_en']);
        }
        if (isset($arguments['name_ua'])) {
            $this->setNameUa($arguments['name_ua']);
        }
        if (isset($arguments['photo_salt'])) {
            $this->setPhotoSalt($arguments['photo_salt']);
        }
    }

    /**
     * @return string
     */
    private function getPhoto(): string
    {
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_' . $this->getPhotoSalt()
            ) . '.jpg';
    }

    public static function getAvailableFields(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameEn(): string
    {
        return $this->nameEn;
    }

    /**
     * @param string $nameEn
     */
    public function setNameEn(string $nameEn): void
    {
        $this->nameEn = $nameEn;
    }

    /**
     * @return string
     */
    public function getNameUa(): string
    {
        return $this->nameUa;
    }

    /**
     * @param string $nameUa
     */
    public function setNameUa(string $nameUa): void
    {
        $this->nameUa = $nameUa;
    }

    /**
     * @return string
     */
    public function getPhotoSalt(): string
    {
        return $this->photoSalt;
    }

    /**
     * @param string $photoSalt
     */
    public function setPhotoSalt(string $photoSalt): void
    {
        $this->photoSalt = $photoSalt;
    }
}