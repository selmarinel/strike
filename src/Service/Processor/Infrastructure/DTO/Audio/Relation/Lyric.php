<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio\Relation;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOIsRelationInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Lyric extends AbstractDTO implements DTOInterface, DTOIsRelationInterface
{
    /** @var string */
    protected $text = '';

    protected $entityType = Type::AUDIO_LYRIC;
    /** @var string */
    protected $templateName = 'lyric';

    protected $entityURL = 'audio/lyrics';

    /**
     * @param array $arguments
     */
    public function fill(array $arguments)
    {
        if (isset($arguments['text'])) {
            $this->setText($arguments['text']);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $current = [
            'name' => 'Текст песни',
            'text' => nl2br($this->getText())
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }
}