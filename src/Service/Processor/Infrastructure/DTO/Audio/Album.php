<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Album extends AbstractDTO implements DTOInterface, DTOFieldsInterface
{
    /** @var string */
    private $id = '';
    /** @var string */
    private $info = '';
    /** @var string */
    private $title = '';
    /** @var string */
    private $year = '';
    /** @var string */
    private $label = '';
    /** @var  string */
    private $photoSalt = '';
    /** @var string */
    protected $templateName = 'album';

    protected $entityURL = 'audio/album';

    /** @var int */
    protected $entityType = Type::AUDIO_ALBUM;

    public function fill(array $arguments)
    {
        if (isset($arguments['id'])) {
            $this->setId($arguments['id']);
        }

        if (isset($arguments['name'])) {
            $this->setTitle($arguments['name']);
        }

        if (isset($arguments['info'])) {
            $this->setInfo($arguments['info']);
        }

        if (isset($arguments['year'])) {
            $this->setYear($arguments['year']);
        }

        if (isset($arguments['label'])) {
            $this->setLabel($arguments['label']);
        }

        if (isset($arguments['photo_salt'])) {
            $this->setPhotoSalt($arguments['photo_salt']);
        }
    }

    public function toArray(): array
    {
        $current = [
            'id' => $this->getId(),
            'name' => $this->getTitle(),
            'description' => $this->cut($this->getInfo()),
            'label' => $this->getLabel(),
            'year' => $this->getYear(),
            'photo' => $this->getPhoto(),
            'tracks' => array_map(function (DTOInterface $track) {
                return $track->toArray();
            }, $this->children)
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'domain',
            'name',
            'description',
            'label',
            'year',
            'photo',
            'tracks'
        ];
    }

    private function getPhoto()
    {
        /** todo fix complexity */
        if (!$this->getEncryption()->getHost()->getHostname()) {
            return '';
        }
        if (!$this->getPhotoSalt()) {
            return '/' . $this->getEncryption()->getEncryption()->encrypt(
                    $this->getEncryption()->getHost()->getHostname() . '_nur_' . $this->getId()
                ) . '.jpg';
        }

        return '/' . $this->getEncryption()->getEncryption()->encrypt(
                $this->getEncryption()->getHost()->getHostname() . '_NUR_' . $this->getPhotoSalt()
            ) . '.jpg';
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear(string $year): void
    {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getPhotoSalt(): string
    {
        return $this->photoSalt;
    }

    /**
     * @param string $photoSalt
     */
    public function setPhotoSalt(string $photoSalt): void
    {
        $this->photoSalt = $photoSalt;
    }
}