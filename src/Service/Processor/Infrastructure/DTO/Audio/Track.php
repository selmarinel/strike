<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOHasTagsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractDTO;

class Track extends AbstractDTO implements DTOInterface, DTOFieldsInterface
{
    /** @var string */
    private $id = '';
    /** @var string */
    private $description = '';
    /** @var string */
    private $title = '';
    /** @var string */
    private $year = '';
    /** @var int */
    private $created = 0;
    /** @var int */
    private $totalListeners = 0;
    /** @var int */
    private $totalDownloads = 0;
    /** @var int */
    private $totalListening = 0;
    /** @var string */
    private $artistName = '';
    /** @var  string */
    private $fileId = '';
    /** @var  string */
    private $servers = '';
    /** @var  int */
    private $duration = 0;
    /** @var int */
    protected $entityType = Type::AUDIO_TRACK;
    /** @var string */
    protected $templateName = 'track';

    protected $entityURL = 'audio';

    protected function generateMeta()
    {
        $result = [
            "@context" => "http://schema.org/",
            "@type" => "AudioObject",
            "@id" => $this->getLink(),
            "name" => $this->getTitle(),
            "contentUrl" => $this->getSourceUrl(),
            "description" => $this->getText(),
            "encodingFormat" => 'audio/mpeg',
            "uploadDate" => date("Y-m-d", $this->getCreated())
        ];
        return "<script type=\"application/ld+json\">" .
            json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            . "</script>";
    }

    public function fill(array $arguments)
    {
        if (isset($arguments['id'])) {
            $this->setId($arguments['id']);
        }

        if (isset($arguments['title'])) {
            $this->setTitle($arguments['title']);
        }

        if (isset($arguments['description'])) {
            $this->setDescription($arguments['description']);
        }

        if (isset($arguments['artist_name'])) {
            $this->setArtistName($arguments['artist_name']);
        }

        if (isset($arguments['create_ts'])) {
            $this->setCreated($arguments['create_ts']);
        }

        if (isset($arguments['total_downloads'])) {
            $this->setTotalDownloads($arguments['total_downloads']);
        }

        if (isset($arguments['total_listeners'])) {
            $this->setTotalListeners($arguments['total_listeners']);
        }

        if (isset($arguments['total_listening'])) {
            $this->setTotalListening($arguments['total_listening']);
        }

        if (isset($arguments['year'])) {
            $this->setYear($arguments['year']);
        }

        if (isset($arguments['file_id'])) {
            $this->setFileId($arguments['file_id']);
        }

        if (isset($arguments['servers'])) {
            $this->setServers($arguments['servers']);
        }

        if (isset($arguments['length'])) {
            $this->setDuration($arguments['length']);
        }
    }

    public function toArray(): array
    {
        $current = [
            'id' => $this->getId(),
            'name' => $this->getTitle(),
            'description' => $this->cut($this->getDescription()),
            'artistName' => $this->getArtistName(),
            'year' => $this->getYear(),
            'duration' => $this->getDuration(),
            'totalListeners' => $this->getTotalListeners(),
            'totalListening' => $this->getTotalListening(),
            'totalDownloads' => $this->getTotalDownloads(),
            'created' => $this->getCreated(),
            'fileId' => $this->getFileId(),
            'servers' => $this->getServers(),
            'source_url' => $this->getSourceUrl(),
            //todo remove url
            'url' => $this->getSourceUrl(),
        ];
        $parent = parent::toArray();
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            self::TOTAL_CHILDREN_FIELD,
            'domain',
            'name',
            'description',
            'artistName',
            'year',
            'duration',
            'totalListeners',
            'totalListening',
            'totalDownloads' .
            'created',
            'source_url'
        ];
    }

    private function getSourceUrl()
    {
        return "/{$this->getEncryption()->getEncryption()->encrypt(
            TYPE::AUDIO_PROVIDER_NUR. '_'.
                $this->getEncryption()->getHost()->getHostname() . '_' . 
                $this->getFileId() .'_' .
                $this->getServers()
        )}.mp3";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear(string $year): void
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getTotalListeners(): int
    {
        return $this->totalListeners;
    }

    /**
     * @param int $totalListeners
     */
    public function setTotalListeners(int $totalListeners): void
    {
        $this->totalListeners = $totalListeners;
    }

    /**
     * @return int
     */
    public function getTotalDownloads(): int
    {
        return $this->totalDownloads;
    }

    /**
     * @param int $totalDownloads
     */
    public function setTotalDownloads(int $totalDownloads): void
    {
        $this->totalDownloads = $totalDownloads;
    }

    /**
     * @return int
     */
    public function getTotalListening(): int
    {
        return $this->totalListening;
    }

    /**
     * @param int $totalListening
     */
    public function setTotalListening(int $totalListening): void
    {
        $this->totalListening = $totalListening;
    }

    /**
     * @return string
     */
    public function getArtistName(): string
    {
        return $this->artistName;
    }

    /**
     * @param string $artistName
     */
    public function setArtistName(string $artistName): void
    {
        $this->artistName = $artistName;
    }

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     */
    public function setFileId(string $fileId): void
    {
        $this->fileId = $fileId;
    }

    /**
     * @return string
     */
    public function getServers(): string
    {
        return $this->servers;
    }

    /**
     * @param string $servers
     */
    public function setServers(string $servers): void
    {
        $this->servers = $servers;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

}