<?php

namespace App\Service\Processor\Infrastructure\DTO\Audio\Collection;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOFieldsInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Infrastructure\DTO\AbstractCollectionDTO;

class Tracks extends AbstractCollectionDTO implements DTOCollectionInterface, DTOFieldsInterface
{
    protected $name = 'Список Треков';

    /** @var string */
    protected $templateName = 'collection/tracks';

    /** @var int */
    protected $entityType = Type::COLLECTION_AUDIO_TRACKS;

    public function toArray(): array
    {
        $parent = parent::toArray();
        $current = [
            'tracks' => array_map(function (DTOInterface $DTO) {
                return $DTO->toArray();
            }, $this->children)
        ];
        return array_merge($parent, $current);
    }

    public static function getAvailableFields(): array
    {
        return [
            self::LINK_FIELD,
            'total',
            'name',
            'domain',
            'artists'
        ];
    }
}