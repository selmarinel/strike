<?php

namespace App\Service\Processor\Infrastructure\Service\SearchParser;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\SearchParser\SearchParserInterface;
use App\Service\Processor\Domain\Service\SearchParser\SearchPreparationInterface;
use App\Service\Search\Domain\Service\Searcher\Provider\ProviderInterface;

class SearchPreparation implements SearchPreparationInterface
{
    /** @var ProviderInterface */
    private $provider;
    /** @var SearchParserInterface */
    private $parser;

    public function __construct(ProviderInterface $provider, SearchParserInterface $parser)
    {
        $this->provider = $provider;
        $this->parser = $parser;
    }

    /**
     * @param GetHostInterface $host
     * @param string $query
     * @return DTOInterface[]
     */
    public function prepare(GetHostInterface $host, string $query): array
    {
        $localRecords = $this->provider->searchByName($query, $host);
        $dtoCollection = [];
        if (!empty($localRecords)) {
            foreach ($localRecords as $entityToHost) {
                $dto = $this->parser->makeDTO($entityToHost, $host);
                if ($dto) {
                    $dtoCollection[] = $dto;
                }
            }
        }
        return $dtoCollection;
    }
}