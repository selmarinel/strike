<?php

namespace App\Service\Processor\Infrastructure\Service\SearchParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\SearchParser\SearchParserInterface;
use App\Service\Processor\Domain\Service\TagParser\TagParserInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Infrastructure\Traits\DTO\DTOFillerTrait;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;

class SearchParser implements SearchParserInterface
{
    use PrepareDTOTrait, DTOFillerTrait;

    /** @var TagParserInterface */
    private $tagParser;
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;
    /** @var EncryptionInterface */
    private $encryption;

    public function __construct(
        TagParserInterface $tagParser,
        EncryptionInterface $encryption,
        EntityLinkGeneratorInterface $entityLinkGenerator)
    {
        $this->tagParser = $tagParser;
        $this->encryption = $encryption;
        $this->entityLinkGenerator = $entityLinkGenerator;
    }

    public function makeDTO(EntityToHost $entityToHost, GetHostInterface $host): ?DTOInterface
    {
        $entityDAO = $this->prepareEntity($entityToHost);
        if (isset(Type::MAP_DTO[$entityDAO->getEntityType()])) {
            $dtoClass = Type::MAP_DTO[$entityDAO->getEntityType()];
            /** @var DTOInterface $dto */
            $dto = new $dtoClass();
            $this->encryption->setHost($host);
            $this->fillDTO($dto, $entityDAO);
            $dto->setLink($this->entityLinkGenerator->generateLink($entityDAO, $host));
            $dto->setTags($this->tagParser->parseTags($entityDAO, $entityToHost, $host));
            return $dto;
        }
        return null;
    }
}