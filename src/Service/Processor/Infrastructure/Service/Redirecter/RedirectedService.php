<?php

namespace App\Service\Processor\Infrastructure\Service\Redirecter;


use App\Service\Base\Domain\Repository\Redirect\RedirectRepositoryInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\Redirecter\RedirectServiceInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException;
use App\Service\Processor\Infrastructure\Exception\Host\HostRedirectGeoException;
use App\Service\Processor\Infrastructure\Exception\Host\RootHostBotRedirectException;
use App\Service\Processor\Infrastructure\Exception\Redirect\RedirectException;
use App\Service\Processor\Infrastructure\VO\Request;

class RedirectedService implements RedirectServiceInterface
{
    /** @var RedirectRepositoryInterface */
    private $redirectRepository;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(
        RedirectRepositoryInterface $redirectRepository,
        HostRepository $hostRepository
    )
    {
        $this->redirectRepository = $redirectRepository;
        $this->hostRepository = $hostRepository;
    }

    /**
     * @param GetRequestInterface $request
     * @param Host $host
     * @throws HostNotFoundException
     * @throws HostRedirectGeoException
     * @throws RedirectException
     * @throws RootHostBotRedirectException
     */
    public function redirect(GetRequestInterface $request, Host $host)
    {
        $redirect = $this->redirectRepository->findRedirectByHostname(
            $request->getScheme() . '://' . $this->hackForDev($request->getHostName()) . $request->getRequestUri()
        );

        if ($redirect) {
            throw new RedirectException($redirect->getRedirectTo());
        }

        $data = $host->getData();

        if (isset($data['redirect'])) {
            $hostToRedirect = $this->hostRepository->findById((int)$data['redirect']);
            if (!$hostToRedirect) {
                throw new HostNotFoundException;
            }
            $redirect = "{$request->getScheme()}://{$hostToRedirect->getHostname()}{$request->getRequestUri()}";
            throw new RedirectException($redirect);
        }

        $redirectedHost = null;

        if ($host->isChild()) {
            if ($host->getGeo() !== $request->getGeo()) {
                $redirectedHost = $this->hostRepository->findNeighbourHostWithGeo($host, $request->getGeo());
            }
        } elseif ($host->isParent()) {
            $redirectedHost = $this->hostRepository->findNeighbourHostWithGeoByParent($host, $request->getGeo());
        }
        if ($redirectedHost && getenv('APP_ENV') == 'prod') {
            if ($request->getIsBot()) {
                if ($host->isParent()) {
                    $exception = new RootHostBotRedirectException();
                    $exception->setUrl($request->getScheme() . '://' . $this->hackForDev($redirectedHost->getHostname()) . $request->getRequestUri());
                    throw $exception;
                }
            } else {
                throw new HostRedirectGeoException(
                    $request->getScheme() . '://' . $this->hackForDev($redirectedHost->getHostname()) . $request->getRequestUri()
                );
            }
        }
    }

    /**
     * @deprecated
     * @param $hostname
     * @return string
     */
    private function hackForDev($hostname): string
    {
        if (mb_strpos($hostname, '127.0.0') !== false) {
            $hostname = $hostname . ":8000";
        }
        return "$hostname";
    }
}