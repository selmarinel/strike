<?php

namespace App\Service\Processor\Infrastructure\Service\Redirecter;


use App\Service\Base\Infrastructure\Repository\Redirect\HostGeoRedirectRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Processor\Domain\Service\Redirecter\HostRedirectServiceInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\Redirect\RedirectException;

class HostGeoRedirectService implements HostRedirectServiceInterface
{
    /** @var HostGeoRedirectRepository */
    private $hostGeoRedirectRepository;

    public function __construct(HostGeoRedirectRepository $hostGeoRedirectRepository)
    {
        $this->hostGeoRedirectRepository = $hostGeoRedirectRepository;
    }

    /**
     * @param GetRequestInterface $request
     * @param Host $host
     * @return bool
     * @throws RedirectException
     */
    public function redirect(GetRequestInterface $request, Host $host): bool
    {
        if ($host->isParent()) {
            $processableHost = $host;
        } else {
            $processableHost = $host->getParent();
        }

        $geoRedirect = $this->hostGeoRedirectRepository->findByFromHost($processableHost);
        if (!$geoRedirect) {
            return false;
        }
        if ($geoRedirect->getHostTo()->getId() === $host->getId()) {
            return true;
        }
        $hostToRedirect = $geoRedirect->getHostTo();
        if (!$hostToRedirect) {
            return false;
        }
        $hostname = $this->hackForDev($hostToRedirect->getHostname());
        $redirect = "{$request->getScheme()}://{$hostname}{$request->getRequestUri()}";
        throw new RedirectException($redirect);
    }

    public function hasRedirectRules(Host $host): bool
    {
        $collection = $this->hostGeoRedirectRepository->findByToHost($host);
        return !empty($collection);
    }

    /**
     * @deprecated
     * @param $hostname
     * @return string
     */
    private function hackForDev($hostname): string
    {
        if (mb_strpos($hostname, '127.0.0') !== false) {
            $hostname = $hostname . ":8000";
        }
        return "$hostname";
    }
}