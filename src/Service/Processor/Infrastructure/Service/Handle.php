<?php

namespace App\Service\Processor\Infrastructure\Service;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\TypedHandle\HandleProcessFactoryInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

class Handle
{
    /** @var UrlParserServiceInterface */
    private $urlParserService;
    /** @var DTOInterface */
    private $DTO;
    /** @var GetHostInterface */
    private $host;
    /** @var \Exception | null */
    private $exception = null;
    /** @var HandleProcessFactoryInterface */
    private $processFactory;

    public function __construct(
        UrlParserServiceInterface $urlParserService,
        HandleProcessFactoryInterface $processFactory
    )
    {
        $this->urlParserService = $urlParserService;
        $this->processFactory = $processFactory;
    }

    /**
     * @param InputGetRequestInterface $request
     *
     * @throws \App\Service\Processor\Infrastructure\Exception\DTO\ReceiverCountException
     * @throws \App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException
     */
    public function process(InputGetRequestInterface $request): void
    {
        $this->host = $this->urlParserService->processRequest($request);
        $receiver = $this->processFactory->getReceiver($this->host);
        $this->DTO = $receiver->receive($this->host, $request);
        $this->exception = $receiver->getException();
    }

    public function getException():?\Exception
    {
        return $this->exception;
    }


    public function getDTO(): DataObjectInterface
    {
        return $this->DTO;
    }

    public function getHost(): GetHostInterface
    {
        return $this->host;
    }
}