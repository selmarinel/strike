<?php

namespace App\Service\Processor\Infrastructure\Service\EntityLinkGenerator;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostInvalidConfigurationException;
use App\Service\Processor\Infrastructure\Service\UrlParser\UrlParserService;

class EntityLinkGeneratorService implements EntityLinkGeneratorInterface
{
    /**
     * @param GetEntityInterface $entity
     * @param GetHostInterface $host
     * @return string
     * @throws \Exception
     */
    public function generateLink(GetEntityInterface $entity, GetHostInterface $host): string
    {
        $config = null;
        foreach ($host->getSlugData() as $slug) {
            if ($slug->getSlugType() == $entity->getEntityType() && !$host->isStatic()) {
                $config = $host->getSlugData()[$entity->getEntityType()];
            } elseif ($host->isStatic() && $slug->getSlugType() == $host->getEntityType()) {
                $config = $host->getSlugData()[$host->getEntityType()];
            }
        }

        if (!$config) {
            if (getenv('APP_ENV') == 'prod') {
                return '#';
            }
            throw new HostInvalidConfigurationException;
        }
        switch ($config->getSlugType()) {
            case Type::NEWS_ARTICLE:
                return $this->slugLinkFromGenerate($host, $entity);
            case Type::COLLECTION_NEWS_CATEGORY:
                return $this->slugStaticLinkGenerate($host, $entity);
            default:
                if ($host->isSearch()) {
                    return $this->searchLinkGenerate($host);
                }
                if ($host->isStatic()) {
                    return $this->staticLinkGenerate($host);
                }

                return $this->dynamicLinkGenerate($config, $entity, $host);
        }
    }

    public function slugLinkFromGenerate(GetHostInterface $host, GetEntityInterface $entity)
    {
        return $this->hackForDev($host->getHostname()) . '/' . $entity->getEntitySlug();
    }

    /**
     * @param GetSlugInterface $config
     * @param GetEntityInterface $entity
     * @param GetHostInterface $host
     * @return string
     */
    public function dynamicLinkGenerate(GetSlugInterface $config, GetEntityInterface $entity, GetHostInterface $host)
    {
        $result = [];
        foreach ($config->getOrder() as $value) {
            switch ($value) {
                case UrlParserService::TYPE_SLUG:
                    $result[] = $config->getSlug();
                    break;
                case UrlParserService::ENTITY_SLUG:
                    $result[] = $entity->getEntitySlug();
                    break;
                case UrlParserService::ENTITY_ID:
                    $result[] = $entity->getEntityId();
                    break;
                default:
                    break;
            }
        }
        $part = implode($config->getSeparator(), $result);
        return $this->hackForDev($host->getHostname()) . '/' . $part;
    }

    public function staticLinkGenerate(GetHostInterface $host)
    {
        return $this->hackForDev($host->getHostname()) . '/' . $host->getTypeSlug();
    }

    public function searchLinkGenerate(GetHostInterface $host)
    {
        //todo think about dynamic search tag
        return $this->hackForDev($host->getHostname()) . '/search/' . $host->getTypeSlug();
    }

    public function slugStaticLinkGenerate(GetHostInterface $host, GetEntityInterface $entity)
    {
        return $this->hackForDev($host->getHostname()) . '/' . $host->getTypeSlug() . '/' . $entity->getEntitySlug();
    }


    /**
     * @deprecated
     * @param $hostname
     * @return string
     */
    private function hackForDev($hostname): string
    {
        if (mb_strpos($hostname, '127.0') !== false) {
            $hostname = $hostname . ":8000";
        }
        return "http://$hostname";
    }

}