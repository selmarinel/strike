<?php

namespace App\Service\Processor\Infrastructure\Service\EntityLinkGenerator;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host as HostModel;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;

class PrepareHostAndEntity implements PrepareHostAndEntityInterface
{
    /** @var UriSlugParserInterface */
    private $uriSlugParser;
    /** @var EntityPreparationInterface */
    private $entityPreparation;
    /** @var GetHostInterface|SetHostInterface */
    private $hostDAO;
    /** @var GetEntityInterface|SetEntityInterface */
    private $entityDAO;

    public function __construct(
        UriSlugParserInterface $uriSlugParser,
        EntityPreparationInterface $entityPreparation
    ) {
        $this->uriSlugParser = $uriSlugParser;
        $this->entityPreparation = $entityPreparation;
    }

    /**
     * @param EntityToHost $entityToHost
     * @throws HostSlugException
     */
    public function prepareFromEntityToHost(EntityToHost $entityToHost)
    {
        /** @var HostModel $host */
        $host = $entityToHost->getHost();

        $this->hostDAO = new Host();
        $this->hostDAO->setHostname($host->getHostname());
        $this->hostDAO->setHostId($host->getId());
        $this->hostDAO->setParentId($host->getParentId());

        foreach ($this->uriSlugParser->parseSlug($host->getData()) as $slug) {
            $this->hostDAO->appendSlug($slug);
        }
        $this->entityDAO = $this->entityPreparation->prepareEntityFromEntityToHost($entityToHost);
        if (isset($this->hostDAO->getSlugData()[$this->entityDAO->getEntityType()])) {
            $this->hostDAO->setStatic($this->hostDAO->getSlugData()[$this->entityDAO->getEntityType()]->isStatic());
        }
        $this->hostDAO->setTypeSlug($this->entityDAO->getEntitySlug());
        $this->hostDAO->setEntityType($this->entityDAO->getEntityType());
    }

    /**
     * @return GetHostInterface|SetHostInterface
     */
    public function getHostDAO(): GetHostInterface
    {
        return $this->hostDAO;
    }

    /**
     * @return GetEntityInterface|SetEntityInterface
     */
    public function getEntityDAO(): GetEntityInterface
    {
        return $this->entityDAO;
    }
}