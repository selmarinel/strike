<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser\Tag;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\Tag\TagChildrenPreparationInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;

class TagChildrenPreparation implements TagChildrenPreparationInterface
{
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityPreparationInterface */
    private $preparation;
    /** @var EntityLinkGeneratorInterface */
    private $generator;
    /** @var int */
    private $count = 0;

    public function __construct(
        EntitiesRepository $entitiesRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityLinkGeneratorInterface $entityLinkGenerator,
        EntityPreparationInterface $preparation
    ) {
        $this->entitiesRepository = $entitiesRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->preparation = $preparation;
        $this->generator = $entityLinkGenerator;
    }

    public function getChildren(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    ) {
        if (!isset(Type::MAP_GENRES[$entity->getEntityType()])) {
            return [];
        }

        $entities = $this->getTagEntities($entity, $host, $paginate);

        $ids = [];
        /** @var Entity $entity */
        foreach ($entities as $entity) {
            $ids[] = $entity->getId();
        }

        $result = [];

        $entitiesToHost = $this->entityToHostRepository->findByHostIdAndEntitiesId($host->getHostId(), $ids);
        if (empty($entitiesToHost) && $host->getParentId() && $host->getAbuseLevel() <= 1) {
            $entitiesToHost = $this->entityToHostRepository->findByHostIdAndEntitiesId($host->getParentId(), $ids);
        }
        /** @var EntityToHost $entityToHost */
        foreach ($entitiesToHost as $entityToHost) {
            /** @var GetEntityInterface|SetEntityInterface $entityDAO */
            $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHost);
            $entityDAO->setLink($this->generator->generateLink($entityDAO, $host));
            $result[] = $entityDAO;
            unset($entityDAO);
        }
        return $result;
    }

    /**
     * @param GetEntityInterface $entity
     * @param GetHostInterface $host
     * @param GetPaginateInterface $paginate
     * @return Entity[]
     */
    private function getTagEntities(
        GetEntityInterface $entity,
        GetHostInterface $host,
        GetPaginateInterface $paginate
    ) {
        $genreType = Type::MAP_GENRES[$entity->getEntityType()];

        $entities = $this->entitiesRepository->getRelatedTagEntities(
            $entity->getEntityId(),
            $genreType,
            $host->getHostId(),
            $host->getParentId(),
            $paginate->getLimit(),
            $paginate->getOffset()
        );

        $this->count = $this->entitiesRepository->getCountOfTags(
            $entity->getEntityId(),
            $genreType,
            $host->getHostId(),
            $host->getParentId());

        return $entities;
    }

    /**
     * @return int
     */
    public function getCountOfChildren(): int
    {
        return $this->count;
    }
}