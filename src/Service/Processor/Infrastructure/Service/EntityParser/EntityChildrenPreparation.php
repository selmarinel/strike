<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree; //todo
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;//todo

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityChildrenPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;

class EntityChildrenPreparation implements EntityChildrenPreparationInterface
{
    /** @var EntityNestedTreeRepository */
    private $treeRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var int */
    private $childrenCount = 0;
    /** @var EntityPreparationInterface */
    private $preparation;

    /** @var EntityNestedTree|null */
    private $node = null;

    public function __construct(
        EntityNestedTreeRepository $treeRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityPreparationInterface $preparation
    ) {
        $this->treeRepository = $treeRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->preparation = $preparation;
    }

    /**
     * @param EntityNestedTree|null $node
     */
    public function setNode(EntityNestedTree $node = null)
    {
        $this->node = $node;
    }

    /**
     * @return EntityNestedTree|null
     */
    public function getNode(): ?EntityNestedTree
    {
        return $this->node;
    }

    /**
     * @return int
     */
    public function getTotalChildren(): int
    {
        return $this->childrenCount;
    }

    /**
     * @param GetHostInterface $host
     * @param GetEntityInterface $entity
     * @param GetPaginateInterface $paginate
     * @return GetEntityInterface|SetEntityInterface[]
     */
    public function prepare(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    ): array {
        $this->node = $this->treeRepository->findByEntityId($entity->getEntityId());

        $result = [];

        if (!$this->node) {
            return $result;
        }

        $children = $this->treeRepository->getChildrenEntities(
            $this->node,
            $paginate->getOffset(),
            $paginate->getLimit());
        $this->childrenCount = $this->treeRepository->getChildrenCount($this->node);

        $ids = [];
        /** @var EntityNestedTree $child */
        foreach ($children as $child) {
            $ids[$child->getEntity()->getId()] = $child->getEntity()->getId();
        }
        $entitiesToHost = $this->entityToHostRepository->findByHostIdAndEntitiesId($host->getHostId(), $ids);

        if (empty($entitiesToHost) && $host->getParentId() && $host->getAbuseLevel() <= 1) {
            $entitiesToHost = $this->entityToHostRepository->findByHostIdAndEntitiesId($host->getParentId(), $ids);
        }
        /** @var EntityToHost $entityToHost */
        foreach ($entitiesToHost as $entityToHost) {
            $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHost);
            $result[] = $entityDAO;
            unset($entityDAO);
        }
        return $result;
    }

    /**
     * @return Entity|null
     */
    public function findNext(): ?GetEntityInterface
    {
        if (!$this->node) {
            return null;
        }
        /** @var EntityNestedTree $next */
        $next = $this->treeRepository->getNext($this->node);
        if (!empty($next) && isset($next[1])) {
            return $this->preparation->prepareEntityFromEntityToHost($next[1]);
        }
        return null;
    }

    /**
     * @return Entity|null
     */
    public function findPrev(): ?GetEntityInterface
    {
        if (!$this->node) {
            return null;
        }
        /** @var EntityNestedTree $next */
        $next = $this->treeRepository->getPrev($this->node);
        if (!empty($next) && isset($next[1])) {
            return $this->preparation->prepareEntityFromEntityToHost($next[1]);
        }
        return null;
    }
}