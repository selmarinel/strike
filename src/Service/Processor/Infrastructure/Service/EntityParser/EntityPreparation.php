<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;

class EntityPreparation implements EntityPreparationInterface
{
    /** @var EntityHostToEntityHostRepository */
    private $repository;

    public function __construct(EntityHostToEntityHostRepository $entityHostToEntityHostRepository)
    {
        $this->repository = $entityHostToEntityHostRepository;
    }

    /**
     * @param EntityToHost $entityToHost
     * @return GetEntityInterface
     */
    public function prepareEntityFromEntityToHost(EntityToHost $entityToHost): GetEntityInterface
    {
        $entityDAO = new Entity();
        $entityDAO->setName($entityToHost->getEntity()->getName());
        $entityDAO->setSourceId($entityToHost->getEntity()->getSourceId());
        $entityDAO->setEntityType($entityToHost->getEntity()->getEntityType());
        $entityDAO->setEntityId($entityToHost->getEntity()->getId());
        $entityDAO->setEntitySlug($entityToHost->getEntitySlug());
        return $entityDAO;
    }

}