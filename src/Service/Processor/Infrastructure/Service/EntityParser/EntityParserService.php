<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser;

use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost as EntityToHostModel;//todo

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityChildrenPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityParserInterface;
use App\Service\Processor\Domain\Service\TagParser\TagParserInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityToHostException;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;

class EntityParserService implements EntityParserInterface
{
    use PrepareDTOTrait;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;
    /** @var EntityChildrenPreparationInterface */
    private $entityChildrenPreparation;
    /** @var TagParserInterface */
    private $tagParser;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        EntityLinkGeneratorInterface $entityLinkGenerator,
        EntityChildrenPreparationInterface $entityChildrenPreparation,
        TagParserInterface $tagParser
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityLinkGenerator = $entityLinkGenerator;
        $this->entityChildrenPreparation = $entityChildrenPreparation;
        $this->tagParser = $tagParser;
    }

    /**
     * @param GetHostInterface $host
     * @return GetEntityToHostInterface
     * @throws EntityNotFoundException
     * @throws EntityToHostException
     */
    public function parse(GetHostInterface $host): GetEntityToHostInterface
    {
        /** @var EntityToHostModel $entityToHost */
        if ($host->getAbuseLevel() <= 1) {
            $entityToHost = $this->entityToHostRepository->findHostOrParentByHostIdAndEntityIdWithParentId(
                $host->getHostId(),
                $host->getEntityId(),
                $host->getParentId(),
                $host->getEntitySlug()
            );
        } else {
            $entityToHost = $this->entityToHostRepository->findByHostIdAndEntityId(
                $host->getHostId(),
                $host->getEntityId(),
                $host->getEntitySlug()
            );
        }
        if (!$entityToHost) {
            throw new EntityNotFoundException;
        }

        $entityDAO = $this->prepareEntity($entityToHost);

        $entityDAO->setTags($this->tagParser->parseTags($entityDAO, $entityToHost, $host));

        if ($entityDAO->getEntityType() !== $host->getEntityType()) {
            throw new EntityNotFoundException;
        }

        $entityDAO->setLink($this->entityLinkGenerator->generateLink($entityDAO, $host));

        $entityToHostDAO = new EntityToHost();
        $entityToHostDAO->setId($entityToHost->getId());
        $entityToHostDAO->setSlug($entityToHost->getEntitySlug());
        $entityToHostDAO->setHost($host);
        $entityToHostDAO->setEntity($entityDAO);

        if (!$this->validate($host, $entityToHostDAO)) {
            throw new EntityNotFoundException;
        }
        return $entityToHostDAO;
    }

    /**
     * @param GetHostInterface $host
     * @param GetEntityInterface $entity
     * @param GetPaginateInterface $paginate
     * @return GetEntityInterface[]
     */
    public function getChildren(
        GetHostInterface $host,
        GetEntityInterface $entity,
        GetPaginateInterface $paginate
    ): array
    {
        $children = $this->entityChildrenPreparation->prepare($host, $entity, $paginate);
        /** @var GetEntityInterface|SetEntityInterface $child */
        foreach ($children as $child) {
            $child->setLink($this->entityLinkGenerator->generateLink($child, $host));
        }
        return $children;
    }

    /**
     * @return int
     */
    public function getCountOfChildren(): int
    {
        return $this->entityChildrenPreparation->getTotalChildren();
    }

    /**
     * @param GetHostInterface $host
     * @return string
     */
    public function getPreviousLink(GetHostInterface $host): string
    {
        $prev = $this->entityChildrenPreparation->findPrev();
        if ($prev) {
            return $this->entityLinkGenerator->generateLink($prev, $host);
        }
        return '';
    }

    /**
     * @param GetHostInterface $host
     * @return string
     */
    public function getNextLink(GetHostInterface $host): string
    {
        $next = $this->entityChildrenPreparation->findNext();
        if ($next) {
            return $this->entityLinkGenerator->generateLink($next, $host);
        }
        return '';
    }

    /**
     * @param GetHostInterface $host
     * @param GetEntityToHostInterface $entityToHost
     * @return bool
     */
    private function validate(GetHostInterface $host, GetEntityToHostInterface $entityToHost)
    {
        return $host->getEntitySlug() === $entityToHost->getSlug();
    }
}