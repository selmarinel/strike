<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser\Relation;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationSquashInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

class RelationSquashService implements RelationSquashInterface
{
    /** @var RelationPreparationInterface */
    private $preparation;

    public function __construct(RelationPreparationInterface $preparation)
    {
        $this->preparation = $preparation;
    }

    /**
     * @param DTOInterface $DTOEntity
     * @param GetEntityToHostInterface $entityToHost
     * @param EncryptionInterface $encryption
     * @param InputGetRequestInterface $request
     */
    public function squash(
        DTOInterface $DTOEntity,
        GetEntityToHostInterface $entityToHost,
        InputGetRequestInterface $request,
        EncryptionInterface $encryption
    ) {
        if (!$DTOEntity) {
            return;
        }
        $collection = [];
        $relations = $this->preparation->prepare($entityToHost, $request);
        /** @var GetEntityInterface $relation */
        foreach ($relations as $relation) {
            if (isset(Type::MAP_DTO[$relation->getEntityType()])) {
                $dtoClass = Type::MAP_DTO[$relation->getEntityType()];
                /** @var DTOInterface $dto */
                $dto = new $dtoClass();
                $dto->setEntityType($relation->getEntityType());
                $dto->setEncryption($encryption);
                $dto->setLink($relation->getLink());
                $dto->setSourceId($relation->getSourceId());
                $collection[] = $dto;
                unset($dto);
            }
        }
        $DTOEntity->setRelations($collection);

    }
}