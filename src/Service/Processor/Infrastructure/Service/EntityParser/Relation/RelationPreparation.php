<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser\Relation;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationPreparationInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;

class RelationPreparation implements RelationPreparationInterface
{
    use PrepareDTOTrait;

    /** @var EntityHostToEntityHostRepository */
    private $repository;

    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;

    public function __construct(
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EntityLinkGeneratorInterface $linkGenerator
    ) {
        $this->repository = $entityHostToEntityHostRepository;
        $this->entityLinkGenerator = $linkGenerator;
    }

    /**
     * @param GetEntityToHostInterface $entityToHost
     * @param InputGetRequestInterface $request
     * @return GetEntityInterface[]
     */
    public function prepare(
        GetEntityToHostInterface $entityToHost,
        InputGetRequestInterface $request
    ): array {
        $relations = $this->repository->findByMainId(
            $entityToHost->getId(),
            $request->getOffset(),
            $request->getLimit(),
            Type::RELATION_TYPE_RELATE);

        $result = [];
        /** @var EntityHostToEntityHost $relation */
        foreach ($relations as $relation) {
            $prepareEntity = $this->prepareEntity($relation->getSubordinate());
            $prepareEntity->setLink(
                $this->entityLinkGenerator->generateLink($prepareEntity, $entityToHost->getHost()));
            $result[] = $prepareEntity;
            unset($prepareEntity);
        }
        return $result;
    }
}