<?php

namespace App\Service\Processor\Infrastructure\Service\EntityParser;

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOHasNavigationInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityParserInterface;
use App\Service\Processor\Domain\Service\EntityParser\PrepareEntityReceiveUrlInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationSquashInterface;
use App\Service\Processor\Domain\Service\EntityParser\Tag\TagChildrenPreparationInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Encryption\SetEncryptionInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Traits\DTO\DTOFillerTrait;
use App\Service\Processor\Infrastructure\Traits\DTO\TagsTrait;

use App\Service\Base\Infrastructure\Entity\Type;

class PrepareEntityReceiveUrl implements PrepareEntityReceiveUrlInterface
{
    use TagsTrait, DTOFillerTrait;

    /** @var EntityParserInterface */
    private $entityParserService;
    /** @var SetEncryptionInterface */
    private $encryption;
    /** @var TagChildrenPreparationInterface */
    private $tagParser;
    /** @var RelationSquashInterface */
    private $relationSquashier;

    public function __construct(
        EntityParserInterface $entityParserService,
        TagChildrenPreparationInterface $tagChildrenPreparation,
        RelationSquashInterface $relationSquash,
        EncryptionInterface $encryption
    )
    {
        $this->entityParserService = $entityParserService;
        $this->tagParser = $tagChildrenPreparation;
        $this->relationSquashier = $relationSquash;
        $this->encryption = $encryption;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface[]
     * @throws \App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request): array
    {
        $this->encryption->setHost($host);
        $entityToHost = $this->entityParserService->parse($host);

        if (isset(Type::MAP_DTO[$entityToHost->getEntity()->getEntityType()])) {
            $dtoClass = Type::MAP_DTO[$entityToHost->getEntity()->getEntityType()];
            /** @var DTOInterface $dto */
            $dto = new $dtoClass();
            $this->fillDTO($dto, $entityToHost->getEntity());
        }
        if (!isset($dto) || !$dto) {
            throw new EntityNotFoundException;
        }

        if ($dto instanceof DTOTagInterface) {
            $children = $this->tagParser->getChildren($host, $entityToHost->getEntity(), $request);
            $dto->setTotalChildren($this->tagParser->getCountOfChildren());
        } else {
            $children = $this->entityParserService->getChildren($host, $entityToHost->getEntity(), $request);
            $dto->setTotalChildren($this->entityParserService->getCountOfChildren());
        }
        // Add relations
        $this->relationSquashier->squash($dto, $entityToHost, $request, $this->encryption);
        // Add navigation links if need
        $this->addNavigation($dto, $host);

        $prepare[0] = $dto;
        foreach ($children as $child) {
            if (isset(Type::MAP_DTO[$child->getEntityType()])) {
                $dtoClass = Type::MAP_DTO[$child->getEntityType()];
                /** @var DTOInterface $dto */
                $dto = new $dtoClass();
                $this->fillDTO($dto, $child);
                $prepare[] = $dto;
                unset($dto);
            }
        }
        return $prepare;
    }

    private function addNavigation(DTOInterface $DTO, GetHostInterface $host)
    {
        if ($DTO instanceof DTOHasNavigationInterface) {
            $DTO->setNextLink($this->entityParserService->getNextLink($host));
            $DTO->setPrevLink($this->entityParserService->getPreviousLink($host));
        }
    }


}