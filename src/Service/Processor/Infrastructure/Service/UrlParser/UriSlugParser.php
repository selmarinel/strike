<?php

namespace App\Service\Processor\Infrastructure\Service\UrlParser;

use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;
use App\Service\Processor\Infrastructure\VO\Slug;

class UriSlugParser implements UriSlugParserInterface
{
    const SLUG_TYPES = 'slug_types';

    const SLUG_TYPE = 'slug_type';
    const STATIC_FIELD = 'static';

    const PATTERN_FIELD = 'pattern';

    /**
     * @param array $slugDataFromHost
     * @return GetSlugInterface[]
     * @throws HostSlugException
     */
    public function parseSlug(array $slugDataFromHost)
    {
        if (!isset($slugDataFromHost[static::SLUG_TYPES])) {
            throw new HostSlugException;
        }
        $slugs = [];
        foreach ($slugDataFromHost[static::SLUG_TYPES] as $index => $property) {
            $slug = new Slug();
            try {
                $slug->setPattern($property[static::PATTERN_FIELD]);
                $slug->setStatic((bool)$property[static::STATIC_FIELD]);
                $slug->setSlugType((int)$property[static::SLUG_TYPE]);
                $slugs[] = $slug;
            } catch (\Exception $exception) {
                throw new HostSlugException;
            }
        }
        return $slugs;
    }
}