<?php

namespace App\Service\Processor\Infrastructure\Service\UrlParser;

use App\Service\Base\Infrastructure\Repository\Redirect\HostGeoRedirectRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;//todo
use App\Service\EntityTextGenerator\Infrastructure\VO\TextGenerateTypeVO;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Redirecter\HostRedirectServiceInterface;
use App\Service\Processor\Domain\Service\Redirecter\RedirectServiceInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;
use App\Service\Processor\Infrastructure\Exception\Redirect\RedirectException;
use App\Service\Processor\Infrastructure\Exception\Slug\SlugNotFoundException;

class UrlParserService implements UrlParserServiceInterface
{
    const TYPE_SLUG = 'slug_type';

    const ENTITY_SLUG = 'entity_slug';

    const ENTITY_ID = 'id';

    /** @var HostRepository */
    private $hostRepository;
    /** @var UriSlugParserInterface */
    private $uriSlugParser;
    /** @var  RedirectServiceInterface */
    private $redirectService;
    /** @var HostRedirectServiceInterface */
    private $geoRedirectService;


    public function __construct(
        HostRepository $hostRepository,
        UriSlugParserInterface $uriSlugParser,
        RedirectServiceInterface $redirectService,
        HostRedirectServiceInterface $geoRedirectService //todo
    )
    {
        $this->hostRepository = $hostRepository;
        $this->uriSlugParser = $uriSlugParser;
        $this->redirectService = $redirectService;
        $this->geoRedirectService = $geoRedirectService;
    }

    /**
     * @param GetRequestInterface $request
     * @param bool $ignoreRedirect
     * @return GetHostInterface
     * @throws HostNotFoundException
     * @throws HostSlugException
     * @throws SlugNotFoundException
     */
    public function processRequest(GetRequestInterface $request, bool $ignoreRedirect = false): GetHostInterface
    {
        $host = $this->hostRepository->findByHostName($request->getHostName());
        if (!$host) {
            throw new HostNotFoundException;
        }
        if ($this->geoRedirectService->redirect($request, $host)) {
            $ignoreRedirect = true;
        }

        if (!$ignoreRedirect) {
            $this->redirectService->redirect($request, $host);
        }
        $slugs = $this->uriSlugParser->parseSlug($host->getData());
        if (empty($slugs)) {
            throw new HostSlugException;
        }

        //** todo make simply  or refact */
        $settings = [];
        foreach ($host->getTextGenerateSettings() as $generateSetting) {
            $textGenerateTypeVO = new TextGenerateTypeVO();
            $textGenerateTypeVO->setType((int)$generateSetting);
            $settings[] = $textGenerateTypeVO;
            unset($textGenerateTypeVO);
        }

        $hostDAO = new Host();
        $hostDAO->setTextGenerateConfiguration($settings);
        //** ** */
        $hostDAO->setHasGeoRedirect($this->geoRedirectService->hasRedirectRules($host));
        //** ** */

        try {
            $matches = [];
            foreach ($slugs as $slug) {
                $entityType = $slug->getSlugType();
                $hostDAO->appendSlug($slug);
                if (preg_match($slug->getPattern(), $request->getSlug(), $matches[$entityType])) {
                    $params = $matches[$entityType];
                    $hostDAO->setHostId($host->getId());
                    $hostDAO->setParentId($host->getParentId());
                    $hostDAO->setHostname($host->getHostname());
                    $hostDAO->setAbuseLevel($host->getAbuseLevel());

                    $hostDAO->setHostType($host->getType());
                    $hostDAO->setTemplateDriver($host->getDriver());

                    if (isset($host->getData()[Host::HOST_SEO_DATA_FIELD])) {
                        $hostDAO->setSeoData($host->getData()[Host::HOST_SEO_DATA_FIELD]);
                    }

                    $hostDAO->setStatic($slug->isStatic());
                    $hostDAO->setEntityType($slug->getSlugType());

                    if (isset($params[static::TYPE_SLUG])) {
                        $hostDAO->setTypeSlug($params[static::TYPE_SLUG]);
                    }
                    if (isset($params[static::ENTITY_SLUG])) {
                        $hostDAO->setEntitySlug($params[static::ENTITY_SLUG]);
                    }
                    if (isset($params[static::ENTITY_ID])) {
                        $hostDAO->setEntityId((int)$params[static::ENTITY_ID]);
                    }
                }
            }
            unset($matches);
        } catch (\Exception $exception) {
            throw new HostSlugException;
        }
        if (!$hostDAO->getTypeSlug() && !$hostDAO->getHostId()) {
            throw new SlugNotFoundException;
        }
        return $hostDAO;
    }
}