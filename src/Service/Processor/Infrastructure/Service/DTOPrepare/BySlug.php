<?php

namespace App\Service\Processor\Infrastructure\Service\DTOPrepare;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Exceptions\Entity\EntityHostNotFoundException;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOPrepare\PrepareBySlugInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityChildrenPreparationInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Traits\DTO\DTOFillerTrait;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;

class BySlug implements PrepareBySlugInterface
{
    use PrepareDTOTrait, DTOFillerTrait;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityHostToEntityHostRepository */
    private $entityToEntityRepository;
    /** @var EntityPreparationInterface */
    private $preparation;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EncryptionInterface $encryption,
        EntityPreparationInterface $entityPreparation,
        EntityLinkGeneratorInterface $entityLinkGenerator
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityToEntityRepository = $entityHostToEntityHostRepository;
        $this->encryption = $encryption;
        $this->entityLinkGenerator = $entityLinkGenerator;
        $this->preparation = $entityPreparation;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface[]|array
     * @throws EntityHostNotFoundException
     * @throws EntityNotFoundException
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request): array
    {
        $entityToHost = $this->entityToHostRepository->findByHostIdAndEntitySlugAndSlugType(
            $host->getHostId(),
            $host->getEntitySlug()
        );
        if (!$entityToHost) {
            throw new EntityHostNotFoundException;
        }

        //hack

        if ($host instanceof SetHostInterface) {
            $host->setEntityId($entityToHost->getEntity()->getId());
            $host->setEntityType($entityToHost->getEntity()->getEntityType());
        }

        $entityDAO = $this->prepareEntity($entityToHost);
        $entityDAO->setLink($this->entityLinkGenerator->generateLink($entityDAO, $host));

        if (isset(Type::MAP_DTO[$entityToHost->getEntity()->getEntityType()])) {
            $dtoClass = Type::MAP_DTO[$entityToHost->getEntity()->getEntityType()];
            /** @var DTOInterface $dto */
            $dto = new $dtoClass();
            $this->fillDTO($dto, $entityDAO);
        }
        if (!isset($dto) || !$dto) {
            throw new EntityNotFoundException;
        }

        $dto->setTotalChildren($this->entityToEntityRepository->getCount($entityToHost->getId(),Type::RELATION_TYPE_RELATE));
        $prepare[0] = $dto;
        unset($dto);

        $collection = $this->entityToEntityRepository
            ->findByMainId(
                $entityToHost->getId(),
                $request->getOffset(),
                $request->getLimit(),
                Type::RELATION_TYPE_RELATE);

        /** @var EntityHostToEntityHost $child */
        foreach ($collection as $child) {
            /** @var GetEntityInterface|SetEntityInterface $childDAO */
            $childDAO = $this->preparation->prepareEntityFromEntityToHost($child->getSubordinate());
            $childDAO->setLink($this->entityLinkGenerator->generateLink($childDAO,$host));
            if (isset(Type::MAP_DTO[$childDAO->getEntityType()])) {
                $dtoClass = Type::MAP_DTO[$childDAO->getEntityType()];
                /** @var DTOInterface $dto */
                $dto = new $dtoClass();
                $this->fillDTO($dto, $childDAO);
                $prepare[] = $dto;
                unset($dto);
            }
        }
        return $prepare;
    }

}