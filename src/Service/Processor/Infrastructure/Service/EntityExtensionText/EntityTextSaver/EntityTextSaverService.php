<?php

namespace App\Service\Processor\Infrastructure\Service\EntityExtensionText\EntityTextSaver;


use App\Service\Base\Infrastructure\Entity\Text\EntityText;
use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextSaver\EntityTextSaverInterface;
use App\Service\Processor\Infrastructure\Service\EntityExtensionText\Exeption\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Service\EntityExtensionText\Exeption\HostNotFoundException;

class EntityTextSaverService implements EntityTextSaverInterface
{
    /** @var EntitiesRepository */
    private $entityRepository;
    /** @var EntityTextRepository */
    private $entityTextRepository;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(
        EntitiesRepository $entitiesRepository,
        HostRepository $hostRepository,
        EntityTextRepository $entityTextRepository
    )
    {
        $this->entityRepository = $entitiesRepository;
        $this->entityTextRepository = $entityTextRepository;
        $this->hostRepository = $hostRepository;
    }

    public function append(int $entityId, int $hostId, string $text): void
    {
        /** @var Entity $entity */
        $entity = $this->entityRepository->find($entityId);

        if (!$entity) {
            throw new EntityNotFoundException;
        }
        /** @var Host $host */
        $host = $this->hostRepository->find($hostId);

        if (!$host) {
            throw new HostNotFoundException;
        }

        $entityText = $this->entityTextRepository->findOneBy([
            'entity' => $entity,
            'host' => $host
        ]);
        if (!$entityText) {
            $entityText = new EntityText();
            $entityText->setEntity($entity);
            $entityText->setHost($host);
        }
        $entityText->setText($text);
        $this->entityTextRepository->append($entityText);
    }

    public function save(): void
    {
        $this->entityTextRepository->commit();
    }
}