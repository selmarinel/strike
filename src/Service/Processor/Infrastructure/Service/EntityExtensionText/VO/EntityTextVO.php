<?php

namespace App\Service\Processor\Infrastructure\Service\EntityExtensionText\VO;


class EntityTextVO
{
    private $text = '';

    private $entityId = 0;

    private $hostId = 0;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return int
     */
    public function getHostId(): int
    {
        return $this->hostId;
    }

    /**
     * @param int $hostId
     */
    public function setHostId(int $hostId)
    {
        $this->hostId = $hostId;
    }

    public function __toArray()
    {
        return [
            'entity_id' => $this->getEntityId(),
            'host_id' => $this->getHostId(),
            'text' => $this->getText()
        ];
    }


}