<?php

namespace App\Service\Processor\Infrastructure\Service\EntityExtensionText\EntityTextFetcher;


use App\Service\Base\Infrastructure\Entity\Text\EntityText;
use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextFetcher\EntityTextGetInterface;
use App\Service\Processor\Infrastructure\Service\EntityExtensionText\VO\EntityTextVO;
use Doctrine\ORM\NonUniqueResultException;

class EntityTextGet implements EntityTextGetInterface
{
    /** @var EntityTextRepository */
    private $repository;

    public function __construct(EntityTextRepository $entityTextRepository)
    {
        $this->repository = $entityTextRepository;
    }

    public function getTextDTO(int $entityId, int $hostId):? EntityTextVO
    {
        $entityTextVO = new EntityTextVO();
        $entityTextVO->setEntityId($entityId);
        $entityTextVO->setHostId($hostId);

        try {
            /** @var EntityText $entityText */
            $entityText = $this->repository->createQueryBuilder('et')
                ->join('et.host', 'h')
                ->join('et.entity', 'e')
                ->where('h.id = :hostId')
                ->andWhere('e.id = :entityId')
                ->setParameter('hostId', $hostId)
                ->setParameter('entityId', $entityId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            $entityText = null;
        }
        if (!$entityText) {
            return $entityTextVO;
        }
        $entityTextVO->setText($entityText->getText());
        return $entityTextVO;

    }
}