<?php

namespace App\Service\Processor\Infrastructure\Service\EntityExtensionText;

use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use App\Service\EntityTextGenerator\Domain\Service\EntityExtension\EntityExtensionInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextInterface;

class EntityTextHandle implements EntityTextInterface
{
    /** @var EntityTextRepository */
    private $repository;
    /** @var EntityExtensionInterface */
    private $extension;

    public function __construct(
        EntityTextRepository $entityTextRepository,
        EntityExtensionInterface $entityExtension
    )
    {
        $this->repository = $entityTextRepository;
        $this->extension = $entityExtension;
    }

    /** @var  GetHostInterface */
    private $host;

    public function setHost(GetHostInterface $host)
    {
        $this->host = $host;
    }

    public function extend(DTOInterface $dataObject)
    {
        $entityText = $this->repository->getByEntityIdAndHostId(
            $this->host->getEntityId(),
            $this->host->getHostId()
        );
        if (!$entityText && $this->host->getParentId()) {
            $entityText = $this->repository->getByEntityIdAndHostId(
                $this->host->getEntityId(),
                $this->host->getParentId()
            );
        }
        if (!$entityText) {
            $this->extension->extend($dataObject, $this->host);
            return;
        }
        $dataObject->setText($entityText->getText());
    }
}