<?php

namespace App\Service\Processor\Infrastructure\Service\TagParser;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Tag;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\TagParser\TagParserInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;

class TagParserService implements TagParserInterface
{
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;

    public function __construct(EntityLinkGeneratorInterface $linkGenerator)
    {
        $this->entityLinkGenerator = $linkGenerator;
    }

    /**
     * @param GetEntityInterface $entity
     * @param EntityToHost $entityToHost
     * @param GetHostInterface $host
     * @return array
     */
    public function parseTags(GetEntityInterface $entity, EntityToHost $entityToHost, GetHostInterface $host)
    {
        $tags = [];
        if (!in_array($entity->getEntityType(), array_keys(Type::MAP_TAG))) {
            return $tags;
        }
        $tagClass = Type::MAP_TAG[$entity->getEntityType()];
        /** @var Host $hostDAO */
        $hostDAO = clone $host;
        $hostDAO->setStatic(false);
        /** @var Tag $tag */
        foreach ($entityToHost->getEntity()->getTags() as $tag) {
            /** @var DTOTagInterface|DTOInterface $tagDTO */
            $tagDTO = new $tagClass();
            $tagDTO->setName($tag->getName());
            $entityDAO = new Entity();
            $entityDAO->setEntitySlug(SlugMakerService::makeSlug($tag->getName()));
            $entityDAO->setEntityId($tag->getEntity()->getId());
            $entityDAO->setName($tag->getName());
            $entityDAO->setEntityType($tagDTO->getEntityType());
            
            $tagDTO->setLink($this->entityLinkGenerator->generateLink($entityDAO, $hostDAO));

            $tags[] = $tagDTO;
            unset($tagDTO);
        }
        return $tags;
    }
}