<?php

namespace App\Service\Processor\Infrastructure\Service\DTOReceiverMap;

use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use App\Service\Processor\Domain\VO\Receiver\SetReceiverParametersInterface;
use App\Service\Processor\Infrastructure\Exception\Map\MapException;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Service\Base\Infrastructure\Entity\Type;

class ReceiverMap implements ReceiveMapInterface
{
    /** @var SetReceiverParametersInterface */
    private $parameters;
    /** @var ContainerInterface */
    private $container;

    /**
     * ReceiverMap constructor.
     * @param SetReceiverParametersInterface $parameters
     * @param ContainerInterface $container
     */
    public function __construct(
        SetReceiverParametersInterface $parameters,
        ContainerInterface $container
    )
    {
        $this->parameters = $parameters;
        $this->container = $container;
    }

    const API_RECEIVER = 'API_RECEIVER';

    protected const HOST = 'HOST';
    protected const USERNAME = 'USERNAME';
    protected const PASSWORD = 'PASSWORD';

    protected const AUDIO = 'AUDIO';
    protected const VIDEO = 'VIDEO';
    protected const MOVIE = 'MOVIE';
    protected const DEEZER = 'DEEZER';

    protected const ARTICLE = 'ARTICLE';

    const MAP = [
        Type::AUDIO_TRACK => self::AUDIO,
        Type::AUDIO_ARTIST => self::AUDIO,
        Type::AUDIO_ALBUM => self::AUDIO,
        //audio genre
        Type::AUDIO_GENRE => self::AUDIO,
        //audio lyric
        Type::AUDIO_LYRIC => self::AUDIO,
        //audio collections
        Type::COLLECTION_AUDIO_ARTISTS => self::AUDIO,
        Type::COLLECTION_AUDIO_TRACKS => self::AUDIO,
        //video
        Type::VIDEO_EPISODE => self::VIDEO,
        Type::VIDEO_SERIES => self::VIDEO,
        Type::VIDEO_SEASON => self::VIDEO,
        //video genre
        Type::VIDEO_SERIES_GENRE => self::VIDEO,
        //movie
        Type::MOVIE_FILM => self::MOVIE,
        //movie genre
        Type::VIDEO_MOVIES_GENRE => self::VIDEO,
        //audio deezer
        Type::DEEZER_AUDIO_ARTIST => self::DEEZER,
        Type::DEEZER_AUDIO_ALBUM => self::DEEZER,
        Type::DEEZER_AUDIO_TRACK => self::DEEZER,
        //audio genre
        Type::DEEZER_AUDIO_GENRE => self::DEEZER,
        //articles
        Type::NEWS_ARTICLE => self::ARTICLE,
    ];

    /**
     * @param DTOInterface $DTO
     * @return GetReceiverParametersInterface|SetReceiverParametersInterface
     * @throws MapException
     */
    public function map(DTOInterface $DTO): GetReceiverParametersInterface
    {
        if (!isset(static::MAP[$DTO->getEntityType()])) {
            throw new MapException;
        }
        $prefix = $this->getFromType(static::MAP[$DTO->getEntityType()]);
        $host = $this->container->getParameter($prefix . self::HOST);
        $username = $this->container->getParameter($prefix . self::USERNAME);
        $password = $this->container->getParameter($prefix . self::PASSWORD);
        $parameters = clone $this->parameters;

        $parameters->setHost($host . $DTO->getEntityURI() . '/' . $DTO->getSourceId());
        $parameters->setUsername($username);
        $parameters->setPassword($password);
        return $parameters;
    }

    public function getFromType(string $type): string
    {
        return static::API_RECEIVER . '_' . $type . '_';
    }
}