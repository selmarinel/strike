<?php

namespace App\Service\Processor\Infrastructure\Service\CollectionParser;

use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\CollectionParser\CollectionParserInterface;
use App\Service\Processor\Domain\Service\CollectionParser\PrepareCollectionReceiveUrlInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\DTO\Root;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Infrastructure\Traits\DTO\DTOFillerTrait;
use App\Service\Processor\Infrastructure\Traits\DTO\TagsTrait;

class PrepareCollectionReceiveUrl implements PrepareCollectionReceiveUrlInterface
{
    use TagsTrait, DTOFillerTrait;

    /** @var CollectionParserInterface */
    private $parser;
    /** @var EncryptionInterface */
    private $encryption;
    /** @var DTOCollectionInterface */
    private $parent;
    /** @var DTOInterface[] */
    private $children = [];

    public function __construct(
        CollectionParserInterface $parser,
        EncryptionInterface $encryption
    )
    {
        $this->parser = $parser;
        $this->encryption = $encryption;
    }

    /**
     * @deprecated
     * @param GetHostInterface $host
     */
    public function prepareRoot(GetHostInterface $host)
    {
        $root = new Root();
        $root->setName('Главная');
        $root->setLink("http://{$host->getHostname()}");
        $root->setEntityType(Type::ROOT);
        $this->parent = $root;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @throws EntityNotFoundException
     */
    public function prepare(GetHostInterface $host, InputGetRequestInterface $request)
    {
        $entityToHost = $this->parser->parse($host);
        $this->encryption->setHost($host);
        $this->children = [];
        if (isset(Type::MAP_COLLECTIONS[$entityToHost->getEntity()->getEntityType()])) {
            $dtoClass = Type::MAP_COLLECTIONS[$entityToHost->getEntity()->getEntityType()];
            /** @var DTOCollectionInterface $dto */
            $dto = new $dtoClass();
            $dto->setName($entityToHost->getEntity()->getName());
            $dto->setEntityType($entityToHost->getEntity()->getEntityType());
            $dto->setLink($entityToHost->getEntity()->getLink());
        }
        if (!isset($dto) || !$dto) {
            throw new EntityNotFoundException;
        }

        $collection = $this->parser->getCollection($entityToHost, $request);
        $dto->setTotalChildren($this->parser->getTotalCollectionCount());
        $this->parent = $dto;
        unset($dto);

        foreach ($collection as $child) {
            if (isset(Type::MAP_DTO[$child->getEntityType()])) {
                $dtoClass = Type::MAP_DTO[$child->getEntityType()];
                /** @var DTOInterface $dto */
                $dto = new $dtoClass();
                $this->fillDTO($dto, $child);
                $this->children[] = $dto;
                unset($dto);
            }
        }

    }

    /**
     * @return DTOCollectionInterface
     */
    public function getParent(): DTOCollectionInterface
    {
        return $this->parent;
    }

    /**
     * @return DTOInterface[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }
}