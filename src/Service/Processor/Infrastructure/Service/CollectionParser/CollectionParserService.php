<?php

namespace App\Service\Processor\Infrastructure\Service\CollectionParser;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository; //todo refact
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\Service\TagParser\TagParserInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost; //todo refact
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository; // todo refact

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost as EntityToHostEntity;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\CollectionParser\CollectionParserInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;
use Doctrine\Common\Persistence\ManagerRegistry;

class CollectionParserService implements CollectionParserInterface
{
    use PrepareDTOTrait;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;
    /** @var EntityHostToEntityHostRepository */
    private $entityToEntityRepository;
    /** @var int */
    private $totalCollection = 0;
    /** @var TagParserInterface */
    private $tagParser;
    /** @var ManagerRegistry */
    private $registry;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EntityLinkGeneratorInterface $entityLinkGenerator,
        TagParserInterface $tagParser,

        ManagerRegistry $registry
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityLinkGenerator = $entityLinkGenerator;
        $this->entityToEntityRepository = $entityHostToEntityHostRepository;
        $this->tagParser = $tagParser;

        $this->registry = $registry;
    }

    /**
     * @param GetHostInterface $host
     * @return GetEntityToHostInterface|EntityToHost
     * @throws EntityNotFoundException
     */
    public function parse(GetHostInterface $host): GetEntityToHostInterface
    {
        $entityToHost = $this->getEntityToHost($host);

        if (!$entityToHost) {
            throw new EntityNotFoundException;
        }

        $collectionDAO = $this->prepareEntity($entityToHost);

        if ($collectionDAO->getEntityType() !== $host->getEntityType()) {
            throw new EntityNotFoundException;
        }
        $collectionDAO->setLink($this->entityLinkGenerator->generateLink($collectionDAO, $host));

        $entityToHostDAO = new EntityToHost();
        $entityToHostDAO->setSlug($entityToHost->getEntitySlug());
        $entityToHostDAO->setHost($host);
        $entityToHostDAO->setEntity($collectionDAO);

        return $entityToHostDAO;
    }

    /**
     * @param GetEntityToHostInterface $entityToHost
     * @param InputGetRequestInterface $request
     * @return GetEntityInterface[]
     */
    public function getCollection(GetEntityToHostInterface $entityToHost, InputGetRequestInterface $request): array
    {
        $collection = $this->entityToEntityRepository
            ->findByMainId(
                $entityToHost->getEntity()->getRelatedId(),
                $request->getOffset(),
                $request->getLimit());
        $this->totalCollection = $this->entityToEntityRepository->getCount($entityToHost->getEntity()->getRelatedId());
        //boost speed for tags
        $this->boostForTags($collection);
        $result = [];
        /** @var EntityHostToEntityHost $entityToHostModel */
        foreach ($collection as $entityToHostModel) {
            $entityDAO = new Entity();
            $entityDAO->setName($entityToHostModel->getSubordinate()->getEntity()->getName());
            $entityDAO->setSourceId($entityToHostModel->getSubordinate()->getEntity()->getSourceId());
            $entityDAO->setEntityType($entityToHostModel->getSubordinate()->getEntity()->getEntityType());
            $entityDAO->setEntityId($entityToHostModel->getSubordinate()->getEntity()->getId());
            $entityDAO->setEntitySlug($entityToHostModel->getSubordinate()->getEntitySlug());
            $entityDAO->setTags(
                $this->tagParser->parseTags(
                    $entityDAO,
                    $entityToHostModel->getSubordinate(),
                    $entityToHost->getHost()));
            /** @var SetHostInterface|GetHostInterface $host */
            $host = clone $entityToHost->getHost();
            $host->setStatic(false);
            $entityDAO->setLink($this->entityLinkGenerator->generateLink($entityDAO, $host));
            unset($host);
            $result[] = $entityDAO;
            unset($entityDAO);
        }
        return $result;
    }

    public function getTotalCollectionCount(): int
    {
        return $this->totalCollection;
    }

    /**
     * @param GetHostInterface $host
     * @return EntityToHostEntity|null
     */
    private function getEntityToHost(GetHostInterface $host):?EntityToHostEntity
    {
        switch ($host->getEntityType()) {
            case Type::COLLECTION_NEWS_CATEGORY:
                $entityToHost = $this->entityToHostRepository->findByHostOrParentHostIdAndTypeSlug(
                    $host->getHostId(),
                    $host->getEntitySlug(),
                    $host->getParentId(),
                    $host->getEntityType()
                );
                return $entityToHost;
            default:
                if ($host->getAbuseLevel() <= 1) {
                    $entityToHost = $this->entityToHostRepository->findByHostOrParentHostIdAndTypeSlug(
                        $host->getHostId(),
                        $host->getTypeSlug(),
                        $host->getParentId(),
                        $host->getEntityType()
                    );
                } else {
                    $entityToHost = $this->entityToHostRepository->findByHostIdAndEntitySlugAndSlugType(
                        $host->getHostId(),
                        $host->getTypeSlug(),
                        $host->getEntityType());
                }
                return $entityToHost;
        }
    }
}