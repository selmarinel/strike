<?php

namespace App\Service\Processor\Infrastructure\Service\RequestParser;


use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Processor\Infrastructure\VO\Request as InputRequest;

class RequestPreparation implements RequestPreparationInterface
{
    /**
     * @param Request $request
     * @return InputGetRequestInterface
     */
    public function prepareFromHttpRequest(Request $request): InputGetRequestInterface
    {
        $inputRequest = new InputRequest();
        $inputRequest->setIsBot($this->isBot($request->headers->get('User-Agent')));
        $inputRequest->setRequestUri($request->getRequestUri());
        $inputRequest->setGeo(mb_strtoupper($request->server->get("GEOIP_COUNTRY_CODE")));
        $inputRequest->setHostname($request->getHost());
        $inputRequest->setSlug($request->get('slug') ?? '');
        $inputRequest->setLimit($request->get('limit') ?? 12);
        $inputRequest->setPage($request->get('page') ?? 1);
        $inputRequest->setScheme($request->getScheme() ?? 'http');


        return $inputRequest;
    }

    public function prepareFromString(string $url): InputGetRequestInterface
    {
        $parsedUrlParts = parse_url($url);
        $requestVO = new InputRequest();
        $requestVO->setHostname($parsedUrlParts['host']);
        $requestVO->setSlug(trim($parsedUrlParts['path'], '/'));
        return $requestVO;
    }

    private function isBot(string $userAgent = null): bool
    {
        if (is_null($userAgent)) {
            $userAgent = '';
        }
        preg_match('/(?i)(ads|google|bing|msn|yandex|baidu|yahoo|ro|career|)bot/', $userAgent, $bot);
        return !empty($bot);
    }

}