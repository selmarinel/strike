<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver\Fetcher;

use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;

class FetcherService implements FetcherInterface
{
    private const DATA_FIELD = 'records';

    private const REQUESTS_COUNT = 12;

    private $exception = false;

    /**
     * @param GetReceiverParametersInterface[] $uriCollection
     * @return array
     */
    public function fetchMany(array $uriCollection)
    {
        if (empty($uriCollection)) {
            return [];
        }

        $curlMultiHandler = new CurlMultiHandler();
        $stack = HandlerStack::create($curlMultiHandler);
        $client = new Client([
            'auth' => [
                $uriCollection[0]->getUsername(),
                $uriCollection[0]->getPassword()
            ],
            'handler' => $stack
        ]);
        $responses = [];
        $exception = false;
        $pool = new Pool($client, $this->getRequests($uriCollection), [
            'concurrency' => static::REQUESTS_COUNT,
            'options' => [
                'timeout' => 5,
                'connect_timeout' => 3
            ],
            'fulfilled' => function (ResponseInterface $response, $index) use (&$responses) {
                $responses[$index] = json_decode($response->getBody()->getContents(), true);
            },
            'rejected' => function (BadResponseException $reason, $index) use (&$exception) {
                $exception = true;
            },
        ]);
        $this->exception = $exception;
        $promise = $pool->promise();
        while ($promise->getState() === 'pending') {
            $curlMultiHandler->tick();
        }
        $result = [];
        foreach ($responses as $index => $response) {
            if (isset($response[static::DATA_FIELD]) && !empty($response[static::DATA_FIELD])) {
                $result[$index] = array_shift($response[static::DATA_FIELD]);
            }
        }
        ksort($result);
        return $result;
    }

    /**
     * @return bool
     */
    public function hasException(): bool
    {
        return $this->exception === true;
    }

    /**
     * @param GetReceiverParametersInterface[] $uris
     * @return \Generator
     */
    private function getRequests(array $uris)
    {
        foreach ($uris as $uri) {
            yield new Request('GET', $uri->getHost());
        }
    }

}