<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver\Filler\CollectionFiller;


use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\Service\Receiver\Filler\CollectionFillerInterface;
use App\Service\Processor\Infrastructure\Exception\DTO\FillDTOException;

class CollectionFillerService implements CollectionFillerInterface
{
    /** @var FetcherInterface */
    private $fetcher;
    /** @var ReceiveMapInterface */
    private $map;
    /** @var */
    private $exception;

    public function __construct(FetcherInterface $fetcher, ReceiveMapInterface $map)
    {
        $this->fetcher = $fetcher;
        $this->map = $map;
    }

    /**
     * @param DTOCollectionInterface $DTOCollection
     * @param DTOInterface[] $DTOCollectionToPrepare
     * @return DTOCollectionInterface
     */
    public function fill(
        DTOCollectionInterface $DTOCollection,
        array $DTOCollectionToPrepare): DTOCollectionInterface
    {
        $uris = [];
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $uris[$index] = $this->map->map($DTO);
        }
        $fetchedData = $this->fetcher->fetchMany($uris);

        if ($this->fetcher->hasException()) {
            $this->exception = new FillDTOException;
        }
        if (count($DTOCollectionToPrepare) !== count($fetchedData)) {
            foreach ($DTOCollectionToPrepare as $index => $DTO) {
                if (!isset($fetchedData[$index])) {
                    $fetchedData[$index] = [];
                }
            }
        }

        if (empty($DTOCollectionToPrepare)) {
            return $DTOCollection;
        }
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $DTO->fill($fetchedData[$index]);
            $DTOCollection->appendChild($DTO);
            unset($DTOCollectionToPrepare[$index]);
        }
        return $DTOCollection;
    }
}