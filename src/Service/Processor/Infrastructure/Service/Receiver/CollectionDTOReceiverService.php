<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\Service\CollectionParser\PrepareCollectionReceiveUrlInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Domain\Service\Receiver\Filler\CollectionFillerInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\DTO\FillDTOException;

use App\Service\Base\Infrastructure\Entity\Type;

class CollectionDTOReceiverService implements CollectionDTOReceiverInterface
{
    /** @var PrepareCollectionReceiveUrlInterface */
    private $prepare;
    /** @var \Exception|null */
    private $exception = null;
    /** @var CollectionFillerInterface */
    private $filler;

    public function __construct(
        PrepareCollectionReceiveUrlInterface $prepareCollectionReceiveUrl,
        CollectionFillerInterface $collectionFiller
    )
    {
        $this->prepare = $prepareCollectionReceiveUrl;
        $this->filler = $collectionFiller;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOCollectionInterface
     * @throws FillDTOException
     */
    public function receive(
        GetHostInterface $host,
        InputGetRequestInterface $request
    ): DTOCollectionInterface
    {
        switch ($host->getEntityType()) {
            case Type::ROOT:
                $this->prepare->prepareRoot($host);
                break;
            default:
                $this->prepare->prepare($host, $request);
                break;
        }

        $collectionEntity = $this->prepare->getParent();
        $collectionEntity->setRequest($request);
        $DTOCollectionToPrepare = $this->prepare->getChildren();
        return $this->filler->fill($collectionEntity, $DTOCollectionToPrepare);
    }

    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}