<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\Service\DTOPrepare\PrepareBySlugInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\Service\Receiver\SlugDTOReceiverInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\DTO\FillDTOException;

class SlugDTOReceiverService implements SlugDTOReceiverInterface
{
    /** @var PrepareBySlugInterface */
    private $prepareService;
    /** @var ReceiveMapInterface */
    private $map;
    /** @var FetcherInterface */
    private $fetcher;
    /** @var  \Exception */
    private $exception;

    public function __construct(
        PrepareBySlugInterface $prepareService,
        ReceiveMapInterface $map,
        FetcherInterface $fetcher)
    {
        $this->prepareService = $prepareService;
        $this->map = $map;
        $this->fetcher = $fetcher;
    }

    public function receive(
        GetHostInterface $host,
        InputGetRequestInterface $request
    ): DataObjectInterface
    {
        $DTOCollectionToPrepare = $this->prepareService->prepare($host, $request);
        $uris = [];
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $uris[$index] = $this->map->map($DTO);
        }
        $fetchedData = $this->fetcher->fetchMany($uris);
        if ($this->fetcher->hasException()) {
            $this->exception = new FillDTOException;
        }
        if (count($DTOCollectionToPrepare) !== count($fetchedData)) {
            foreach ($DTOCollectionToPrepare as $index => $DTO) {
                if (!isset($fetchedData[$index])) {
                    $fetchedData[$index] = [];
                }
            }
        }

        $parentDTO = $DTOCollectionToPrepare[0];
        $parentDTO->fill($fetchedData[0]);
        $parentDTO->setRequest($request);

        unset($DTOCollectionToPrepare[0], $fetchedData[0]);
        if (empty($DTOCollectionToPrepare)) {
            return $parentDTO;
        }
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $DTO->fill($fetchedData[$index]);
            $parentDTO->appendChild($DTO);
            unset($DTOCollectionToPrepare[$index]);
        }
        return $parentDTO;
    }

    /**
     * @return \Exception|null
     */
    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}