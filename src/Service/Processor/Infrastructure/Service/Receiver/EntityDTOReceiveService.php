<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\EntityParser\PrepareEntityReceiveUrlInterface;
use App\Service\Processor\Domain\Service\Receiver\EntityDTOReceiverInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\Exception\DTO\FillDTOException;
use App\Service\Processor\Infrastructure\Exception\DTO\ReceiverCountException;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;

class EntityDTOReceiveService implements EntityDTOReceiverInterface
{
    /** @var FetcherInterface */
    private $fetcher;
    /** @var PrepareEntityReceiveUrlInterface */
    private $prepare;
    /** @var ReceiveMapInterface */
    private $map;
    /** @var \Exception|null */
    private $exception = null;

    public function __construct(
        PrepareEntityReceiveUrlInterface $prepareEntityReceiveUrl,
        ReceiveMapInterface $map,
        FetcherInterface $fetcher

    )
    {
        $this->fetcher = $fetcher;
        $this->prepare = $prepareEntityReceiveUrl;
        $this->map = $map;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DTOInterface
     * @throws FillDTOException
     */
    public function receive(
        GetHostInterface $host,
        InputGetRequestInterface $request
    ): DTOInterface
    {
        $DTOCollectionToPrepare = $this->prepare->prepare($host, $request);
        $uris = [];
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $uris[$index] = $this->map->map($DTO);
        }
        $fetchedData = $this->fetcher->fetchMany($uris);
        if ($this->fetcher->hasException()) {
            $this->exception = new FillDTOException;
        }

        if (count($DTOCollectionToPrepare) !== count($fetchedData)) {
            foreach ($DTOCollectionToPrepare as $index => $DTO) {
                if (!isset($fetchedData[$index])) {
                    $fetchedData[$index] = [];
                }
            }
        }
        $parentDTO = $DTOCollectionToPrepare[0];
        $parentDTO->fill($fetchedData[0]);
        $parentDTO->setRequest($request);

        unset($DTOCollectionToPrepare[0], $fetchedData[0]);
        if (empty($DTOCollectionToPrepare)) {
            return $parentDTO;
        }
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $DTO->fill($fetchedData[$index]);
            $parentDTO->appendChild($DTO);
            unset($DTOCollectionToPrepare[$index]);
        }
        return $parentDTO;
    }

    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}