<?php

namespace App\Service\Processor\Infrastructure\Service\Receiver;


use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Exceptions\Entity\EntityHostNotFoundException;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost as EntityToHostEntity;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOSearchInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\Service\Receiver\SearchResultDTOReceiverInterface;
use App\Service\Processor\Domain\Service\SearchParser\SearchParserInterface;
use App\Service\Processor\Domain\Service\SearchParser\SearchPreparationInterface;
use App\Service\Processor\Domain\Service\TagParser\TagParserInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\DTO\Search\Search;
use App\Service\Processor\Infrastructure\DTO\Search\YouTubeVideo;
use App\Service\Processor\Infrastructure\Exception\DTO\FillDTOException;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Traits\DTO\TagsTrait;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;
use App\Service\Search\Domain\Service\Searcher\Provider\ProviderInterface;

class SearchResultDTOReceiverService implements SearchResultDTOReceiverInterface
{
    use PrepareDTOTrait, TagsTrait;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var SearchEntityRepository */
    private $customSearchRepository;
    /** @var ProviderInterface */
    private $searchProvider;
    /** @var FetcherInterface */
    private $fetcher;
    /** @var ReceiveMapInterface */
    private $map;
    /** @var SearchPreparationInterface */
    private $preparation;
    /** @var \Exception|null */
    private $exception = null;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        SearchEntityRepository $searchEntityRepository,
        SearchPreparationInterface $preparation,
        ReceiveMapInterface $map,
        FetcherInterface $fetcher,
        ProviderInterface $provider
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->customSearchRepository = $searchEntityRepository;
        $this->searchProvider = $provider;
        $this->map = $map;
        $this->fetcher = $fetcher;
        $this->preparation = $preparation;
    }

    /**
     * @param GetHostInterface $host
     * @param InputGetRequestInterface $request
     * @return DataObjectInterface
     * @throws EntityHostNotFoundException
     * @throws EntityNotFoundException
     */
    public function receive(
        GetHostInterface $host,
        InputGetRequestInterface $request
    ): DataObjectInterface
    {
        if ($host->getAbuseLevel() <= 1) {
            $entityToHost = $this->entityToHostRepository->findByHostIdAndEntitySlugAndSlugType(
                $host->getParentId(),
                $host->getEntitySlug(),
                $host->getEntityType()
            );
        } else {
            $entityToHost = $this->entityToHostRepository->findByHostIdAndEntitySlugAndSlugType(
                $host->getHostId(),
                $host->getEntitySlug(),
                $host->getEntityType()
            );
        }

        if (!$entityToHost) {
            throw new EntityHostNotFoundException;
        }
        /** @var SearchEntity $searchEntity */
        $searchEntity = $this->customSearchRepository->findOneBy([
            'entityToHost' => $entityToHost,
            'status' => SearchEntity::PROCESSED
        ]);
        if (!$searchEntity) {
            throw new EntityNotFoundException;
        }

        $DTO = new Search();
        $DTO->setQuery($searchEntity->getQuery());

        $DTOCollectionToPrepare = $this->preparation->prepare($host, $searchEntity->getQuery());
        $this->appendLocalResults($DTO, $DTOCollectionToPrepare);
        $this->appendYouTubeResults($DTO, $searchEntity->getResults());

        return $DTO;
    }

    /**
     * @param DTOSearchInterface $DTOParent
     * @param array $DTOCollectionToPrepare
     * @return DTOSearchInterface
     */
    private function appendLocalResults(DTOSearchInterface $DTOParent, array $DTOCollectionToPrepare)
    {
        if (empty($DTOCollectionToPrepare)) {
            return $DTOParent;
        }
        $uris = [];
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $uris[$index] = $this->map->map($DTO);
        }
        $fetchedData = $this->fetcher->fetchMany($uris);
        if ($this->fetcher->hasException()) {
            $this->exception = new FillDTOException;
        }
        if (count($DTOCollectionToPrepare) !== count($fetchedData)) {
            //hack
            foreach ($DTOCollectionToPrepare as $index => $DTO) {
                if (!isset($fetchedData[$index])) {
                    $fetchedData[$index] = [];
                }
            }
        }

        if (empty($DTOCollectionToPrepare)) {
            return $DTOParent;
        }
        foreach ($DTOCollectionToPrepare as $index => $DTO) {
            $DTO->fill($fetchedData[$index]);
            $DTOParent->appendResult($DTO);
            unset($DTOCollectionToPrepare[$index]);
        }
        return $DTOParent;
    }

    public function getException():?\Exception
    {
        return $this->exception;
    }

    /**
     * @param DTOSearchInterface $DTOSearch
     * @param array $DTOYouTubeCollection
     * @return DTOSearchInterface
     */
    private function appendYouTubeResults(DTOSearchInterface $DTOSearch, array $DTOYouTubeCollection)
    {
        foreach ($DTOYouTubeCollection as $result) {
            $item = new YouTubeVideo();
            $item->setId($result->getVideoId());
            $item->setSrc($result->getSrc());
            $item->setTitle($result->getTitle());
            $DTOSearch->appendChild($item);
        }
        return $DTOSearch;
    }
}