<?php

namespace App\Service\Processor\Infrastructure\Service\TypedHandle;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\ReceiverInterface;
use App\Service\Processor\Domain\Service\TypedHandle\HandleProcessFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HandleProcessFactory implements HandleProcessFactoryInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getReceiver(GetHostInterface $host): ReceiverInterface
    {
        if ($host->isArticles()) {
            return $this->container->get('articles.receiver');
        }
        if ($host->isStatic()) {
            return $this->container->get('static.receiver');
        }
        if ($host->isSearch()) {
            return $this->container->get('search.receiver');
        }
        return $this->container->get('dynamic.receiver');
    }
}