<?php

namespace App\Service\Processor\Infrastructure\Traits\DTO;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;

/**
 * Trait DTOFillerTrait
 * @package App\Service\Processor\Infrastructure\Traits\DTO
 * @property EncryptionInterface $encryption
 *
 * @method addTagsToDTO(DTOInterface $dto, GetEntityInterface $entity)
 */
trait DTOFillerTrait
{
    private function fillDTO(DTOInterface $dto, GetEntityInterface $entity, bool $tags = true)
    {
        $dto->setEntityType($entity->getEntityType());
        $dto->setEncryption($this->encryption);
        $dto->setLink($entity->getLink());
        $dto->setSourceId($entity->getSourceId());
        $dto->setCreated((int)$entity->getCreated());

        if ($tags && method_exists($this,'addTagsToDTO')) {
            $this->addTagsToDTO($dto, $entity);
        }

        if (method_exists($dto, 'setName')) {
            $dto->setName($entity->getName());
        }
        if (method_exists($dto, 'setTitle')) {
            $dto->setTitle($entity->getName());
        }
    }
}