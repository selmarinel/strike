<?php

namespace App\Service\Processor\Infrastructure\Traits\DTO;


use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DTO\Collection\DTOHasTags;
use App\Service\Processor\Domain\DTO\DTOInterface;

trait TagsTrait
{
    public function addTagsToDTO(DTOInterface $DTO, GetEntityInterface $entity)
    {
        if ($DTO instanceof DTOHasTags && !empty($entity->getTags())) {
            $DTO->setTags($entity->getTags());
        }
    }
}