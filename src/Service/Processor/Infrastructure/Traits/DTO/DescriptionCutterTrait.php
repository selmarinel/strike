<?php

namespace App\Service\Processor\Infrastructure\Traits\DTO;


trait DescriptionCutterTrait
{
    private $patterns = [
        '/<img[^>]*>/',
        '/<a[^>]*>[\s\S]+<\/a>/'
    ];

    public function cut(string $description)
    {
        if (empty($description)) {
            return $description;
        }
        foreach ($this->patterns as $pattern) {
            $description = preg_replace($pattern, '', $description);
        }
        return $description;
    }
}