<?php

namespace App\Service\Processor\Infrastructure\Traits\DTO;


trait RatingCalculateTrait
{
    private function getRecursiveRating(string $key)
    {
        $arr = str_split($key, 1);
        $key = 0;
        foreach ($arr as $grp) {
            $key += (int)$grp;
        }
        if ($key >= 10) {
            $key = $this->getRecursiveRating((string)$key);
        }
        return $key;
    }

    public function calculateRating(array $arguments,
                                    string $ratingName,
                                    string $entityName,
                                    int $from = 4,
                                    int $to = 7)
    {
        $rating = (isset($arguments[$ratingName])) ? $arguments[$ratingName] : 0;
        if (!(int)$rating) {
            $hex_str = md5(isset($arguments[$entityName]) ? $arguments[$entityName] : getenv('APP_SECRET'));
            $arr = str_split($hex_str, 4);
            $key = 0;
            foreach ($arr as $grp) {
                $id = hexdec($grp);
                $key += (int)$id;
            }
            $key = $this->getRecursiveRating((string)$key);
            $rating = $from + ($to - $from) * (int)$key / 10;
        }
        return $rating;
    }
}