<?php

namespace App\Service\Processor\Infrastructure\Traits\DTO;

use JasonGrimes\Paginator;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

/**
 * Trait PaginateTrait
 * @package App\Service\Processor\Infrastructure\Traits\DTO
 * @property int $totalChildren
 * @property InputGetRequestInterface $request
 * @property string $link
 */
trait PaginateTrait
{
    public function getLink(): string
    {
        return $this->link;
    }

    public function preparePaginate()
    {
        if (!is_null($this->request)) {
            $totalPages = ceil($this->totalChildren / $this->request->getLimit());

            $totalItems = $totalPages * $this->request->getLimit();
            $page = $this->request->getPage();
            $limit = $this->request->getLimit();

            $urlPattern = $this->getLink() . "?limit=$limit&page=(:num)";
            $paginator = new Paginator($totalItems, $limit, $page, $urlPattern);
            return $paginator->getPages();
        }
        return [];
    }
}