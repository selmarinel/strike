<?php

namespace App\Service\Processor\Infrastructure\Traits\Parser;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost; //todo
use App\Service\Base\Infrastructure\Entity\Tag;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Trait PrepareDTOTrait
 * @package App\Service\Processor\Infrastructure\Traits\Parser
 * @property EntityLinkGeneratorInterface $entityLinkGenerator
 * @property RegistryInterface $registry
 */
trait PrepareDTOTrait
{
    public function prepareEntity(EntityToHost $entityToHost)
    {
        $entityDAO = new Entity();
        $entityDAO->setRelatedId($entityToHost->getId());
        $entityDAO->setEntityId($entityToHost->getEntity()->getId());
        $entityDAO->setName($entityToHost->getEntity()->getName());
        $entityDAO->setSourceId($entityToHost->getEntity()->getSourceId());
        $entityDAO->setEntityType($entityToHost->getEntity()->getEntityType());
        $entityDAO->setCreated($entityToHost->getEntity()->getCreateTs());
        $entityDAO->setEntitySlug($entityToHost->getEntitySlug());
        return $entityDAO;
    }

    /**
     * @param EntityHostToEntityHost[] $collection
     */
    public function boostForTags(array $collection)
    {
        //fetch entity ids
        $entityIds = [];
        foreach ($collection as $entityToHostModel) {
            $entityIds[$entityToHostModel->getSubordinate()->getEntity()->getId()]
                = $entityToHostModel->getSubordinate();
        }

        $sql = "
        SELECT
          t2.entity_id AS entity_id,
          t.entity_id AS id,
          t.name AS name,
          e.id AS entity_tag_id,
          e.name AS entity_tag_name 
        FROM tag t 
        INNER JOIN entity e ON t.entity_id = e.id
        INNER JOIN entities_to_tags t2 ON t.id = t2.tag_id 
        WHERE t2.entity_id IN (:ids);";


        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('entity_tag_id', 'entity_tag_id');
        $rsm->addScalarResult('entity_tag_name', 'entity_tag_name');
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('entity_id', 'entity_id');

        /** @var EntityManager $manager */
        $manager = $this->registry->getManager();
        $tags = $query = $manager->createNativeQuery($sql, $rsm)
            ->setParameter('ids', array_keys($entityIds))
            ->getResult();

        $tagMap = [];
        foreach ($tags as $tag) {
            $tagMap[$tag['entity_id']][] = $tag;
        }

        foreach ($collection as $entityHostToEntityHost) {
            $entity = $entityHostToEntityHost->getSubordinate()->getEntity();
            $tagsCollection = [];
            if (isset($tagMap[$entity->getId()])) {
                $tags = $tagMap[$entity->getId()];
                foreach ($tags as $tag) {
                    $entityModel = new \App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity();
                    $entityModel->setId($tag['entity_tag_id']);
                    $entityModel->setName($tag['entity_tag_name']);

                    $tagModel = new Tag();
                    $tagModel->setName($tag['name']);
                    $tagModel->setId($tag['id']);
                    $tagModel->setEntity($entityModel);

                    $tagsCollection[] = $tagModel;
                    unset($tag, $tagModel, $entityModel);
                }
            }
            $entity->setTags($tagsCollection);
            unset($tagsCollection);
        }
    }
}