<?php

namespace App\Service\Processor\Infrastructure\DAO;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityTextGenerator\Infrastructure\VO\TextGenerateTypeVO;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;

class Host implements SetHostInterface, GetHostInterface
{
    const HOST_SEO_DATA_FIELD = 'seo_data';
    const SEO_META_TAGS = 'global_meta_tags';

    /** @var int */
    private $hostId = 0;
    /** @var string */
    private $hostname = '';
    /** @var string */
    private $entitySlug = '';
    /** @var string */
    private $typeSlug = '';
    /** @var string */
    private $entityId = '';
    /** @var bool */
    private $static = false;
    /** @var int */
    private $parentId = null;
    /** @var GetSlugInterface[] */
    private $slugData = [];
    /** @var int */
    private $entityType = 0;
    /** @var string */
    private $robotsTxt = '';
    /** @var int */
    private $hostType = 0;
    /** @var string */
    private $templateDriver = '';
    /** @var int */
    private $abuseLevel = 0;
    /** @var array */
    private $seoData = [];
    /** @var array */
    private $textGenerateConfiguration = [];
    /** @var bool */
    private $hasGeoRedirect = false; //todo change name

    /**
     * @return array
     */
    public function getSeoData(): array
    {
        return $this->seoData;
    }

    /**
     * @param array $seoData
     */
    public function setSeoData(array $seoData): void
    {
        $this->seoData = $seoData;
    }


    /**
     * @return string
     */
    public function getTemplateDriver(): string
    {
        /** todo change default driver */ //todo for future updates
        return $this->templateDriver ?: 'base';
    }

    /**
     * @param string $templateDriver
     */
    public function setTemplateDriver(string $templateDriver): void
    {
        $this->templateDriver = $templateDriver;
    }

    public function getHostTypeDriver(): string
    {
        return Type::HOST_TYPE_DRIVER_MAP[$this->hostType];
    }

    /**
     * @param int $hostType
     */
    public function setHostType(int $hostType): void
    {
        $this->hostType = $hostType;
    }

    /**
     * @return string
     */
    public function getRobotsTxt(): string
    {
        return $this->robotsTxt;
    }

    /**
     * @param mixed $robotsTxt
     */
    public function setRobotsTxt(string $robotsTxt): void
    {
        $this->robotsTxt = $robotsTxt;
    }

    /**
     * @return int
     */
    public function getEntityType(): int
    {
        return $this->entityType;
    }

    /**
     * @param int $entityType
     */
    public function setEntityType(int $entityType)
    {
        $this->entityType = $entityType;
    }

    /**
     * @return GetSlugInterface[]
     */
    public function getSlugData(): array
    {
        return $this->slugData;
    }

    /**
     * @param GetSlugInterface[] $slugData
     */
    public function setSlugData(array $slugData): void
    {
        $this->slugData = $slugData;
    }

    public function appendSlug(GetSlugInterface $slug)
    {
        $this->slugData[$slug->getSlugType()] = $slug;
    }

    /**
     * @return int
     */
    public function getHostId(): int
    {
        return $this->hostId;
    }

    /**
     * @param int $hostId
     */
    public function setHostId(int $hostId): void
    {
        $this->hostId = $hostId;
    }

    /**
     * @return string
     */
    public function getEntitySlug(): string
    {
        return $this->entitySlug;
    }

    /**
     * @param string $entitySlug
     */
    public function setEntitySlug(string $entitySlug): void
    {
        $this->entitySlug = $entitySlug;
    }

    /**
     * @return string
     */
    public function getTypeSlug(): string
    {
        return $this->typeSlug;
    }

    /**
     * @param string $typeSlug
     */
    public function setTypeSlug(string $typeSlug): void
    {
        $this->typeSlug = $typeSlug;
    }

    /**
     * @return string
     */
    public function getEntityId(): string
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId(string $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname): void
    {
        $this->hostname = $hostname;
    }

    /**
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * @param bool $static
     */
    public function setStatic(bool $static): void
    {
        $this->static = $static;
    }

    /**
     * @return int
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId = null): void
    {
        $this->parentId = $parentId;
    }

    public function getAbuseLevel(): int
    {
        return $this->abuseLevel;
    }

    public function setAbuseLevel(int $abuseLevel)
    {
        $this->abuseLevel = $abuseLevel;
    }

    public function isSearch(): bool
    {
        return $this->entityType == Type::SEARCH_TYPE;
    }

    public function isArticles(): bool
    {
        return $this->entityType === Type::NEWS_ARTICLE;
    }

    /**
     * @return int
     */
    public function getHostType(): int
    {
        return $this->hostType;
    }

    /**
     * @return TextGenerateTypeVO[]
     */
    public function getTextGenerateConfiguration(): array
    {
        return $this->textGenerateConfiguration;
    }

    /**
     * @param array $configuration
     */
    public function setTextGenerateConfiguration(array $configuration)
    {
        $this->textGenerateConfiguration = $configuration;
    }

    /**
     * @return bool
     */
    public function hasGeoRedirect(): bool
    {
        return $this->hasGeoRedirect;
    }

    /**
     * @param bool $hasGeoRedirect
     */
    public function setHasGeoRedirect(bool $hasGeoRedirect)
    {
        $this->hasGeoRedirect = $hasGeoRedirect;
    }
}