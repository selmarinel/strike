<?php

namespace App\Service\Processor\Infrastructure\DAO;

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;

class Entity implements SetEntityInterface, GetEntityInterface
{
    private $relatedId = 0;
    /** @var int */
    private $entityId = 0;
    /** @var string */
    private $sourceId = '';
    /** @var string */
    private $name = '';
    /** @var int */
    private $entityType = 0;
    /** @var string */
    private $link = '';
    /** @var string */
    private $entitySlug = '';
    /** @var array */
    private $tags = [];
    /** @var string */
    private $created = '';

    private $relations = [];

    /**
     * @return self[]
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @param self[] $relations
     */
    public function setRelations(array $relations)
    {
        $this->relations = $relations;
    }


    /**
     * @param $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return int
     */
    public function getRelatedId(): int
    {
        return $this->relatedId;
    }

    /**
     * @param int $id
     */
    public function setRelatedId(int $id): void
    {
        $this->relatedId = $id;
    }

    /**
     * @return string
     */
    public function getEntitySlug(): string
    {
        return $this->entitySlug;
    }

    /**
     * @param string $entitySlug
     */
    public function setEntitySlug(string $entitySlug): void
    {
        $this->entitySlug = $entitySlug;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getSourceId(): string
    {
        return $this->sourceId;
    }

    /**
     * @param string $sourceId
     */
    public function setSourceId(string $sourceId): void
    {
        $this->sourceId = $sourceId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getEntityType(): int
    {
        return $this->entityType;
    }

    /**
     * @param int $entityType
     */
    public function setEntityType(int $entityType): void
    {
        $this->entityType = $entityType;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getCreated() :string
    {
        return $this->created;
    }

    public function setCreated(string $created)
    {
        $this->created = $created;
    }
}