<?php

namespace App\Service\Processor\Infrastructure\DAO;

use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\GetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\EntityToHost\SetEntityToHostInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;

class EntityToHost implements SetEntityToHostInterface, GetEntityToHostInterface
{
    /** @var int */
    private $id = 0;
    /** @var SetEntityInterface|GetEntityInterface */
    private $entity;
    /** @var SetHostInterface|GetHostInterface */
    private $host;
    /** @var string */
    private $slug = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    public function getEntity(): GetEntityInterface
    {
        return $this->entity;
    }

    public function getHost(): GetHostInterface
    {
        return $this->host;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setEntity(GetEntityInterface $entity)
    {
        $this->entity = $entity;
    }

    public function setHost(GetHostInterface $host)
    {
        $this->host = $host;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

}