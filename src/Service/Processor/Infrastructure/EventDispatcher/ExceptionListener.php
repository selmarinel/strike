<?php

namespace App\Service\Processor\Infrastructure\EventDispatcher;


use App\Service\Processor\Infrastructure\Exception\DTO\DTOException;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityToHostException;
use App\Service\Processor\Infrastructure\Exception\Host\HostException;
use App\Service\Processor\Infrastructure\Exception\Host\HostRedirectGeoException;
use App\Service\Processor\Infrastructure\Exception\Host\RootHostBotRedirectException;
use App\Service\Processor\Infrastructure\Exception\Map\MapException;
use App\Service\Processor\Infrastructure\Exception\Redirect\RedirectException;
use App\Service\Processor\Infrastructure\Exception\Slug\SlugException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        $availableExceptions = [
            EntityToHostException::class,
            SlugException::class,
            MapException::class,
            HostException::class,
            DTOException::class
        ];

        foreach ($availableExceptions as $exception) {
            if ($event->getException() instanceof $exception) {
                return $event->setResponse(new Response('',
                    $event->getException()->getCode()));
            }
        }

        if ($event->getException() instanceof RootHostBotRedirectException) {
            /** @var RootHostBotRedirectException $exception */
            $exception = $event->getException();
            $string = "<!DOCTYPE html><html><head>" .
                "<meta charset=\"UTF-8\" />" .
                "<title>Redirecting to {$exception->getUrl()}</title></head>" .
                "<body>Redirecting to <a href=\"{$exception->getUrl()}\">{$exception->getUrl()}</a>.</body></html>";
            return $event->setResponse(new Response($string, $event->getException()->getCode()));
        }
        if ($event->getException() instanceof HostRedirectGeoException ||
            $event->getException() instanceof RedirectException
        ) {
            return $event->setResponse(new RedirectResponse($event->getException()->getMessage()));
        }
    }
}