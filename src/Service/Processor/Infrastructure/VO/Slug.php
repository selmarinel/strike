<?php

namespace App\Service\Processor\Infrastructure\VO;


use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\ParserFactory; //todo

use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;
use App\Service\Processor\Domain\VO\Slug\SetSlugInterface;

class Slug implements SetSlugInterface, GetSlugInterface
{
    const PATTERN_ORDER = 'order';
    const PATTERN_SEPARATOR = 'separator';
    const PATTERN_SLUG = 'slug';

    /** @var array */
    private $pattern = [];
    /** @var bool */
    private $static = false;
    /** @var int */
    private $slugType = 0;

    public function getPattern(): string
    {
        $service = ParserFactory::createTypedParser($this->isStatic(), $this->slugType);
        $service->setTypeSlug($this->pattern[static::PATTERN_SLUG]);
        $service->setSeparator((string)$this->pattern[static::PATTERN_SEPARATOR]);
        $service->setOrder($this->pattern[static::PATTERN_ORDER]);
        return $service->getPatternString();
    }

    /**
     * @return array
     */
    public function getOrder(): array
    {
        $pattern = $this->pattern;
        if (isset($pattern[static::PATTERN_ORDER])) {
            return $pattern[static::PATTERN_ORDER];
        }
        return [];
    }

    public function getSeparator(): string
    {
        $pattern = $this->pattern;
        if (isset($pattern[static::PATTERN_SEPARATOR])) {
            return $pattern[static::PATTERN_SEPARATOR];
        }
        return '';
    }

    public function isStatic(): bool
    {
        return $this->static;
    }

    public function getSlug(): string
    {
        $pattern = $this->pattern;
        if (isset($pattern[static::PATTERN_SLUG])) {
            return $pattern[static::PATTERN_SLUG];
        }
        return '';
    }

    public function getSlugType(): int
    {
        return $this->slugType;
    }

    public function setPattern(array $pattern)
    {
        return $this->pattern = $pattern;
    }

    public function setStatic(bool $static)
    {
        return $this->static = $static;
    }

    public function setSlugType(int $slugType)
    {
        $this->slugType = $slugType;
    }
}