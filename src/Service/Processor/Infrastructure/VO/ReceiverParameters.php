<?php

namespace App\Service\Processor\Infrastructure\VO;


use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use App\Service\Processor\Domain\VO\Receiver\SetReceiverParametersInterface;

class ReceiverParameters implements GetReceiverParametersInterface, SetReceiverParametersInterface
{
    /** @var string */
    private $username = '';
    /** @var string */
    private $password = '';
    /** @var string */
    private $host = '';

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }
}