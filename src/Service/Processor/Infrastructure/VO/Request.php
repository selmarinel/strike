<?php

namespace App\Service\Processor\Infrastructure\VO;

use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Domain\VO\Request\InputSetRequestInterface;

class Request implements InputGetRequestInterface, InputSetRequestInterface
{
    /** @var string */
    private $slug = '';
    /** @var string */
    private $hostname = '';
    /** @var string */
    private $scheme = 'http';
    /** @var int */
    private $page = 1;
    /** @var int */
    private $limit = 10;
    /** @var string */
    private $geo = '';
    /** @var  bool */
    private $isBot = false;
    /** @var string  */
    private $requestUri = '';

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     */
    public function setScheme(string $scheme): void
    {
        if (in_array($scheme, ['http', 'https'])) {
            $this->scheme = $scheme;
        } else {
            $this->scheme = 'http';
        }
    }


    public function getHostName(): string
    {
        return $this->hostname;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setHostname(string $hostname)
    {
        $this->hostname = $hostname;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        if ($this->page > 0) {
            return ($this->page - 1) * $this->limit;
        }
        return 0;
    }

    /**
     * @return string
     */
    public function getGeo(): string
    {
        return $this->geo;
    }

    /**
     * @param string $geo
     */
    public function setGeo(string $geo)
    {
        $this->geo = $geo;
    }

    /**
     * @return bool
     */
    public function getIsBot(): bool
    {
        return $this->isBot;
    }

    /**
     * @param bool $isBot
     */
    public function setIsBot(bool $isBot)
    {
        $this->isBot = $isBot;
    }

    /**
     * @return string
     */
    public function getRequestUri(): string
    {
        return $this->requestUri;
    }

    /**
     * @param string $requestUri
     */
    public function setRequestUri(string $requestUri)
    {
        $this->requestUri = $requestUri;
    }
}