<?php

namespace App\Service\Processor\Infrastructure\VO;

use App\Service\Encryption\Domain\Service\EncryptionInterface as EncryptionServiceInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;

class Encryption implements EncryptionInterface
{
    public function __construct(EncryptionServiceInterface $encryption)
    {
        $this->encryption = $encryption;
    }

    /** @var EncryptionServiceInterface */
    private $encryption;
    /** @var GetHostInterface */
    private $host;

    /**
     * @return EncryptionServiceInterface
     */
    public function getEncryption(): EncryptionServiceInterface
    {
        return $this->encryption;
    }

    /**
     * @return GetHostInterface
     */
    public function getHost(): GetHostInterface
    {
        return $this->host;
    }

    /**
     * @param GetHostInterface $host
     */
    public function setHost(GetHostInterface $host): void
    {
        $this->host = $host;
    }
}