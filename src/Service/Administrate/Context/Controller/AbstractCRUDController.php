<?php

namespace App\Service\Administrate\Context\Controller;

use App\Service\Administrate\Domain\Controller\CRUDControllerInterface;
use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Exception\SaveException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Trait CRUDTrait
 * @package App\Service\Administrate\Infrastructure\Controller
 * @mixin CRUDControllerInterface
 */
abstract class AbstractCRUDController extends Controller
{
    /** @var CRUDRepositoryInterface  */
    protected $entityRepository;

    public function show($id)
    {
        $entity = $this->entityRepository->getById($id);
        if (!$entity) {
            return new JsonResponse([], JsonResponse::HTTP_NOT_FOUND);
        }
        return new JsonResponse($entity->serialize(), 200);
    }

    public function showMany(Request $request)
    {
        return new JsonResponse($this->entityRepository->getCollection($request), 200);
    }

    public function delete($id)
    {
        try {
            if (!$this->entityRepository->deleteById($id)) {
                return new JsonResponse([], 404);
            }
            return new JsonResponse([], 204);
        } catch (SaveException $saveException) {
            return new JsonResponse(['error' => $saveException->getMessage()], $saveException->getCode());
        }
    }


    public function create(Request $request)
    {
        try {
            $entity = $this->entityRepository->createFromRequest($request);
            if (!$entity) {
                return new JsonResponse($this->entityRepository->getValidationErrors(), 418);
            }
            return new JsonResponse($entity->serialize(), 201);
        } catch (SaveException $saveException) {
            return new JsonResponse(['error' => $saveException->getMessage()], $saveException->getCode());
        }
    }

    public function update($id, Request $request)
    {
        try {
            $entity = $this->entityRepository->updateFromRequest($id, $request);
            if (!$entity) {
                return new JsonResponse($this->entityRepository->getValidationErrors(), 418);
            }
            return new JsonResponse($entity->serialize(), 200);
        } catch (SaveException $saveException) {
            return new JsonResponse(['error' => $saveException->getMessage()], $saveException->getCode());
        }
    }
}