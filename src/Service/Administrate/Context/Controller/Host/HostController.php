<?php

namespace App\Service\Administrate\Context\Controller\Host;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Domain\Controller\CRUDControllerInterface;
use App\Service\Administrate\Exception\SaveException;
use App\Service\Administrate\Context\Controller\AbstractCRUDController;
use App\Service\Administrate\Infrastructure\Repository\Host\HostRepository;
use App\Service\Base\Infrastructure\Entity\Text\EntityText;
use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextFetcher\EntityTextGetInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityParserInterface;
use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Templator\Domain\TemplateLocalizationInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HostController extends AbstractCRUDController implements AdministrateControllerInterface, CRUDControllerInterface
{

    public function __construct(HostRepository $hostRepository)
    {
        $this->entityRepository = $hostRepository;
    }

    public function create(Request $request)
    {
        try {
            /** @var Host $entity */
            $entity = $this->entityRepository->createFromRequest($request);
            $this->entityRepository->createSubDomainsFromRequest($request, $entity);
            if (!$entity) {
                return new JsonResponse($this->entityRepository->getValidationErrors(), 418);
            }
            return new JsonResponse($entity->serialize(), 201);
        } catch (SaveException $saveException) {
            return new JsonResponse(['error' => $saveException->getMessage()], $saveException->getCode());
        }
    }

    public function available($host_id, Request $request)
    {
        try {
            $result = $this->entityRepository->available($request, $host_id);
            if (!$result) {
                return new JsonResponse(['error' => 'Host not found'], JsonResponse::HTTP_NOT_FOUND);
            }
            return new JsonResponse($result, JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function templatesAction(TemplateLocalizationInterface $localization)
    {
        return new JsonResponse(['drivers' => $localization->localize()]);
    }

    /**
     * @param Request $request
     * @param RequestPreparationInterface $preparation
     * @param UrlParserServiceInterface $urlParserService
     * @param EntityParserInterface $parser
     * @param EntityTextGetInterface $entityTextGet
     * @return JsonResponse
     */
    public function checkLink(
        Request $request,
        RequestPreparationInterface $preparation,
        UrlParserServiceInterface $urlParserService,
        EntityParserInterface $parser,
        EntityTextGetInterface $entityTextGet
    )
    {
        $request = json_decode($request->getContent(), true);

        $link = ($request['link'])??'/';
        if (!$link) {
            return new JsonResponse(['error' => 'Invalid link'], JsonResponse::HTTP_BAD_REQUEST);
        }
        $host = $urlParserService->processRequest($preparation->prepareFromString($link), true);

        try {
            $entityToHost = $parser->parse($host);

            $entityTextVO = $entityTextGet->getTextDTO(
                $entityToHost->getEntity()->getEntityId(),
                $entityToHost->getHost()->getHostId());

            return new JsonResponse($entityTextVO->__toArray(), JsonResponse::HTTP_OK);
        } catch (EntityNotFoundException $exception) {
            return new JsonResponse(['error' => 'Not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    }

    public function availableTemplatesAction(
        \App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository $hostRepository,
        TemplateLocalizationInterface $localization
    )
    {
        $used = $hostRepository->createQueryBuilder('h')
            ->select('h.driver, h.type')
            ->groupBy('h.driver,h.type')
            ->getQuery()
            ->getScalarResult();

        $usedAudio = array_map(function ($element) {
            return $element['driver'];
        }, array_filter($used, function ($element) {
            return $element['type'] == 1;
        }));
        $usedVideo = array_map(function ($element) {
            return $element['driver'];
        }, array_filter($used, function ($element) {
            return $element['type'] == 2;
        }));

        $drivers = $localization->localize();
        $result = [];
        foreach ($drivers as $type => $driverList) {
            if ($type === 'audio') {
                $result['audio'] = array_diff($driverList, $usedAudio);
            }
            if ($type === 'video') {
                $result['video'] = array_diff($driverList, $usedVideo);
            }
        }
        return new JsonResponse($result);
    }

    public function category()
    {
        try {
            $result = $this->entityRepository->getHostWithCategory();
            if (!$result) {
                return new JsonResponse(['error' => 'Hosts not found'], JsonResponse::HTTP_NOT_FOUND);
            }
            return new JsonResponse($result, JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}