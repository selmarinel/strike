<?php

namespace App\Service\Administrate\Context\Controller\Entity;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Domain\Controller\CRUDControllerInterface;
use App\Service\Administrate\Context\Controller\AbstractCRUDController;
use App\Service\Administrate\Infrastructure\Repository\Entity\EntityRepository;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextSaver\EntityTextSaverInterface;
use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Processor\Infrastructure\Service\EntityExtensionText\Exeption\EntityExtensionException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EntityController extends AbstractCRUDController implements AdministrateControllerInterface, CRUDControllerInterface
{
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
//todo rebuild tree
        return parent::delete($id);
    }

    /**
     * @param Request $request
     * @param EntityTextSaverInterface $entityTextSaver
     * @return JsonResponse
     */
    public function setEntityText(Request $request, EntityTextSaverInterface $entityTextSaver)
    {

        $request = json_decode($request->getContent(), true);
        $entityId = isset($request['entity_id']) ? $request['entity_id'] : '';
        $hostId = isset($request['host_id']) ? $request['host_id'] : '';
        $text = isset($request['text']) ? $request['text'] : '';

        $validationErrors = [];
        if (!$entityId) {
            $validationErrors['entity'] = 'Entity is required';
        }
        if (!$hostId) {
            $validationErrors['host'] = 'Host is required';
        }
        if (!$text) {
            $validationErrors['text'] = 'Text must be not empty';
        }
        if (!empty($validationErrors)) {
            return new JsonResponse(['errors' => $validationErrors], JsonResponse::HTTP_BAD_REQUEST);
        }
        try {
            $entityTextSaver->append($entityId, $hostId, $text);
            $entityTextSaver->save();
            return new JsonResponse(['message' => 'success'], JsonResponse::HTTP_OK);
        } catch (EntityExtensionException $entityExtensionException) {
            return new JsonResponse(['error' => 'Text not saved'], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @param UrlParserServiceInterface $parserService
     * @param RequestPreparationInterface $preparation
     * @param EntityTextSaverInterface $entityTextSaver
     * @return JsonResponse
     */
    public function setEntityTextFromFile(
        Request $request,
        UrlParserServiceInterface $parserService,
        RequestPreparationInterface $preparation,
        EntityTextSaverInterface $entityTextSaver
    )
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('input');
        if (!$file) {
            return new JsonResponse(['message' => 'File is required'], JsonResponse::HTTP_BAD_REQUEST);
        }
        if ($file->getClientOriginalExtension() !== 'csv') {
            return new JsonResponse(['message' => 'File must be in csv format'], JsonResponse::HTTP_BAD_REQUEST);
        }
        $errors = [];
        $fiO = new \SplFileObject($file->getRealPath());
        while (!$fiO->eof()) {
            try {
                $line = trim($fiO->fgets());
                if (empty($line)) {
                    continue;
                }
                list($domain, $text) = explode('ę', $line);

                $requestVO = $preparation->prepareFromString($domain);
                $hostDTO = $parserService->processRequest($requestVO, true);
                try {
                    $entityTextSaver->append($hostDTO->getEntityId(), $hostDTO->getHostId(), $text);
                } catch (EntityExtensionException $exception) {
                    $errors[] = $domain;
                }
                unset($requestVO, $hostDTO);
                gc_collect_cycles();
            } catch (\Exception $exception) {
               $errors[] = $exception->getMessage();
            }
        }
        $entityTextSaver->save();
        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST);
        }
        return new JsonResponse(['message' => 'success'], JsonResponse::HTTP_OK);
    }
}