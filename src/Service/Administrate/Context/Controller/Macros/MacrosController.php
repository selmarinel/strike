<?php

namespace App\Service\Administrate\Context\Controller\Macros;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Domain\Controller\CRUDControllerInterface;
use App\Service\Administrate\Infrastructure\Exception\Macros\ClosedFunctionalityException;
use App\Service\Administrate\Infrastructure\Repository\Host\HostRepository;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\GenTechMigrationTool\Domain\Service\MacrosFromFileService\MacrosFromFileInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\IncorrectDataInServiceException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MacrosToFileVo;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\PattedDecorator\Infrastructure\Repository\Template\MacrosRepository; // todo
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Administrate\Context\Controller\AbstractCRUDController;

class MacrosController extends AbstractCRUDController implements AdministrateControllerInterface, CRUDControllerInterface
{
    public function __construct(MacrosRepository $repository)
    {
        $this->entityRepository = $repository;
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse|void
     * @throws ClosedFunctionalityException
     */
    public function delete($id)
    {
        throw new ClosedFunctionalityException;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function processCSV(Request $request, MacrosFromFileInterface $macrosFromFileService)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('input');
        if (!$file) {
            return new JsonResponse(['message' => 'File is required'], JsonResponse::HTTP_BAD_REQUEST);
        }
        if ($file->getClientOriginalExtension() !== 'csv') {
            return new JsonResponse(['message' => 'File must be in csv format'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $fiO = new \SplFileObject($file->getRealPath());
        $errors = [];

        $successCount = 0;
        $lineNum = 0;
        while (!$fiO->eof()) {
            $line = trim($fiO->fgets());
            if (empty($line)) {
                continue;
            }
            try {
                list($domain, $entityType, $title, $h1, $desc) = explode(
                    'ę',
                    $line
                );
                if (!in_array($entityType, array_keys(Type::MAP_DTO + Type::MAP_COLLECTIONS))) {
                    $errors[] = ['message' => "Not supporting entity_type line {$lineNum}"];
                    continue;
                }
                $vo = new MacrosToFileVo();
                $vo->setDescription((string)$desc);
                $vo->setDomain((string)$domain);
                $vo->setEntityType((int)$entityType);
                $vo->setH1((string)$h1);
                $vo->setTitle((string)$title);
                $macrosFromFileService->process($vo);
                $successCount++;
            } catch (IncorrectDataInServiceException $dataInServiceException) {
                $errors[] = ['message' => $dataInServiceException->getMessage(), 'line' => $lineNum];
            } catch (\Exception $exception) {
                $errors[] = ['message' => "Have trouble with line {$lineNum} line is ({$line})", 'line' => $lineNum];
            }
            $lineNum++;
        }
        if (!empty($errors)) {
            return new JsonResponse([
                'errors' => $errors,
                'errorCount' => count($errors),
                'successCount' => $successCount
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
        return new JsonResponse(['message' => 'complete', 'successCount' => $successCount], JsonResponse::HTTP_OK);
    }
}