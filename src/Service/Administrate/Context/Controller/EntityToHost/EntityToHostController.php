<?php

namespace App\Service\Administrate\Context\Controller\EntityToHost;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Context\Controller\AbstractCRUDController;
use App\Service\Administrate\Infrastructure\Repository\EntityToHost\EntityToHostRepository;
use Symfony\Component\HttpFoundation\JsonResponse;


class EntityToHostController extends AbstractCRUDController implements AdministrateControllerInterface
{
    public function __construct(EntityToHostRepository $entityToHostRepository)
    {
        $this->entityRepository = $entityToHostRepository;
    }

    public function hostsWithContent()
    {
        return new JsonResponse($this->entityRepository->getHostsWithContent());
    }
}