<?php

namespace App\Service\Administrate\Context\Controller\EntityType;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Infrastructure\Repository\EntityType\EntityTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class EntityTypeController extends Controller implements AdministrateControllerInterface
{
    public function getList(EntityTypeRepository $entityTypeRepository)
    {
        return new JsonResponse($entityTypeRepository->getAllEntityDTO(), JsonResponse::HTTP_OK);
    }
}