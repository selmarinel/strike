<?php

namespace App\Service\Administrate\Domain\Repository\Entity;


interface EntityValidateInterface
{
    public function getValidationErrors(): array;

}