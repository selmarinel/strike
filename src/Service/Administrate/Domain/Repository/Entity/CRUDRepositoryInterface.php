<?php

namespace App\Service\Administrate\Domain\Repository\Entity;


interface CRUDRepositoryInterface extends CRUDEntityRepositoryInterface,EntityValidateInterface
{

}