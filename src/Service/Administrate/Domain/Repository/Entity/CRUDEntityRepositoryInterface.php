<?php

namespace App\Service\Administrate\Domain\Repository\Entity;


use Symfony\Component\HttpFoundation\Request;

interface CRUDEntityRepositoryInterface
{
    /**
     * @param Request $request
     * @return null|\Serializable
     */
    public function createFromRequest(Request $request): ?\Serializable;

    /**
     * @param $id
     * @return null|\Serializable
     */
    public function getById($id): ?\Serializable;

    /**
     * @param $id
     * @param Request $request
     * @return null|\Serializable
     */
    public function updateFromRequest($id, Request $request): ?\Serializable;

    public function deleteById($id): ?bool;

    public function getCollection(Request $request): array;
}