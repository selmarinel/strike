<?php

namespace App\Service\Administrate\Domain\Controller;


use Symfony\Component\HttpFoundation\Request;

interface CRUDControllerInterface
{
    public function create(Request $request);

    public function show($id);

    public function delete($id);

    public function showMany(Request $request);

    public function update($id, Request $request);
}