<?php

namespace App\Service\Administrate\Exception\Auth;


class AuthException extends \Exception
{
    protected $code = 401;

    protected $message = 'unauthorized';
}