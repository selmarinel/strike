<?php

namespace App\Service\Administrate\Exception\Host;


use App\Service\Administrate\Exception\SaveException;

class HostSaveException extends SaveException
{

}