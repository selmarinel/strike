<?php

namespace App\Service\Administrate\Exception;


class SaveException extends \Exception
{
    protected $code = 400;
}