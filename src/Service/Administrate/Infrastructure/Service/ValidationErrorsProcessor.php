<?php

namespace App\Service\Administrate\Infrastructure\Service;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationErrorsProcessor
{
    private $errors = [];

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     */
    public function process(ConstraintViolationListInterface $constraintViolationList)
    {
        $this->errors = [];
        if ($constraintViolationList->count()) {
            /** @var ConstraintViolationInterface $constraintViolation */
            foreach ($constraintViolationList as $constraintViolation) {
                $this->errors[$constraintViolation->getPropertyPath()] = $constraintViolation->getMessage();
            }
        }
    }

    public function addError(string $path, string $message)
    {
        $this->errors[$path] = $message;
    }

    /**
     * @return bool
     */
    public final function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * @return array
     */
    public final function getErrors(): array
    {
        return $this->errors;
    }
}