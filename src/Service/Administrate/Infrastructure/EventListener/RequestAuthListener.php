<?php

namespace App\Service\Administrate\Infrastructure\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestAuthListener
{
    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->isMethod('OPTIONS')) {
            return $event->setResponse(new JsonResponse([], JsonResponse::HTTP_NO_CONTENT));
        }
    }

}