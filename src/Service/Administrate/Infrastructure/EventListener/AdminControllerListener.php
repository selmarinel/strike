<?php

namespace App\Service\Administrate\Infrastructure\EventListener;

use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Administrate\Exception\Auth\AuthException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class AdminControllerListener
{
    private $salt;

    public function __construct(string $salt = '')
    {
        $this->salt = $salt;
    }

    /**
     * @param FilterControllerEvent $event
     * @return JsonResponse|void
     * @throws AuthException
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $controller = $event->getController();

        if(!is_array($controller)){
            return;
        }

        if($controller[0] instanceof AdministrateControllerInterface){
            $token = $request->headers->get('AUTH_TOKEN');
            $userId = $request->headers->get('USER_ID');

            if (!$token || !$userId) {
                throw new AuthException;
            }

            $check = md5($userId . $this->salt) === $token;

            if (!$check) {
                throw new AuthException;
            }
        }
    }
}