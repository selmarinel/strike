<?php

namespace App\Service\Administrate\Infrastructure\EventListener;

use App\Service\Administrate\Exception\Auth\AuthException;
use App\Service\Administrate\Infrastructure\Exception\Macros\ClosedFunctionalityException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        if ($event->getException() instanceof AuthException) {
            return $event->setResponse(new JsonResponse(['error' => $event->getException()->getMessage()],
                $event->getException()->getCode()));
        }
        if($event->getException() instanceof ClosedFunctionalityException){
            return $event->setResponse(new JsonResponse(['error' => $event->getException()->getMessage()],
                $event->getException()->getCode()));
        }
    }
}