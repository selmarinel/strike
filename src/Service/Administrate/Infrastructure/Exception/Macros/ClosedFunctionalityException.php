<?php

namespace App\Service\Administrate\Infrastructure\Exception\Macros;


class ClosedFunctionalityException extends \Exception
{
    protected $code = 418;
    protected $message = 'Not now';
}