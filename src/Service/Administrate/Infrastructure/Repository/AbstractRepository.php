<?php

namespace App\Service\Administrate\Infrastructure\Repository;


use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Exception\SaveException;
use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use App\Service\EntityProcessor\Domain\Database\Repositories\RepositoryInterface; //todo
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Trait AbstractRepository
 * @package App\Service\Administrate\Infrastructure\Saver
 * @mixin CRUDRepositoryInterface
 */
abstract class AbstractRepository
{
    /** @var RepositoryInterface */
    protected $repository;
    /** @var ValidatorInterface */
    protected $validator;

    /** @var ValidationErrorsProcessor */
    protected $validationProcessor;

    /** @var \Serializable */
    protected $entityClass;

    /**
     * @param $id
     * @return null|object|\Serializable
     */
    public function getById($id): ?\Serializable
    {
        return $this->repository->findById($id);
    }

    /**
     * @param Request $request
     * @return \Serializable|null
     * @throws SaveException
     */
    public function createFromRequest(Request $request): ?\Serializable
    {
        $request = json_decode($request->getContent(), true);
        if (empty($request)) {
            return null;
        }
        $entity = new $this->entityClass();
        return $this->save($entity, $request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Serializable|null
     * @throws SaveException
     */
    public function updateFromRequest($id, Request $request): ?\Serializable
    {
        $request = json_decode($request->getContent(), true);
        /** @var \Serializable $serializable */
        $serializable = $this->repository->findById($id);
        if (!$serializable) {
            return null;
        }
        return $this->save($serializable, $request);
    }

    /**
     * @param $id
     * @return bool|null
     * @throws SaveException
     */
    public function deleteById($id): ?bool
    {
        /** @var \Serializable $serializable */
        $serializable = $this->repository->findById($id);
        if (!$serializable) {
            return null;
        }
        try {
            $this->repository->remove($serializable);
            return true;
        } catch (OptimisticLockException $optimisticLockException) {
            throw new SaveException;
        } catch (ORMException $ORMException) {
            throw new SaveException;
        } catch (DBALException $DBALException) {
            throw new SaveException;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getCollection(Request $request): array
    {
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;

        $query = $this->repository->createQueryBuilder('e')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $collection = new Paginator($query);

        $result = [];
        $result['count'] = $collection->count();
        $result['data'] = [];
        foreach ($collection as $model) {
            $result['data'][] = $model->serialize();
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->validationProcessor->getErrors();
    }

    /**
     * @param \Serializable $serializable
     * @param array $parameters
     * @return null|\Serializable
     * @throws SaveException
     */
    protected function save(\Serializable $serializable, array $parameters): ?\Serializable
    {
        try {
            $serializable->unserialize($parameters);
            $this->validationProcessor->process($this->validator->validate($serializable));
            if ($this->validationProcessor->hasErrors()) {
                return null;
            }
            return $this->repository->save($serializable);
        } catch (OptimisticLockException $optimisticLockException) {
            throw new SaveException($optimisticLockException->getMessage());
        } catch (ORMException $ORMException) {
            throw new SaveException($ORMException->getMessage());
        } catch (DBALException $DBALException) {
            throw new SaveException($DBALException->getMessage());
        }
    }

}