<?php

namespace App\Service\Administrate\Infrastructure\Repository\EntityType;

use App\Service\Base\Infrastructure\Entity\Type;

class EntityTypeRepository
{
    const MAP = [
        0 => [
            Type::ROOT,
            Type::STATIC
        ],
        Type::HOST_TYPE_AUDIO => [
            Type::AUDIO_TRACK,
            Type::AUDIO_ALBUM,
            Type::AUDIO_ARTIST,
            Type::AUDIO_GENRE,
            Type::AUDIO_LYRIC,
            Type::COLLECTION_AUDIO_ARTISTS,
            Type::COLLECTION_AUDIO_TRACKS
        ],
        Type::HOST_TYPE_VIDEO => [
            Type::VIDEO_EPISODE,
            Type::VIDEO_SERIES,
            Type::VIDEO_SEASON,
            Type::VIDEO_SERIES_GENRE,
            Type::COLLECTION_VIDEO_SERIES
        ],
        Type::HOST_TYPE_MOVIE => [
            Type::MOVIE_FILM,
            Type::VIDEO_MOVIES_GENRE,
            Type::COLLECTION_MOVIE_FILMS,
        ]

    ];

    public function getAllEntityDTO()
    {
        $result = [];
        foreach (Type::DTO_MAP as $type => $data) {
            $result[$type] = $data['name'];
        }
        ksort($result);
        return $result;
    }

    public function getEntityTypesByHostType(int $type)
    {
        $result = [];
        $types = self::MAP[0];
        $types = array_merge($types, self::MAP[$type]);
        foreach ($types as $type) {
            if (isset(Type::DTO_MAP[$type])) {
                $result[$type] = Type::DTO_MAP[$type]['class'];
            }
        }
        ksort($result);
        return $result;

    }
}