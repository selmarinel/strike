<?php

namespace App\Service\Administrate\Infrastructure\Repository\Entity;

use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Infrastructure\Repository\AbstractRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;//todo
use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EntityRepository extends AbstractRepository implements CRUDRepositoryInterface
{
    protected $entityClass = Entity::class;

    /**
     * HostRepository constructor.
     * @param EntitiesRepository $repository
     * @param ValidatorInterface $validator
     * @param ValidationErrorsProcessor $validationErrorsProcessor
     */
    public function __construct(
        EntitiesRepository $repository,
        ValidatorInterface $validator,
        ValidationErrorsProcessor $validationErrorsProcessor
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validationProcessor = $validationErrorsProcessor;
    }

    public function getCollection(Request $request): array
    {
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;
        $entityType = $request->query->get('entity_type') ?: null;
        $sourceId = $request->query->get('source_id') ?: null;

        $queryBuilder = $this->repository->createQueryBuilder('e');
        if ($entityType) {
            $queryBuilder->andWhere('e.entity_type = :entity_type')
                ->setParameter('entity_type', (int)$entityType);
        }
        if ($sourceId) {
            $queryBuilder->andWhere('e.source_id = :source_id')
                ->setParameter('source_id', (int)$sourceId);
        }
        $queryBuilder->setFirstResult($offset)
            ->setMaxResults($limit);
        $collection = new Paginator($queryBuilder);

        $result = [];
        $result['count'] = $collection->count();
        $result['data'] = [];
        foreach ($collection as $model) {
            $result['data'][] = $model->serialize();
        }
        return $result;
    }
}