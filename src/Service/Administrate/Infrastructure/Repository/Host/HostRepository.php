<?php

namespace App\Service\Administrate\Infrastructure\Repository\Host;

use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Exception\Host\HostSaveException;
use App\Service\Administrate\Exception\SaveException;
use App\Service\Administrate\Infrastructure\Repository\AbstractRepository;
use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host; //todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository as Repository;//todo
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\Processor\Infrastructure\DAO\Host as HostDAO;

class HostRepository extends AbstractRepository implements CRUDRepositoryInterface
{
    protected $entityClass = Host::class;

    /**
     * HostRepository constructor.
     * @param Repository $repository
     * @param ValidatorInterface $validator
     * @param ValidationErrorsProcessor $validationErrorsProcessor
     */
    public function __construct(
        Repository $repository,
        ValidatorInterface $validator,
        ValidationErrorsProcessor $validationErrorsProcessor
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validationProcessor = $validationErrorsProcessor;
    }

    /**
     * @param \Serializable $serializable
     * @param array $parameters
     * @return null|\Serializable
     * @throws \App\Service\Administrate\Exception\SaveException
     */
    protected function save(\Serializable $serializable, array $parameters): ?\Serializable
    {
        /** @var Host $serializable */
        $this->processParametersToFetchEntities($parameters);

        if (!isset($parameters['parent'])) {
            $query = $this->repository->createQueryBuilder('h')
                ->where('h.type = :type')
                ->andWhere('h.driver = :drive')
                ->andWhere('h.parent is null')
                ->setParameter('type', $parameters['type'])
                ->setParameter('drive', $parameters['driver']);

            if ($serializable->getId()) {
                $query
                    ->andWhere('h.id != :id')
                    ->setParameter('id', $serializable->getId());
            }
            $existsDriver = $query
                ->getQuery()
                ->getResult();

            if ($existsDriver && count($existsDriver)) {
                throw new HostSaveException('Host with this driver exists');
            }
        }

        if ($parameters['data'] && $parameters['data']['slug_types'] && is_array($parameters['data']['slug_types'])) {
            foreach ($parameters['data']['slug_types'] as $index => $slugTypeData) {
                if (!$slugTypeData['pattern']) {
                    throw new HostSaveException;
                    continue;
                }
                if (!$serializable->getId()) {
                    if (!isset($parameters['parent_id'])) {
                        $type = $slugTypeData['slug_type'];
                        $parameters['data']['slug_types'][$index]['pattern']['slug']
                            = $this->repository->randomizeSlug($type);
                    }
                }
            }
        }
        return parent::save($serializable, $parameters);
    }

    /**
     * @param array $parameters
     */
    private function processParametersToFetchEntities(array &$parameters)
    {
        if (isset($parameters['parent_id'])) {
            $parameters['parent'] = $this->repository->findById($parameters['parent_id']);
        }
    }

    public function createSubDomainsFromRequest(Request $request, Host $host)
    {
        $request = json_decode($request->getContent(), true);
        if (!isset($request['sub_domains'])) {
            return null;
        }
        $subDomains = explode(',', $request['sub_domains']);
        foreach ($subDomains as $subDomain) {
            $request['hostname'] = $subDomain . "." . $host->getHostname();
            $request['geo'] = $this->repository->getGeoFromSubDomain($subDomain);
            $request['parent_id'] = $host->getId();
            $request['abuse_level'] = $this->repository->getAbuseLevelFromSubDomain($subDomain);
            $request['data'] = $host->getData();
            $request['type'] = $host->getType();
            $request['driver'] = $host->getDriver();
            $newRequest = new Request([], [], [], [], [], [], json_encode($request));
            try {
                $this->createFromRequest($newRequest);
            } catch (SaveException $e) {
                //ignore
                throw $e;
                //todo log exception
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getCollection(Request $request): array
    {
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;
        $isParent = $request->query->get('parent') ?: 0;
        $parentId = $request->query->get('parent_host_id') ?: 0;

        $queryBuilder = $this->repository
            ->createQueryBuilder('h');

        if ($isParent) {
            $queryBuilder->where('h.parent is null');
        }
        if ($parentId) {
            $queryBuilder
                ->join('h.parent', 'p')
                ->andWhere('p.id = :parentId')
                ->setParameter('parentId', $parentId);
        }

        $queryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $collection = new Paginator($queryBuilder);

        $result = [];
        $result['count'] = $collection->count();
        $result['data'] = [];
        foreach ($collection as $model) {
            $result['data'][] = $model->serialize();
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return null|array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function available(Request $request, $id)
    {
        $request = json_decode($request->getContent(), true);

        $available = (bool)$request['status'];

        $host = $this->repository->findById($id);
        if (!$host) {
            return null;
        }
        $status = ($available) ? Type::HOST_STATUS_ACTIVE : Type::HOST_STATUS_DISABLE;

        $result = [];

        $host->setStatus($status);
        $this->repository->simpleSave($host);
        $result[] = [
            'host_id' => $host->getId(),
            'available' => (bool)$host->getStatus()
        ];
        if (!$host->getParentId()) {
            foreach ($host->getChildren() as $child) {
                $child->setStatus($status);
                $this->repository->simpleSave($child);
                $result[] = [
                    'host_id' => $child->getId(),
                    'available' => (bool)$host->getStatus()
                ];
            }
        }
        return $result;
    }

    public function getHostWithCategory(): array
    {
        $result = [];
        /** @var Host[] $hosts */
        $hosts = $this->repository
            ->createQueryBuilder('h')
            ->where('h.parent is null')
            ->getQuery()
            ->getResult();

        foreach ($hosts as $host) {
            $result[$host->getHostname()] = $host->getType();
        }

        return $result;
    }
}
