<?php

namespace App\Service\Administrate\Infrastructure\Repository\EntityToHost;

use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Exception\SaveException;
use App\Service\Administrate\Infrastructure\Repository\AbstractRepository;
use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository as Repository;//todo
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;//todo
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class EntityToHostRepository extends AbstractRepository implements CRUDRepositoryInterface
{
    protected $entityClass = EntityToHost::class;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var UriSlugParserInterface */
    private $parser;

    /**
     * HostRepository constructor.
     * @param Repository $repository
     * @param ValidatorInterface $validator
     * @param ValidationErrorsProcessor $validationErrorsProcessor
     * @param HostRepository $hostRepository
     * @param EntitiesRepository $entitiesRepository
     */
    public function __construct(
        Repository $repository,
        ValidatorInterface $validator,
        ValidationErrorsProcessor $validationErrorsProcessor,
        HostRepository $hostRepository,
        EntitiesRepository $entitiesRepository,
        UriSlugParserInterface $uriSlugParser
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validationProcessor = $validationErrorsProcessor;
        $this->hostRepository = $hostRepository;
        $this->entitiesRepository = $entitiesRepository;
        $this->parser = $uriSlugParser;
    }

    /**
     * @param \Serializable $serializable
     * @param array $parameters
     * @return null|\Serializable
     * @throws SaveException
     */
    protected function save(\Serializable $serializable, array $parameters): ?\Serializable
    {
        try {
            $this->processParametersToFetchEntities($parameters);
            $serializable->unserialize($parameters);
            $this->validationProcessor->process($this->validator->validate($serializable));
            if ($this->validationProcessor->hasErrors()) {
                return null;
            }
            return $this->repository->save($serializable);
        } catch (OptimisticLockException $optimisticLockException) {
            throw new SaveException($optimisticLockException->getMessage());
        } catch (ORMException $ORMException) {
            throw new SaveException($ORMException->getMessage());
        } catch (DBALException $DBALException) {
            throw new SaveException($DBALException->getMessage());
        }
    }

    public function getHostsWithContent()
    {
        $hosts = $this->hostRepository->createQueryBuilder('h')
            ->where('h.parent is not null')
            ->andWhere('h.abuse_level = :abuse_level')
            ->setParameter('abuse_level',1)
            ->getQuery()
            ->getResult();
        $result = [];
        /** @var Host $host */
        foreach ($hosts as $host) {
            $slugData = $this->parser->parseSlug($host->getData());
            $types = self::AvailablePages[$host->getType()];
            $result[$host->getHostname()][] = "http://{$host->getHostname()}";
            foreach ($slugData as $slug) {
                if (in_array($slug->getSlugType(), $types)) {
                    $result[$host->getHostname()][] = "http://{$host->getHostname()}/{$slug->getSlug()}";
                }
            }
        }
        return $result;
    }

    const AvailablePages = [
        Type::HOST_TYPE_AUDIO => [
            Type::COLLECTION_AUDIO_ARTISTS,
            Type::COLLECTION_AUDIO_TRACKS,
        ],
        Type::HOST_TYPE_VIDEO => [
            Type::COLLECTION_VIDEO_SERIES,
            Type::COLLECTION_MOVIE_FILMS,
        ]
    ];

    /**
     * @param array $parameters
     */
    private function processParametersToFetchEntities(array &$parameters)
    {
        if (isset($parameters['host_id'])) {
            $parameters['host'] = $this->hostRepository->findById($parameters['host_id']);
        }
        if (isset($parameters['entity_id'])) {
            $parameters['entity'] = $this->entitiesRepository->findById($parameters['entity_id']);
        }
    }

}