<?php

namespace App\Service\EntityTextGenerator\Context\Command;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityTextGenerator\Domain\Service\Parser\ParserFactoryInterface;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\HandleException;
use App\Service\EntityTextGenerator\Infrastructure\Logger\EntityTextGeneratorLog;
use Doctrine\ORM\EntityManagerInterface;
use KronikarzClient\Infrastructure\Logger\Scripter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessOneEntityCommand extends Command
{
    /** @var EntityManagerInterface */
    private $manager;
    /** @var  OutputInterface */
    private $out;
    /** @var ParserFactoryInterface */
    private $parserFactory;
    /** @var SaverInterface */
    private $saver;

    private const TYPES = [
        GeneratorType::KINOPOISK_TYPE,
        GeneratorType::YOUTUBE_CAPTION_TYPE
    ];

    private const KINOPOISK_ENTITY_TYPES_TO_PARSE = [
        Type::MOVIE_FILM,
        Type::VIDEO_SERIES
    ];

    public function __construct(
        EntityManagerInterface $entityManager,
        ParserFactoryInterface $parserFactory,
        SaverInterface $saver)
    {
        $this->manager = $entityManager;
        $this->parserFactory = $parserFactory;
        $this->saver = $saver;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName("process:one:entity")
            ->setAliases(['p:o:e'])
            ->addArgument('entityId', InputArgument::REQUIRED)
            ->addArgument('type', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;
        $entityId = $input->getArgument('entityId');
        $type = $input->getArgument('type');

        if (!in_array($type, self::TYPES)) {
            return 1;
        }
        $output->writeln("Process Started");
        /** @var Entity $entity */
        $entity = $this->manager
            ->getRepository(Entity::class)
            ->find($entityId);
        if (!$entity) {
            return 1;
        }

        if ($type == GeneratorType::KINOPOISK_TYPE &&
            !in_array($entity->getEntityType(), self::KINOPOISK_ENTITY_TYPES_TO_PARSE)) {
            return 1;
        }

        return $this->process($entity, $type);
    }

    /**
     * @param Entity $entity
     * @param int $type
     * @return int
     */
    private function process(Entity $entity, int $type)
    {
        try {
            chronicle(EntityTextGeneratorLog::create(
                $type,
                EntityTextGeneratorLog::ACTION_CREATING,
                $entity));
            $parser = $this->parserFactory->createParserService($type);
            $key = $this->generateKey($entity);
            $text = $parser->process($key);
            if (!$text) {
                $this->out->writeln('Empty Text');
                $this->saveWithError($entity, $type, 'EmptyText');
                return 1;
            }
            if (!$this->checkEncoding($text)) {
                $this->out->writeln('Invalid Encode');
                $this->saveWithError($entity, $type, 'Invalid Encode');
                return 1;
            }
            if ($this->checkTextLength($text)) {
                $this->out->writeln('Invalid text length');
                $this->saveWithError($entity, $type, 'Invalid text length');
                return 1;
            }

            $this->saver->createNewEntityTypedText($entity, $type, $text);
            $this->saver->commit();
            chronicle(EntityTextGeneratorLog::create($type,
                EntityTextGeneratorLog::ACTION_SUCCESS,
                $entity));
        } catch (HandleException $exception) {
            $this->out->writeln("<error>{$exception->getMessage()}</error>");
            $this->saveWithError($entity, $type, $exception->getMessage());
            return 1;
        } catch (\Exception $exception) {
            $this->out->writeln("FATAL <error>{$exception->getMessage()}</error>");
            $this->saveWithError($entity, $type, $exception->getMessage());
            return 1;
        } finally {
            gc_collect_cycles();
            if (isset($parser)) {
                unset($parser);
            }
            if (isset($text)) {
                unset($text);
            }
        }
        return 0;
    }

    /**
     * @param Entity $entity
     * @param int $type
     * @param string $message
     */
    private function saveWithError(Entity $entity, int $type, string $message)
    {
        $this->saver->createNewEntityTypedText($entity, $type, '', EntityTypedText::FAILED);
        $this->saver->commit();
        chronicle(EntityTextGeneratorLog::create(
            $type,
            EntityTextGeneratorLog::ACTION_FAIL,
            $entity,
            $message));
    }

    private function checkEncoding(string $text)
    {
        $check = mb_detect_encoding($text, 'UTF-8', true);
        return $check;
    }

    private function checkTextLength(string $text)
    {
        return mb_strlen($text) < 5;
    }

    /**
     * @param Entity $entity
     * @return string
     */
    private function generateKey(Entity $entity)
    {
        $tree = $this->manager->getRepository(EntityNestedTree::class);
        /** @var EntityNestedTree $node */
        $node = $tree->findByEntity($entity);

        if ($node->getLevel() == 0) {
            return $this->prepareKey($entity);
        }
        /** @var EntityNestedTree $root */
        $root = $node->getRoot();
        if (!$root) {
            return $this->prepareKey($entity);
        }
        $rootEntity = $root->getEntity();
        return "{$rootEntity->getName()} {$entity->getName()}";
    }

    private const NEED_TO_ESCAPE = [
        Type::DEEZER_AUDIO_ARTIST,
        Type::AUDIO_ARTIST,
    ];

    private function prepareKey(Entity $entity)
    {
        if (in_array($entity->getEntityType(), self::NEED_TO_ESCAPE)) {
            return "\"{$entity->getName()}\"";
        }
        return $entity->getName();
    }
}