<?php

namespace App\Service\EntityTextGenerator\Context\Command;

use App\Service\Base\Infrastructure\Entity\Text\EntityText;
use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use App\Service\Base\Infrastructure\Repository\TypedText\EntityTypedTextRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityTextGenerator\Domain\Translator\TranslationCoreInterface;
use App\Service\EntityTextGenerator\Infrastructure\Logger\TranslatorLog;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateTranslatedTextsCommand extends Command
{
    private const GEOS = [
        'ua',
        'kz'
    ];

    private const SQL = 'SELECT ett.*
        FROM entity_typed_text ett
        JOIN entity_to_host eth ON eth.entity_id = ett.entity_id
        WHERE eth.host_id = :hostId 
        AND ett.status = :status 
        AND ett.type_id IN (:types)
        AND NOT EXISTS(SELECT id FROM entity_text et WHERE et.entity_id = ett.entity_id)
        LIMIT :limit OFFSET :offset';

    /** @var HostRepository */
    private $hostRepository;
    /** @var TranslationCoreInterface */
    private $translation;
    /** @var EntityTextRepository */
    private $entityTextRepository;
    /** @var EntityTypedTextRepository */
    private $entityTypedTextRepository;
    /** @var EntityManagerInterface */
    private $manager;
    /** @var ResultSetMapping */
    private $rsm;

    public function __construct(
        EntityManagerInterface $manager,
        TranslationCoreInterface $translationCore
    )
    {
        $this->manager = $manager;
        $this->hostRepository = $manager->getRepository(Host::class);
        $this->entityTextRepository = $manager->getRepository(EntityText::class);
        $this->entityTypedTextRepository = $manager->getRepository(EntityTypedText::class);
        $this->translation = $translationCore;
        $this->rsm = new ResultSetMapping();
        $this->rsm->addScalarResult('id', 'id');
        $this->rsm->addScalarResult('type_id', 'type_id');
        $this->rsm->addScalarResult('text', 'text');
        $this->rsm->addScalarResult('entity_id', 'entity_id');
        $this->rsm->addScalarResult('status', 'status');
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('generate:translated:texts')
            ->setAliases(['g:t:t', 'getto'])
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL, 'HostType', null)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit', 1000);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();

        $limit = ((int)$input->getOption('limit')) ?: 1000;
        foreach ($this->getHosts($input->getOption('type')) as $host) {
            $output->writeln("Host [<info>{$host->getHostname()}</info>]");
            //fetch entities on host
            $offset = 0;
            $resulted = $this->prepareHost($host, $limit, $offset);
            if (empty($resulted)) {
                $output->writeln('<comment>Skip</comment> Not found entities');
                continue;
            }
            $progressBar = new ProgressBar($output, count($resulted));
            $progressBar->setFormat("%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");
            $progressBar->start();

            foreach ($resulted as $entityId => $text) {
                try {
                    $entity = $this->manager->find(Entity::class, $entityId);
                    $entityText = $this->getEntityText($entity, $host);
                    if ($entityText) {
                        unset($entityText, $entity, $resulted[$entityId]);
                        continue;
                    }
                    chronicle(TranslatorLog::create(
                        TranslatorLog::ACTION_CREATING,
                        [
                            'entity_id' => $entityId,
                            'host' => $host->getHostname()
                        ],
                        ''));
                    $translated = $this->translation->translate($text, $host->getGeo());
                    if (!$translated) {
                        chronicle(TranslatorLog::create(
                            TranslatorLog::ACTION_FAIL_EMPTY_TRANSLATE,
                            [
                                'entity_id' => $entityId,
                                'host' => $host->getHostname()
                            ],
                            ''
                        ));
                        continue;
                    }
                    $this->createEntityText($entity, $host, $translated);

                    chronicle(TranslatorLog::create(
                        TranslatorLog::ACTION_SUCCESS,
                        [
                            'entity_id' => $entityId,
                            'host' => $host->getHostname()
                        ],
                        ''
                    ));
                } catch (\Exception $exception) {
                    chronicle(TranslatorLog::create(
                        TranslatorLog::ACTION_EXCEPTION,
                        $exception,
                        ''
                    ));
                } finally {
                    $progressBar->advance();
                    gc_collect_cycles();
                }
            }

            $this->manager->flush();
            $this->manager->clear(EntityText::class);

            $progressBar->finish();
            $output->writeln('');
            gc_collect_cycles();
        }
    }

    /**
     * @param int|null $type
     * @return Host[]
     */
    private function getHosts(int $type = null): array
    {
        $builder = $this->hostRepository->createQueryBuilder('h')
            ->where('h.geo in (:geo)')
            ->andWhere('h.textGenerateSettings not like :settings')
            ->andWhere('h.parent is not null')
            ->andWhere('h.abuse_level = :abuse_level')
            ->setParameter('geo', self::GEOS)
            ->setParameter('settings', '[]')
            ->setParameter('abuse_level', 1);

        if ($type) {
            $builder->andWhere('h.type = :type')
                ->setParameter('type', $type);
        }
        return $builder->getQuery()->getResult();
    }

    /**
     * @param Host $host
     * @param int $limit
     * @param int $offset
     * @return array
     */
    private function prepareHost(Host $host, int $limit, int &$offset): array
    {
        $collection = $this->manager
            ->createNativeQuery(self::SQL, $this->rsm)
            ->setParameter('status', EntityTypedText::PROCESSED)
            ->setParameter('types', $host->getTextGenerateSettings())
            ->setParameter('hostId', $host->getParentId())
            ->setParameter('limit', $limit)
            ->setParameter('offset', $offset)
            ->getResult();

        if (empty($collection)) {
            return [];
        }

        $resulted = [];
        foreach ($collection as $item) {
            $resulted[$item['entity_id']][$item['type_id']] = $item['text'];
        }
        foreach ($resulted as $entityId => $data) {

            $text = $this->getText($host, $data);
            if (!$text) {
                unset($resulted[$entityId]);
                continue;
            }
            $resulted[$entityId] = $text;
        }
        $offset += $limit;
        return $resulted;
    }

    /**
     * @param Host $host
     * @param array $data
     * @return string
     */
    private function getText(Host $host, array $data): string
    {
        $text = '';
        foreach ($host->getTextGenerateSettings() as $type) {
            if (isset($data[$type])) {
                $text = $text . $data[$type];
            }
        }
        return $text;
    }

    private function getEntityText(Entity $entity, Host $host)
    {
        return $this->manager->createQueryBuilder()
            ->select('et')
            ->from(EntityText::class, 'et')
            ->where('et.entity = :entity')
            ->andWhere('et.host = :host')
            ->setParameter('entity', $entity)
            ->setParameter('host', $host)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    private function createEntityText(Entity $entity, Host $host, string $translated)
    {
        $entityText = new EntityText();
        $entityText->setEntity($entity);
        $entityText->setHost($host);
        $entityText->setText($translated);
        $entityText->setType(EntityText::TYPE_AUTO);
        $entityText->setAvailable(true);
        $this->manager->persist($entityText);
    }
}