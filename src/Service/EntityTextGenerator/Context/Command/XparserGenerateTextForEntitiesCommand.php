<?php

namespace App\Service\EntityTextGenerator\Context\Command;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\Base\Infrastructure\Repository\Processor\EntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\Handle\HandleInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\HandleFactoryInterface;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Context\Command\InvalidKeyInfoException;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\NotFoundEntitiesException;
use App\Service\EntityTextGenerator\Infrastructure\Logger\EntityTextGeneratorLog;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO\KeyInfoVO;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class XparserGenerateTextForEntitiesCommand extends Command
{
    /** @var EntityManagerInterface */
    private $_em;
    /** @var EntityRepository */
    private $entityRepository;
    /** @var HandleFactoryInterface */
    private $handleFactory;

    const PATTERNS = [
        Type::DEEZER_AUDIO_TRACK => '/(?<name>.+)\s(слушать онлайн)$/',
        Type::DEEZER_AUDIO_ARTIST => '/(?<name>.+)\s(слушать)$/',
        Type::MOVIE_FILM => '/(?<name>.+)\s(смотреть фильм онлайн)$/',
        Type::VIDEO_EPISODE => '/(?<name>.+)\s(смотреть онлайн)$/',
        Type::VIDEO_SERIES => '/(?<name>.+)\s(смотреть)$/',
    ];

    public function __construct(
        EntityManagerInterface $manager,
        HandleFactoryInterface $handleFactory
    )
    {
        $this->entityRepository = $manager->getRepository(Entity::class);
        $this->_em = $manager;
        $this->handleFactory = $handleFactory;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName('generate:text:entities:xparser')
            ->setDescription('Command for generating text from xparser csv')
            ->addArgument('dirPath', InputArgument::REQUIRED, 'Path to csv for import')
            ->setAliases(['g:t:e:x']);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();
        $directoryPath = $input->getArgument("dirPath");
        if ($directoryPath && is_dir($directoryPath)) {
            $files = array_diff(scandir($directoryPath), ['..', '.']);

            foreach ($files as $file) {
                try {
                    $fileObject = new \SplFileObject($directoryPath . "/" . $file);
                    $keyInfo = $this->getKeyInfo($fileObject);
                    $output->writeln("<info>KEY</info> [{$keyInfo->getKey()}], <info>TYPE</info> [{$keyInfo->getEntityType()}]");
                    $handle = $this->handleFactory->createHandlerService($keyInfo->getEntityType());
                    $text = $this->fetchTextFromFile($fileObject);

                    $handle->process($keyInfo, $text);
                    $output->writeln("<info>Saved</info>");
                } catch (InvalidKeyInfoException $invalidKeyInfoException) {
                    $output->writeln("<error>{$invalidKeyInfoException->getMessage()}</error>");
                    chronicle(EntityTextGeneratorLog::create(
                        GeneratorType::XPARSER_TYPE,
                        EntityTextGeneratorLog::ACTION_KEY,
                        ['file' => $fileObject->getFilename()],
                        $invalidKeyInfoException->getMessage()
                    ));
                } catch (NotFoundEntitiesException $notFoundEntitiesException) {
                    $output->writeln("<error>{$notFoundEntitiesException->getMessage()}</error>");
                    chronicle(EntityTextGeneratorLog::create(
                        GeneratorType::XPARSER_TYPE,
                        EntityTextGeneratorLog::ACTION_NOT_FOUND,
                        ['file' => $fileObject->getFilename()],
                        $notFoundEntitiesException->getMessage()
                    ));
                    //try again
                    $this->retryWithRegexp($output, $fileObject);
                } finally {
                    unlink($directoryPath . "/" . $file);
                    gc_collect_cycles();
                }
            }
        }
    }

    private function retryWithRegexp(
        OutputInterface $output,
        \SplFileObject $fileObject
    )
    {
        $output->writeln("[v]<comment>Retry search entity by regexp</comment>");
        try {
            $keyInfo = $this->getKeyInfo($fileObject);
            $handle = $this->handleFactory->createHandlerService($keyInfo->getEntityType());
            $handle->process($keyInfo, $this->fetchTextFromFile($fileObject), true);
            $output->writeln("<info>Saved</info>");
        } catch (\Exception $exception) {
            $output->writeln("<error>{$exception->getMessage()}</error>");
            chronicle(EntityTextGeneratorLog::create(
                GeneratorType::XPARSER_TYPE,
                EntityTextGeneratorLog::ACTION_NOT_FOUND,
                ['file' => $fileObject->getFilename()],
                $exception->getMessage()
            ));
        } finally {
            unseT($keyInfo, $handle);
        }
    }

    /**
     * @param \SplFileObject $fileObject
     * @return string
     */
    private function fetchTextFromFile(\SplFileObject $fileObject): string
    {
        $fileName = $fileObject->getBasename('.' . $fileObject->getExtension());
        $text = '';
        while (!$fileObject->eof()) {
            $line = trim($fileObject->fgets());
            if (empty($line)) {
                continue;
            }
            $text .= $this->getText($line, $fileName);
        }

        return $text;
    }

    /**
     * @param \SplFileObject $fileObject
     * @return KeyInfoVO
     * @throws InvalidKeyInfoException
     */
    private function getKeyInfo(\SplFileObject $fileObject): KeyInfoVO
    {
        $fileName = $fileObject->getBasename('.' . $fileObject->getExtension());
        foreach (self::PATTERNS as $type => $pattern) {
            if (preg_match($pattern, $fileName, $match)) {
                $keyInfo = new KeyInfoVO();
                $keyInfo->setEntityType($type);
                $keyInfo->setKey($match['name']);
                return $keyInfo;
            }
        }
        throw new InvalidKeyInfoException;
    }

    /**
     * @param string $text
     * @param string $fileName
     * @return string
     */
    private function getText(string $text, string $fileName): string
    {
        //there are double spaces
        $text = str_replace("  ", " ", $text);
        $replaceString = $fileName . ";";
        return str_replace($replaceString, "", $text);
    }
}