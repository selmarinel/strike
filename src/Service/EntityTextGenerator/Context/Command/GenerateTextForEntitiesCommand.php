<?php

namespace App\Service\EntityTextGenerator\Context\Command;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Phalcon\Events\ManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class GenerateTextForEntitiesCommand extends Command
{
    /** @var ManagerInterface */
    private $manager;

    const MAX_PROCESSES = 10;

    private const AVAILABLE_TYPES = [
        Type::DEEZER_AUDIO_ARTIST,
        Type::DEEZER_AUDIO_TRACK,
        Type::AUDIO_TRACK,
        Type::AUDIO_ARTIST,
        Type::VIDEO_SERIES,
        Type::VIDEO_EPISODE,
        Type::MOVIE_FILM
    ];


    /** @var  OutputInterface */
    private $out;
    /** @var KernelInterface */
    private $kernel;
    /** @var int */
    private $typeToParse;

    public function __construct(
        EntityManagerInterface $manager,
        KernelInterface $kernel
    )
    {
        $this->kernel = $kernel;
        $this->manager = $manager;
        $this->manager->getConfiguration()->setSQLLogger(null);

        parent::__construct(null);
    }

    protected function configure()
    {
        $this->setName("generate:text:entities")
            ->setAliases(["g:t:e"])
            ->addOption('type', 't', InputOption::VALUE_REQUIRED, 'Parser type 1 - Kinopoisk, 2 - Youtube')
            ->addOption('entity_type', 'et', InputOption::VALUE_OPTIONAL, 'Entity Type to process')
            ->addOption('overwrite', 'w', InputOption::VALUE_OPTIONAL, 'Overwrite text', false)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit', 1000)
            ->setDescription("Generate text from different");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();
        $this->out = $output;
        $typeToParse = (int)$input->getOption('type');
        if (!in_array($typeToParse, [
            GeneratorType::KINOPOISK_TYPE,
            GeneratorType::YOUTUBE_CAPTION_TYPE
        ])
        ) {
            $output->writeln("<error>Type $typeToParse not supported, choose 1 - Kinopoisk, 2 - Youtube</error>");
            return;
        }

        $limit = (int)$input->getOption('limit');
        $overwrite = (bool)$input->getOption('overwrite');
        $countOfProcesses = 0;

        $types = $this->getTypes($typeToParse, (int)$input->getOption('entity_type'));
        if (empty($types)) {
            $output->writeln('<error>Empty types</error>');
        }
        $processes = [];
        $entities = $this->getEntities($limit, $types, $overwrite);

        $progressBar = new ProgressBar($this->out, count($entities));
        $progressBar->start();

        foreach ($entities as $entity) {
            $progressBar->advance();
            $this->startProcess($processes, $countOfProcesses, $typeToParse, $entity);
            if ($countOfProcesses <= self::MAX_PROCESSES) {
                continue;
            } else {
                /**
                 * @var int $index
                 * @var Process $process
                 */
                foreach ($processes as $index => $process) {
                    $process->wait();
                    $countOfProcesses = $countOfProcesses - 1;
                }
                $processes = [];
                $countOfProcesses = 0;
                continue;
            }
        }
        $progressBar->finish();
        $this->out->writeln('');
    }

    private function getTypes(int $typeToParse, int $entityType = null)
    {
        if ($entityType) {
            if (!(in_array($entityType, self::AVAILABLE_TYPES))) {
                return [];
            }
            return [$entityType];
        }

        if ($typeToParse === GeneratorType::KINOPOISK_TYPE) {
            return [
                Type::MOVIE_FILM,
                Type::VIDEO_SERIES
            ];
        }
        return self::AVAILABLE_TYPES;
    }

    private function getEntities(int $limit, array $types, bool $overwrite = false)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Entity::class, "en");
        $rsm->addFieldResult('en', 'id', 'id');
        $rsm->addFieldResult('en', 'name', 'name');

        $sql = "SELECT e.id, e.name " .
            " FROM entity e " .
            " WHERE e.entity_type in (:types)";

        if (!$overwrite) {
            $sql = $sql . " and (SELECT COUNT(ett.id) FROM entity_typed_text ett WHERE ett.entity_id = e.id) = 0";
        }
        $sql = $sql . ' LIMIT :limit';

        $entities = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter("types", $types)
            ->setParameter('limit', $limit)
            ->getResult();

        return $entities;
    }

    private function startProcess(array &$processes, int &$countOfProcesses, int $typeToParse, Entity $entity)
    {
        $dir = $this->kernel->getRootDir() . '/..';

        $process = new Process("{$dir}/bin/console p:o:e {$entity->getId()} {$typeToParse}");
        $process->enableOutput();
        $process->setTimeout(120);
        $process->start();

        $processes[$process->getPid()] = $process;
        $countOfProcesses = $countOfProcesses + 1;
    }
}