<?php

namespace App\Service\EntityTextGenerator\Context\Controller;


use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\EntityTextGenerator\Domain\Report\ReportInterface;
use App\Service\EntityTextGenerator\Infrastructure\Report\VO\FilterVO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportController extends Controller implements AdministrateControllerInterface
{
    public function __invoke(Request $request, ReportInterface $report)
    {
        return new Response(file_get_contents($report->generate(FilterVO::createFromRequest($request))), Response::HTTP_OK, [
            'Content-type' => 'application/octet-stream'
        ]);
    }
}