<?php

namespace App\Service\EntityTextGenerator\Domain\Service\EntityExtension;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;

interface EntityExtensionInterface
{
    public function extend(DTOInterface $DTO, GetHostInterface $host);
}