<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser;

interface ParserInterface
{
    public function process(string $query): string;
}