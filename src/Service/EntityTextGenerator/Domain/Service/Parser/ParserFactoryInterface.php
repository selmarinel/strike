<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser;


interface ParserFactoryInterface
{
    public function createParserService(int $type): ParserInterface;
}