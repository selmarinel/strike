<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption;


interface VideoProcessorInterface
{
    public function processVideo(string $videoId, string $query, string &$content);
}