<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption;


interface KeyGeneratorInterface
{
    public function getApiKey();
}