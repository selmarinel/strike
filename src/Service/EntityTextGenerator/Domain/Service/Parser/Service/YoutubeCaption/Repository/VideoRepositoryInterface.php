<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\Repository;


use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\ProxyFetcherInterface;

interface VideoRepositoryInterface
{
    public function getVideoIds(string &$query, ProxyFetcherInterface $fetcher);
}