<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption;


use Psr\Http\Message\ResponseInterface;

interface ProxyFetcherInterface
{
    public function fetch(string $url, \Generator $proxies): ResponseInterface;

    public function getProxy();
}