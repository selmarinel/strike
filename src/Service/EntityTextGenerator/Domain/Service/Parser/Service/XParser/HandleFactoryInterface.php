<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser;

use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\Handle\HandleInterface;

interface HandleFactoryInterface
{
    public function createHandlerService(int $type): HandleInterface;
}