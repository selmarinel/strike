<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\Handle;

use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\NotFoundEntitiesException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO\KeyInfoVO;

interface HandleInterface
{
    /**
     * @param KeyInfoVO $keyInfo
     * @param string $text
     * @param bool $byRegexp
     * @return
     */
    public function process(KeyInfoVO $keyInfo, string $text, bool $byRegexp = false);
}