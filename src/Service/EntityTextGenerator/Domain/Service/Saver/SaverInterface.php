<?php

namespace App\Service\EntityTextGenerator\Domain\Service\Saver;

use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityTextGenerator\Domain\VO\SaverVOInterface;

interface SaverInterface
{
    public function createNewEntityTypedText(
        Entity $entity,
        int $typeId,
        string $text,
        int $status = EntityTypedText::PROCESSED): EntityTypedText;

    public function commit();
}