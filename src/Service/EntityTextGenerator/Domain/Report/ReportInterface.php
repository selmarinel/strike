<?php

namespace App\Service\EntityTextGenerator\Domain\Report;


use App\Service\EntityTextGenerator\Infrastructure\Report\VO\FilterVO;

interface ReportInterface
{
    public function generate(FilterVO $filter);
}