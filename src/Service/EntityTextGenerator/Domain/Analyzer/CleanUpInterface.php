<?php

namespace App\Service\EntityTextGenerator\Domain\Analyzer;


interface CleanUpInterface
{
    public function cleanText(string &$text);
}