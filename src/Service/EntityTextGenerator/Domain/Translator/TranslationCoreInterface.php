<?php

namespace App\Service\EntityTextGenerator\Domain\Translator;


interface TranslationCoreInterface
{
    public function translate(string $text, string $lang): string;
}