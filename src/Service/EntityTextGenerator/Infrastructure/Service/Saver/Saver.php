<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Saver;

use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\Base\Infrastructure\Repository\TypedText\EntityTypedTextRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use Doctrine\ORM\EntityManagerInterface;

class Saver implements SaverInterface
{
    /** @var EntityManagerInterface $_em */
    private $_em;
    /** @var  EntityTypedTextRepository */
    private $entityTypedTextRepository;

    public function __construct(EntityManagerInterface $manager, EntityTypedTextRepository $repository)
    {
        $this->entityTypedTextRepository = $repository;
        $this->_em = $manager;
    }

    /**
     * @param Entity $entity
     * @param int $typeId
     * @param string $text
     * @param int $status
     * @return EntityTypedText
     */
    public function createNewEntityTypedText(
        Entity $entity,
        int $typeId,
        string $text,
        int $status = EntityTypedText::PROCESSED): EntityTypedText
    {
        $entityTypedText = $this->entityTypedTextRepository->findOneBy([
            'entity' => $entity,
            'typeId' => $typeId
        ]);

        if (!$entityTypedText) {
            $entityTypedText = new EntityTypedText();
            $entityTypedText->setEntity($entity);
            $entityTypedText->setTypeId($typeId);
        }
        $entityTypedText->setText($text);
        $entityTypedText->setStatus($status);
        $this->_em->persist($entityTypedText);
        return $entityTypedText;
    }

    public function commit()
    {
        $this->_em->flush();
        $this->_em->clear(EntityTypedText::class);
    }
}