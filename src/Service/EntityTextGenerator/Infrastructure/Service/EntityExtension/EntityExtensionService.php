<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\EntityExtension;


use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\Base\Infrastructure\Repository\TypedText\EntityTypedTextRepository;
use App\Service\EntityTextGenerator\Domain\Service\EntityExtension\EntityExtensionInterface;
use App\Service\EntityTextGenerator\Infrastructure\VO\TextGenerateTypeVO;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use Doctrine\ORM\EntityManagerInterface;

class EntityExtensionService implements EntityExtensionInterface
{
    /** @var EntityManagerInterface */
    private $manager;
    /** @var EntityTypedTextRepository */
    private $entityTypedTextRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->manager = $entityManager;
        $this->entityTypedTextRepository = $this->manager->getRepository(EntityTypedText::class);
    }

    public function extend(DTOInterface $DTO, GetHostInterface $host)
    {
        $configuration = $host->getTextGenerateConfiguration();

        if (!$configuration) {
            return;
        }
        $result = [];
        foreach ($configuration as $item) {
            /** @var EntityTypedText $entityTypedText */
            $entityTypedText = $this->entityTypedTextRepository->findByEntityIdAndType(
                (int)$host->getEntityId(), $item->getType());
            if ($entityTypedText) {
                $result[] = $entityTypedText->getText();
            }
        }
        $DTO->setText(nl2br(implode("\n", $result)));
    }
}