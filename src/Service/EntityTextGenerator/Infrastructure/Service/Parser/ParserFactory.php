<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser;

use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityTextGenerator\Domain\Service\Parser\ParserFactoryInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\ParserInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Factory\FactoryNotSupportedTypeException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Handle\HandleKinopoisk;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Handle\HandleXParser;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Handle\HandleYoutubeCaption;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ParserFactory implements ParserFactoryInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createParserService(int $type): ParserInterface
    {
        switch ($type) {
            case GeneratorType::KINOPOISK_TYPE:
                return $this->container->get(HandleKinopoisk::class);
            case GeneratorType::YOUTUBE_CAPTION_TYPE:
                return $this->container->get(HandleYoutubeCaption::class);
            case GeneratorType::XPARSER_TYPE:
                return $this->container->get(HandleXParser::class);
            default:
                throw new FactoryNotSupportedTypeException;
        }
    }
}