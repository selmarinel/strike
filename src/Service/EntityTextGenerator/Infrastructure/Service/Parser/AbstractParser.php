<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser;

use GuzzleHttp\Client;

abstract class AbstractParser
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }
}