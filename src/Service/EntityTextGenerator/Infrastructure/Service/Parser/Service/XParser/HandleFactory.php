<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\HandleFactoryInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\Handle\HandleInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Factory\FactoryNotSupportedTypeException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\HandleArtist;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\HandleEpisode;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\HandleMovie;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\HandleSeries;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\HandleTrack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HandleFactory implements HandleFactoryInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createHandlerService(int $type): HandleInterface
    {
        switch ($type) {
            case Type::DEEZER_AUDIO_TRACK:
                return $this->container->get(HandleTrack::class);
            case Type::DEEZER_AUDIO_ARTIST:
                return $this->container->get(HandleArtist::class);
            case Type::MOVIE_FILM:
                return $this->container->get(HandleMovie::class);
            case Type::VIDEO_SERIES:
                return $this->container->get(HandleSeries::class);
            case Type::VIDEO_EPISODE:
                return $this->container->get(HandleEpisode::class);
            default:
                throw new FactoryNotSupportedTypeException;
        }
    }
}