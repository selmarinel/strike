<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Processor\EntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\XParser\Handle\HandleInterface;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\NotFoundEntitiesException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO\KeyInfoVO;
use Doctrine\ORM\EntityManagerInterface;

class HandleTrack extends AbstractHandle implements HandleInterface
{
    /** @var EntityRepository */
    private $entityRepository;
    /** @var EntityNestedTreeRepository */
    private $entityNestedTreeRepository;

    public function __construct(EntityManagerInterface $manager, SaverInterface $saver)
    {
        parent::__construct($manager, $saver);
        $this->entityRepository = $manager->getRepository(Entity::class);
        $this->entityNestedTreeRepository = $manager->getRepository(EntityNestedTree::class);
    }

    const RELATION_MAP = [
        Type::AUDIO_TRACK => Type::AUDIO_ARTIST,
        Type::DEEZER_AUDIO_TRACK => Type::DEEZER_AUDIO_ARTIST,
    ];

    /**
     * @param KeyInfoVO $keyInfo
     * @param string $text
     * @param bool $byRegexp
     * @throws NotFoundEntitiesException
     */
    public function process(KeyInfoVO $keyInfo, string $text, bool $byRegexp = false): void
    {
        preg_match("/^(?<artist>.+)\s\+\s(?<track>.+)$/", $keyInfo->getKey(), $match);

        if (!isset($match['track']) || !isset($match['artist'])) {
            throw new NotFoundEntitiesException;
        }
        $trackKeyInfo = clone $keyInfo;
        $trackKeyInfo->setKey($match['track']);
        $trackEntities = $this->getEntities($trackKeyInfo, $byRegexp);
        if (count($trackEntities) === 1) {
            $trackEntity = array_shift($trackEntities);
            $this->getSaver()->createNewEntityTypedText($trackEntity, self::TYPE, $text);
            $this->getSaver()->commit();
            return;
        }

        $artistKeyInfo = clone $keyInfo;
        $artistKeyInfo->setKey($match['artist']);
        $artistKeyInfo->setEntityType(self::RELATION_MAP[$keyInfo->getEntityType()]);

        //need to get tracks
        $trackEntities = $this->findArtistsTracksByArtistNameAndTrackName($artistKeyInfo, $trackKeyInfo);

        if (empty($trackEntities)) {
            throw new NotFoundEntitiesException;
        }
        $this->saveEntities($trackEntities, self::TYPE, $text);
    }

    private function findArtistsTracksByArtistNameAndTrackName(KeyInfoVO $artistKeyInfo, KeyInfoVO $trackKeyInfo)
    {
        $artist = $this->entityRepository->findOneBy([
            'name' => $artistKeyInfo->getKey(),
            'entity_type' => $artistKeyInfo->getEntityType()
        ]);
        if (!$artist) {
            throw new NotFoundEntitiesException;
        }
        /** @var EntityNestedTree $node */
        $node = $this->entityNestedTreeRepository->findByEntity($artist);
        if (!$node) {
            throw new NotFoundEntitiesException;
        }
        $leafNodes = $this->entityNestedTreeRepository->getLeafs($node);

        $result = [];
        /** @var EntityNestedTree $leafNode */
        foreach ($leafNodes as $leafNode) {
            $entity = $leafNode->getEntity();
            if ($entity->getEntityType() === $trackKeyInfo->getEntityType() &&
                $entity->getName() === $trackKeyInfo->getKey()
            ) {
                $result[] = $entity;
            }
        }
        return $result;
    }
}