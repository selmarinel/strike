<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\Helper;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use App\Service\EntityTextGenerator\Infrastructure\Logger\EntityTextGeneratorLog;

/**
 * Trait SaveEntitiesTrait
 * @property SaverInterface $saver
 */
trait SaveEntitiesTrait
{
    /**
     * @param Entity[] $entities
     * @param int $type
     * @param string $text
     */
    public function saveEntities(array $entities, int $type, string $text)
    {
        foreach ($entities as $entity) {
            chronicle(EntityTextGeneratorLog::create(
                $type,
                EntityTextGeneratorLog::ACTION_CREATING,
                $entity));
            $this->saver->createNewEntityTypedText($entity, $type, $text);
            chronicle(EntityTextGeneratorLog::create(
                $type,
                EntityTextGeneratorLog::ACTION_SUCCESS,
                $entity));
        }
        $this->saver->commit();
    }
}