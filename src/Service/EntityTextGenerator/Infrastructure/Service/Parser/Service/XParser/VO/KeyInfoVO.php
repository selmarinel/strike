<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO;


class KeyInfoVO
{
    /** @var string */
    private $key = '';
    /** @var int */
    private $entityType = 0;

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return int
     */
    public function getEntityType(): int
    {
        return $this->entityType;
    }

    /**
     * @param int $entityType
     */
    public function setEntityType(int $entityType)
    {
        $this->entityType = $entityType;
    }


}