<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\Helper;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO\KeyInfoVO;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Trait GetEntitiesTrait
 * @package App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\Helper
 * @property EntityManagerInterface $_em
 */
trait GetEntitiesTrait
{
    public function getEntities(KeyInfoVO $keyInfoVO, bool $byRegexp = false)
    {
        $sql = 'SELECT e.* FROM entity e WHERE e.entity_type = :entity_type';
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Entity::class, 'e');
        $rsm->addFieldResult('e', 'id', 'id');
        $rsm->addFieldResult('e', 'name', 'name');
        $rsm->addFieldResult('e', 'source_id', 'source_id');
        $rsm->addFieldResult('e', 'entity_type', 'entity_type');


        if ($byRegexp) {
            $key = preg_replace('/\s+/', '([ ]+)', addslashes($keyInfoVO->getKey()));
            $keyInfoVO->setKey("^{$key}$");
            unset($key);
            $sql .= ' AND e.name REGEXP :name';
        } else {
            $sql .= ' AND e.name = :name';
        }
        return $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('entity_type', $keyInfoVO->getEntityType())
            ->setParameter('name', $keyInfoVO->getKey())
            ->getResult();
    }
}