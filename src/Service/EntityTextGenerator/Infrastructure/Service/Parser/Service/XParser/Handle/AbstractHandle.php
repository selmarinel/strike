<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle;


use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityTextGenerator\Domain\Service\Saver\SaverInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\NotFoundEntitiesException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\Helper\GetEntitiesTrait;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\Handle\Helper\SaveEntitiesTrait;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\XParser\VO\KeyInfoVO;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractHandle
{
    use SaveEntitiesTrait, GetEntitiesTrait;

    /** @var EntityManagerInterface */
    private $_em;
    /** @var SaverInterface */
    private $saver;

    const TYPE = GeneratorType::XPARSER_TYPE;

    public function __construct(
        EntityManagerInterface $entityManager,
        SaverInterface $saver
    )
    {
        $this->_em = $entityManager;
        $this->saver = $saver;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getManager(): EntityManagerInterface
    {
        return $this->_em;
    }

    /**
     * @return SaverInterface
     */
    public function getSaver(): SaverInterface
    {
        return $this->saver;
    }

    /**
     * @param KeyInfoVO $keyInfo
     * @param string $text
     * @param bool $byRegexp
     * @throws NotFoundEntitiesException
     */
    public function process(KeyInfoVO $keyInfo, string $text, bool $byRegexp = false)
    {
        $entities = $this->getEntities($keyInfo, $byRegexp);

        if (!$entities) {
            throw new NotFoundEntitiesException;
        }
        $this->saveEntities($entities, self::TYPE, $text);
    }
}