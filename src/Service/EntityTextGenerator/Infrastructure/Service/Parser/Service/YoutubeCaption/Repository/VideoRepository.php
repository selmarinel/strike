<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\YoutubeCaption\Repository;

use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\KeyGeneratorInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\ProxyFetcherInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\Repository\VideoRepositoryInterface;
use GuzzleHttp\Exception\RequestException;

class VideoRepository implements VideoRepositoryInterface
{
    const MAX_RESULTS = 5;
    /** @var \Generator */
    private $apiKeys;

    public function __construct(KeyGeneratorInterface $keyGenerator)
    {
        $this->apiKeys = $keyGenerator->getApiKey();
    }

    public function getVideoIds(string &$query, ProxyFetcherInterface $fetcher)
    {
        $ISO = 'RU';

        $query = urlencode(html_entity_decode($query));

        $maxResults = self::MAX_RESULTS;
        $apiKey = $this->apiKeys->current();

        if (!$apiKey) {
            throw new \Exception('All keys are forbidden');
        }
        $hasException = false;
        $url = "https://www.googleapis.com/youtube/v3/search?q={$query}&maxResults={$maxResults}&" .
            "part=snippet&videoCaption=closedCaption&type=video&key={$apiKey}&regionCode={$ISO}";
        $data = null;
        try {
            $response = $fetcher->fetch($url, $fetcher->getProxy());
            $data = json_decode($response->getBody()->getContents(), true);
            if ($data['kind'] != 'youtube#searchListResponse') {
                $hasException = true;
            }
        } catch (RequestException $exception) {
            $hasException = true;
        }

        if ($hasException) {
            $this->apiKeys->next();
            return $this->getVideoIds($query, $fetcher);
        }
        $urls = [];
        if (!empty($data)) {
            foreach ($data['items'] as $val)
                $urls[] = $val['id']['videoId'];
        }
        return $urls;
    }
}