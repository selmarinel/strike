<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\YoutubeCaption\KeyGenerator;


use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\KeyGeneratorInterface;

class ApiKey implements KeyGeneratorInterface
{
    private const YOUTUBE_API_KEYS = [
        'AIzaSyAhy66nQnxmrHDmrqGcrjFxEUZKiqqhrpA',
        'AIzaSyCFGjYfGnA5BtFEPb71b3jhlFwvoNzv4bQ',
        'AIzaSyAf9dLtfydNAFhSn7fm4cBXylXnqdxmcbw',
        'AIzaSyBQdpDOOtl4gbeKHZhWRapw500xFVdZfJk',
        'AIzaSyBtN_y7ofBP5n1vpMLNcnS09jc9rZ0kFWk',
        'AIzaSyD8xMT88hwm0LHDI0Q2FL7rtSpSMLkaOxI',
        'AIzaSyCxUHYu20garUs1new1GGWWHJh9ucl3X-g',
        'AIzaSyDB00V3Te5_ebd1VMp6o4JmK55wj7cNE2I',
        'AIzaSyDtk8hdlsS8COxeqmiSrOe1FpAYwndjqz4',
        'AIzaSyDCJV1gbISOMuHLYyg5-pRZPOmzRM2C1Sg',
        'AIzaSyBDPn_VeXAHS_Gmn6WcRzuERQ2Qy1qd0uU',
        'AIzaSyB4Y6lbMuaA1YKNq8PTurHHTTh9ENIJzAE',
        'AIzaSyAw84lZ-KpiaHhZwiqed54XmtDaxn9nQg0',
        'AIzaSyCciVIGZbyEcilSxZag_vBJoK7fu7fVvIQ',
        'AIzaSyAQH67GTKk__KEq659d1xzNt10MXp7rSHg',
        'AIzaSyA8uP_phCjqsi14F3zg-sM7ju-fijXyD44',
        'AIzaSyDpdfZ6tqQMPYA-JZRlQq5bai2p_dPet7w',
        'AIzaSyB0z2gJ5KxsNR30eItyOgSDv5K3dra3b00',
        'AIzaSyBSOfhcDv2fh5sPDAVOMnybXnnP8y4zlTc',
        'AIzaSyBXqkTSq8rMaKlzpPNPNZnqmbFUpv-xdPw',
    ];

    /**
     * @return \Generator
     */
    public function getApiKey()
    {
        foreach (self::YOUTUBE_API_KEYS as $key) {
            yield $key;
        }
    }
}