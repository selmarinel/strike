<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\YoutubeCaption\Processor;


use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\ProxyFetcherInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\VideoProcessorInterface;
use GuzzleHttp\Exception\RequestException;

class VideoProcessor implements VideoProcessorInterface
{
    /** @var ProxyFetcherInterface */
    private $fetcher;

    public function __construct(ProxyFetcherInterface $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    private const YOUTUBE_URL = 'https://www.youtube.com/watch?v=';

    public function processVideo(string $videoId, string $query, string &$content)
    {
        if (!$this->fetcher) {
            throw new \Exception('Fetcher is not set');
        }
        try {
            $captionUrl = $this->getBaseClosedCaptionsUrl($videoId);
            if (!$captionUrl) {
                return null;
            }

            $availableTracks = $this->getAvailableTracks($captionUrl);
            if (!$availableTracks) {
                return null;
            }
        } catch (RequestException $exception) {
            return null;
        }

        $tracks = $this->processTracks($captionUrl, $videoId, $availableTracks);
        if (is_array($tracks)) {
            foreach ($tracks as $key => $value) {
                $value['query'] = $query;
                $content .= $value['content'];
            }
        }
    }

    /**
     * @param string $videoId
     * @return null|string
     */
    private function getBaseClosedCaptionsUrl(string $videoId):?string
    {
        $url = self::YOUTUBE_URL . $videoId;
        $response = $this->fetcher->fetch($url, $this->fetcher->getProxy());
        $responseText = $response->getBody()->getContents();

        $matches = [];
        if (!preg_match('/TTS_URL\': "(.+?)"/i', $responseText, $matches)) {
            return null;
        }
        if (!preg_match("/<title>(.*)<\/title>/siU", $responseText, $title_matches)) {
            return null;
        }
        $content = str_replace(['\\u0026', '\\/'], ['&', '/'], $matches[1]);
        return $content;
    }

    /**
     * @param $baseUrl
     * @return array|null
     */
    private function getAvailableTracks(string $baseUrl): array
    {
        $tracks = [];
        $url = $baseUrl . '&type=list&tlangs=1&fmts=1&vssids=1&asrs=1';
        $response = $this->fetcher->fetch($url, $this->fetcher->getProxy());
        $responseText = $response->getBody()->getContents();
        if (!$responseXml = simplexml_load_string($responseText)) {
            return null;
        }

        if (!$responseXml->track) {
            return null;
        }
        foreach ($responseXml->track as $track) {
            $tracks[] = [
                'id' => (string)$track['id'],
                'name' => (string)$track['name'],
                'lang' => (string)$track['lang_code'],
                'kind' => (string)$track['kind'],
                'lang_default' => (string)$track['lang_default']
            ];
        }
        return $tracks;
    }

    /**
     * @param string $captionUrl
     * @param string $videoId
     * @param array $tracks
     * @return array
     */
    private function processTracks(string $captionUrl, string $videoId, array $tracks): array
    {
        $result = [];
        foreach ($tracks as $key => $track) {
            try {
                $url = $this->getClosedCaptionText($captionUrl, $track);
                if (!$url) {
                    continue;
                }

                $response = $this->fetcher->fetch($url, $this->fetcher->getProxy());
            } catch (RequestException $exception) {
                continue;
            }
            $xmlData = $response->getBody()->getContents();
            $xmlNode = simplexml_load_string($xmlData);
            $asArray = (array)$xmlNode;

            $content = '';
            if (is_array($asArray['text'])) {
                foreach ($asArray['text'] as $value) {
                    $content .= $value . ' ';
                }
            } elseif (is_string($asArray['text'])) {
                $content = $asArray['text'];
            } else {
                continue;
            }

            $content = $this->cleanText($content);
            if ($track['lang_default'] == true && $track['kind'] !== 'asr') {
                $result[] = [
                    'id' => $videoId,
                    'lang' => $track['lang'],
                    'content' => $content
                ];
            }
        }
        return $result;
    }

    /**
     * @param $baseUrl
     * @param array $track
     * @return null|string
     */
    private function getClosedCaptionText($baseUrl, array $track):?string
    {
        //language
        $language = 'ru';
        $url = $baseUrl . "&type=track&lang={$language}&name=" . urlencode($track['name']) . "&kind={$track['kind']}&fmt=1";
        $response = $this->fetcher->fetch($url, $this->fetcher->getProxy());
        $responseText = $response->getBody()->getContents();
        $responseXml = simplexml_load_string($responseText);
        if (!$responseXml) {
            return null;
        }
        if (!$responseXml->text) {
            return null;
        }
        $videoText = [];
        foreach ($responseXml->text as $textNode) {
            if ($text = trim((string)$textNode)) {
                $videoText[] = addslashes(htmlspecialchars_decode(strip_tags((string)$textNode), ENT_QUOTES));
            }
        }
        return $url;
    }

    /**
     * @param string $content
     * @return string
     */
    private function cleanText(string $content): string
    {
        $content = preg_replace('/(\v|\s)+/', ' ', str_replace(';', '.', $content));
        $content = trim(stripslashes($content));
        return $content;
    }
}