<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Service\YoutubeCaption\Fetcher;


use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\ProxyFetcherInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class ClientWrapper implements ProxyFetcherInterface
{
    /** @var Client */
    private $client;

    private const PROXY_LIST = [
        '195.201.250.207:3122',
        '195.201.250.212:3122',
        '195.201.250.215:3122',
        '195.201.249.147:3122',
        '195.201.250.208:3122',
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string $url
     * @param \Generator $proxies
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function fetch(string $url, \Generator $proxies): ResponseInterface
    {
        $proxy = $proxies->current();
        try {
            return $this->client->get($url, [
                'proxy' => $proxy
            ]);
        } catch (RequestException $exception) {
            if ($exception->getCode() === Response::HTTP_NOT_FOUND) {
                throw $exception;
            }
            $proxies->next();
            return $this->fetch($url, $proxies);
        }
    }

    /**
     * @return \Generator
     */
    public function getProxy()
    {
        foreach (self::PROXY_LIST as $proxy) {
            yield $proxy;
        }
    }

}