<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Handle;

use App\Service\EntityTextGenerator\Domain\Analyzer\CleanUpInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\ParserInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\ProxyFetcherInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\Repository\VideoRepositoryInterface;
use App\Service\EntityTextGenerator\Domain\Service\Parser\Service\YoutubeCaption\VideoProcessorInterface;

class HandleYoutubeCaption implements ParserInterface
{
    /** @var VideoRepositoryInterface */
    private $videoRepository;
    /** @var ProxyFetcherInterface */
    private $fetcher;
    /** @var VideoProcessorInterface */
    private $processor;

    const MAX_CHARACTERS = 3000;
    /** @var CleanUpInterface */
    private $cleanUp;

    public function __construct(
        VideoRepositoryInterface $videoRepository,
        VideoProcessorInterface $processor,
        ProxyFetcherInterface $fetcher,
        CleanUpInterface $cleanUp
    )
    {
        $this->videoRepository = $videoRepository;
        $this->fetcher = $fetcher;
        $this->processor = $processor;
        $this->cleanUp = $cleanUp;
    }


    public function process(string $query): string
    {
        $content = '';
        $videoIds = $this->videoRepository->getVideoIds($query, $this->fetcher);
        foreach ($videoIds as $videoId) {
            $this->processor->processVideo($videoId, $query, $content);

            if ($this->checkContentLength($content)) {
                break;
            }
        }
        $this->cleanUp->cleanText($content);
        return $content;
    }

    private function checkContentLength(string $content)
    {
        return mb_strlen($content) > self::MAX_CHARACTERS;
    }
}