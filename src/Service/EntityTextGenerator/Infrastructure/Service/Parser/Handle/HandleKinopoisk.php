<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Service\Parser\Handle;

use App\Service\EntityTextGenerator\Domain\Service\Parser\ParserInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Handle\KinopoiskStatusErrorException;
use App\Service\EntityTextGenerator\Infrastructure\Service\Parser\AbstractParser;
use GuzzleHttp\Client;

class HandleKinopoisk implements ParserInterface
{
    const API_URL = 'https://vid.dzenkino.com.ua/ajax_actions?action=kinopoisk&event=search&query=';
    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string $query
     * @return string
     * @throws KinopoiskStatusErrorException
     */
    public function process(string $query): string
    {
        $resultString = "";

        $response = $this->client->request(
            "GET",
            self::API_URL . $query,
            [
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
                ],
            ]
        );

        $contentJson = $response->getBody()->getContents();

        $content = json_decode($contentJson, true);

        if ($content['status'] == "error") {
            throw new KinopoiskStatusErrorException();
        }

        foreach ($content['data'] as $element) {
            if (is_string($element)) {
                $resultString .= $element;
            } elseif (is_array($element)) {
                $resultString .= ". " . implode(". ", $element);
            }
        }

        return $resultString;
    }
}