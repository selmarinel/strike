<?php

namespace App\Service\EntityTextGenerator\Infrastructure\VO;


class TextGenerateTypeVO
{
    /** @var int */
    private $type = 0;

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }

}