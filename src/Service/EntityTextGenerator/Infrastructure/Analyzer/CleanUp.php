<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Analyzer;


use App\Service\EntityTextGenerator\Domain\Analyzer\CleanUpInterface;

class CleanUp implements CleanUpInterface
{
    public function cleanText(string &$text)
    {
        $this->cleanLinks($text);
        $this->cleanSymbols($text);
        $this->cleanSentences($text);
    }

    public function cleanSymbols(string &$text)
    {
        $text = preg_replace('/[^a-zA-Zа-яА-Я0-9\s\,\.\!\?\;\"\']/ui', '', $text);
    }

    public function cleanSentences(string &$text)
    {
        $sentences = preg_split('/(?<=[.?!])\s+(?=[a-z])/ui', $text);
        $this->processSentences($sentences);
        $content = '';
        foreach ($sentences as $sentence) {
            $content .= $sentence . ' ';
        }
        $text = $content;
    }

    public function processSentences(array &$sentences)
    {
        foreach ($sentences as $index => $sentence) {
            if (mb_strlen($sentence) < 10) {
                unset($sentences[$index]);
                continue;
            }
            $worlds = preg_split('/(\s)+/ui', $sentence);
            if (count($worlds) <= 2) {
                unset($sentences[$index]);
                continue;
            }
        }
    }

    public function cleanLinks(string &$text)
    {
        $text = preg_replace("@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@", '', $text);
    }
}