<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Logger;


use Logger\Message\StrikeLogMessage;

class TranslatorLog extends StrikeLogMessage
{
    protected $service = 'entity_text_generator_translator';

    public static function create(string $action, $data, string $message = '')
    {
        $log = new self;
        $log->setType($action);
        $log->setData(self::processData($data));
        $log->setMessage($message);
        return $log;
    }

    const ACTION_CREATING = 'CREATING';
    const ACTION_SUCCESS = 'SUCCESS';
    const ACTION_EXCEPTION = 'EXCEPTION';
    const ACTION_FAIL_EMPTY_TRANSLATE = 'FAIL_EMPTY_TRANSLATE';

    public static function processData($data): string
    {
        if ($data instanceof \Exception) {
            $data = [
                'message' => $data->getMessage(),
                'code' => $data->getCode(),
                'class' => get_class($data),
            ];
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        } elseif (is_string($data)) {
            return $data;
        } elseif (is_array($data)) {
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        }
        return 'Unsupported Data type';
    }
}