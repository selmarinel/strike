<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Logger;

use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use Logger\Message\StrikeLogMessage;

class EntityTextGeneratorLog extends StrikeLogMessage
{
    public static function create(int $type, string $action, $data, $message = '')
    {
        $log = new self();
        if (isset(self::TYPE_MAP_PART[$type])) {
            $action = self::TYPE_MAP_PART[$type] . "_{$action}";
        }
        $log->setType($action);
        $log->setData(self::processData($data));
        $log->setMessage($message);
        return $log;
    }

    protected $service = 'entity_typed_text';

    const TYPE_MAP_PART = [
        GeneratorType::KINOPOISK_TYPE => 'KINOPOISK',
        GeneratorType::YOUTUBE_CAPTION_TYPE => 'YOUTUBE_CAPTION',
        GeneratorType::XPARSER_TYPE => 'XPARSER',
        GeneratorType::WIKI_TYPE => 'WIKI',
    ];

    const ACTION_CREATING = 'CREATING';
    const ACTION_SUCCESS = 'SUCCESS';
    const ACTION_FAIL = 'FAIL';
    const ACTION_KEY = 'KEY_FAIL';
    const ACTION_NOT_FOUND = 'ENTITY_NOT_FOUND';

    public static function processData($data): string
    {
        if ($data instanceof Entity) {
            $data = [
                'entity_id' => $data->getId(),
                'entity_name' => $data->getName(),
                'entity_type' => $data->getEntityType()
            ];
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        } elseif ($data instanceof \Exception) {
            $data = [
                'message' => $data->getMessage(),
                'code' => $data->getCode(),
                'class' => get_class($data),
            ];
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        } elseif (is_string($data)) {
            return $data;
        } elseif (is_array($data)) {
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        }
        return 'Unsupported Data type';
    }
}