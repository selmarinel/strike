<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Exception\Handle;

class KinopoiskStatusErrorException extends HandleException
{
    protected $message = 'Response has the status - error';
}