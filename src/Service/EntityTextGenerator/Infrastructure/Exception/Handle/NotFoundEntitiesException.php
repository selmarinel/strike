<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Exception\Handle;


class NotFoundEntitiesException extends HandleException
{
    protected $message = 'Entities not found';
}