<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Exception\Context\Command;


class InvalidKeyInfoException extends \Exception
{
    protected $message = 'Not supported Type';
}