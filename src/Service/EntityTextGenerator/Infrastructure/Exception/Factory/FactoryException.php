<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Exception\Factory;


class FactoryException extends \Exception
{
    protected $code = 400;
}