<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Translator\TranslateCore;


use App\Service\EntityTextGenerator\Domain\Translator\TranslationCoreInterface;
use App\Service\EntityTextGenerator\Infrastructure\Exception\Translator\NotValidApiKeysException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Response;

class TranslateCore implements TranslationCoreInterface
{
    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    const API_KEYS = [
        'trnsl.1.1.20181008T114438Z.350cc85a245f50c9.c5927d084ae4c94e59dff96ab0cd49088ff7c7a5',
        'trnsl.1.1.20181008T141812Z.99379b57b30ea0d3.bd9677e261f428b46ad67f5538bb281d140e576b',
        'trnsl.1.1.20160901T135536Z.c5490608b9dbba82.f84009eff2b843af6bd13ed50ffa4618135d53fb',
        'trnsl.1.1.20181008T141348Z.323a1a499d60c504.1515cfc46abe3e55a3f4defa69f5e87e2a0bb5ce',
        'trnsl.1.1.20180608T141251Z.12bcf59761acf81a.99ae78bc0fa783841a22787d9d0a3c4c321ccca8',
        'trnsl.1.1.20160427T090542Z.215614dc868234d9.b35eea85665d7d86ae7ca8a936c99a9e1a70a100',
        'trnsl.1.1.20160518T115608Z.101fff9294cf0106.e604c02a77fc39197fad915f3798116a42282756',
        'trnsl.1.1.20160518T092304Z.21d7370b7748c383.7f1cfe29ced63ecb79ed39ce45c53d3801b7ee3e',
        'trnsl.1.1.20160518T090413Z.c283e288fabcfeac.0f1bd9325521053c62e7bdeb95465b2c95d7c712',
        'trnsl.1.1.20160531T091955Z.8eca30ed4b8acdbf.e466dd706ddeaa753227ffab024633c9b0a59c13',
        'trnsl.1.1.20160531T100006Z.43904ef9bff0ff65.3a855acb217e8221c01f0f21335f9b130a57af16',
        'trnsl.1.1.20160608T111224Z.a348192369ff3a69.3dc2953d5a8b1a997f47938eb6df447777c62da6',
        'trnsl.1.1.20160608T111553Z.33504f3a6b244aa3.cbe93f8902a8885f1f5412a9f501775b2548a3b6',
        'trnsl.1.1.20160608T111553Z.83530f3413c1a90f.a3843a04830082225ada243414d6e7fc5e934da8',
        'trnsl.1.1.20160608T111451Z.d72aa75416f62f77.83ffd07159699985f97b56f710c84bb0f246e1ad',
        'trnsl.1.1.20160608T113430Z.e3f9fb70dd72ea5d.847f7982b42be5de6baf1a3e9ad772972276c1a7',
        'trnsl.1.1.20160608T113727Z.f0baea7c07e11542.8c92b9d8c9d9101b52d2e328d051155e7e66c58d',
        'trnsl.1.1.20160919T075546Z.80d5aea020f0155b.361cf79006e087b05eb11d0dd2e79bf099839a96',
        'trnsl.1.1.20160919T080152Z.db290426a312e10c.e4b608df24e94d7a8fe642e7a1d5b04a505ef0f8',
        'trnsl.1.1.20160919T080334Z.2fca45426148010d.545db26946a34d51af79b43e6a9bff075f3fbc35',
        'trnsl.1.1.20160919T080410Z.6275a0c43ff4c858.7348ed865819c6788828e3f968b705b175a89441',
        'trnsl.1.1.20160919T082153Z.a14944b93ca4656c.bca52ea534615ed0394f5138951b82afe6ae7bd1',
        'trnsl.1.1.20160919T082019Z.4e3879676ab0e2d5.938bfaebf22fcef799244d4639ec347308b62813',
        'trnsl.1.1.20160919T082150Z.5a785aaa7e7e6b4e.36e3c502369cd49cfeea900689e18ced0bdd7ce6',
        'trnsl.1.1.20160919T082230Z.cdd4aea35b36d10c.5fb5778ac4eca5601947b1559ed571c76e9ddc8d',
        'trnsl.1.1.20160919T082324Z.10c48e354fda11f7.ffbcd45b95081208ab437c5920670b61404ec20d',
        'trnsl.1.1.20180612T065623Z.4e760cdd00009519.872c0aa715a3e36b0941748b233407d949d900cb',
        'trnsl.1.1.20180612T065656Z.c37df54bfe70d3de.a90d00ad30b8de2c0be1ac6033424e777c59cc0c',
        'trnsl.1.1.20180612T065723Z.12f2ba2c1c0e7d42.b9bf16369ba05b48bdf784d933de0ae605d9869a',
        'trnsl.1.1.20180612T065805Z.c3c6d949361bc148.241c5d3e64a66a3cadabf71d37abe756d497ad0e',
        'trnsl.1.1.20180612T065916Z.23ba14b38efa5653.60fa90048978e7208e6ee2f04381016217c1b680',
        'trnsl.1.1.20181018T114120Z.e0094e63b64714f4.d26b97e10387a2318e14b897970469335b71a70e',
        'trnsl.1.1.20181018T115020Z.9bc6f35b071e1693.d0c4c91ea0bb09c15c68bb6b102e3eda86a39d6f',
        'trnsl.1.1.20181018T120031Z.f9d0d57742ca415e.f9f87100b5267bf11480a98290873fdf87b2a8f4',
        'trnsl.1.1.20181018T122744Z.de3e936ef9786faa.eab9f44c6cb737a3420a9df578324d877ac8022f',
        'trnsl.1.1.20181018T130522Z.19a40b854146480e.a7a10d2742a8c27d39fa5db38c04e7136d49e471',
        'trnsl.1.1.20181018T130719Z.7fc8519ebdd803af.ec6437937b5a82820a38e4bc81ee27ccda3830b6',
        'trnsl.1.1.20181018T131316Z.97deb2c87971e9a0.69bacf39d928318aafd0d0e17bf3a79b779c46d4',
        'trnsl.1.1.20181018T133128Z.2fb6d8263ce08a3f.63a4ccb43016431b854372557bb1acdfa08fa5f0',
        'trnsl.1.1.20181018T133252Z.f4479df012830f33.50bdbb8a08e6914d1f7b17ae88a2b4a102ff03b5',
        'trnsl.1.1.20181018T133416Z.366ff382adec4e06.1d2ce741353d79b8b55a291ade681ce6ed707547',
        'trnsl.1.1.20181018T133626Z.d59375c58ab32740.6dd5d69931ee485cd813365ea7fa1d2e3e6ca292',
        'trnsl.1.1.20181018T134107Z.36fe32fde7a54d3b.c74ceaeb6f2fb90850ac01454a5e86271731fa6b',
        'trnsl.1.1.20181018T134402Z.b0548283d44cd670.2a49bef25cec2f2cb92fe0c5ec3186c708adcd48',
        'trnsl.1.1.20181018T134535Z.0a58ecdc7584d38d.b72f615a9c025040d7d36c9a642e17fc44129077',
        'trnsl.1.1.20181018T134642Z.222ba0238755125e.eb7968071211d2b99750791352fcd48e6c57330b',
        'trnsl.1.1.20181018T134953Z.f10e524a054c8a03.d9457ea6e41442118211c5963738e0d46beaba78',
        'trnsl.1.1.20181018T135101Z.0b635d3dacc028b2.8f5d3cc49ac448aa7fff3704a3f4d604dd5bea08',
        'trnsl.1.1.20181018T135223Z.9268dac3f37eb2ab.9fada7c297f80f7c2e72f050a65cefc74a36aaa4',
        'trnsl.1.1.20181018T135401Z.c2a65ca2c2e79a95.f9861a00c447941b56f64d02e7a2097b6c4340be',
    ];

    const LANGUAGES = [
        'ua' => 'uk',
        'kz' => 'kk'
    ];

    const MAX_CHARACTERS = 10000;

    public function translate(string $text, string $lang): string
    {
        $generator = $this->getKey();
        $result = '';
        $parts = $this->cutText($text);
        foreach ($parts as $part) {
            $result .= $this->fetch($generator, $part, $lang);
        }
        return $result;
    }

    public function cutText(string $text): array
    {
        if (mb_strlen($text) < self::MAX_CHARACTERS) {
            return [$text];
        }

        $sentences = preg_split('/(?<=[.?!;])\s+(?=[a-zA-ZА-Яа-яёЁыЫ])/ui', $text);

        $results = [];
        $i = 0;
        $results[$i] = '';
        foreach ($sentences as $sentence) {
            if (mb_strlen($results[$i] . ' ' . $sentence) < self::MAX_CHARACTERS) {
                $results[$i] = $results[$i] . ' ' . $sentence;
            } else {
                $i++;
                $results[$i] = $sentence;
            }
        }
        return $results;
    }

    /**
     * @param \Generator $generator
     * @param string $text
     * @param string $lang
     * @return string
     * @throws NotValidApiKeysException
     */
    private function fetch(\Generator $generator, string &$text, string &$lang): string
    {
        $key = $generator->current();
        if (!$key) {
            throw new NotValidApiKeysException;
        }
        $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=' . $key;
        $data = [
            'text' => $text,
            'lang' => 'ru-' . self::LANGUAGES[strtolower($lang)],
        ];
        try {
            $response = $this->client->post($url, [
                'form_params' => $data
            ]);
            $body = json_decode($response->getBody()->getContents(), true);
            if (isset($body['text']) && !empty($body['text'])) {
                return array_shift($body['text']);
            }
            return '';
        } catch (RequestException $exception) {
            if ($exception->getCode() === Response::HTTP_FORBIDDEN) {
                $generator->next();
                return $this->fetch($generator, $text, $lang);
            }
            return '';
        }
    }

    /**
     * @return \Generator
     */
    private function getKey()
    {
        foreach (self::API_KEYS as $apiKey) {
            yield $apiKey;
        }
    }
}