<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Report\VO;


use Symfony\Component\HttpFoundation\Request;

class FilterVO
{
    public static function createFromRequest(Request $request)
    {
        $limit = $request->get('limit') ?? 1000;
        $from = $request->get('from') ?? date('Y-m-d');
        $to = $request->get('to') ?? date('Y-m-d');
        $vo = new self();
        $vo->setLimit($limit);
        $vo->setFrom("{$from} 00:00:00");
        $vo->setTo("{$to} 23:59:59");
        return $vo;
    }

    /** @var string */
    private $from = '';
    /** @var string */
    private $to = '';
    /** @var int */
    private $limit = 1000;

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }
}