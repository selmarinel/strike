<?php

namespace App\Service\EntityTextGenerator\Infrastructure\Report;


use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\Base\Infrastructure\Entity\TypedText\GeneratorType;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityTextGenerator\Domain\Report\ReportInterface;
use App\Service\EntityTextGenerator\Infrastructure\Report\VO\FilterVO;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class CSVReport implements ReportInterface
{
    const FILENAME = '/tmp/report.csv';
    /** @var EntityManagerInterface */
    private $manager;
    /** @var EntityLinkGeneratorInterface */
    private $generator;
    /** @var PrepareHostAndEntityInterface */
    private $preparation;

    public function __construct(
        EntityManagerInterface $entityManager,
        PrepareHostAndEntityInterface $preparation,
        EntityLinkGeneratorInterface $generator)
    {
        $this->manager = $entityManager;
        $this->generator = $generator;
        $this->preparation = $preparation;
    }

    private const ENTITY_QUERY = 'SELECT e.id entityId,' .
    ' ett.type_id typeId,' .
    ' ett.updated,' .
    ' e.source_id sourceId,' .
    ' e.entity_type entityType ' .
    ' FROM entity_typed_text ett ' .
    ' JOIN entity e ON e.id = ett.entity_id' .
    ' WHERE ett.status = :status AND ' .
    ' ett.updated >= :from AND ett.updated <= :to' .
    ' LIMIT :limit OFFSET :offset';

    private const LINK_QUERY = 'SELECT h.id,' .
    ' h.data,' .
    ' h.hostname,' .
    ' eth.entity_slug ' .
    ' FROM host h ' .
    ' JOIN entity_to_host eth ON h.id = eth.host_id ' .
    ' WHERE eth.entity_id = :entityId' .
    ' AND h.text_generate_settings like :type ' .
    ' LIMIT 1';

    public function generate(FilterVO $filter)
    {
        gc_enable();
        $this->makeFile();

        $offset = 0;

        $entityMapping = new ResultSetMapping();
        $entityMapping->addEntityResult(Entity::class, 'e', 'e');
        $entityMapping->addFieldResult('e', 'entityId', 'id');
        $entityMapping->addFieldResult('e', 'sourceId', 'source_id');
        $entityMapping->addFieldResult('e', 'entityType', 'entity_type');
        $entityMapping->addScalarResult('updated', 'updated');
        $entityMapping->addScalarResult('typeId', 'typeId', 'integer');

        $linkMapping = new ResultSetMapping();
        $linkMapping->addEntityResult(Host::class, 'h', 'h');
        $linkMapping->addFieldResult('h', 'id', 'id');
        $linkMapping->addFieldResult('h', 'data', 'data');
        $linkMapping->addFieldResult('h', 'hostname', 'hostname');
        $linkMapping->addScalarResult('entity_slug', 'entity_slug');

        do {
            $entities = $this->getEntities($filter, $offset, $entityMapping);
            foreach ($entities as $var) {
                try {
                    $link = $this->generateLink($var, $linkMapping);
                    if (!$link) {
                        continue;
                    }
                    $name = $this->getTypeName($var['typeId']);
                    $this->writeFile("{$link}|{$name}|{$var['updated']}");
                    unset($link, $name);
                } catch (\Exception $exception) {

                } finally {
                    gc_collect_cycles();
                }
            }
            $offset = $offset + $filter->getLimit();
        } while (!empty($entities));

        return self::FILENAME;
    }

    private function getEntities(FilterVO $filterVO, int &$offset, ResultSetMapping &$rsm)
    {
        return $this->manager->createNativeQuery(self::ENTITY_QUERY, $rsm)
            ->setParameter('status', EntityTypedText::PROCESSED)
            ->setParameter('from', $filterVO->getFrom())
            ->setParameter('to', $filterVO->getTo())
            ->setParameter('limit', $filterVO->getLimit())
            ->setParameter('offset', $offset)
            ->getResult();

    }

    private function generateLink(array $data, ResultSetMapping &$rsm)
    {
        if (!isset($data['e'])) {
            return false;
        }
        if (!$data['e'] instanceof Entity) {
            return false;
        }

        /** @var Entity $var ['e'] */

        $result = $this->manager->createNativeQuery(self::LINK_QUERY, $rsm)
            ->setParameter('entityId', $data['e']->getId())
            ->setParameter('type', "%{$data['typeId']}%")
            ->getOneOrNullResult();

        if (empty($result)) {
            unset($result);
            return false;
        }

        $entityToHost = new EntityToHost();
        $entityToHost->setHost($result['h']);
        $entityToHost->setEntity($data['e']);
        $entityToHost->setEntitySlug($result['entity_slug']);

        $this->preparation->prepareFromEntityToHost($entityToHost);
        unset($entityToHost);
        $link = $this->generator->generateLink($this->preparation->getEntityDAO(), $this->preparation->getHostDAO());

        return $link;
    }

    private function makeFile()
    {
        file_put_contents(self::FILENAME, 'url|type|date' . PHP_EOL, FILE_TEXT);
    }

    private function writeFile(string $message)
    {
        file_put_contents(self::FILENAME, $message . PHP_EOL, FILE_APPEND);
    }

    private function getTypeName(int &$type)
    {
        if (!isset(GeneratorType::NAMES[$type])) {
            return 'invalid';
        }
        return GeneratorType::NAMES[$type];
    }

}