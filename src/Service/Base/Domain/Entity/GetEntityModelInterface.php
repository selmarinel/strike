<?php

namespace App\Service\Base\Domain\Entity;


interface GetEntityModelInterface
{
    public function toArray(): array;
}