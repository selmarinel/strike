<?php

namespace App\Service\Base\Domain\Entity;


interface SetEntityModelInterface
{
    public function fill(array $parameters);

}