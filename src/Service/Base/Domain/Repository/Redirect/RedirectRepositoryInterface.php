<?php

namespace App\Service\Base\Domain\Repository\Redirect;

use App\Service\Base\Infrastructure\Entity\Redirect\Redirect;

interface RedirectRepositoryInterface
{
    /**
     * @param string $hostname
     * @return null|Redirect
     */
    public function findRedirectByHostname(string $hostname);
}