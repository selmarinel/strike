<?php

namespace App\Service\Base\Infrastructure\Repository\Redirect;

use App\Service\Base\Infrastructure\Entity\Redirect\HostGeoRedirect;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class HostGeoRedirectRepository extends ServiceEntityRepository
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @deprecated
     * @param ManagerRegistry $registry
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ManagerRegistry $registry, EventDispatcherInterface $eventDispatcher)
    {
        $entityClass = HostGeoRedirect::class;
        $this->dispatcher = $eventDispatcher;
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $id
     * @return null|object
     * @deprecated
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param HostGeoRedirect $serializable
     * @return HostGeoRedirect
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(HostGeoRedirect $serializable): HostGeoRedirect
    {
        $this->getEntityManager()->persist($serializable);
        $this->getEntityManager()->flush();

        return $serializable;
    }

    /**
     * @param HostGeoRedirect $serializable
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(HostGeoRedirect $serializable)
    {
        $this->getEntityManager()->remove($serializable);
        $this->getEntityManager()->flush();
        return true;
    }

    /**
     * @param Host $host
     * @return HostGeoRedirect|null
     */
    public function findByFromHost(Host $host):?HostGeoRedirect
    {
        /** @var HostGeoRedirect|null $entity */
        $entity = $this->findOneBy(['hostFrom' => $host]);
        return $entity;
    }

    /**
     * @param Host $host
     * @return HostGeoRedirect[]
     */
    public function findByToHost(Host $host): array
    {
        /** @var HostGeoRedirect[] $entities */
        $entities = $this->findBy(['hostTo' => $host]);
        return $entities;
    }
}