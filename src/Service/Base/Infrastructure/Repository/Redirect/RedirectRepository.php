<?php

namespace App\Service\Base\Infrastructure\Repository\Redirect;

use App\Service\Base\Domain\Repository\Redirect\RedirectRepositoryInterface;
use App\Service\Base\Infrastructure\Entity\Redirect\Redirect;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Serializable;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RedirectRepository extends ServiceEntityRepository implements RedirectRepositoryInterface
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @deprecated
     * @param ManagerRegistry $registry
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ManagerRegistry $registry, EventDispatcherInterface $eventDispatcher)
    {
        $entityClass = Redirect::class;
        $this->dispatcher = $eventDispatcher;
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $id
     * @return null|object
     * @deprecated
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Redirect $serializable
     * @return Redirect
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Redirect $serializable): Redirect
    {
        $this->getEntityManager()->persist($serializable);
        $this->getEntityManager()->flush();

        return $serializable;
    }

    /**
     * @param Redirect $serializable
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Redirect $serializable)
    {
        $this->getEntityManager()->remove($serializable);
        $this->getEntityManager()->flush();
        return true;
    }

    /**
     * @param string $hostname
     * @return Redirect|null
     */
    public function findRedirectByHostname(string $hostname)
    {
        return $this->findOneBy([
            'redirectFrom' => $hostname,
        ]);
    }
}