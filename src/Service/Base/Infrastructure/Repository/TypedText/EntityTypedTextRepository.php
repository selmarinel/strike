<?php

namespace App\Service\Base\Infrastructure\Repository\TypedText;

use App\Service\Base\Infrastructure\Entity\TypedText\EntityTypedText;
use App\Service\EntityProcessor\Domain\Database\Repositories\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EntityTypedTextRepository extends ServiceEntityRepository implements RepositoryInterface
{
    /** @deprecated
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = EntityTypedText::class;
        parent::__construct($registry, $entityClass);
    }

    public function findById($id)
    {
        return $this->find($id);
    }

    public function save(\Serializable $serializable): \Serializable
    {
        $this->_em->persist($serializable);
        $this->_em->flush();
    }

    public function remove(\Serializable $serializable)
    {
        $this->getEntityManager()->remove($serializable);
        $this->getEntityManager()->flush();
        return true;
    }

    public function append(EntityTypedText $entityText)
    {
        $this->_em->persist($entityText);
    }

    public function commit()
    {
        $this->_em->flush();
    }

    /**
     * @param int $entityId
     * @param int $type
     * @param bool $active
     * @return EntityTypedText|null
     */
    public function findByEntityIdAndType(int $entityId, int $type, bool $active = true):?EntityTypedText
    {
        $builder = $this->createQueryBuilder('ett')
            ->join('ett.entity', 'e')
            ->where('e.id = :entityId')
            ->andWhere('ett.typeId = :type')
            ->setParameter('entityId', $entityId)
            ->setParameter('type', $type);
        if ($active) {
            $builder->andWhere('ett.status = :status')
                ->setParameter('status', EntityTypedText::PROCESSED);
        }
        return $builder->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();


    }
}