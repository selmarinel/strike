<?php

namespace App\Service\Base\Infrastructure\Repository\Processor;

use App\Service\Base\Infrastructure\Entity\Processor\EntityModel;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }
}