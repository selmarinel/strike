<?php

namespace App\Service\Base\Infrastructure\Repository\Text;


use App\Service\Base\Infrastructure\Entity\Text\EntityText;
use App\Service\EntityProcessor\Domain\Database\Repositories\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EntityTextRepository extends ServiceEntityRepository implements RepositoryInterface
{
    /** @deprecated
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = EntityText::class;
        parent::__construct($registry, $entityClass);
    }


    public function findById($id)
    {
        return $this->find($id);
    }

    public function save(\Serializable $serializable): \Serializable
    {
        $this->_em->persist($serializable);
        $this->_em->flush();
    }

    public function remove(\Serializable $serializable)
    {
        $this->getEntityManager()->remove($serializable);
        $this->getEntityManager()->flush();
        return true;
    }

    public function getByEntityIdAndHostId(string $entityId, int $hostId):?EntityText
    {
        try {
            $result = $this->createQueryBuilder('et')
                ->join('et.entity', 'e')
                ->join('et.host', 'h')
                ->where('h.id = :hostId')
                ->andWhere('e.id = :entityId')
                ->setParameter('hostId', $hostId)
                ->setParameter('entityId', $entityId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
            return $result;
        } catch (\Exception $exception) {
            return null;
        }
    }


    public function append(EntityText $entityText)
    {
        $this->_em->persist($entityText);
    }

    public function commit()
    {
        $this->_em->flush();
    }

}