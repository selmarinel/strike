<?php

namespace App\Service\Base\Infrastructure\Repository\Seo;


use App\Service\Base\Domain\Repository\Seo\EntityCrossLinkRepositoryInterface;
use App\Service\Base\Infrastructure\Entity\Seo\EntityCrossLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EntityCrossLinkRepository extends ServiceEntityRepository implements EntityCrossLinkRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntityCrossLink::class);
    }

    public function prepareToSave(EntityCrossLink $link)
    {
        $this->_em->persist($link);
    }

    public function flush()
    {
        $this->_em->flush();
        $this->_em->clear();
    }
}