<?php

namespace App\Service\Base\Infrastructure\Repository\Metric\Host;


use App\Service\Base\Infrastructure\Entity\Metric\Host\MetricHostLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class MetricHostLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MetricHostLog::class);
    }

    public function getEntityManager()
    {
        return parent::getEntityManager(); // TODO: Change the autogenerated stub
    }
}