<?php

namespace App\Service\Base\Infrastructure\Repository\Metric\Host;


use App\Service\Base\Infrastructure\Entity\Metric\Host\MetricHost;
use App\Service\Base\Infrastructure\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

class MetricHostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MetricHost::class);
    }

    public function getEntityManager()
    {
        return parent::getEntityManager(); // TODO: Change the autogenerated stub
    }

    /**
     * @return int
     */
    public function getTotalMeasuredHosts(): int
    {
        try {
            return $this->createQueryBuilder('h')
                ->select('count(DISTINCT h.host)')
                ->join('h.host','h2')
                ->where('h2.status = :status')
                ->setParameter('status', Type::HOST_STATUS_ACTIVE)
                ->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }
}