<?php

namespace App\Service\Base\Infrastructure\Entity\Redirect;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Service\Base\Infrastructure\Repository\Redirect\RedirectRepository;

/**
 * Class Redirect
 * @package App\Service\Base\Infrastructure\Entity\Redirect
 * @ORM\Entity(repositoryClass=RedirectRepository::class)
 * @ORM\Table(
 *     indexes={
 *      @Index(name="redirect",columns={"redirect_from"}),
 *      },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_redirect", columns={"redirect_from", "redirect_to"})
 *     })
 *
 *
 */
class Redirect
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string",length=255, nullable=false)
     */
    private $redirectFrom;

    /**
     * @var string
     * @ORM\Column(type="string",length=255, nullable=false)
     */
    private $redirectTo;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status = 301;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRedirectFrom(): string
    {
        return $this->redirectFrom;
    }

    /**
     * @param string $redirectFrom
     */
    public function setRedirectFrom(string $redirectFrom)
    {
        $this->redirectFrom = $redirectFrom;
    }

    /**
     * @return string
     */
    public function getRedirectTo(): string
    {
        return $this->redirectTo;
    }

    /**
     * @param string $redirectTo
     */
    public function setRedirectTo(string $redirectTo)
    {
        $this->redirectTo = $redirectTo;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

}