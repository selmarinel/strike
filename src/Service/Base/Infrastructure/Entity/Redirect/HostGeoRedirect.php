<?php

namespace App\Service\Base\Infrastructure\Entity\Redirect;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Base\Infrastructure\Repository\Redirect\HostGeoRedirectRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Class HostGeoRedirect
 * @package App\Service\Base\Infrastructure\Entity\Redirect
 * @ORM\Entity(repositoryClass=HostGeoRedirectRepository::class)
 */
class HostGeoRedirect
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Host
     * @ORM\OneToOne(targetEntity=Host::class)
     * @JoinColumn(name="host_from_id", referencedColumnName="id")
     */
    private $hostFrom;

    /**
     * @var Host
     * @ORM\OneToOne(targetEntity=Host::class)
     * @JoinColumn(name="host_to_id", referencedColumnName="id")
     */
    private $hostTo = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Host
     */
    public function getHostFrom(): Host
    {
        return $this->hostFrom;
    }

    /**
     * @param Host $hostFrom
     */
    public function setHostFrom(Host $hostFrom)
    {
        $this->hostFrom = $hostFrom;
    }

    /**
     * @return Host
     */
    public function getHostTo(): Host
    {
        return $this->hostTo;
    }

    /**
     * @param Host $hostTo
     */
    public function setHostTo(Host $hostTo)
    {
        $this->hostTo = $hostTo;
    }
}