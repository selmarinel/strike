<?php

namespace App\Service\Base\Infrastructure\Entity\Seo;

use Doctrine\ORM\Mapping as ORM;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Base\Infrastructure\Repository\Seo\EntityCrossLinkRepository;

/**
 * Class EntityCrossLink
 * @package App\Service\Base\Infrastructure\Entity\Seo
 *
 * @ORM\Entity(repositoryClass=EntityCrossLinkRepository::class)
 */
class EntityCrossLink
{
//    /**
//     * @var int
//     * @ORM\Id()
//     * @ORM\GeneratedValue()
//     * @ORM\Column(type="bigint", options={"unsigned"=true})
//     */
//    private $id;

    /**
     * @ORM\Id()
     * @ORM\CustomIdGenerator()
     * @ORM\Column(type="string")
     */
    private $id;
    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $links;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param array $links
     */
    public function setLinks(array $links): void
    {
        $this->links = $links;
    }

    /**
     * @param int $id
     */
    public function appendLink(int $id)
    {
        $this->links[] = $id;
    }
}