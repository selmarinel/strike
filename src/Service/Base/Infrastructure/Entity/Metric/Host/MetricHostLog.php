<?php

namespace App\Service\Base\Infrastructure\Entity\Metric\Host;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MetricHostItem
 * @package App\Service\Base\Infrastructure\Entity\Metric\Host
 * @ORM\Entity()
 */
class MetricHostLog
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var MetricHost
     * @ORM\ManyToOne(targetEntity="MetricHost", inversedBy="items")
     * @ORM\JoinColumn(name="metric_host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $metricHost;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $contentLength;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $time;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * MetricHostItem constructor.
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime('now'));
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime(int $time): void
    {
        $this->time = $time;
    }

    /**
     * @return MetricHost
     */
    public function getMetricHost(): MetricHost
    {
        return $this->metricHost;
    }

    /**
     * @param MetricHost $metricHost
     */
    public function setMetricHost(MetricHost $metricHost): void
    {
        $this->metricHost = $metricHost;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getContentLength(): int
    {
        return $this->contentLength;
    }

    /**
     * @param int $contentLength
     */
    public function setContentLength(int $contentLength): void
    {
        $this->contentLength = $contentLength;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param \DateTimeInterface $created
     */
    public function setCreated(\DateTimeInterface $created): void
    {
        $this->created = $created;
    }
}