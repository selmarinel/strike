<?php

namespace App\Service\Base\Infrastructure\Entity;

use App\Service\Processor\Infrastructure\DTO\Article\Article;
use App\Service\Processor\Infrastructure\DTO\Article\Collection\Category;
use App\Service\Processor\Infrastructure\DTO\Audio\Album;
use App\Service\Processor\Infrastructure\DTO\Audio\Artist;
use App\Service\Processor\Infrastructure\DTO\Audio\Collection\Artists;
use App\Service\Processor\Infrastructure\DTO\Audio\Collection\Tracks;
use App\Service\Processor\Infrastructure\DTO\Audio\Relation\Lyric;
use App\Service\Processor\Infrastructure\DTO\Audio\Track;
use App\Service\Processor\Infrastructure\DTO\Deezer\DeezerAlbumDTO;
use App\Service\Processor\Infrastructure\DTO\Deezer\DeezerArtistDTO;
use App\Service\Processor\Infrastructure\DTO\Deezer\DeezerTrackDTO;
use App\Service\Processor\Infrastructure\DTO\Deezer\Tag\DeezerGenreDTO;
use App\Service\Processor\Infrastructure\DTO\Movie\Collection\Films;
use App\Service\Processor\Infrastructure\DTO\Movie\Film;
use App\Service\Processor\Infrastructure\DTO\Root;
use App\Service\Processor\Infrastructure\DTO\Search\Search;
use App\Service\Processor\Infrastructure\DTO\Video\Collection\TVSeries;
use App\Service\Processor\Infrastructure\DTO\Video\Episode;
use App\Service\Processor\Infrastructure\DTO\Video\Season;
use App\Service\Processor\Infrastructure\DTO\Video\Series;
use App\Service\Processor\Infrastructure\DTO\Audio\Tag\Genre as AudioGenre;
use App\Service\Processor\Infrastructure\DTO\Video\Tag\Genre as VideoGenre;
use App\Service\Processor\Infrastructure\DTO\Movie\Tag\Genre as MovieGenre;

class Type
{
    const HOST_TYPE_AUDIO = 1;
    const HOST_TYPE_VIDEO = 2;
    const HOST_TYPE_MOVIE = 3;
    const HOST_TYPE_NEWS = 4;

    const HOST_STATUS_ACTIVE = 1;
    const HOST_STATUS_DISABLE = 0;

    const RELATION_TYPE_LIST = 1;
    const RELATION_TYPE_RELATE = 2;

    const AUDIO_PROVIDER_NUR = 'nur';
    const AUDIO_PROVIDER_DEEZER = 'deezer';

    const HOST_TYPE_DRIVER_MAP = [
        self::HOST_TYPE_AUDIO => 'audio',
        self::HOST_TYPE_VIDEO => 'video',
        self::HOST_TYPE_MOVIE => 'movie',
        self::HOST_TYPE_NEWS => 'news'
    ];

    const ROOT = -1;
    const STATIC = 0;

    const AUDIO_TRACK = 1;
    const AUDIO_ARTIST = 2;
    const AUDIO_ALBUM = 3;

    const COLLECTION_AUDIO_ARTISTS = 4;
    const COLLECTION_AUDIO_TRACKS = 5;

    const AUDIO_LYRIC = 6;

    const VIDEO_EPISODE = 12;
    const VIDEO_SERIES = 13;
    const VIDEO_SEASON = 14;

    const COLLECTION_VIDEO_SERIES = 15;

    const MOVIE_FILM = 21;
    const COLLECTION_MOVIE_FILMS = 22;

    const DEEZER_AUDIO_ARTIST = 31;
    const DEEZER_AUDIO_ALBUM = 32;
    const DEEZER_AUDIO_TRACK = 33;

    const DEEZER_AUDIO_ARTIST_COLLECTION = 34;
    const DEEZER_AUDIO_TRACK_COLLECTION = 35;

    const DEEZER_AUDIO_PLAYLIST = 36;

    const DEEZER_AUDIO_GENRE = 37;

    //tags
    const AUDIO_GENRE = 7;
    const VIDEO_SERIES_GENRE = 17;
    const VIDEO_MOVIES_GENRE = 27;

    const NEWS_ARTICLE = 51;
    const COLLECTION_NEWS_CATEGORY = 55;
    const NEWS_TAG = 57;

    //search
    const SEARCH_TYPE = 101;

    const MAP_COLLECTIONS = [
        Type::ROOT => Root::class,
        Type::COLLECTION_AUDIO_ARTISTS => Artists::class,
        Type::COLLECTION_AUDIO_TRACKS => Tracks::class,
        Type::COLLECTION_VIDEO_SERIES => TVSeries::class,
        Type::COLLECTION_MOVIE_FILMS => Films::class,
        Type::COLLECTION_NEWS_CATEGORY => Category::class,
    ];

    const MAP_GENRES = [
        Type::AUDIO_GENRE => Type::AUDIO_ARTIST,
        Type::VIDEO_SERIES_GENRE => Type::VIDEO_SERIES,
        Type::VIDEO_MOVIES_GENRE => Type::MOVIE_FILM,
        Type::DEEZER_AUDIO_GENRE => Type::DEEZER_AUDIO_ARTIST,
    ];

    const MAP_TAG = [
        Type::AUDIO_ARTIST => AudioGenre::class,
        Type::VIDEO_SERIES => VideoGenre::class,
        Type::MOVIE_FILM => MovieGenre::class,
        Type::DEEZER_AUDIO_ARTIST => DeezerGenreDTO::class,
    ];

    const MAP_DTO = [
        //audio
        Type::AUDIO_TRACK => Track::class,
        Type::AUDIO_ARTIST => Artist::class,
        Type::AUDIO_ALBUM => Album::class,
        Type::AUDIO_LYRIC => Lyric::class,
        Type::AUDIO_GENRE => AudioGenre::class,
        //video
        Type::VIDEO_SERIES => Series::class,
        Type::VIDEO_EPISODE => Episode::class,
        Type::VIDEO_SEASON => Season::class,
        Type::VIDEO_SERIES_GENRE => VideoGenre::class,
        //movie
        Type::MOVIE_FILM => Film::class,
        Type::VIDEO_MOVIES_GENRE => MovieGenre::class,

        Type::SEARCH_TYPE => Search::class,

        Type::DEEZER_AUDIO_TRACK => DeezerTrackDTO::class,
        Type::DEEZER_AUDIO_ARTIST => DeezerArtistDTO::class,
        Type::DEEZER_AUDIO_ALBUM => DeezerAlbumDTO::class,

        Type::DEEZER_AUDIO_GENRE => DeezerGenreDTO::class,

        Type::NEWS_ARTICLE => Article::class,


    ];


    const DTO_MAP = [
        Type::ROOT => [
            'class' => Root::class,
            'name' => 'Root'
        ],
        Type::AUDIO_TRACK => [
            'class' => Track::class,
            'name' => 'Audio Track'
        ],
        Type::AUDIO_ALBUM => [
            'class' => Album::class,
            'name' => 'Audio Album'
        ],
        Type::AUDIO_ARTIST => [
            'class' => Artist::class,
            'name' => 'Audio Artist'
        ],
        Type::COLLECTION_AUDIO_ARTISTS => [
            'class' => Artists::class,
            'name' => 'Audio Artists List'
        ],
        Type::COLLECTION_AUDIO_TRACKS => [
            'class' => Tracks::class,
            'name' => 'Audio Tracks list'
        ],
        Type::STATIC => [
            'class' => Root::class,//todo
            'name' => 'Static Page'
        ],
        Type::VIDEO_SERIES => [
            'class' => Series::class,
            'name' => 'Video Series'
        ],
        Type::VIDEO_SEASON => [
            'class' => Season::class,
            'name' => 'Video Season'
        ],
        Type::VIDEO_EPISODE => [
            'class' => Episode::class,
            'name' => 'Video Episode'
        ],
        Type::COLLECTION_VIDEO_SERIES => [
            'class' => TVSeries::class,
            'name' => 'Video Series List'
        ],
        Type::MOVIE_FILM => [
            'class' => Film::class,
            'name' => 'Movie Cinema'
        ],
        Type::AUDIO_LYRIC => [
            'class' => Lyric::class,
            'name' => 'Audio Lyric'
        ],
        Type::AUDIO_GENRE => [
            'class' => AudioGenre::class,
            'name' => 'Audio Genre',
        ],
        Type::VIDEO_MOVIES_GENRE => [
            'class' => MovieGenre::class,
            'name' => 'Movie Genre',
        ],
        Type::VIDEO_SERIES_GENRE => [
            'class' => VideoGenre::class,
            'name' => 'Series Genre',
        ],

        Type::DEEZER_AUDIO_ARTIST => [
            'class' => DeezerArtistDTO::class,
            'name' => 'Deezer Audio Artist',
        ],
        Type::DEEZER_AUDIO_ALBUM => [
            'class' => DeezerAlbumDTO::class,
            'name' => 'Deezer Audio Album',
        ],
        Type::DEEZER_AUDIO_TRACK => [
            'class' => DeezerTrackDTO::class,
            'name' => 'Deezer Audio Track',
        ],
        Type::NEWS_ARTICLE => [
            'class' => Article::class,
            'name' => 'News Article',
        ]
    ];
}
