<?php

namespace App\Service\Base\Infrastructure\Entity\Search;

use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Search\Infrastructure\Service\VO\Youtube\ResponseYoutubeItemVO;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Class SearchEntity
 * @package App\Service\Base\Infrastructure\Entity\Search
 * @ORM\Entity(repositoryClass=SearchEntityRepository::class)
 * @ORM\Table(
 *      indexes={
 *          @Index(name="status_idx", columns={"status"})
 *     },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_hash", columns={"hash"})
 *     })
 */
class SearchEntity
{
    const UNPROCESSED = 0;
    const PROCESSED = 1;
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $query;

    /**
     * @var EntityToHost
     * @ORM\ManyToOne(targetEntity=EntityToHost::class)
     * @ORM\JoinColumn(name="entity_host_id", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $entityToHost;

    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $results;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return EntityToHost|null
     */
    public function getEntityToHost(): ?EntityToHost
    {
        return $this->entityToHost;
    }

    /**
     * @param EntityToHost $entityToHost
     */
    public function setEntityToHost(EntityToHost $entityToHost): void
    {
        $this->entityToHost = $entityToHost;
    }

    /**
     * @return ResponseYoutubeItemVO[]
     */
    public function getResults(): array
    {
        $result = [];
        foreach ($this->results as $item) {
            $itemVO = new ResponseYoutubeItemVO();
            $itemVO->setTitle(isset($item['title']) ? $item['title'] : '');
            $itemVO->setVideoId(isset($item['id']) ? $item['id'] : '');
            $itemVO->setSrc(isset($item['src']) ? $item['src'] : '');
            $itemVO->setDescription(isset($item['description']) ? $item['description'] : '');
            $result[] = $itemVO;
            unset($itemVO);
        }
        return $result;
    }

    /**
     * @param ResponseYoutubeItemVO[] $results
     */
    public function setResults(array $results): void
    {
        $this->results = array_map(function (ResponseYoutubeItemVO $itemVO) {
            return $itemVO->toArray();
        }, $results);
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query): void
    {
        $this->query = $query;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost(Host $host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }
}