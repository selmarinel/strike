<?php

namespace App\Service\Base\Infrastructure\Entity;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use Doctrine\ORM\Mapping as ORM;
use App\Service\Base\Infrastructure\Repository\Processor\TagRepository;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Class Tag
 * @package App\Service\Base\Infrastructure\Entity
 * @ORM\Entity(repositoryClass=TagRepository::class)
 * @ORM\Table(
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_entity_tag", columns={"entity_id"})
 *     }
 *)
 */
class Tag
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Entity
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;
    /**
     * @var string
     * @ORM\Column()
     */
    private $name = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}