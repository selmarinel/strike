<?php

namespace App\Service\Base\Infrastructure\Entity\Text;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\Mapping as ORM;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\Base\Infrastructure\Repository\Text\EntityTextRepository;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Class EntityText
 * @ORM\Entity(repositoryClass=EntityTextRepository::class)
 * @ORM\Table(
 *     indexes={@ORM\Index(name="entity_host_idx", columns={"entity_id","host_id"})},
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_entity_host_text_id", columns={"entity_id","host_id"})
 *     }
 *)
 */
class EntityText
{
    const TYPE_MANUAL = 1;
    const TYPE_AUTO = 2;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Entity
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;
    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;
    /**
     * @var int
     * @ORM\Column(type="integer", options={"default":1})
     */
    private $type = self::TYPE_MANUAL;

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost(Host $host)
    {
        $this->host = $host;
    }

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $text = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $available = true;

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     */
    public function setAvailable(bool $available)
    {
        $this->available = $available;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }
}