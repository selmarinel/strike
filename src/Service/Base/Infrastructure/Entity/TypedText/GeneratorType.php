<?php

namespace App\Service\Base\Infrastructure\Entity\TypedText;


class GeneratorType
{
    public const KINOPOISK_TYPE = 1;

    public const YOUTUBE_CAPTION_TYPE = 2;

    public const XPARSER_TYPE = 3;

    public const WIKI_TYPE = 4;

    public const NAMES = [
        self::KINOPOISK_TYPE => 'Kinopoisk',
        self::YOUTUBE_CAPTION_TYPE => 'Youtube Caption',
        self::XPARSER_TYPE => 'XParser',
        self::WIKI_TYPE => 'Wikipedia'
    ];
}