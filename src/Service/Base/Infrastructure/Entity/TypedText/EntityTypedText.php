<?php

namespace App\Service\Base\Infrastructure\Entity\TypedText;

use Doctrine\ORM\Mapping as ORM;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\Base\Infrastructure\Repository\TypedText\EntityTypedTextRepository;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Class EntityTypedText
 * @ORM\Entity(repositoryClass=EntityTypedTextRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_entity_id_type_id", columns={"entity_id","type_id"})
 *     }
 *)
 */
class EntityTypedText
{
    const FAILED = 2;
    const PROCESSED = 1;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Entity
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $typeId;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $text = '';

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdated(new \DateTime('now'));

        if ($this->getUpdated() == null) {
            $this->setCreated(new \DateTime('now'));
        }
    }

    public function __construct()
    {
        $this->setCreated(new \DateTime('now'));
        $this->setUpdated(new \DateTime('now'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId(int $typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }
}