<?php

namespace App\Service\Encryption\Infrastructure\Handle;

use App\Service\Encryption\Domain\Handle\ImageDecryptServiceInterface;
use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\ResponseVOInterface;
use App\Service\Encryption\Domain\VO\ImageRequestVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;
use App\Service\Encryption\Infrastructure\Providers\DeezerProviderDecrypter;
use App\Service\Encryption\Infrastructure\Providers\NurProviderDecrypter;
use App\Service\Encryption\Infrastructure\VO\ProviderVO;
use App\Service\Encryption\Infrastructure\VO\ResponseVO;
use SpaceBro\ImageFileStorage\Exceptions\HashNotSupported;
use SpaceBro\ImageFileStorage\Service\ImageStorage;

class ImageDecryptService implements ImageDecryptServiceInterface
{
    const INTERNAL_SEPARATOR = '/internal';
    const DEEZER = 'deezer';
    const NUR = 'nur';

    /**
     * @param DecryptionInterface $decryption
     * @param ImageRequestVOInterface $imagesRequestVO
     * @return ResponseVOInterface
     * @throws InvalidHashInput
     * @throws \SpaceBro\ImageFileStorage\Exceptions\HashNotSupported
     */
    public function makeDecrypt(
        DecryptionInterface $decryption,
        ImageRequestVOInterface $imagesRequestVO
    ): ResponseVOInterface {
        try {
            $hash = $decryption->decrypt($imagesRequestVO->getImageHash());
        } catch (InvalidHashInput $e) {
            throw $e;
        }
        preg_match('/(?<host>[\S]+)_(?<provider>[\S]+)_(?<hash>[\S]+)/', $hash, $details);

        if (empty($details)) {
            throw new InvalidHashInput;
        }

        if ($imagesRequestVO->getHost() !== $details['host']) {
            throw new InvalidHashInput;
        }

        $providerVO = new ProviderVO();
        $providerVO->setWidth($imagesRequestVO->getWidth());
        $providerVO->setHeight($imagesRequestVO->getHeight());
        $providerVO->setInternalSeparator(self::INTERNAL_SEPARATOR);

        switch ($details['provider']) {
            case self::DEEZER:
                $imageStorage = new DeezerProviderDecrypter();
                break;
            case self::NUR:
                $imageStorage = new NurProviderDecrypter();
                break;
            default:
                throw new InvalidHashInput();
        }

        try {
            $imageResponse = $imageStorage->decryptImage($details['hash'], $providerVO);
            return $imageResponse;
        } catch (HashNotSupported $exception) {
            throw new InvalidHashInput;
        }
    }
}