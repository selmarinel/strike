<?php

namespace App\Service\Encryption\Infrastructure\Handle;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Encryption\Domain\Handle\AudioDecryptServiceInterface;
use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ResponseVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;
use App\Service\Encryption\Infrastructure\Providers\DeezerProviderDecrypter;
use App\Service\Encryption\Infrastructure\Providers\NurProviderDecrypter;

class AudioDecryptService implements AudioDecryptServiceInterface
{


    /**
     * @param DecryptionInterface $decryption
     * @param AudioRequestVOInterface $audioRequestVO
     * @return ResponseVOInterface
     * @throws InvalidHashInput
     * @throws \SpaceBro\TrackFileStorage\Exceptions\InvalidParamsException
     */
    public function makeDecrypt(
        DecryptionInterface $decryption,
        AudioRequestVOInterface $audioRequestVO
    ): ResponseVOInterface
    {

        $hash = $decryption->decrypt($audioRequestVO->getResourceHash());
        preg_match('/(?<provider>[\S]+)_(?<host>[\S]+)_(?<source>[\S]+)_(?<server>[\S]+)/', $hash, $details);

        if (empty($details)) {
            throw new InvalidHashInput;
        }

        if ($audioRequestVO->getHost() !== $details['host']) {
            throw new InvalidHashInput;
        }

        switch ($details['provider']) {
            case Type::AUDIO_PROVIDER_NUR:
                $audioStorage = new NurProviderDecrypter();
                break;
            case Type::AUDIO_PROVIDER_DEEZER:
                $audioStorage = new DeezerProviderDecrypter();
                break;
            default:
                throw new InvalidHashInput;
        }

        return $audioStorage->decryptAudio($audioRequestVO, $details);
    }
}