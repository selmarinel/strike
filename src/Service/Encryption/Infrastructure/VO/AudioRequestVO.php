<?php

namespace App\Service\Encryption\Infrastructure\VO;


use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;

class AudioRequestVO implements AudioRequestVOInterface
{
    private $host = '';

    private $resourceHash = '';

    private $params = [];

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getResourceHash(): string
    {
        return $this->resourceHash;
    }

    /**
     * @param string $resourceHash
     */
    public function setResourceHash(string $resourceHash): void
    {
        $this->resourceHash = $resourceHash;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }
}