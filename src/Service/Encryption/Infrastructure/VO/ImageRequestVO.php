<?php

namespace App\Service\Encryption\Infrastructure\VO;


use App\Service\Encryption\Domain\VO\ImageRequestVOInterface;

class ImageRequestVO implements ImageRequestVOInterface
{
    /** @var string */
    private $imageHash = '';
    /** @var string */
    private $host = '';
    /** @var int */
    private $width = 200;
    /** @var int */
    private $height = 300;

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @param string $imageHash
     */
    public function setImageHash(string $imageHash): void
    {
        $this->imageHash = $imageHash;
    }

    public function getImageHash(): string
    {
        return $this->imageHash;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

}