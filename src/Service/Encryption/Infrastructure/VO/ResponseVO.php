<?php

namespace App\Service\Encryption\Infrastructure\VO;


use App\Service\Encryption\Domain\VO\ResponseVOInterface;

class ResponseVO implements ResponseVOInterface
{
    /** @var string  */
    private $redirect = '';
    /** @var string  */
    private $host = '';
    /** @var string  */
    private $scheme = 'http';

    /**
     * @return string
     */
    public function getRedirect(): string
    {
        return $this->redirect;
    }

    /**
     * @param string $redirect
     */
    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     */
    public function setScheme(string $scheme): void
    {
        $this->scheme = $scheme;
    }
}