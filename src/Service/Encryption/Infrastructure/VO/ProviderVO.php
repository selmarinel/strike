<?php

namespace App\Service\Encryption\Infrastructure\VO;

use App\Service\Encryption\Domain\VO\ProviderVOInterface;

class ProviderVO implements ProviderVOInterface
{
    private $width;
    private $height;
    private $separator;

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getInternalSeparator(): string
    {
        return $this->separator;
    }

    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    public function setInternalSeparator(string $separator): void
    {
        $this->separator = $separator;
    }

}