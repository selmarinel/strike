<?php

namespace App\Service\Encryption\Infrastructure\EventListener;

use App\Service\Encryption\Exceptions\InvalidHashInput;
use SpaceBro\Encryption\Exceptions\InvalidHashInput as InvalidPackageHashInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    /**
    * @param GetResponseForExceptionEvent $event
    */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        if ($event->getException() instanceof InvalidHashInput ||
            $event->getException() instanceof InvalidPackageHashInput
        ) {
            $response = new Response(
                '',
                Response::HTTP_NOT_FOUND
            );

            return $event->setResponse($response);
        }
    }
}