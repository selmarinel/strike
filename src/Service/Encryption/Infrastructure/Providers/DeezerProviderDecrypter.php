<?php

namespace App\Service\Encryption\Infrastructure\Providers;

use App\Service\Encryption\Domain\ProviderDecrypter\ProviderDecrypter;
use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ProviderVOInterface;
use App\Service\Encryption\Infrastructure\VO\ResponseVO;
use App\Service\Search\Domain\Service\Searcher\Provider\ProviderInterface;
use SpaceBro\ImageFileStorage\Service\DzenImageStorage;

class DeezerProviderDecrypter implements ProviderDecrypter
{
    const INTERNAL_SEPARATOR = '/internal';

    public function decryptImage(string $path, ProviderVOInterface $providerVO): ResponseVO
    {
        $imageStorage = new DzenImageStorage();

        $urlPart = parse_url($imageStorage->url($path));
        $imageResponse = new ResponseVO();
        $imageResponse->setHost($urlPart['host']);
        $imageResponse->setRedirect($this->produceLink($urlPart['path'], $providerVO));

        return $imageResponse;
    }

    public function decryptAudio(AudioRequestVOInterface $audioRequestVO, array $details): ResponseVO
    {
        $url = parse_url($details['source']);
        $responseVO = new ResponseVO();
        $responseVO->setHost($url['host']);
        $responseVO->setRedirect(self::INTERNAL_SEPARATOR . $url['path']);
        return $responseVO;
    }


    /**
     * @param string $path
     * @param ProviderVOInterface $providerVO
     * @return string
     */
    protected function produceLink(string $path, ProviderVOInterface $providerVO)
    {
        return
            $providerVO->getInternalSeparator() .
            $path;
    }
}