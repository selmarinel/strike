<?php

namespace App\Service\Encryption\Infrastructure\Providers;

use App\Service\Encryption\Domain\ProviderDecrypter\ProviderDecrypter;
use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ProviderVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;
use App\Service\Encryption\Infrastructure\VO\ResponseVO;
use SpaceBro\ImageFileStorage\Service\DzenImageStorage;
use SpaceBro\ImageFileStorage\Service\ImageStorage;
use SpaceBro\TrackFileStorage\Service\TrackFileStorage;

class NurProviderDecrypter implements ProviderDecrypter
{
    const INTERNAL_SEPARATOR = '/internal';

    private const IM2 = 'im2';

    public function decryptImage(string $path, ProviderVOInterface $providerVO): ResponseVO
    {
        preg_match("/^([a-z]+[0-9]+)\/([a-z0-9]+)/", $path, $params);
        $serverId = $params[1];

        if ($serverId === self::IM2) {
            $imageStorage = new ImageStorage();
        } else {
            $imageStorage = new DzenImageStorage();
        }

        $urlPart = parse_url($imageStorage->url($path));
        $imageResponse = new ResponseVO();
        $imageResponse->setHost($urlPart['host']);
        $imageResponse->setRedirect($this->produceLink($urlPart['path'], $providerVO));

        return $imageResponse;
    }

    public function decryptAudio(AudioRequestVOInterface $audioRequestVO, array $details): ResponseVO
    {
        $trackFileStorage = new TrackFileStorage($audioRequestVO->getParams());

        $urls = $trackFileStorage->getFileUrls($details['source'], $details['server']);


        if (empty($urls)) {
            throw new InvalidHashInput;
        }
        $urlPart = parse_url(array_shift($urls));

        $responseVO = new ResponseVO();
        $responseVO->setHost($urlPart['host']);
        $responseVO->setRedirect(self::INTERNAL_SEPARATOR .  $urlPart['path'] . '?id=' . $details['source']);

        return $responseVO;
    }

    /**
     * @param string $path
     * @param ProviderVOInterface $providerVO
     * @return string
     */
    protected function produceLink(string $path, ProviderVOInterface $providerVO)
    {
        return
            $providerVO->getInternalSeparator() .
            $path .
            "?w={$providerVO->getWidth()}" .
            "&h={$providerVO->getHeight()}";
    }
}