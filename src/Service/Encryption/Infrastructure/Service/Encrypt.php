<?php

namespace App\Service\Encryption\Infrastructure\Service;

use App\Service\Encryption\Domain\Service\EncryptionInterface;
use SpaceBro\Encryption\Service\Encryption;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Encrypt implements EncryptionInterface
{
    /** @var ContainerInterface  */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $string
     * @return string
     * @throws \SpaceBro\Encryption\Exceptions\InvalidHashInput
     */
    public function encrypt(string $string): string
    {
        $encryptService = new Encryption([
            'secret_key' => $this->container->getParameter('secret_key'),
            'secret_iv' => $this->container->getParameter('secret_iv'),
            'encryption_method' => $this->container->getParameter('encryption_method')
        ]);

        return $encryptService->encrypt($string);
    }
}