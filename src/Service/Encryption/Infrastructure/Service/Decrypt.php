<?php

namespace App\Service\Encryption\Infrastructure\Service;

use App\Service\Encryption\Domain\Service\DecryptionInterface;
use SpaceBro\Encryption\Service\Decryption;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Decrypt implements DecryptionInterface
{
    /** @var ContainerInterface  */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $string
     * @return string
     * @throws \SpaceBro\Encryption\Exceptions\InvalidHashInput
     */
    public function decrypt(string $string): string
    {
        $encryptService = new Decryption([
            'secret_key' => $this->container->getParameter('secret_key'),
            'secret_iv' => $this->container->getParameter('secret_iv'),
            'encryption_method' => $this->container->getParameter('encryption_method')
        ]);
        return $encryptService->decrypt($string);
    }
}