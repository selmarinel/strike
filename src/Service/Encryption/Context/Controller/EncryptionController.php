<?php

namespace App\Service\Encryption\Context\Controller;

use App\Service\Assets\Domain\Service\ImageConstructorInterface;
use App\Service\Encryption\Domain\Handle\AudioDecryptServiceInterface;
use App\Service\Encryption\Domain\Handle\ImageDecryptServiceInterface;
use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ImageRequestVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EncryptionController extends Controller
{
    /**
     * @param Request $request
     * @param DecryptionInterface $decryption
     * @param ImageRequestVOInterface $imageRequestVO
     * @param ImageDecryptServiceInterface $imageDecryptService
     * @param ImageConstructorInterface $imageConstructor
     * @return Response
     */
    public function images(
        Request $request,
        DecryptionInterface $decryption,
        ImageRequestVOInterface $imageRequestVO,
        ImageDecryptServiceInterface $imageDecryptService,
        ImageConstructorInterface $imageConstructor
    ) {
        $imageRequestVO->setHost($request->getHost());
        $imageRequestVO->setImageHash($request->get('image_hash') ?? '');
        $imageRequestVO->setWidth($request->get('w') ?? 200);
        $imageRequestVO->setHeight($request->get('h') ?? 300);
        try {
            $imageResponse = $imageDecryptService->makeDecrypt($decryption, $imageRequestVO);
        } catch (InvalidHashInput $exception) {
            $image = $imageConstructor->produceImage($request->get('image_hash'));
            return new Response($image, Response::HTTP_OK, ['content-type' => 'image/jpeg']);
        }
        $response = new JsonResponse();
        $response->headers->set('X-Accel-Redirect', $imageResponse->getRedirect());
        $response->headers->set('X-Accel-Host', $imageResponse->getHost());
        $response->headers->set('X-Accel-Scheme', $imageResponse->getScheme());
        return $response;
    }

    /**
     * @param Request $request
     * @param DecryptionInterface $decryption
     * @param AudioRequestVOInterface $audioRequestVO
     * @param AudioDecryptServiceInterface $audioDecryptService
     * @param ContainerInterface $container
     * @return JsonResponse
     * @throws InvalidHashInput
     */
    public function resource(
        Request $request,
        DecryptionInterface $decryption,
        AudioRequestVOInterface $audioRequestVO,
        AudioDecryptServiceInterface $audioDecryptService,
        ContainerInterface $container
    ) {
        $audioRequestVO->setHost($request->getHost());
        $audioRequestVO->setResourceHash($request->get('resource_hash') ?? '');
        $audioRequestVO->setParams([
            'host' => $container->getParameter('host'),
            'host_prefix' => $container->getParameter('host_prefix'),
            'allowed_servers' => $container->getParameter('allowed_servers')
        ]);

        $audioResponse = $audioDecryptService->makeDecrypt($decryption, $audioRequestVO);

        $response = new JsonResponse();
        $response->headers->set('X-Accel-Redirect', $audioResponse->getRedirect());
        $response->headers->set('X-Accel-Host', $audioResponse->getHost());
        $response->headers->set('X-Accel-Scheme', $audioResponse->getScheme());

        return $response;
    }
}