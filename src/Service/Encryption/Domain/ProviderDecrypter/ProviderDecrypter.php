<?php

namespace App\Service\Encryption\Domain\ProviderDecrypter;

use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ProviderVOInterface;
use App\Service\Encryption\Infrastructure\VO\ResponseVO;

interface ProviderDecrypter
{
    public function decryptImage(string $path, ProviderVOInterface $providerVO): ResponseVO;

    public function decryptAudio(AudioRequestVOInterface $audioRequestVO, array $details): ResponseVO;
}