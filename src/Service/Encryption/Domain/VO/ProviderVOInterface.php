<?php

namespace App\Service\Encryption\Domain\VO;

interface ProviderVOInterface
{
    public function getWidth(): int;

    public function getHeight(): int;

    public function getInternalSeparator(): string;

    public function setWidth(int $width): void;

    public function setHeight(int $height): void;

    public function setInternalSeparator(string $separator): void;
}