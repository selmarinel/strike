<?php

namespace App\Service\Encryption\Domain\VO;


interface ResponseVOInterface
{
    public function getRedirect(): string;

    public function getHost(): string;

    public function getScheme(): string;
}