<?php

namespace App\Service\Encryption\Domain\VO;


interface ImageRequestVOInterface
{
    /** @return string */
    public function getImageHash(): string;

    /** @return string */
    public function getHost(): string;

    public function setImageHash(string $imageHash);

    public function setHost(string $host);

    public function setWidth(int $width);

    public function setHeight(int $height);

    public function getWidth(): int;

    public function getHeight(): int;
}