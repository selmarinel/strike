<?php

namespace App\Service\Encryption\Domain\VO;


interface AudioRequestVOInterface
{
    /**
     * @return string
     */
    public function getHost(): string;

    /**
     * @param string $host
     */
    public function setHost(string $host): void;

    /**
     * @return string
     */
    public function getResourceHash(): string;

    /**
     * @param string $resourceHash
     */
    public function setResourceHash(string $resourceHash): void;

    /**
     * @return array
     */
    public function getParams(): array;

    /**
     * @param array $params
     */
    public function setParams(array $params): void;
}