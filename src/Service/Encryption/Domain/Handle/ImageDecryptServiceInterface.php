<?php

namespace App\Service\Encryption\Domain\Handle;


use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\ResponseVOInterface;
use App\Service\Encryption\Domain\VO\ImageRequestVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;

interface ImageDecryptServiceInterface
{
    /**
     * @param DecryptionInterface $decryption
     * @param ImageRequestVOInterface $imagesRequestVO
     * @return ResponseVOInterface
     * @throws InvalidHashInput
     */
    public function makeDecrypt(
        DecryptionInterface $decryption,
        ImageRequestVOInterface $imagesRequestVO
    ): ResponseVOInterface;
}