<?php

namespace App\Service\Encryption\Domain\Handle;


use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\AudioRequestVOInterface;
use App\Service\Encryption\Domain\VO\ResponseVOInterface;
use App\Service\Encryption\Exceptions\InvalidHashInput;

interface AudioDecryptServiceInterface
{
    /**
     * @param DecryptionInterface $decryption
     * @param AudioRequestVOInterface $audioRequestVO
     * @return ResponseVOInterface
     * @throws InvalidHashInput
     */
    public function makeDecrypt(
        DecryptionInterface $decryption,
        AudioRequestVOInterface $audioRequestVO
    ): ResponseVOInterface;
}