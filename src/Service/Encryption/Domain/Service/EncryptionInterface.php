<?php

namespace App\Service\Encryption\Domain\Service;

interface EncryptionInterface
{
    public function encrypt(string $string): string;
}