<?php

namespace App\Service\Encryption\Domain\Service;

use App\Service\Encryption\Exceptions\InvalidHashInput;

interface DecryptionInterface
{
    /**
     * @param string $string
     * @return string
     * @throws InvalidHashInput
     */
    public function decrypt(string $string): string;
}