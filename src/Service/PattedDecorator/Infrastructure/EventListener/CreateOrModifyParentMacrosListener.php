<?php

namespace App\Service\PattedDecorator\Infrastructure\EventListener;


use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\PattedDecorator\Infrastructure\Database\Repository\MacrosRepository;
use App\Service\PattedDecorator\Infrastructure\EventDispatcher\CreateOrModifyParentMacrosEvent;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;

class CreateOrModifyParentMacrosListener
{
    /** @var MacrosRepository */
    private $macrosRepository;

    public function __construct(MacrosRepository $macrosRepository)
    {
        $this->macrosRepository = $macrosRepository;
    }

    public function onSaveMacros(CreateOrModifyParentMacrosEvent $event)
    {
        $macros = $event->getMacros();
        if (!$macros) {
            return null;
        }
        $host = $macros->getHost();

        if (!$event->isForce()) {
            return null;
        }

        if (empty($host->getChildren())) {
            return null;
        }
        $children = $host->getChildren();
        foreach ($children as $child) {
            try {
                /** @var Macros $childMacros */
                $childMacros = $this->macrosRepository->findByHostIdAndEntityType(
                    $child->getId(),
                    $macros->getEntityType()
                );
                $childMacros->prepareSeoData($macros->getSeoData());
                $this->macrosRepository->save($childMacros->getTemplate());

            } catch (NonUniqueResultException $e) {
                continue;
            } catch (ORMException $e) {
                continue;
            }
        }
    }
}