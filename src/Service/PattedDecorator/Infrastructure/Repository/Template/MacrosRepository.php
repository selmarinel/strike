<?php

namespace App\Service\PattedDecorator\Infrastructure\Repository\Template;

use App\Service\Administrate\Domain\Repository\Entity\CRUDRepositoryInterface;
use App\Service\Administrate\Infrastructure\Repository\AbstractRepository;
use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\PattedDecorator\Infrastructure\Database\Repository\MacrosRepository as Repository;

/**
 * Class MacrosRepository
 * @package App\Service\PattedDecorator\Infrastructure\Saver\Template
 * @deprecated
 */
class MacrosRepository extends AbstractRepository implements CRUDRepositoryInterface
{
    protected $entityClass = Macros::class;
    /** @var HostRepository */
    private $hostRepository;

    /**
     * HostRepository constructor.
     * @param Repository $repository
     * @param ValidatorInterface $validator
     * @param ValidationErrorsProcessor $validationErrorsProcessor
     * @param HostRepository $hostRepository
     */
    public function __construct(
        Repository $repository,
        ValidatorInterface $validator,
        ValidationErrorsProcessor $validationErrorsProcessor,
        HostRepository $hostRepository
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validationProcessor = $validationErrorsProcessor;
        $this->hostRepository = $hostRepository;
    }

    public function getCollection(Request $request): array
    {
        $limit = $request->query->get('limit') ?: 20;
        $offset = $request->query->get('offset') ?: 0;

        $entityType = $request->query->get('entity_type') ?: null;
        $hostId = $request->query->get('host_id') ?: null;

        $queryBuilder = $this->repository->createQueryBuilder('t')
            ->join('t.host', 'h');

        if ($entityType) {
            $queryBuilder->andWhere('t.entity_type = :entity_type')
                ->setParameter('entity_type', (int)$entityType);
        }
        if ($hostId) {
            $queryBuilder->andWhere('h.id = :host_id')
                ->setParameter('host_id', $hostId);
        }
        $queryBuilder
            ->orderBy('t.entity_type')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $collection = new Paginator($queryBuilder);

        $result = [];
        $result['count'] = $collection->count();
        $result['data'] = [];
        foreach ($collection as $model) {
            $result['data'][] = $model->serialize();
        }
        return $result;
    }

    /**
     * @param Host $host
     * @param int $entityType
     * @param bool $static
     * @return Macros|null
     * @throws NonUniqueResultException
     */
    public function getByHostIdAndEntityType(Host $host, int $entityType, bool $static = false): ?Macros
    {
        $template = $this->repository->findByHostIdAndEntityType($host->getId(), $entityType);

        $seoData = [];
        if ($template) {
            $seoData = $template->getSeoData();
        }

        if (!$template) {
            if ($host->getParent()) {
                $template = $this->repository->findByHostIdAndEntityType($host->getParentId(), $entityType);
            }
        }
        $template = ($template) ?: $this->repository->getDefault($static);

        if (!empty($seoData)) {
            $template->setSeoData($seoData);
        }
        return $template;
    }

    /**
     * @param \Serializable $serializable
     * @param array $parameters
     * @return null|\Serializable
     * @throws \App\Service\Administrate\Exception\SaveException
     */
    protected function save(\Serializable $serializable, array $parameters): ?\Serializable
    {
        $this->processParametersToFetchEntities($parameters);
        return parent::save($serializable, $parameters);
    }

    /**
     * @param array $parameters
     */
    private function processParametersToFetchEntities(array &$parameters)
    {
        if (isset($parameters['host_id'])) {
            $parameters['host'] = $this->hostRepository->findById($parameters['host_id']);
        }
    }
}