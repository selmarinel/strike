<?php

namespace App\Service\PattedDecorator\Infrastructure\Database\Repository;

use App\Service\EntityProcessor\Domain\Database\Repositories\RepositoryInterface;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\PattedDecorator\Infrastructure\EventDispatcher\CreateOrModifyParentMacrosEvent;
use App\Service\PattedDecorator\Infrastructure\EventDispatcher\CreateOrModifyParentTemplateEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Serializable;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * EntityRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 * @deprecated
 */
class MacrosRepository extends ServiceEntityRepository implements RepositoryInterface
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @deprecated
     * @param ManagerRegistry $registry
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ManagerRegistry $registry, EventDispatcherInterface $eventDispatcher)
    {
        $entityClass = Macros::class;
        $this->dispatcher = $eventDispatcher;
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $id
     * @return null|object
     * @deprecated
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Serializable $serializable
     * @return Serializable
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(\Serializable $serializable): Serializable
    {
        $this->getEntityManager()->persist($serializable);
        $this->getEntityManager()->flush();
        $event = new CreateOrModifyParentMacrosEvent();
        $event->setMacros($serializable);
        $this->dispatcher->dispatch('app.save.macros', $event);

        return $serializable;
    }

    /**
     * @param Serializable $serializable
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(\Serializable $serializable)
    {
        $this->getEntityManager()->remove($serializable);
        $this->getEntityManager()->flush();
        return true;
    }

    /**
     * @param int $hostId
     * @param int $entityType
     * @return Macros|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByHostIdAndEntityType(int $hostId, int $entityType)
    {
        return $this->createQueryBuilder('t')
            ->join('t.host', 'h')
            ->where('t.entity_type = :entity_type')
            ->setParameter('entity_type', $entityType)
            ->andWhere('h.id = :host_id')
            ->setParameter('host_id', $hostId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param bool $isStatic
     * @return Macros
     */
    public function getDefault(bool $isStatic = false): Macros
    {
        $template = new Macros();
        $template->setSeoData([]);
        $template->setId(0);
        return $template;
    }


}