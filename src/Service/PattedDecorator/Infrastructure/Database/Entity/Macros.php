<?php

namespace App\Service\PattedDecorator\Infrastructure\Database\Entity;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\Mapping as ORM;
use App\Service\PattedDecorator\Infrastructure\Database\Repository\MacrosRepository;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Class Templates
 * @ORM\Entity(repositoryClass=MacrosRepository::class)
 *
 * @ORM\Table(
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_entity_type_host", columns={"entity_type", "host_id"})
 *     }
 * )
 * @deprecated
 */
class Macros implements \Serializable
{
    const SEO_TITLE = 'seo_title';
    const SEO_DESCRIPTION = 'seo_description';
    const SEO_H1 = 'seo_h1';
    const SEO_TEXT = 'seo_text';
    const SEO_META_TAGS = 'seo_meta_tags';

    const DEFAULT_DATA = [
        self::SEO_META_TAGS => '',
        self::SEO_DESCRIPTION => '',
        self::SEO_H1 => '',
        self::SEO_TEXT => '',
        self::SEO_TITLE => '',
    ];

    public const DEFAULT = '<section><h1>[[seo_h1]]</h1><p>[[seo_text]]</p></section>';
    public const DEFAULT_STATIC = '<h1>[[seo_h1]]</h1><p>[[seo_text]]</p>';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     */
    private $id;
    /**
     * @var int
     * @var int
     * @ORM\Column(type="integer")
     */
    private $entity_type;
    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $seoData;

    public function __construct()
    {
        $this->setSeoData([]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * @param mixed $entity_type
     */
    public function setEntityType($entity_type): void
    {
        $this->entity_type = $entity_type;
    }

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost(Host $host): void
    {
        $this->host = $host;
    }

    /**
     * @return array
     */
    public function getSeoData(): array
    {
        $result = [];
        if (!empty($this->seoData)) {
            foreach ($this->seoData as $index => $seoDatum) {
                if (is_string($seoDatum) || is_numeric($seoDatum)) {
                    $result[$index] = $seoDatum;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $seoData
     */
    public function setSeoData(array $seoData): void
    {
        $this->seoData = $seoData;
    }

    /**
     * @return array|string
     */
    public function serialize()
    {
        return [
            'id' => $this->getId(),
            'entity_type' => $this->getEntityType(),
            'host_id' => $this->getHost()->getId(),
            'host_name' => $this->getHost()->getHostname(),
            'data' => $this->getSeoData()
        ];
    }

    /**
     * @param string $serialized
     * @return bool|void
     */
    public function unserialize($serialized)
    {
        if (!is_array($serialized)) {
            return null;
        }
        if (isset($serialized['entity_type'])) {
            $this->setEntityType($serialized['entity_type']);
        }
        if (isset($serialized['host']) && $serialized['host'] instanceof Host) {
            $this->setHost($serialized['host']);
        }
        if (isset($serialized['data'])) {
            $this->prepareSeoData($serialized['data']);
        }
    }

    public function prepareSeoData(array $seoData): void
    {
        $defaultData = self::DEFAULT_DATA;

        foreach ($seoData as $index => $seoDatum) {
            if (mb_strpos($index, 'seo_') === false) {
                $index = "seo_$index";
                $seoData[$index] = $seoDatum;
                unset($seoData[$index]);
            }
            if (isset($defaultData[$index])) {
                $defaultData[$index] = $seoDatum;
            }
        }
        $this->setSeoData($defaultData);
    }
}