<?php

namespace App\Service\PattedDecorator\Infrastructure\EventDispatcher;

use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use Symfony\Component\EventDispatcher\Event;

class CreateOrModifyParentMacrosEvent extends Event
{
    private $macros = null;

    private $force = false;

    public function setMacros(Macros $macros)
    {
        $this->macros = $macros;
    }

    public function getMacros(): ?Macros
    {
        return $this->macros;
    }

    /**
     * @return bool
     */
    public function isForce(): bool
    {
        return $this->force;
    }

    /**
     * @param bool $force
     */
    public function setForce(bool $force): void
    {
        $this->force = $force;
    }
}