<?php

namespace App\Service\EntityProcessor\Exceptions\UriRunner;


class InvalidPatternException extends \Exception
{
    protected $message = 'Invalid pattern Exception';
}