<?php

namespace App\Service\EntityProcessor\Exceptions\ParseURI;

class InvalidUriException extends ParseUriException
{
    protected $message = 'Invalid Uri';
}