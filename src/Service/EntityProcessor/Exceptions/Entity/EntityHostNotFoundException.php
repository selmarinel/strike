<?php

namespace App\Service\EntityProcessor\Exceptions\Entity;


use Symfony\Component\HttpFoundation\Response;

class EntityHostNotFoundException extends EntityException
{
    protected $code = Response::HTTP_NOT_FOUND;
}