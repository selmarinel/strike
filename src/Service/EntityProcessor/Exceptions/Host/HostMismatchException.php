<?php

namespace App\Service\EntityProcessor\Exceptions\Host;


class HostMismatchException extends HostException
{
    protected $message = 'Host Mismatch Exception';
}