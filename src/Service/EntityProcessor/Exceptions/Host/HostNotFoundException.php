<?php

namespace App\Service\EntityProcessor\Exceptions\Host;


use Symfony\Component\HttpFoundation\Response;

class HostNotFoundException extends HostMismatchException
{
    protected $message = 'Host Not Found';

    protected $code = Response::HTTP_NOT_FOUND;
}