<?php

namespace App\Service\EntityProcessor\Exceptions\Host;


class InvalidHostParametersException extends HostException
{
    protected $message = 'Host parameters are invalid';
}