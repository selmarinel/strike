<?php

namespace App\Service\EntityProcessor\Exceptions\LinkGenerator;


class LinkGenerateLogicalException extends \Exception
{
    protected $message = 'Not supported types in your host for this entity';
}