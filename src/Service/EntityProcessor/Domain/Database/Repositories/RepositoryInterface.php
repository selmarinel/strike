<?php

namespace App\Service\EntityProcessor\Domain\Database\Repositories;


interface RepositoryInterface
{
    public function findById($id);

    public function save(\Serializable $serializable): \Serializable;

    public function remove(\Serializable $serializable);
}