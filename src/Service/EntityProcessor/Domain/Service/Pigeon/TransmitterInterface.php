<?php

namespace App\Service\EntityProcessor\Domain\Service\Pigeon;


use App\Service\EntityProcessor\Domain\DAO\EntityDAOInterface;
use App\Service\EntityProcessor\Domain\DAO\HostDAOInterface;
use App\Service\EntityProcessor\Domain\VO\SlugRequestVOInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;

interface TransmitterInterface
{
    public function fetch(int $type, MigrateVOInterface $migrateVO);

    public function setParameters(TransmitterParametersVOInterface $transmitterParametersVO);

    public function fetchById(int $type, string $id);

    public function fetchSubItems(int $type, int $id, int $items);

    public function fetchAllWithLimitAndOffset(int $type, int $limit, int $offset);
}