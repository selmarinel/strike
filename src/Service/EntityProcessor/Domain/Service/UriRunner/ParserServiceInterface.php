<?php

namespace App\Service\EntityProcessor\Domain\Service\UriRunner;


interface ParserServiceInterface
{
    public function setTypeSlug(string $typeSlug);

    public function setSeparator(string $separator = '');

    public function setOrder(array $order = []);

    public function getPatternString(): string;

    public function getPatternArray(): array;
}