<?php

namespace App\Service\EntityProcessor\Domain\DTO;


use App\Service\EntityProcessor\Domain\DAO\HostDAOInterface;

interface UriParsedDTOInterface
{
    public function getId(): int;

    public function getTypeSlug(): string;

    public function getEntitySlug(): string;

    public function getEntityType(): int;

    public function isStatic(): bool;

    public function getHost(): HostDAOInterface;
}