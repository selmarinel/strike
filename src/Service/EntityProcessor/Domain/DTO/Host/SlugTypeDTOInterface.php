<?php

namespace App\Service\EntityProcessor\Domain\DTO\Host;


interface SlugTypeDTOInterface
{
    public function getPattern(): string;

    public function isStatic(): bool;

    public function getSlugType(): int;

    public function getTypeSlug():string;
}