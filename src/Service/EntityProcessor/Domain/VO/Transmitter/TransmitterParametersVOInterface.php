<?php

namespace App\Service\EntityProcessor\Domain\VO\Transmitter;


interface TransmitterParametersVOInterface
{
    public function getUserName(): string;

    public function setUserName(string $userName): void;

    public function getPassword(): string;

    public function setPassword(string $password): void;
}