<?php

namespace App\Service\EntityProcessor\Domain\DAO;


interface EntityToHostDAOInterface
{
    public function getId(): int;

    public function getHostId(): int;

    public function getEntityId(): ?string;

    public function getEntitySlug(): string;

    public function getChildrenIds(): array;
}