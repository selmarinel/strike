<?php

namespace App\Service\EntityProcessor\Domain\DAO;


use App\Service\EntityProcessor\Domain\DTO\Host\SlugTypeDTOInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

interface HostDAOInterface
{
    /** @return int */
    public function getHostId(): int;

    /** @return string */
    public function getHostname(): string;

    /** @return bool */
    public function isChildren(): bool;

    /** todo its not very good */
    /** @return Host */
    public function getHostEntity(): Host;

    /**@return SlugTypeDTOInterface[] */
    public function getSlugTypesData(): array;
}