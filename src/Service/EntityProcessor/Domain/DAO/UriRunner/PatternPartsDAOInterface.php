<?php

namespace App\Service\EntityProcessor\Domain\DAO\UriRunner;


interface PatternPartsDAOInterface
{
    public function getSlugPart(array $orderList): array;
}