<?php

namespace App\Service\EntityProcessor\Infrastructure\EventDispatcher;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Symfony\Component\EventDispatcher\Event;

class CreateChildHostEvent extends Event
{
    private $host;

    private $force = false;

    public function setHost(Host $host)
    {
        $this->host = $host;
    }

    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @return bool
     */
    public function isForce(): bool
    {
        return $this->force;
    }

    /**
     * @param bool $force
     */
    public function setForce(bool $force): void
    {
        $this->force = $force;
    }
}