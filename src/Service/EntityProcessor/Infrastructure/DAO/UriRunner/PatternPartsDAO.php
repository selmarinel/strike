<?php

namespace App\Service\EntityProcessor\Infrastructure\DAO\UriRunner;


use App\Service\EntityProcessor\Domain\DAO\UriRunner\PatternPartsDAOInterface;
use App\Service\EntityProcessor\Exceptions\UriRunner\InvalidPatternException;
use App\Service\EntityProcessor\Infrastructure\DTO\UriParsedDTO;

class PatternPartsDAO implements PatternPartsDAOInterface
{
    const AVAILABLE_CONSTANTS = [
        UriParsedDTO::TYPE_SLUG => 'typeSlug',
        UriParsedDTO::ENTITY_SLUG => 'entitySlug',
        UriParsedDTO::ENTITY_HOST_ID => 'identifier',
    ];

    private $typeSlug;

    private $entitySlug;

    private $identifier;

    /**
     * PatternPartsDAO constructor.
     * @param string|null $slug
     * @throws InvalidPatternException
     */
    public function __construct(string $slug = null)
    {
        $this->typeSlug = $this->setPartTypeSlug($slug);
        $entitySlugPrefix = UriParsedDTO::ENTITY_SLUG;
        $this->entitySlug = "(?<$entitySlugPrefix>[a-zA-Z0-9_-]+)";
        $identifierPrefix = UriParsedDTO::ENTITY_HOST_ID;
        $this->identifier = "(?<$identifierPrefix>\\d+)";
    }

    /**
     * @param string $typeSlug
     * @return string
     * @throws InvalidPatternException
     */
    private function setPartTypeSlug(string $typeSlug = '')
    {
        if (!$typeSlug) {
            throw new InvalidPatternException();
        }
        $typeSlugPrefix = UriParsedDTO::TYPE_SLUG;
        return "(?<$typeSlugPrefix>$typeSlug)";
    }

    /**
     * @param array $orderList
     * @return array
     */
    public function getSlugPart(array $orderList): array
    {
        $parts = [];
        foreach ($orderList as $item) {
            $var = self::AVAILABLE_CONSTANTS[$item];
            $parts[] = $this->$var;
        }
        return $parts;
    }
}