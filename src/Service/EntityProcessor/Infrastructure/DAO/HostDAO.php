<?php

namespace App\Service\EntityProcessor\Infrastructure\DAO;


use App\Service\EntityProcessor\Domain\DAO\HostDAOInterface;
use App\Service\EntityProcessor\Domain\DTO\Host\SlugTypeDTOInterface;
use App\Service\EntityProcessor\Exceptions\Host\InvalidHostParametersException;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;

class HostDAO implements HostDAOInterface
{
    /** @var Host */
    private $host;

    const SLUG_TYPES = 'slug_types';

    public function __construct(Host $host)
    {
        $this->host = $host;
    }

    /**
     * @return Host
     */
    public function getHostEntity(): Host
    {
        return $this->host;
    }

    public function getHostId(): int
    {
        return $this->host->getId();
    }

    public function isChildren(): bool
    {
        return !is_null($this->host->getParentId());
    }


    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->host->getHostname();
    }

    /**
     * @return SlugTypeDTOInterface[]
     * @throws InvalidHostParametersException
     */
    public function getSlugTypesData(): array
    {
        $data = $this->host->getData();

        if (!isset($data[static::SLUG_TYPES])) {
            throw new InvalidHostParametersException;
        }
        $slugTypes = [];
        foreach ($data[static::SLUG_TYPES] as $index => $property) {
            try {
                $slugType = new SlugTypeDTO();
                $slugType->setPattern($property[SlugTypeDTO::PATTERN_FIELD]);
                $slugType->setStatic((bool)$property[SlugTypeDTO::STATIC_FIELD]);
                $slugType->setSlugType((int)$property[SlugTypeDTO::SLUG_TYPE]);
                $slugTypes[] = $slugType;
            } catch (\Exception $exception) {
                throw new InvalidHostParametersException;
            }
        }
        return $slugTypes;
    }
}