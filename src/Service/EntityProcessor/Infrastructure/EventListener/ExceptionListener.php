<?php

namespace App\Service\EntityProcessor\Infrastructure\EventListener;


use App\Service\Encryption\Exceptions\InvalidHashInput;
use App\Service\EntityProcessor\Exceptions\Entity\EntityHostNotFoundException;
use App\Service\EntityProcessor\Exceptions\Entity\EntityNotFoundException;
use App\Service\EntityProcessor\Exceptions\Host\HostMismatchException;
use App\Service\EntityProcessor\Exceptions\Host\HostNotFoundException;
use App\Service\EntityProcessor\Exceptions\ParseURI\InvalidUriException;
use App\Service\EntityProcessor\Exceptions\UriRunner\InvalidPatternException;
use App\Service\EntityProcessor\Infrastructure\Service\Pigeon\Exception\PigeonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Twig\Environment;

class ExceptionListener
{
    private $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        if ($event->getException() instanceof InvalidPatternException) {
            $response = new JsonResponse(['error' => $event->getException()->getMessage()],
                $event->getException()->getCode());
            return $event->setResponse($response);
        }

        if ($event->getException() instanceof HostNotFoundException
            || $event->getException() instanceof EntityNotFoundException
            || $event->getException() instanceof EntityHostNotFoundException
            || $event->getException() instanceof InvalidHashInput
        ) {
            $response = new Response('', Response::HTTP_NOT_FOUND);
            return $event->setResponse($response);
        }

        if ($event->getException() instanceof PigeonException) {
            $response = new JsonResponse(['message' => $event->getException()->getMessage()],
                Response::HTTP_BAD_REQUEST);
            return $event->setResponse($response);
        }

        if ($event->getException() instanceof InvalidUriException ||
            $event->getException() instanceof HostMismatchException) {
            $response = new Response('', Response::HTTP_NOT_FOUND);
            return $event->setResponse($response);
        }

    }
}