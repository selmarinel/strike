<?php

namespace App\Service\EntityProcessor\Infrastructure\EventListener;


use App\Service\EntityProcessor\Infrastructure\DAO\HostDAO;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityProcessor\Infrastructure\EventDispatcher\CreateChildHostEvent;

class CreateChildHostListener
{
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(HostRepository $hostRepository)
    {
        $this->hostRepository = $hostRepository;
    }

    /**
     * @param CreateChildHostEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onCreateHost(CreateChildHostEvent $event)
    {
        $host = $event->getHost();
        if ($host->getParent()) {
            $data = $host->getData();
            if (empty($data[HostDAO::SLUG_TYPES])) {
                $host->setData($host->getParent()->getData());
                $host->setType($host->getParent()->getType());
                $host->setDriver($host->getParent()->getDriver());
                $this->hostRepository->simpleSave($host);
            }
        } else {
            $children = $host->getChildren();
            foreach ($children as $child) {
                $child->setData($host->getData());
                $child->setDriver($host->getDriver());
                $this->hostRepository->simpleSave($child);
            }
        }
    }
}