<?php

namespace App\Service\EntityProcessor\Infrastructure\EventListener;

use App\Service\Administrate\Infrastructure\Repository\EntityType\EntityTypeRepository;
use App\Service\EntityProcessor\Infrastructure\EventDispatcher\CreateChildHostEvent;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\PattedDecorator\Infrastructure\Database\Repository\MacrosRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;

class CreateHostTemplatesListener
{
    /** @var MacrosRepository */
    private $macrosRepository;

    public function __construct(
        MacrosRepository $macrosRepository
    )
    {
        $this->macrosRepository = $macrosRepository;
    }

    /**
     * @param CreateChildHostEvent $event
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onCreateHost(CreateChildHostEvent $event)
    {
        $entityTypeRepository = new EntityTypeRepository();
        $availableTypes = $entityTypeRepository->getEntityTypesByHostType($event->getHost()->getType());

        $host = $event->getHost();
        $parentHost = $host->getParent();

        foreach ($availableTypes as $type => $dto) {
            //если нету парента, значит хост парент
            if ($parentHost) {
                $parentMacros = $this->macrosRepository->findByHostIdAndEntityType($parentHost->getId(), $type);
            } else {
                $parentMacros = $this->macrosRepository->findByHostIdAndEntityType($host->getId(), $type);
            }

            if(!$parentMacros){
                $parentMacros = new Macros();
                $parentMacros->setSeoData([]);
            }
            $macros = $this->macrosRepository->findByHostIdAndEntityType($host->getId(), $type);


            if (!$macros) {
                $macros = new Macros();
                $macros->setHost($event->getHost());
                $macros->setEntityType($type);
                $macros->setSeoData($parentMacros->getSeoData());
            } else {
                if (empty($macros->getSeoData()) || $event->isForce()) {
                    $macros->setSeoData($parentMacros->getSeoData());
                }
            }
            $this->macrosRepository->save($macros);
        }
    }
}