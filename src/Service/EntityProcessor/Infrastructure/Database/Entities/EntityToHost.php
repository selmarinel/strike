<?php

namespace App\Service\EntityProcessor\Infrastructure\Database\Entities;

use Doctrine\ORM\Mapping as ORM;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=EntityToHostRepository::class)
 * @ORM\Table(
 *     indexes={@Index(name="search_slug",columns={"host_id","entity_slug"})},
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_entity_id_slug_host", columns={"entity_id", "entity_slug", "host_id"})
 *     }
 *)
 */
class EntityToHost implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $createTs;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $entity_slug;

    /**
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $entity = null;

    public function __construct()
    {
        $this->createTs = time();
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreateTs(): ?int
    {
        return $this->createTs;
    }

    public function setCreateTs(int $createTs): void
    {
        $this->createTs = $createTs;
    }

    /**
     * @return string
     */
    public function getEntitySlug(): string
    {
        return $this->entity_slug;
    }

    /**
     * @param string $entity_slug
     */
    public function setEntitySlug(string $entity_slug): void
    {
        $this->entity_slug = $entity_slug;
    }

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost(Host $host): void
    {
        $this->host = $host;
    }

    /**
     * @return Entity
     */
    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity|null $entity
     */
    public function setEntity(Entity $entity = null): void
    {
        $this->entity = $entity;
    }

    public function serialize()
    {
        return [
            'id' => $this->getId(),
            'entity_slug' => $this->getEntitySlug(),
            'entity' => $this->getEntity()->getId(),
            'host' => $this->getHost()->getHostname(),
            'created' => $this->getCreateTs()
        ];
    }

    public function unserialize($serialized)
    {
        if (!is_array($serialized)) {
            return false;
        }
        if (isset($serialized['entity_slug'])) {
            $this->setEntitySlug($serialized['entity_slug']);
        }
        if (isset($serialized['host']) && $serialized['host'] instanceof Host) {
            $this->setHost($serialized['host']);
        }
        if (isset($serialized['entity']) && $serialized['entity'] instanceof Entity) {
            $this->setEntity($serialized['entity']);
        }
    }
}
