<?php

namespace App\Service\EntityProcessor\Infrastructure\Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\Base\Infrastructure\Entity\Tag;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass=EntitiesRepository::class)
 *
 * @ORM\Table(
 *     indexes={
 *          @Index(name="type_idx",columns={"entity_type"})
 *     },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_type_source_id", columns={"entity_type", "source_id"})
 *     }
 * )
 */
class Entity implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     */
    private $id;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $entity_type = 0;
    /**
     * @var string
     * @ORM\Column(type="string",length=255, nullable=true)
     */
    private $source_id = null;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $createTs;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $score = 0;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @ManyToMany(targetEntity=Tag::class)
     * @JoinTable(name="entities_to_tags",
     *      joinColumns={@JoinColumn(name="entity_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;

    public function __construct()
    {
        $this->createTs = time();
        $this->tags = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEntityType(): int
    {
        return $this->entity_type;
    }

    /**
     * @param int $entity_type
     */
    public function setEntityType(int $entity_type): void
    {
        $this->entity_type = $entity_type;
    }

    /**
     * @return string
     */
    public function getSourceId(): string
    {
        return $this->source_id;
    }

    /**
     * @param string $source_id
     */
    public function setSourceId(string $source_id): void
    {
        $this->source_id = $source_id;
    }

    /**
     * @return int
     */
    public function getCreateTs(): int
    {
        return $this->createTs;
    }

    /**
     * @param int $createTs
     */
    public function setCreateTs(int $createTs): void
    {
        $this->createTs = $createTs;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function serialize()
    {
        return [
            'id' => $this->getId(),
            'entity_type' => $this->getEntityType(),
            'name' => $this->getName(),
            'source_id' => $this->getSourceId(),
            'created' => $this->getCreateTs()
        ];
    }

    public function unserialize($serialized)
    {
        if (!is_array($serialized)) {
            return false;
        }
        if (isset($serialized['entity_type'])) {
            $this->setEntityType($serialized['entity_type']);
        }
        if (isset($serialized['source_id'])) {
            $this->setSourceId((string)$serialized['source_id']);
        }

        if (isset($serialized['name'])) {
            $this->setName($serialized['name']);
        }

        if (isset($serialized['score'])) {
            $this->setScore($serialized['score']);
        }

        if (!$this->createTs) {
            $this->setCreateTs(time());
        }
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score)
    {
        $this->score = $score;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
    }
}
