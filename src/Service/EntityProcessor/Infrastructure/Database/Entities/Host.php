<?php

namespace App\Service\EntityProcessor\Infrastructure\Database\Entities;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Service\Base\Infrastructure\Entity\Redirect\HostGeoRedirect; //todo
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 *
 * @ORM\Entity(repositoryClass=HostRepository::class)
 * @ORM\Table(
 *     indexes={@ORM\Index(name="hostname_idx", columns={"hostname"})},
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_geo_hostname", columns={"hostname", "geo"})
 *     }
 * )
 * @deprecated
 */
class Host implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $hostname;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $geo;

    /**
     * @var array
     * @OneToMany(targetEntity=Host::class, mappedBy="parent")
     */
    private $children;

    /**
     * @var Host
     * @ManyToOne(targetEntity=Host::class, inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent = null;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $abuse_level;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    private $textGenerateSettings = [];

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type = 0;
    /**
     * @var string
     * @ORM\Column()
     */
    private $driver = '';

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default":1})
     */
    private $status = 1;

    /**
     * exists parent - is child
     * not exists parent - is parent
     * @return bool
     */
    public function isParent()
    {
        return is_null($this->getParent());
    }

    /**
     * not parent is child
     * @return bool
     */
    public function isChild()
    {
        return !$this->isParent();
    }

    /**
     * Host constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->textGenerateSettings = [];
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function setParent(Host $host)
    {
        $this->parent = $host;
    }

    /**
     * @return null|Host
     */
    public function getParent(): ?Host
    {
        return $this->parent;
    }

    /**
     * @return Host[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname): void
    {
        $this->hostname = $hostname;
    }

    /**
     * @return string|null
     */
    public function getGeo(): ?string
    {
        return $this->geo;
    }

    /**
     * @param string|null $geo
     */
    public function setGeo(?string $geo): void
    {
        $this->geo = mb_strtoupper($geo);
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return ($this->parent) ? $this->parent->getId() : null;
    }

    /**
     * @return int
     */
    public function getAbuseLevel(): int
    {
        return $this->abuse_level;
    }

    /**
     * @param int $abuse_level
     */
    public function setAbuseLevel(int $abuse_level): void
    {
        $this->abuse_level = $abuse_level;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getTextGenerateSettings(): array
    {
        $settings = $this->textGenerateSettings;
        if (!$settings) {
            return [];
        }
        return $settings;
    }

    /**
     * @param array $textGenerateSettings
     */
    public function setTextGenerateSettings(array $textGenerateSettings)
    {
        $this->textGenerateSettings = $textGenerateSettings;
    }

    public function serialize()
    {
        return [
            'id' => $this->getId(),
            'hostname' => $this->getHostname(),
            'geo' => $this->getGeo(),
            'abuse_level' => $this->getAbuseLevel(),
            'parent_id' => $this->getParentId(),
            'data' => $this->getData(),
            'type' => $this->getType(),
            'driver' => $this->getDriver(),
            'status' => (bool)$this->getStatus()
        ];
    }

    public function unserialize($serialized)
    {
        if (!is_array($serialized)) {
            return false;
        }
        if (isset($serialized['hostname'])) {
            $this->setHostname($serialized['hostname']);
        }
        if (isset($serialized['geo'])) {
            $this->setGeo($serialized['geo']);
        }
        if (isset($serialized['parent'])) {
            $this->setParent($serialized['parent']);
        }
        if (isset($serialized['data'])) {
            $this->setData($serialized['data']);
        }
        if (isset($serialized['abuse_level'])) {
            $this->setAbuseLevel((int)$serialized['abuse_level']);
        }
        if (isset($serialized['type'])) {
            $this->setType((int)$serialized['type']);
        }
        if (isset($serialized['driver'])) {
            $this->setDriver($serialized['driver']);
        }
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        if (!in_array($this->type, [
            Type::HOST_TYPE_AUDIO,
            Type::HOST_TYPE_VIDEO,
            Type::HOST_TYPE_MOVIE,
            Type::HOST_TYPE_NEWS,
        ])
        ) {
            return Type::HOST_TYPE_AUDIO;
        }
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDriver(): string
    {
        return $this->driver;
    }

    /**
     * @param string $driver
     */
    public function setDriver(string $driver): void
    {
        $this->driver = $driver;
    }
}
