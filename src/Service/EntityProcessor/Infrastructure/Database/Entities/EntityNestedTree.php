<?php

namespace App\Service\EntityProcessor\Infrastructure\Database\Entities;


use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="entity_nested_tree")
 * @ORM\Entity(repositoryClass="App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository")
 */
class EntityNestedTree
{

    const ROOT = 1;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer", name="lft")
     */
    private $lft;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer", name="rft")
     */
    private $rgt;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="EntityNestedTree", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */

    private $parent;

//    /**
//     * @Gedmo\TreeRoot
//     * @ORM\Column(type="integer", nullable=true)
//     */
//    private $root;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="EntityNestedTree")
     * @ORM\JoinColumn(name="root", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer", nullable=true)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="EntityNestedTree", mappedBy="parent")
     */
    private $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity): void
    {
        $this->entity = $entity;
    }


    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level): void
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param mixed $root
     */
    public function setRoot($root): void
    {
        $this->root = $root;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(EntityNestedTree $parent = null): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    public function __toString()
    {
        return (string)$this->getEntity()->getId();
    }

    public function __toArray()
    {
        $children = [];
        /** @var self $child */
        foreach ($this->getChildren() as $child) {
            $children[] = $child->__toArray();
        }
        return [
            'entity_id' => $this->getEntity()->getId(),
            'lft' => $this->getLft(),
            'rft' => $this->getRgt(),
            'level' => $this->getLevel(),
            '_children' => $children
        ];
    }
}