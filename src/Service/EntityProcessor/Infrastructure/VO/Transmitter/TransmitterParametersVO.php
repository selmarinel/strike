<?php

namespace App\Service\EntityProcessor\Infrastructure\VO\Transmitter;


use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;

class TransmitterParametersVO implements TransmitterParametersVOInterface
{
    /** @var string  */
    private $userName = '';
    /** @var string  */
    private $password = '';

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}