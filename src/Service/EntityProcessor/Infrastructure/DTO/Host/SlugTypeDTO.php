<?php

namespace App\Service\EntityProcessor\Infrastructure\DTO\Host;


use App\Service\EntityProcessor\Domain\DTO\Host\SlugTypeDTOInterface;
use App\Service\EntityProcessor\Exceptions\UriRunner\InvalidPatternException;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\ParserFactory;

/**
 * Class SlugTypeDTO
 * @package App\Service\EntityProcessor\Infrastructure\DTO\Host
 * @deprecated
 */
class SlugTypeDTO implements SlugTypeDTOInterface
{
    const SLUG_TYPE = 'slug_type';
    const STATIC_FIELD = 'static';

    const PATTERN_FIELD = 'pattern';
    const PATTERN_ORDER = 'order';
    const PATTERN_SEPARATOR = 'separator';
    const PATTERN_SLUG = 'slug';

    /** @var array */
    private $pattern;
    /** @var bool */
    private $static = false;

    /** @var int */
    private $slugType;

    /**
     * @param array $pattern
     * @throws InvalidPatternException
     */
    public function setPattern(array $pattern)
    {
        if (!isset($pattern[static::PATTERN_SLUG])) {
            throw new InvalidPatternException;
        }
        if (!isset($pattern[static::PATTERN_SEPARATOR])) {
            throw new InvalidPatternException;
        }
        if (!isset($pattern[static::PATTERN_ORDER])) {
            throw new InvalidPatternException;
        }

        $this->pattern = [
            static::PATTERN_SLUG => $pattern[static::PATTERN_SLUG],
            static::PATTERN_SEPARATOR => $pattern[static::PATTERN_SEPARATOR],
            static::PATTERN_ORDER => $pattern[static::PATTERN_ORDER],
        ];
    }

    /**
     * @param bool $static
     */
    public function setStatic(bool $static)
    {
        $this->static = $static;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        $service = ParserFactory::createParser($this->isStatic());
        $service->setTypeSlug($this->pattern[static::PATTERN_SLUG]);
        $service->setSeparator((string)$this->pattern[static::PATTERN_SEPARATOR]);
        $service->setOrder($this->pattern[static::PATTERN_ORDER]);
        return $service->getPatternString();
    }

    public function getTypeSlug(): string
    {
        return $this->pattern[static::PATTERN_SLUG];
    }

    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * @return int
     */
    public function getSlugType(): int
    {
        return $this->slugType;
    }

    /**
     * @param int $slugType
     */
    public function setSlugType(int $slugType): void
    {
        $this->slugType = $slugType;
    }
}