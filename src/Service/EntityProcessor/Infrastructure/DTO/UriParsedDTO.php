<?php

namespace App\Service\EntityProcessor\Infrastructure\DTO;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\DAO\HostDAOInterface;
use App\Service\EntityProcessor\Domain\DTO\UriParsedDTOInterface;

class UriParsedDTO implements UriParsedDTOInterface
{
    /** @var int */
    private $id = 0;
    /** @var string */
    private $typeSlug;
    /** @var string */
    private $entitySlug;
    /** @var int */
    private $entityType;
    /** @var HostDAOInterface */
    private $host;

    /** @var bool */
    private $static = false;

    const TYPE_SLUG = 'slug_type';

    const ENTITY_SLUG = 'entity_slug';

    const ENTITY_HOST_ID = 'id';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTypeSlug(): string
    {
        return $this->typeSlug;
    }

    /**
     * @param string $typeSlug
     */
    public function setTypeSlug(string $typeSlug): void
    {
        $this->typeSlug = $typeSlug;
    }

    /**
     * @return string
     */
    public function getEntitySlug(): string
    {
        return $this->entitySlug;
    }

    /**
     * @param string $entitySlug
     */
    public function setEntitySlug(string $entitySlug): void
    {
        $this->entitySlug = $entitySlug;
    }

    /**
     * @return int
     */
    public function getEntityType(): int
    {
        return $this->entityType;
    }

    /**
     * @param int $entityType
     */
    public function setEntityType(int $entityType): void
    {
        $this->entityType = $entityType;
    }

    /**
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * @param bool $static
     */
    public function setStatic(bool $static): void
    {
        $this->static = $static;
    }

    /**
     * @return HostDAOInterface
     */
    public function getHost(): HostDAOInterface
    {
        return $this->host;
    }

    /**
     * @param HostDAOInterface $hostDAO
     */
    public function setHost(HostDAOInterface $hostDAO)
    {
        $this->host = $hostDAO;
    }

    /**
     * @param array $params
     * @param int $type
     * @return UriParsedDTO
     */
    public static function createFromParsed(array $params, int $type)
    {
        $self = new self();
        $self->setEntityType((int)$type);
        if (isset($params[static::ENTITY_SLUG])) {
            $self->setEntitySlug($params[static::ENTITY_SLUG]);
        }
        if (isset($params[static::TYPE_SLUG])) {
            $self->setTypeSlug($params[static::TYPE_SLUG]);
        }
        if (isset($params[static::ENTITY_HOST_ID])) {
            $self->setId((int)$params[static::ENTITY_HOST_ID]);
        }
        return $self;
    }

    public static function createRoot(HostDAOInterface $hostDAO)
    {
        $self = new self();
        $self->setEntityType(Type::ROOT);
        $self->setStatic(true);
        $self->setHost($hostDAO);
        return $self;
    }
}
