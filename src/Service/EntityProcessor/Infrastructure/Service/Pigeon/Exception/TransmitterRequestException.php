<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\Pigeon\Exception;


class TransmitterRequestException extends PigeonException
{
    protected $message = 'Transmitter Exception';
}