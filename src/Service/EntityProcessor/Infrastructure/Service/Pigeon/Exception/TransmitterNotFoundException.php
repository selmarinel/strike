<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\Pigeon\Exception;


use Symfony\Component\HttpFoundation\Response;

class TransmitterNotFoundException extends TransmitterRequestException
{
    protected $code = Response::HTTP_NOT_FOUND;
}