<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\Pigeon;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Service\Pigeon\Exception\TransmitterRequestException;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\VO\Receiver\GetReceiverParametersInterface;
use GuzzleHttp\ClientInterface;

class Transmitter implements TransmitterInterface
{
    /** @var ClientInterface */
    protected $client;
    /** @var EntityNestedTreeRepository */
    private $nestedTreeRepository;
    /** @var TransmitterParametersVOInterface */
    private $parameters;
    /** @var ReceiveMapInterface */
    private $map;

    /**
     * Transmitter constructor.
     * @param ClientInterface $client
     * @param EntityNestedTreeRepository $nestedTreeRepository
     * @param TransmitterParametersVOInterface $transmitterParametersVO
     * @param ReceiveMapInterface $map
     */
    public function __construct(
        ClientInterface $client,
        EntityNestedTreeRepository $nestedTreeRepository,
        TransmitterParametersVOInterface $transmitterParametersVO,
        ReceiveMapInterface $map
    )
    {
        $this->client = $client;
        $this->nestedTreeRepository = $nestedTreeRepository;
        $this->parameters = $transmitterParametersVO;
        $this->map = $map;
    }

    public function setParameters(TransmitterParametersVOInterface $transmitterParametersVO)
    {
        $this->parameters = $transmitterParametersVO;
    }

    /**
     * @param int $type
     * @param MigrateVOInterface $migrateVO
     * @return mixed
     * @throws TransmitterRequestException
     */
    public function fetch(int $type, MigrateVOInterface $migrateVO)
    {
        $entityDTO = Type::DTO_MAP[$type]['class'];
        /** @var DTOInterface $DTO */
        $DTO = new $entityDTO();
        /** @var GetReceiverParametersInterface $params */
        $params = $this->map->map($DTO);
        $hostToFetch = "{$params->getHost()}fetch";
        $authParameters = [
            'auth' => [$params->getUserName(), $params->getPassword()],
            'form_params' => [
                'name' => $migrateVO->getTitle(),
                'id' => $migrateVO->getId()
            ]
        ];

        $response = $this->client->post($hostToFetch, $authParameters);

        $data = json_decode($response->getBody()->getContents(), true);
        /** todo make dynamicly fetched data or modify api */
        if (in_array($type, [Type::DEEZER_AUDIO_ARTIST, Type::DEEZER_AUDIO_ALBUM, Type::DEEZER_AUDIO_TRACK])) {
            return $data;
        }
        if (!$data['records']) {
            throw new TransmitterRequestException;
        }
        return array_shift($data['records']);
    }

    /**
     * @param int $type
     * @param int $id
     * @param int $items
     * @return null
     */
    public function fetchSubItems(int $type, int $id, int $items)
    {
        $entityDTO = Type::DTO_MAP[$type]['class'];
        /** @var DTOInterface $DTO */
        $DTO = new $entityDTO();
        /** @var GetReceiverParametersInterface $params */
        $params = $this->map->map($DTO);
        $hostToFetch = "{$params->getHost()}";
        $authParameters = [
            'auth' => [$params->getUserName(), $params->getPassword()],
        ];

        $map = [
            Type::AUDIO_ALBUM => 'albums',
            Type::AUDIO_TRACK => 'tracks',
        ];

        $items = $map[$items];

        $hostToFetch = "{$hostToFetch}$id/$items";
        $response = $this->client->get($hostToFetch, $authParameters);
        $data = json_decode($response->getBody()->getContents(), true);

        if (!$data['records']) {
            return null;
        }
        return $data['records'];
    }

    /**
     * @param int $type
     * @param string $id
     * @return null
     */
    public function fetchById(int $type, string $id)
    {
        $entityDTO = Type::DTO_MAP[$type]['class'];

        /** @var DTOInterface $DTO */
        $DTO = new $entityDTO();

        /** @var GetReceiverParametersInterface $params */
        $params = $this->map->map($DTO);
        $hostToFetch = "{$params->getHost()}";
        $authParameters = [
            'auth' => [$params->getUserName(), $params->getPassword()],
        ];

        $hostToFetch = "{$hostToFetch}$id";

        $response = $this->client->get($hostToFetch, $authParameters);
        $data = json_decode($response->getBody()->getContents(), true);
        //todo refact
        if (in_array($type, [Type::DEEZER_AUDIO_ARTIST, Type::DEEZER_AUDIO_ALBUM, Type::DEEZER_AUDIO_TRACK])) {
            return $data;
        }

        if (!$data['records']) {
            return null;
        }
        return array_shift($data['records']);
    }

    public function fetchAllWithLimitAndOffset(int $type, int $limit, int $offset)
    {
        $entityDTO = Type::DTO_MAP[$type]['class'];
        /** @var DTOInterface $DTO */
        $DTO = new $entityDTO();
        $params = $this->map->map($DTO);
        $hostToFetch = "{$params->getHost()}all?limit={$limit}&offset={$offset}";
        $authParameters = [
            'auth' => [$params->getUserName(), $params->getPassword()],
        ];

        $response = $this->client->get($hostToFetch, $authParameters);
        $data = json_decode($response->getBody()->getContents(), true);

        if (!$data['records']) {
            return null;
        }

        return $data['records'];
    }
}