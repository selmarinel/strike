<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers;


use App\Service\EntityProcessor\Domain\Service\UriRunner\ParserServiceInterface;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\DTO\UriParsedDTO;

class StaticParserService extends AbstractParser implements ParserServiceInterface
{
    protected $separator = '';

    public function setTypeSlug(string $typeSlug)
    {
        $this->typeSlug = $typeSlug;
    }

    public function setSeparator(string $separator = '')
    {
        $this->separator = '';
    }

    public function setOrder(array $order = [])
    {
        $this->order = [];
    }

    public function getPatternString(): string
    {
        $typeSlugPrefix = UriParsedDTO::TYPE_SLUG;
        $slug = "(?<$typeSlugPrefix>{$this->typeSlug})";
        return static::BEGIN_PATTERN . $slug . static::END_PATTERN;
    }

    public function getPatternArray(): array
    {
        return [
            SlugTypeDTO::PATTERN_SLUG => $this->getTypeSlug(),
            SlugTypeDTO::PATTERN_ORDER => $this->getOrder(),
            SlugTypeDTO::PATTERN_SEPARATOR => $this->getSeparator()
        ];
    }

}