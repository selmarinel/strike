<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers;


use App\Service\EntityProcessor\Infrastructure\DTO\UriParsedDTO;

abstract class AbstractParser
{
    const BEGIN_PATTERN = '/^';

    const END_PATTERN = '$/';

    const AVAILABLE_CONSTANTS = [
        UriParsedDTO::TYPE_SLUG => 'typeSlug',
        UriParsedDTO::ENTITY_SLUG => 'entitySlug',
        UriParsedDTO::ENTITY_HOST_ID => 'identifier',
    ];

    /** @var string */
    protected $typeSlug;
    /** @var string */
    protected $separator;
    /** @var array */
    protected $order = [];

    /**
     * @return mixed
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param array $order
     */
    public function setOrder(array $order = [])
    {
        $this->order = $order;
    }

    /**
     * @return array
     */
    public function getOrder(): array
    {
        return $this->order;
    }

    /**
     * @param string $typeSlug
     */
    public function setTypeSlug(string $typeSlug)
    {
        $this->typeSlug = $typeSlug;
    }

    /**
     * @return mixed
     */
    public function getTypeSlug()
    {
        return $this->typeSlug;
    }
}