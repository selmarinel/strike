<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers;


use App\Service\EntityProcessor\Domain\Service\UriRunner\ParserServiceInterface;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\DTO\UriParsedDTO;

class ArticleParserService extends AbstractParser implements ParserServiceInterface
{

    public function setSeparator(string $separator = '')
    {
        $this->separator = '';
    }

    public function getPatternString(): string
    {
        $entitySlugPrefix = UriParsedDTO::ENTITY_SLUG;
        return static::BEGIN_PATTERN . "(?<$entitySlugPrefix>.+)" . static::END_PATTERN;
    }

    public function getPatternArray(): array
    {
        return [
            SlugTypeDTO::PATTERN_SLUG => $this->getTypeSlug(),
            SlugTypeDTO::PATTERN_ORDER => $this->getOrder(),
            SlugTypeDTO::PATTERN_SEPARATOR => $this->getSeparator()
        ];
    }
}