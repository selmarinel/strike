<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers;


use App\Service\EntityProcessor\Domain\Service\UriRunner\ParserServiceInterface;
use App\Service\EntityProcessor\Exceptions\UriRunner\InvalidPatternException;
use App\Service\EntityProcessor\Infrastructure\DAO\UriRunner\PatternPartsDAO;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\DTO\UriParsedDTO;

class SearchParserService extends AbstractParser implements ParserServiceInterface
{

    const AVAILABLE_CONSTANTS = [
        UriParsedDTO::ENTITY_SLUG => 'entitySlug',
        UriParsedDTO::TYPE_SLUG => 'typeSlug'
    ];

    /**
     * @param string $separator
     * @throws InvalidPatternException
     */
    public function setSeparator(string $separator = '')
    {
        if (!$separator) {
            throw new InvalidPatternException;
        }
        $this->separator = $separator[0];
    }

    /**
     * @return string
     * @throws InvalidPatternException
     */
    public function getPartSeparator()
    {
        if (!$this->separator) {
            throw new InvalidPatternException();
        }
        return "\\{$this->separator}";
    }

    /**
     * @return array
     */
    public function makeOrdering()
    {
        $order = [];
        $availableConstants = array_keys(self::AVAILABLE_CONSTANTS);

        foreach ($this->order as $item) {
            if (in_array($item, $availableConstants)) {
                $order[] = $item;
            }
        }

        //validate and setup default
        foreach ($availableConstants as $availableConstant) {
            if (!in_array($availableConstant, $order)) {
                array_push($order, $availableConstant);
            }
        }
        return $order;
    }

    /**
     * @return string
     * @throws InvalidPatternException
     */
    public function getPatternString(): string
    {
        $orderList = $this->makeOrdering();
        $parts = (new PatternPartsDAO($this->typeSlug))->getSlugPart($orderList);
        return static::BEGIN_PATTERN . implode($this->getPartSeparator(), $parts) . static::END_PATTERN;
    }

    /**
     * @return array
     */
    public function getPatternArray(): array
    {
        return [
            SlugTypeDTO::PATTERN_SLUG => $this->getTypeSlug(),
            SlugTypeDTO::PATTERN_ORDER => $this->getOrder(),
            SlugTypeDTO::PATTERN_SEPARATOR => $this->getSeparator()
        ];
    }
}