<?php

namespace App\Service\EntityProcessor\Infrastructure\Service\UriRunner;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\UriRunner\ParserServiceInterface;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers\ArticleCategoryParserService;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers\ArticleParserService;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers\DynamicParserService;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers\SearchParserService;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\Parsers\StaticParserService;

/**
 * Class ParserFactory
 * @package App\Service\EntityProcessor\Infrastructure\Service\UriRunner
 * @deprecated
 */
class ParserFactory
{
    const DYNAMIC = 0;
    const STATIC = 1;

    /**
     * @param bool $isStatic
     * @return ParserServiceInterface
     */
    public static function createParser(bool $isStatic = false): ParserServiceInterface
    {
        if ($isStatic) {
            return new StaticParserService();
        }
        return new DynamicParserService();
    }

    /**
     * @param bool $isStatic
     * @param int $type
     * @return ParserServiceInterface|SearchParserService
     */
    public static function createTypedParser(bool $isStatic = false, int $type)
    {
        switch ($type) {
            case Type::SEARCH_TYPE:
                return new SearchParserService;
            case Type::NEWS_ARTICLE:
                return new ArticleParserService;
            case Type::COLLECTION_NEWS_CATEGORY:
                return new ArticleCategoryParserService;
            default:
                return self::createParser($isStatic);
        }
    }
}