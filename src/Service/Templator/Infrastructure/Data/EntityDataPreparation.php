<?php

namespace App\Service\Templator\Infrastructure\Data;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\EntityExtensionText\EntityTextInterface;
use App\Service\Templator\Domain\Data\EntityDataPreparationInterface;
use App\Service\Templator\Domain\VO\MacrosVOInterface;

class EntityDataPreparation implements EntityDataPreparationInterface
{
    /** @var EntityTextInterface */
    private $entityTextService;

    public function __construct(EntityTextInterface $entityTextService)
    {
        $this->entityTextService = $entityTextService;
    }

    private const LIMITLESS_LIST = [6, 12, 24, 48, 96];

    public function prepare(GetHostInterface $host, DataObjectInterface $DTO): array
    {
        $data = [
            'domain' => $host->getHostname(),
            'limitless' => self::LIMITLESS_LIST
        ];
        $this->entityTextService->setHost($host);
        if ($DTO instanceof DTOInterface) {
            $this->entityTextService->extend($DTO);
        }
        return array_merge($data, $DTO->toArray());
    }

    public function seo(MacrosVOInterface $templateVO): array
    {
        return $templateVO->getData();
    }
}