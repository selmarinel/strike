<?php

namespace App\Service\Templator\Infrastructure\Template;

use App\Service\Base\Infrastructure\Repository\Templator\MacrosRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;

use App\Service\Templator\Domain\Template\Extensions\MacroExtensionInterface;
use App\Service\Templator\Domain\Template\MacrosParserInterface;
use App\Service\Templator\Domain\VO\MacrosVOInterface;
use App\Service\Templator\Infrastructure\Template\Extensions\ADIncluder\AdExtension;
use App\Service\Templator\Infrastructure\Template\Extensions\GlobalMetaTags\GlobalMetaTagsExtension;
use App\Service\Templator\Infrastructure\Template\Extensions\MetaSchema\MetaSchemaExtension;
use App\Service\Templator\Infrastructure\Template\Extensions\SeoGeo\SeoGeoExtension;
use App\Service\Templator\Infrastructure\VO\MacrosVO;

use Doctrine\ORM\NonUniqueResultException;
use Psr\Container\ContainerExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MacrosParser implements MacrosParserInterface
{
    /** @var MacrosRepository */
    private $macrosRepository;

    private $extensions = [
        SeoGeoExtension::SERVICE_NAME,
        GlobalMetaTagsExtension::SERVICE_NAME,
        AdExtension::SERVICE_NAME,
        MetaSchemaExtension::SERVICE_NAME,

    ];
    /** @var ContainerInterface */
    private $container;

    public function __construct(
        MacrosRepository $macrosRepository,
        ContainerInterface $container
    )
    {
        $this->macrosRepository = $macrosRepository;
        $this->container = $container;
    }

    /**
     * @param GetHostInterface $host
     * @param DataObjectInterface $DTO
     *
     * @return MacrosVOInterface
     * @throws NonUniqueResultException
     */
    public function getMacros(GetHostInterface $host, DataObjectInterface $DTO): MacrosVOInterface
    {
        $macros = $this->macrosRepository->findByHostIdAndEntityType(
            $host->getHostId(),
            $DTO->getEntityType()
        );

        if ($host->getParentId()) {
            $parentMacros = $this->macrosRepository->findByHostIdAndEntityType(
                $host->getParentId(),
                $DTO->getEntityType()
            );
            if (!$parentMacros) {
                $parentMacros = $this->macrosRepository->getDefault();
            }

            if (!$macros) {
                $macros = $parentMacros;
            }
            $macros->setSeoData(
                $this->mergeParentDataWithData(
                    $parentMacros->getSeoData(),
                    $macros->getSeoData()
                ));
        }

        if (!$macros) {
            $macros = $this->macrosRepository->getDefault($host->isStatic());
        }
        $vo = new MacrosVO();
        $vo->setData($macros->getSeoData());
        return $vo;
    }

    public function run(string &$content, GetHostInterface $host, array $data)
    {
        $this->appendExtensions($host, $data);
        $toReplace = [];
        $onReplace = [];
        foreach ($data as $macros => $value) {
            $toReplace[$macros] = "/\[\[$macros\]\]/";
            $onReplace[$macros] = $this->parseFork($value);
        }
        $content = preg_replace($toReplace, $onReplace, $content);
        $content = preg_replace('/\[\[[A-Za-z0-9_]+\]\]/', '', $content);
    }

    private function mergeParentDataWithData(array $parent, array $data): array
    {
        $dataToSave = $parent;
        foreach ($data as $key => $value) {
            if (!$value) {
                continue;
            }
            $dataToSave[$key] = $value;
        }

        return $dataToSave;
    }

    /**
     * @param GetHostInterface $host
     * @param array $data
     */
    private function appendExtensions(GetHostInterface $host, array &$data)
    {
        foreach ($this->extensions as $extension) {
            try {
                /** @var MacroExtensionInterface $extension */
                $extension = $this->container->get($extension);
                if ($extension) {
                    $extension->appendExtension($host, $data);
                }
            } catch (ContainerExceptionInterface $e) {
                //pass
            }
        }
    }

    /**
     * Process nested variation placeholders:
     *
     * random                 -> [!хуй|пизда|жигурда!]
     * implode                -> [% [!1|2|3|4|5!] | пизда| жигурда %]*
     * implode with separator -> [#,# хуй | пизда| жигурда #]
     *
     * @param string $value
     * @return string
     */
    private function parseFork(string $value)
    {
        if (preg_match('/(\[!|\[%|\[#)/u', $value)) {
            $value = html_entity_decode($value);
            $i = 0;

            while (($i++) <= 3) {
                if (!preg_match('/(\[!|\[%|\[#)/u', $value)) {
                    break;
                }

                if (preg_match_all('/\[!([^!#;]*)!\]/u', $value, $pocketList)) {
                    foreach ($pocketList[0] as $pKey => $pPattern) {
                        if (preg_match('/|/u', $pPattern)) {
                            $list = explode('|', $pocketList[1][$pKey]);
                            $value = str_replace('[!' . $pocketList[1][$pKey] . '!]', $list[array_rand($list)], $value);
                        }
                    }
                }

                if (preg_match_all('/\[%([^!#;]*)%\]/u', $value, $pocketList)) {
                    foreach ($pocketList[0] as $pKey => $pPattern) {
                        if (preg_match('/|/u', $pPattern)) {
                            $list = explode('|', $pocketList[1][$pKey]);
                            shuffle($list);
                            $value = str_replace('[%' . $pocketList[1][$pKey] . '%]', implode('', $list), $value);
                        }
                    }
                }

                if (preg_match_all('/\[#(.)#([^!#;]*)#\]/u', $value, $pocketList)) {
                    foreach ($pocketList[0] as $pKey => $pPattern) {
                        if (preg_match('/|/u', $pPattern)) {
                            $list = explode('|', $pocketList[2][$pKey]);
                            shuffle($list);
                            $value = str_replace('[#' . $pocketList[1][$pKey] . '#' . $pocketList[2][$pKey] . '#]', implode($pocketList[1][$pKey], $list), $value);
                        }
                    }
                }
            }
        }
        return $value;
    }
}