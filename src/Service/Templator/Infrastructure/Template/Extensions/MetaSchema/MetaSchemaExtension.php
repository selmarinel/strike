<?php

namespace App\Service\Templator\Infrastructure\Template\Extensions\MetaSchema;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Template\Extensions\MacroExtensionInterface;

class MetaSchemaExtension implements MacroExtensionInterface
{
    const MACRO_NAME = 'seo_meta_schema';

    const SERVICE_NAME = 'templator.widgets.seo.meta.schema';

    public function appendExtension(GetHostInterface $host, array &$data)
    {
        $data[self::MACRO_NAME] = '{% if __meta is defined %}{{ __meta | raw }}{% endif %}';
    }
}