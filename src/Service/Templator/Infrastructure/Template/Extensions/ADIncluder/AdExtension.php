<?php

namespace App\Service\Templator\Infrastructure\Template\Extensions\ADIncluder;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Template\Extensions\MacroExtensionInterface;

class AdExtension implements MacroExtensionInterface
{
    const MACRO_NAME_PART = 'ad_block_';

    const SERVICE_NAME = 'templator.widgets.ad';

    public function appendExtension(GetHostInterface $host, array &$data)
    {
        $data[self::MACRO_NAME_PART . 'top'] = "<!--# include virtual=\"/ad/top\" -->";
        $data[self::MACRO_NAME_PART . 'bottom'] = "<!--# include virtual=\"/ad/bottom\" -->";
        $data[self::MACRO_NAME_PART . 'context'] = "<!--# include virtual=\"/ad/context\" -->";
    }
}