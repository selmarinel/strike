<?php


namespace App\Service\Templator\Infrastructure\Template\Extensions\GlobalMetaTags;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Template\Extensions\GlobalMetaTags\Service\GlobalMetaTagsInterface;
use App\Service\Templator\Domain\Template\Extensions\MacroExtensionInterface;

class GlobalMetaTagsExtension implements MacroExtensionInterface
{
    const MACRO_NAME = 'seo_global_meta_tags';

    const SERVICE_NAME = 'templator.widgets.global.meta.tags';
    /** @var GlobalMetaTagsInterface */
    private $service;


    public function __construct(GlobalMetaTagsInterface $metaTags)
    {
        $this->service = $metaTags;
    }

    public function appendExtension(GetHostInterface $host, array &$data)
    {
        $data[self::MACRO_NAME] = $this->service->go($host);
    }
}