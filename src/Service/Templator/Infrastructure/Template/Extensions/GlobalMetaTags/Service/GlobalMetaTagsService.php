<?php

namespace App\Service\Templator\Infrastructure\Template\Extensions\GlobalMetaTags\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Templator\Domain\Template\Extensions\GlobalMetaTags\Service\GlobalMetaTagsInterface;

class GlobalMetaTagsService implements GlobalMetaTagsInterface
{

    public function go(GetHostInterface $host)
    {
        $seoData = $host->getSeoData();
        //todo
        // @rule if redirected host @means not need to meta tags
        if ($host->hasGeoRedirect()) {
            return '';
        }

        if (isset($seoData[Host::SEO_META_TAGS]) && is_string($seoData[Host::SEO_META_TAGS])) {
            return $seoData[Host::SEO_META_TAGS];
        }
        return '';
    }
}