<?php

namespace App\Service\Templator\Infrastructure\Template\Extensions\SeoGeo;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Template\Extensions\MacroExtensionInterface;
use App\Service\Templator\Domain\Template\Extensions\SeoGeo\Service\SeoGeoInterface;

class SeoGeoExtension implements MacroExtensionInterface
{
    const MACRO_NAME = 'seo_geo';

    const SERVICE_NAME = 'templator.widgets.seo.geo';

    /** @var SeoGeoInterface */
    private $seoGeo;

    public function __construct(SeoGeoInterface $seoGeo)
    {
        $this->seoGeo = $seoGeo;
    }

    public function appendExtension(GetHostInterface $host, array &$data)
    {
        $data[self::MACRO_NAME] = $this->seoGeo->getLinks($host);
    }
}