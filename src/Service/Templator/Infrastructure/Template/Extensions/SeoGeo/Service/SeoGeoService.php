<?php

namespace App\Service\Templator\Infrastructure\Template\Extensions\SeoGeo\Service;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Templator\Domain\Template\Extensions\SeoGeo\Service\SeoGeoInterface;

class SeoGeoService implements SeoGeoInterface
{
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;

    public function __construct(
        HostRepository $hostRepository,
        EntityLinkGeneratorInterface $linkGenerator
    ) {
        $this->hostRepository = $hostRepository;
        $this->linkGenerator = $linkGenerator;
    }

    public function getLinks(GetHostInterface $host): string
    {
        $links = [];
        //todo
        // @rule if redirected host @means not need to meta tags
        if ($host->hasGeoRedirect()) {
            return '';
        }

        if ($host->getParentId()) {
            $hosts = $this->hostRepository->findChildren($host->getParentId());
            /** @var Host $hostModel */
            foreach ($hosts as $hostModel) {
                /** @var \App\Service\Processor\Infrastructure\DAO\Host $geoHost */
                $geoHost = clone $host;
                $geoHost->setHostname($hostModel->getHostname());
                $entity = new Entity();
                $entity->setEntityId((int)$host->getEntityId());
                $entity->setEntitySlug($host->getEntitySlug());
                $entity->setEntityType($host->getEntityType());
                $links[] = [
                    "__link" => $this->linkGenerator->generateLink($entity, $geoHost),
                    "lang" => ($hostModel->getGeo()) ? "ru_{$hostModel->getGeo()}" : "x-default"
                ];
                unset($entity, $geoHost);
            }
        }
        return $this->makeSeo($links);
    }

    private function makeSeo(array $links)
    {
        $result = [];
        foreach ($links as $link) {
            $result[] = "<link rel='alternate' hreflang='{$link['lang']}' href='{$link['__link']}'/>";
        }
        return implode("\n", $result);
    }
}