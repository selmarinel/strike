<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\Breadcrumbs\Service;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use App\Service\Templator\Domain\Widgets\Extensions\Breadcrumbs\Service\BreadcrumbsServiceInterface;

/**
 * Class BreadcrumbsService
 * @package App\Service\Templator\Infrastructure\Widgets\Extensions\Breadcrumbs\Service
 *
 * TODO MAKE SOMETHING WITH THIS
 */
class BreadcrumbsService implements BreadcrumbsServiceInterface
{
    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;
    /** @var  EntityNestedTreeRepository */
    private $entityNestedTree;
    /** @var EntityToHostRepository */
    private $entitiesToHostRepository;
    /** @var EntityPreparationInterface */
    private $preparation;
    /** @var EntityHostToEntityHostRepository */
    private $repository;

    public function __construct(
        EntityLinkGeneratorInterface $linkGenerator,
        EntityNestedTreeRepository $entityNestedTree,
        EntityToHostRepository $entitiesToHostRepository,
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EntityPreparationInterface $preparation
    ) {
        $this->linkGenerator = $linkGenerator;
        $this->entityNestedTree = $entityNestedTree;
        $this->entitiesToHostRepository = $entitiesToHostRepository;
        $this->preparation = $preparation;
        $this->repository = $entityHostToEntityHostRepository;
    }

    public function generateLinks(GetHostInterface $host)
    {
        $links = [
            'links' => [],
            'active' => ['title' => '']
        ];
        if (!$host->getParentId() || empty($host->getEntityId())) {
            return $links;
        }

        /** @var EntityNestedTree $node */
        $node = $this->entityNestedTree->findByEntityId($host->getEntityId());

        if (!$node) {
            return $this->processRelation($host);
        }

        $links['active'] = [
            'title' => $node->getEntity()->getName(),
        ];

        while ($node->getParent()) {
            $node = $node->getParent();

            $entityToHost = $this->entitiesToHostRepository->findHostOrParentByHostIdAndEntityIdWithParentId(
                $host->getHostId(),
                $node->getEntity()->getId(),
                $host->getParentId()
            );

            if (!$entityToHost) {
                continue;
            }

            $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHost);
            $parent[] = [
                'title' => $entityToHost->getEntity()->getName(),
                'uri' => $this->linkGenerator->generateLink($entityDAO, $host)
            ];
        }

        $parent[] = [
            'title' => 'Главная',
            'uri' => '/'
        ];

        $links['links'] = array_reverse($parent);
        return $links;
    }

    /**
     * @param GetHostInterface $host
     * @return array
     */
    private function processRelation(GetHostInterface $host)
    {
        $links = [
            'links' => [],
            'active' => ['title' => '']
        ];
        /** @var EntityHostToEntityHost $entityHostToEntityHost */
        try {
            $entityHostToEntityHost = $this->repository->createQueryBuilder('ehteh')
                ->select('ehteh, m')
                ->join('ehteh.main', 'm')
                ->join('m.entity', 'me')
                ->join('ehteh.subordinate', 's')
                ->join('s.entity', 'e')
                ->join('s.host', 'sh')
                ->join('m.host', 'mh')
                ->where('sh = mh')
                ->andWhere('e.id = :entity_id')
                ->andWhere('mh.id = :host_id')
                ->setParameter('entity_id', $host->getEntityId())
                ->setParameter('host_id', $host->getHostId())
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (\Exception $exception) {
            return $links;
        }

        if(!$entityHostToEntityHost && $host->getParentId()){
            try {
                $entityHostToEntityHost = $this->repository->createQueryBuilder('ehteh')
                    ->select('ehteh, m')
                    ->join('ehteh.main', 'm')
                    ->join('m.entity', 'me')
                    ->join('ehteh.subordinate', 's')
                    ->join('s.entity', 'e')
                    ->join('s.host', 'sh')
                    ->join('m.host', 'mh')
                    ->where('sh = mh')
                    ->andWhere('e.id = :entity_id')
                    ->andWhere('mh.id = :host_id')
                    ->setParameter('entity_id', $host->getEntityId())
                    ->setParameter('host_id', $host->getParentId())
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (\Exception $exception) {
                return $links;
            }
        }

        if(!$entityHostToEntityHost){
            return $links;
        }

        $main = $entityHostToEntityHost->getMain();
        $entityToHost  = clone $main;
        /** @var EntityNestedTree $node */
        $node = $this->entityNestedTree->findByEntity($entityToHost->getEntity());

        if (!$node) {
            return $links;
        }

        $links['active'] = ['title' => $entityHostToEntityHost->getSubordinate()->getEntity()->getName()];

        $entityToHostParent = $this->entitiesToHostRepository->findHostOrParentByHostIdAndEntityIdWithParentId(
            $host->getHostId(),
            $main->getEntity()->getId(),
            $host->getParentId()
        );
        if (!$entityToHostParent) {
            return $links;
        }

        $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHostParent);

        $parent[] = [
            'title' => $main->getEntity()->getName(),
            'uri' => $this->linkGenerator->generateLink($entityDAO, $host)
        ];

        while ($node->getParent()) {
            $node = $node->getParent();
            $entityToHost = $this->entitiesToHostRepository->findHostOrParentByHostIdAndEntityIdWithParentId(
                $host->getHostId(),
                $node->getEntity()->getId(),
                $host->getParentId()
            );
            if(!$entityToHost){
                continue;
            }
            $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHost);

            $parent[] = [
                'title' => $entityToHost->getEntity()->getName(),
                'uri' => $this->linkGenerator->generateLink($entityDAO, $host)
            ];
        }

        $parent[] = [
            'title' => 'Главная',
            'uri' => '/'
        ];
        $links['links'] = array_reverse($parent);
        return $links;
    }
}