<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\YoutubeAdding\Service;


use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Infrastructure\DTO\Search\YouTubeVideo;
use App\Service\Templator\Domain\Widgets\Extensions\YoutubeAdding\Service\YoutubeAddingInterface;

class YoutubeAddingService implements YoutubeAddingInterface
{
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var SearchEntityRepository */
    private $search;

    public function __construct(EntityToHostRepository $entityToHostRepository, SearchEntityRepository $searchEntityRepository)
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->search = $searchEntityRepository;
    }

    public function process(GetHostInterface $host): array
    {
        $entityToHost = $this->entityToHostRepository->findByHostIdAndEntityId(
            $host->getHostId(),
            $host->getEntityId()
        );
        if (!$entityToHost) {
            $entityToHost = $this->entityToHostRepository->findByHostIdAndEntityId(
                $host->getParentId(),
                $host->getEntityId()
            );
        }

        if (!$entityToHost) {
            return [];
        }
        /** @var SearchEntity $search */
        $search = $this->search->findOneBy([
            'entityToHost' => $entityToHost
        ]);
        if (!$search) {
            return [];
        }
        $result = [];
        foreach ($search->getResults() as $searchResult) {
            $youtubeResult = new YouTubeVideo();
            $youtubeResult->setId($searchResult->getVideoId());
            $youtubeResult->setTitle($searchResult->getTitle());
            $youtubeResult->setSrc($searchResult->getSrc());
            $youtubeResult->setDescription($searchResult->getDescription());
            $result[] = $youtubeResult->toArray();
            unset($youtubeResult, $searchResult);
        }
        return $result;
    }
}