<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\YoutubeAdding;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Domain\Widgets\Extensions\YoutubeAdding\Service\YoutubeAddingInterface;
use Twig\Environment;

class YoutubeAddingExtension implements WidgetExtensionInterface
{
    const SERVICE_NAME = 'templator.widget.extension.youtube.adding';
    const WIDGET_NAME = 'youtube_adding';

    /** @var YoutubeAddingInterface */
    private $youtubeAdding;
    /** @var Environment  */
    private $twig;

    public function __construct(YoutubeAddingInterface $youtubeAdding, Environment $environment)
    {
        $this->youtubeAdding = $youtubeAdding;
        $this->twig = $environment;
    }

    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        if (in_array(self::WIDGET_NAME, array_keys($widgetNames))) {
            $collection = array_merge($data, ['__youtube' => $this->youtubeAdding->process($host)]);
            $widgetsMap[self::WIDGET_NAME] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $collection);
        }
    }
}