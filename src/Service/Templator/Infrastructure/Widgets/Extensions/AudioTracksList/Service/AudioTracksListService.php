<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\AudioTracksList\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\DTO\Audio\Collection\Tracks;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\VO\Request;
use App\Service\Templator\Domain\Widgets\Extensions\AudioTracksList\Service\AudioTracksListInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\ListCollectionTrait;

class AudioTracksListService implements AudioTracksListInterface
{
    use ListCollectionTrait;

    /** @var CollectionDTOReceiverInterface */
    private $receiver;

    public function __construct(CollectionDTOReceiverInterface $receiver
    ) {
        $this->receiver = $receiver;
    }

    public function execute(GetHostInterface $host, int $count = 12): array
    {
        $result = $this->makeList($host, Type::COLLECTION_AUDIO_TRACKS, 'Топ Треков', $count);
        if (!empty($result)) {
            return $result;
        }
        $dto = new Tracks();
        $dto->setName('Топ Треков');
        return $dto->toArray();
    }
}