<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\AudioArtistsList;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\AudioArtistsList\Service\AudioArtistsListInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\AudioArtistsList\Service\AudioArtistsListService;
use App\Service\Templator\Infrastructure\Widgets\ExtensionTrait;
use Twig\Environment;

class AudioArtistsListExtension implements WidgetExtensionInterface
{
    use ExtensionTrait;

    const SERVICE_NAME = 'templator.widget.extension.artists';

    const WIDGET_NAME = 'audio_artists_list';

    const DEFAULT_COUNT = 12;

    const WIDGET_PATTERN = '/(?<name>' . self::WIDGET_NAME . ')(\#)?(?<count>[0-9]*)?/';

    /** @var AudioArtistsListInterface */
    private $artistsList;
    /** @var Environment  */
    private $twig;

    public function __construct(AudioArtistsListInterface $artistsList, Environment $environment)
    {
        $this->artistsList = $artistsList;
        $this->twig = $environment;
    }

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        $count = self::DEFAULT_COUNT;
        $fullName = self::WIDGET_NAME;

        $this->processCollection(
            $widgetNames,
            self::WIDGET_PATTERN,
            $count,
            $fullName,
            self::DEFAULT_COUNT
        );

        if (in_array($fullName, array_keys($widgetNames))) {
            $collection = array_merge($data, $this->artistsList->execute($host, $count));
            $widgetsMap[$fullName] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $collection);
        }
        unset($count,$fullName);
    }
}