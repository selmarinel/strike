<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\AudioArtistsList\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\DTO\Audio\Collection\Artists;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\VO\Request;
use App\Service\Templator\Domain\Widgets\Extensions\AudioArtistsList\Service\AudioArtistsListInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\ListCollectionTrait;

class AudioArtistsListService implements AudioArtistsListInterface
{
    use ListCollectionTrait;

    /** @var CollectionDTOReceiverInterface */
    private $receiver;

    public function __construct(CollectionDTOReceiverInterface $receiver
    ) {
        $this->receiver = $receiver;
    }

    public function execute(GetHostInterface $host, int $count): array
    {
        $result = $this->makeList($host, Type::COLLECTION_AUDIO_ARTISTS, 'Топ Артистов', $count);
        if (!empty($result)) {
            return $result;
        }
        $dto = new Artists();
        $dto->setName('Топ Артистов');
        return $dto->toArray();
    }
}