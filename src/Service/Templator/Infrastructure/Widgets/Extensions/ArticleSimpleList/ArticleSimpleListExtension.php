<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\ArticleSimpleList;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Templator\Domain\Widgets\Extensions\ArticleSimpleList\Service\ArticleListInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Domain\Widgets\WidgetNeedRequestInterface;
use App\Service\Templator\Infrastructure\Widgets\ExtensionTrait;
use Twig\Environment;

class ArticleSimpleListExtension implements WidgetExtensionInterface, WidgetNeedRequestInterface
{
    use ExtensionTrait;

    const SERVICE_NAME = 'templator.widget.extension.news';

    const WIDGET_NAME = 'news';

    const DEFAULT_COUNT = 12;

    const WIDGET_PATTERN = '/(?<name>' . self::WIDGET_NAME . ')(\#)?(?<count>[0-9]*)?(\#[r])?(?<rows>[0-9]*)?/';

    /** @var ArticleListInterface */
    private $articleList;
    /** @var Environment */
    private $twig;
    /** @var  InputGetRequestInterface|null */
    private $request;

    public function __construct(ArticleListInterface $articleList, Environment $environment)
    {
        $this->articleList = $articleList;
        $this->twig = $environment;
    }

    public function setRequest(InputGetRequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        $count = self::DEFAULT_COUNT;
        $fullName = self::WIDGET_NAME;
        $this->processValue(
            $widgetNames,
            self::WIDGET_PATTERN,
            'count',
            $fullName,
            $count, null
        );
        $rows = 3;
        $this->processValue(
            $widgetNames,
            self::WIDGET_PATTERN,
            'rows',
            $fullName,
            $rows, 3);

        if (in_array($fullName, array_keys($widgetNames))) {
            $collection = array_merge($data, $this->articleList->execute($host, $this->request, $count), ['__rows' => $rows]);
            $widgetsMap[$fullName] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $collection);
        }
        unset($count, $fullName);
    }
}