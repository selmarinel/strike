<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\ArticleSimpleList\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity as EntityModel;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\Receiver\Filler\CollectionFillerInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Domain\VO\Request\InputSetRequestInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DTO\Article\Collection\Category;
use App\Service\Processor\Infrastructure\Traits\DTO\DTOFillerTrait;
use App\Service\Templator\Domain\Widgets\Extensions\ArticleSimpleList\Service\ArticleListInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class ArticleSimpleListService implements ArticleListInterface
{
    use DTOFillerTrait;

    /** @var EntityManagerInterface */
    private $_em;
    /** @var CollectionFillerInterface */
    private $filler;
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;

    public function __construct(
        EncryptionInterface $encryption,
        EntityManagerInterface $entityManager,
        CollectionFillerInterface $collectionFiller,
        EntityLinkGeneratorInterface $entityLinkGenerator
    )
    {
        $this->_em = $entityManager;
        $this->filler = $collectionFiller;
        $this->entityLinkGenerator = $entityLinkGenerator;
        $this->encryption = $encryption;
    }

    public function execute(GetHostInterface $host, InputGetRequestInterface $request, int $count = null): array
    {
        if ($count) {
            /** @var InputSetRequestInterface|InputGetRequestInterface $request */
            $request->setLimit($count);
        }
        /** @var SetHostInterface $hostToFetch */
        $hostToFetch = clone $host;
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_0', 'entityId');
        $rsm->addScalarResult('entity_type_1', 'entityType');
        $rsm->addScalarResult('source_id_2', 'entitySource');
        $rsm->addScalarResult('name_5', 'entityName');
        $rsm->addScalarResult('entity_slug_8', 'entitySlug');

        $dql = $this->_em->createQueryBuilder()
            ->select('e, eh')
            ->from(EntityModel::class, 'e')
            ->join(EntityToHost::class, 'eh', 'WITH', 'eh.entity = e')
            ->join('eh.host', 'h')
            ->where('h.id = :hostId')
            ->andWhere('e.entity_type = :entityType')
            ->orderBy('e.score', 'desc')
            ->getQuery()
            ->getDQL();
        $entities = $this->_em->createQuery($dql)
            ->setResultSetMapping($rsm)
            ->setParameter('hostId', $host->getHostId())
            ->setParameter('entityType', Type::NEWS_ARTICLE)
            ->setMaxResults($request->getLimit())
            ->setFirstResult($request->getOffset())
            ->getResult();

        $collection = [];
        /** @var array $entityModel */
        foreach ($entities as $entityResult) {
            $entityDAO = new Entity();
            $entityDAO->setName($entityResult['entityName']);
            $entityDAO->setSourceId($entityResult['entitySource']);
            $entityDAO->setEntityType($entityResult['entityType']);
            $entityDAO->setEntityId($entityResult['entityId']);
            $entityDAO->setEntitySlug($entityResult['entitySlug']);
            $hostToFetch->setStatic(false);
            $hostToFetch->setEntityType($entityDAO->getEntityType());
            $hostToFetch->setEntitySlug($entityDAO->getEntitySlug());
            $hostToFetch->setEntityId($entityDAO->getEntityId());
            $entityDAO->setLink($this->entityLinkGenerator->generateLink($entityDAO, $hostToFetch));
            unset($host);
            $collection[] = $entityDAO;
            unset($entityDAO);
        }

        $children = [];
        /** @var \App\Service\Processor\Infrastructure\DAO\Entity $child */
        foreach ($collection as $child) {
            if (isset(Type::MAP_DTO[$child->getEntityType()])) {
                $dtoClass = Type::MAP_DTO[$child->getEntityType()];
                /** @var DTOInterface $dto */
                $dto = new $dtoClass();
                $this->fillDTO($dto, $child);
                $children[] = $dto;
                unset($dto);
            }
        }
        $collectionEntity = $this->filler->fill(new Category(), $children);

        $news = [];
        /** @var DTOInterface $child */
        foreach ($collectionEntity->getChildren() as $child) {
            $news[] = $child->toArray();
        }
        unset($hostToFetch);
        return ['news' => $news];
    }
}