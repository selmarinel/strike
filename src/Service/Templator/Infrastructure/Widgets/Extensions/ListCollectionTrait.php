<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\VO\Request;

/**
 * Trait ListCollectionTrait
 * @package App\Service\Templator\Infrastructure\Widgets\Extensions
 * @property CollectionDTOReceiverInterface $receiver
 */
trait ListCollectionTrait
{
    /**
     * @param GetHostInterface $host
     * @param int $type
     * @param string $name
     * @param int $limit
     * @return array
     */
    public function makeList(GetHostInterface $host, int $type, string $name, int $limit = 12): array
    {
        /** @var Host $hostToFetch */
        $hostToFetch = clone $host;
        if (!isset($host->getSlugData()[$type])) {
            return [];
        }
        $slug = $host->getSlugData()[$type];
        $hostToFetch->setEntityType($type);
        $hostToFetch->setEntitySlug($slug->getSlug());
        $hostToFetch->setTypeSlug($slug->getSlug());
        $hostToFetch->setStatic(true);
        $request = new Request();
        $request->setLimit($limit);
        try {
            $dto = $this->receiver->receive($hostToFetch, $request);
            $dto->setName($name);
            $dto->setRequest($request);
            unset($hostToFetch);
            return $dto->toArray();
        } catch (EntityNotFoundException $exception) {
            return [];
        }
    }
}