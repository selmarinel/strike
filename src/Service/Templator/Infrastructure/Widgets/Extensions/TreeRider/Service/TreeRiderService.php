<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\TreeRider\Service;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Entity\GetEntityInterface;
use App\Service\Processor\Domain\DAO\Entity\SetEntityInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Templator\Domain\Widgets\Extensions\TreeRider\Service\TreeRiderInterface;

class TreeRiderService implements TreeRiderInterface
{
    /** @var EntityNestedTreeRepository */
    private $treeRepository;
    /** @var EntityPreparationInterface */
    private $preparation;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $generator;

    public function __construct(
        EntityNestedTreeRepository $treeRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityPreparationInterface $preparation,
        EntityLinkGeneratorInterface $generator
    )
    {
        $this->treeRepository = $treeRepository;
        $this->preparation = $preparation;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->generator = $generator;
    }

    public function process(GetHostInterface $host)
    {
        $links = [];
        $node = $this->treeRepository->findByEntityId($host->getEntityId());
        if (!$node) {
            return ['links' => $links];
        }
        $parentNode = $node->getParent();
        if (!$parentNode) {
            return ['links' => $links];
        }
        $children = $this->treeRepository
            ->getChildrenQueryBuilder($parentNode)
            ->getQuery()
            ->getResult();
        $ids = [];
        /** @var EntityNestedTree $child */
        foreach ($children as $child) {
            $ids[] = $child->getEntity()->getId();
        }
        if ($host->getParentId() && $host->getAbuseLevel() <= 1) {
            $entitiesToHost = $this->entityToHostRepository
                ->createQueryBuilder('eth')
                ->select('eth,h,e')
                ->join('eth.entity', 'e')
                ->join('eth.host', 'h')
                ->where('h.id = :host_id')
                ->andWhere('e.id in (:entities)')
                ->setParameter('host_id', $host->getParentId())
                ->setParameter('entities', $ids)
                ->getQuery()
                ->getResult();
        } else {
            $entitiesToHost = $this->entityToHostRepository
                ->createQueryBuilder('eth')
                ->select('eth,h,e')
                ->join('eth.entity', 'e')
                ->join('eth.host', 'h')
                ->where('h.id = :host_id')
                ->andWhere('e.id in (:entities)')
                ->setParameter('host_id', $host->getHostId())
                ->setParameter('entities', $ids)
                ->getQuery()
                ->getResult();
        }

        unset($ids);
        $links = [];
        foreach ($entitiesToHost as $index => $entityToHost) {
            /** @var SetEntityInterface|GetEntityInterface $entityDAO */
            $entityDAO = $this->preparation->prepareEntityFromEntityToHost($entityToHost);

            $links [] = [
                "__link" => $this->generator->generateLink($entityDAO, $host),
                "num" => $index + 1,
                "name" => $entityDAO->getName()

            ];
            unset($entityDAO);
        }
        return ['links' => $links];
    }
}