<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\TreeRider;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\TreeRider\Service\TreeRiderInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use Twig\Environment;

class TreeItemsExtension implements WidgetExtensionInterface
{
    const SERVICE_NAME = 'templator.widget.extension.tree.items';

    const WIDGET_NAME = 'tree_items';

    /** @var TreeRiderInterface */
    private $rider;
    /** @var Environment */
    private $twig;

    public function __construct(TreeRiderInterface $rider, Environment $twig)
    {
        $this->twig = $twig;
        $this->rider = $rider;
    }

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        if (in_array(self::WIDGET_NAME, array_keys($widgetNames))) {
            $breadcrumbs = array_merge($data, $this->rider->process($host));
            $widgetsMap[self::WIDGET_NAME] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $breadcrumbs);
        }
    }


}