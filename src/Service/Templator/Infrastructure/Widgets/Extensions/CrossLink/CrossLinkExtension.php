<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\CrossLink;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\CrossLink\Service\CrossLinkServiceInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use Twig\Environment;

class CrossLinkExtension implements WidgetExtensionInterface
{
    /** @var CrossLinkServiceInterface */
    private $crossLinkService;
    /** @var Environment */
    private $twig;

    const SERVICE_NAME = 'templator.widget.extension.cross';

    const WIDGET_NAME = 'cross_links';

    public function __construct(CrossLinkServiceInterface $crossLinkService, Environment $twig)
    {
        $this->crossLinkService = $crossLinkService;
        $this->twig = $twig;
    }

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(
        GetHostInterface $host,
        array &$widgetsMap,
        array &$data,
        array &$widgetNames
    ) {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        if (in_array(self::WIDGET_NAME, array_keys($widgetNames))) {
            $breadcrumbs = array_merge($data, $this->crossLinkService->getLinks($host, rand(1, 3)));
            $widgetsMap[self::WIDGET_NAME] = $this
                ->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $breadcrumbs);
        }
    }


}