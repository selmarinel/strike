<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\CrossLink\Service;


use App\Service\Base\Domain\Repository\Seo\EntityCrossLinkRepositoryInterface;
use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\Base\Infrastructure\Repository\Seo\EntityCrossLinkRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Templator\Domain\Widgets\Extensions\CrossLink\Service\CrossLinkServiceInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\CrossLink\VO\CrossLinkVO;

class CrossLinkService implements CrossLinkServiceInterface
{
    const MIN_LINKS = 5;

    const WIDGET_NAME = 'cross_links';

    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;
    /** @var EntityCrossLinkRepositoryInterface|EntityCrossLinkRepository */
    private $entityCrossLinkRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var SearchEntityRepository */
    private $searchEntityRepository;

    public function __construct(
        EntityCrossLinkRepositoryInterface $entityCrossLinkRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityLinkGeneratorInterface $linkGenerator,
        SearchEntityRepository $searchEntityRepository
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->linkGenerator = $linkGenerator;
        $this->entityCrossLinkRepository = $entityCrossLinkRepository;
        $this->searchEntityRepository = $searchEntityRepository;
    }

    /**
     * @param GetHostInterface $host
     * @param int $countRange
     * @return array
     */
    public function getLinks(GetHostInterface $host, int $countRange = 0): array
    {
        $links = [];
        $key = md5($host->getParentId() . ($host->getEntitySlug() ?: $host->getTypeSlug()));
        $entityCrossLink = $this->entityCrossLinkRepository->find($key);
        $hostId = $host->getParentId();
        if (!$entityCrossLink) {
            $key = md5($host->getHostId() . ($host->getEntitySlug() ?: $host->getTypeSlug()));
            $entityCrossLink = $this->entityCrossLinkRepository->find($key);
            $hostId = $host->getHostId();
        }
        if (!$entityCrossLink) {
            return ['links' => $links];
        }
        $entityIds = $entityCrossLink->getLinks();
        $ids = [];
        foreach (array_rand($entityIds, 10) as $randomKey) {
            $ids[] = $entityIds[$randomKey];
        }
        unset($entityIds);

        //TODO: abuse level
        $collection = $this->entityToHostRepository
            ->createQueryBuilder('eh')
            ->select('eh,e')
            ->join('eh.entity', 'e')
            ->join('eh.host', 'h')
            ->where('eh.id in (:ids)')
            ->andWhere('h.id = :hostId')
            ->setParameter('hostId', $hostId)
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();

        unset($ids);
        /** @var EntityToHost $entityToHost */
        foreach ($collection as $entityToHost) {
            /** @var Host $cloneHost */
            $cloneHost = clone $host;
            $cloneHost->setStatic(false);
            $entity = new Entity();
            $entity->setEntityType($entityToHost->getEntity()->getEntityType());
            $entity->setEntityId($entityToHost->getEntity()->getId());
            $entity->setEntitySlug($entityToHost->getEntitySlug());
            $cloneHost->setEntityType($entityToHost->getEntity()->getEntityType());

            $name = $entityToHost->getEntity()->getName();
            if ($entityToHost->getEntity()->getEntityType() === Type::SEARCH_TYPE) {
                /** @var SearchEntity $searchEntity */
                $searchEntity = $this->searchEntityRepository->findOneBy([
                    'entityToHost' => $entityToHost
                ]);
                $name = $entityToHost->getEntitySlug();
                if ($searchEntity) {
                    $name = $searchEntity->getQuery();
                }
                $cloneHost->setTypeSlug($entityToHost->getEntitySlug());
            }
            $links[] = [
                'link' => $this->linkGenerator->generateLink($entity, $cloneHost),
                'name' => $name
            ];
        }
        return ['links' => $links];
    }


}