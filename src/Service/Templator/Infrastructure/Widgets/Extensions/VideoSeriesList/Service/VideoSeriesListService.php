<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\VideoSeriesList\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Infrastructure\DTO\Video\Collection\TVSeries;
use App\Service\Templator\Domain\Widgets\Extensions\VideoSeriesList\Service\VideoSeriesListInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\ListCollectionTrait;

class VideoSeriesListService implements VideoSeriesListInterface
{
    use ListCollectionTrait;

    /** @var CollectionDTOReceiverInterface */
    private $receiver;

    /**
     * VideoSeriesListService constructor.
     * @param CollectionDTOReceiverInterface $receiver
     */
    public function __construct(CollectionDTOReceiverInterface $receiver
    ) {
        $this->receiver = $receiver;
    }

    public function execute(GetHostInterface $host, int $count = 8): array
    {
        $result = $this->makeList($host, Type::COLLECTION_VIDEO_SERIES, 'Топ Сериалов', $count);
        if (!empty($result)) {
            return $result;
        }
        $dto = new TVSeries();
        $dto->setName('Топ Сериалов');
        return $dto->toArray();
    }
}