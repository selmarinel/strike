<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\VideoSeriesList;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\VideoSeriesList\Service\VideoSeriesListInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Infrastructure\Widgets\ExtensionTrait;
use Twig\Environment;

class TvSeriesListExtension implements WidgetExtensionInterface
{
    use ExtensionTrait;

    const SERVICE_NAME = 'templator.widget.extension.tv.series.list';

    const WIDGET_NAME = 'tv_series_list';

    const DEFAULT_COUNT = 8;

    const WIDGET_PATTERN = '/(?<name>' . self::WIDGET_NAME . ')(\#)?(?<count>[0-9]*)?/';

    /** @var VideoSeriesListInterface */
    private $list;
    /** @var Environment */
    private $twig;

    public function __construct(
        VideoSeriesListInterface $list,
        Environment $twig
    ) {
        $this->list = $list;
        $this->twig = $twig;
    }

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        $count = self::DEFAULT_COUNT;
        $fullName = self::WIDGET_NAME;

        $this->processCollection($widgetNames, self::WIDGET_PATTERN, $count, $fullName);

        if (in_array($fullName, array_keys($widgetNames))) {
            $collection = array_merge($data, $this->list->execute($host, $count));
            $widgetsMap[$fullName] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $collection);
        }
        unset($count,$fullName);
    }


}