<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\CategoryBreadcrumbs;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\CategoryBreadcrumbs\Service\CategoryBreadcrumbsServiceInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use Twig\Environment;

class CategoryBreadcrumbsExtension implements WidgetExtensionInterface
{
    /** @var CategoryBreadcrumbsServiceInterface */
    private $breadcrumbs;
    /** @var Environment */
    private $twig;

    public function __construct(
        CategoryBreadcrumbsServiceInterface $breadcrumbs,
        Environment $twig
    )
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->twig = $twig;
    }

    const SERVICE_NAME = 'templator.widget.extension.category.breadcrumbs';

    const WIDGET_NAME = 'category_breadcrumbs';

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        if (in_array(self::WIDGET_NAME, array_keys($widgetNames))) {
            $breadcrumbs = $this->breadcrumbs->getBreadcrumbs($host);
            $widgetsMap[self::WIDGET_NAME] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $breadcrumbs);
        }
    }
}