<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\CategoryBreadcrumbs\Service;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\Templator\Domain\Widgets\Extensions\CategoryBreadcrumbs\Service\CategoryBreadcrumbsServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class CategoryBreadcrumbsService implements CategoryBreadcrumbsServiceInterface
{
    const MAP_CATEGORIES = [
        Type::COLLECTION_NEWS_CATEGORY,
    ];

    const MAP_ARTICLES = [
        Type::NEWS_ARTICLE,
    ];

    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;
    /** @var EntityManagerInterface */
    private $_em;
    /** @var EntityPreparationInterface */
    private $preparation;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntityPreparationInterface $preparation,
        EntityLinkGeneratorInterface $entityLinkGenerator
    )
    {
        $this->_em = $entityManager;
        $this->preparation = $preparation;
        $this->entityLinkGenerator = $entityLinkGenerator;
    }

    public function getBreadcrumbs(GetHostInterface $host)
    {
        $links = [];
        $active = ['title' => ''];

        $root = [
            'title' => 'Home',
            'uri' => '/'
        ];

        $links[] = $root;

        if (in_array($host->getEntityType(), self::MAP_CATEGORIES)) {
            /** @var EntityToHost|null $entityToHost */
            $entityToHost = $this->_em->createQueryBuilder()
                ->from(EntityToHost::class, 'eh')
                ->select('eh')
                ->join('eh.host', 'h')
                ->join('eh.entity', 'e')
                ->where('h.id = :hostId')
                ->andWhere('eh.entity_slug = :entitySlug')
                ->andWhere('e.entity_type = :entityType')
                ->setParameter('hostId', $host->getHostId())
                ->setParameter('entitySlug', $host->getEntitySlug())
                ->setParameter('entityType', $host->getEntityType())
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
            if ($entityToHost) {
                $active = [
                    'title' => $entityToHost->getEntity()->getName()
                ];
            }
        }
        if (in_array($host->getEntityType(), self::MAP_ARTICLES)) {
            $entityToHost = $this->_em->createQueryBuilder()
                ->from(EntityToHost::class, 'eh')
                ->select('eh')
                ->join('eh.host', 'h')
                ->join('eh.entity', 'e')
                ->where('h.id = :hostId')
                ->andWhere('e.id = :entityId')
                ->setParameter('hostId', $host->getHostId())
                ->setParameter('entityId', $host->getEntityId())
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
            if ($entityToHost) {
                $active = [
                    'title' => $entityToHost->getEntity()->getName()
                ];
                /** @var EntityHostToEntityHost $mainRelation */

                $mainRelation = $this->_em->createQueryBuilder()
                    ->select('ehteh, m')
                    ->from(EntityHostToEntityHost::class, 'ehteh')
                    ->join('ehteh.main', 'm')
                    ->where('ehteh.subordinate = :subordinate')
                    ->andWhere('ehteh.type = :type')
                    ->setParameter('type', Type::RELATION_TYPE_LIST)
                    ->setParameter('subordinate', $entityToHost)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                if ($mainRelation) {
                    $entity = $this->preparation->prepareEntityFromEntityToHost($mainRelation->getMain());
                    /** @var SetHostInterface $clonedHost */
                    $clonedHost = clone $host;
                    $clonedHost->setEntitySlug($entity->getEntitySlug());
                    $clonedHost->setEntityType($entity->getEntityType());
                    $clonedHost->setStatic(true);
                    $slug = $clonedHost->getSlugData()[$clonedHost->getEntityType()];
                    $clonedHost->setTypeSlug($slug->getSlug());

                    $links[] = [
                        'title' => $entity->getName(),
                        'uri' => $this->entityLinkGenerator->generateLink($entity, $clonedHost)
                    ];
                }
            }
        }

        return [
            'links' => $links,
            'active' => $active
        ];
    }
}