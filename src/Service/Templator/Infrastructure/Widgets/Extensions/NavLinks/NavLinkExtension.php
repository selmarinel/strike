<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\NavLinks;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Infrastructure\Widgets\ExtensionTrait;
use Twig\Environment;

class NavLinkExtension implements WidgetExtensionInterface
{
    use ExtensionTrait;

    /** @var Environment */
    private $twig;
    /** @var NavLinkServiceFactory */
    private $navLinkServiceFactory;

    public function __construct(
        NavLinkServiceFactory $navLinkServiceFactory,
        Environment $twig)
    {
        $this->navLinkServiceFactory = $navLinkServiceFactory;
        $this->twig = $twig;
    }

    const WIDGET_PATTERN = '/(?<name>' . self::WIDGET_NAME . ')(\#)?(?<category>[c])?/';

    const SERVICE_NAME = 'templator.widget.extension.nav';

    const DATA_NAME = '__nav';

    const WIDGET_NAME = 'nav_links';

    /**
     * @param GetHostInterface $host
     * @param array $widgetsMap
     * @param array $data
     * @param array $widgetNames
     * @throws \Twig_Error
     */
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames)
    {
        $type = $host->getHostTypeDriver();
        $driver = $host->getTemplateDriver();

        $isCategory = false;
        $fullName = self::WIDGET_NAME;

        $this->processValue($widgetNames, self::WIDGET_PATTERN, 'category', $fullName, $isCategory, false);

        $navLinks = $this->navLinkServiceFactory->getService($isCategory);

        if (in_array($fullName, array_keys($widgetNames))) {
            $links = array_merge($data, $navLinks->getLinks($host));
            $widgetsMap[$fullName] = $this->twig
                ->render("driver/{$type}/{$driver}/partial/" . self::WIDGET_NAME . ".twig", $links);
        }
    }
}