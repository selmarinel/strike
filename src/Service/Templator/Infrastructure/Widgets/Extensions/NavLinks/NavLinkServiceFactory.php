<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\NavLinks;

use App\Service\Templator\Domain\Widgets\Extensions\NavLinks\NavLinkServiceFactoryInterface;
use App\Service\Templator\Domain\Widgets\Extensions\NavLinks\Service\NavLinksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NavLinkServiceFactory implements NavLinkServiceFactoryInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(
        ContainerInterface $containerAware
    )
    {
        $this->container = $containerAware;
    }

    public function getService(bool $isCategory): NavLinksInterface
    {
        if ($isCategory) {
            return $this->container->get('widget.service.category.nav.links');
        }
        return $this->container->get('widget.service.nav.links');
    }
}