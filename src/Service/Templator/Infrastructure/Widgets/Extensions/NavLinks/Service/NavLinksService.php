<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\NavLinks\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Templator\Domain\Widgets\Extensions\NavLinks\Service\NavLinksInterface;

class NavLinksService implements NavLinksInterface
{
    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;

    public function __construct(EntityLinkGeneratorInterface $linkGenerator)
    {
        $this->linkGenerator = $linkGenerator;
    }

    const NAMES_MAP = [
        Type::COLLECTION_AUDIO_ARTISTS => 'Артисты',
        Type::COLLECTION_AUDIO_TRACKS => 'Треки',
        Type::COLLECTION_VIDEO_SERIES => 'Сериалы',
        Type::COLLECTION_MOVIE_FILMS => 'Фильмы',
        Type::ROOT => 'Главная'
    ];

    public function getLinks(GetHostInterface $host): array
    {
        $links = [];

        foreach ($host->getSlugData() as $slug) {
            if ($slug->isStatic() && isset(static::NAMES_MAP[$slug->getSlugType()])) {
                $entity = new Entity();
                /** @var Host $cloneHost */
                $cloneHost = clone $host;
                $cloneHost->setStatic(true);
                $cloneHost->setTypeSlug($slug->getSlug());
                $cloneHost->setEntityType($slug->getSlugType());
                $links[$slug->getSlugType()] = [
                    '__link' => $this->linkGenerator->generateLink($entity, $cloneHost),
                    'name' => static::NAMES_MAP[$slug->getSlugType()]
                ];
                unset($cloneHost);

            }
        }
        ksort($links);
        return ['links' => $links];
    }
}