<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\NavLinks\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Templator\Domain\Widgets\Extensions\NavLinks\Service\CategoryNavLinksInterface;

class CategoryNavLinkService implements CategoryNavLinksInterface
{
    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;

    const NAMES_MAP = [
        Type::COLLECTION_NEWS_CATEGORY => 'Category',
        Type::ROOT => 'Home'
    ];

    public function __construct(
        EntityLinkGeneratorInterface $linkGenerator,
        EntityToHostRepository $entityToHostRepository
    )
    {
        $this->linkGenerator = $linkGenerator;
        $this->entityToHostRepository = $entityToHostRepository;
    }

    public function getLinks(GetHostInterface $host): array
    {
        $links = [];

        foreach ($host->getSlugData() as $slug) {
            if ($slug->isStatic() && isset(static::NAMES_MAP[$slug->getSlugType()])) {
                $entityCategoryToHostCollection = $this->getCategoriesEntity($host, $slug->getSlugType());
                foreach ($entityCategoryToHostCollection as $entityCategoryToHost) {
                    $entity = new Entity();
                    $entity->setEntitySlug($entityCategoryToHost->getEntitySlug());
                    /** @var Host $cloneHost */
                    $cloneHost = clone $host;
                    $cloneHost->setStatic(true);
                    $cloneHost->setTypeSlug($slug->getSlug());
                    $cloneHost->setEntityType($slug->getSlugType());
                    $links[] = [
                        '__link' => $this->linkGenerator->generateLink($entity, $cloneHost),
                        'name' => $entity->getEntitySlug()
                    ];
                    unset($cloneHost);
                }
            }
        }
        $links[Type::ROOT] = [
            '__link' => '/',
            'name' => self::NAMES_MAP[Type::ROOT]
        ];
        ksort($links);
        return ['links' => $links];
    }

    /**
     * @param GetHostInterface $host
     * @param int $entityType
     * @return EntityToHost[]
     */
    private function getCategoriesEntity(GetHostInterface $host, int $entityType)
    {
        $entityToHosts = $this->entityToHostRepository->createQueryBuilder('eh')
            ->join('eh.host', 'h')
            ->join('eh.entity', 'e')
            ->where('h.id = :hostId')
            ->setParameter('hostId', $host->getHostId())
            ->andWhere('e.entity_type = :entityType')
            ->setParameter('entityType', $entityType)
            ->getQuery()
            ->getResult();
        return $entityToHosts;
    }
}