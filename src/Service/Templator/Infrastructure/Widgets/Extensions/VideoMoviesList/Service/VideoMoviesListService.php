<?php

namespace App\Service\Templator\Infrastructure\Widgets\Extensions\VideoMoviesList\Service;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\Service\Receiver\CollectionDTOReceiverInterface;
use App\Service\Processor\Infrastructure\DTO\Movie\Collection\Films;
use App\Service\Templator\Domain\Widgets\Extensions\VideoMoviesList\Service\VideoMoviesListInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\ListCollectionTrait;

class VideoMoviesListService implements VideoMoviesListInterface
{
    use ListCollectionTrait;

    /** @var CollectionDTOReceiverInterface */
    private $receiver;

    /**
     * @param CollectionDTOReceiverInterface $receiver
     */
    public function __construct(CollectionDTOReceiverInterface $receiver
    ) {
        $this->receiver = $receiver;
    }

    public function execute(GetHostInterface $host, int $count = 8): array
    {
        $result = $this->makeList($host,Type::COLLECTION_MOVIE_FILMS,'Топ Фильмов',$count);
        if(!empty($result)){
            return $result;
        }
        $dto = new Films();
        $dto->setName('Топ Фильмов');
        return $dto->toArray();
    }
}