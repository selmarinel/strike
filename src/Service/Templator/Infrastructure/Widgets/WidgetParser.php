<?php

namespace App\Service\Templator\Infrastructure\Widgets;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Templator\Domain\Widgets\Extensions\WidgetExtensionInterface;
use App\Service\Templator\Domain\Widgets\WidgetNeedRequestInterface;
use App\Service\Templator\Domain\Widgets\WidgetParserInterface;
use App\Service\Templator\Infrastructure\Widgets\Extensions\ArticleSimpleList\ArticleSimpleListExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\AudioArtistsList\AudioArtistsListExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\AudioTracksList\AudioTracksListExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\Breadcrumbs\BreadcrumbsExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\CategoryBreadcrumbs\CategoryBreadcrumbsExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\CrossLink\CrossLinkExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\NavLinks\NavLinkExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\TreeRider\TreeItemsExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\VideoMoviesList\VideoMoviesListExtension;
use App\Service\Templator\Infrastructure\Widgets\Extensions\VideoSeriesList\TvSeriesListExtension;

use App\Service\Templator\Infrastructure\Widgets\Extensions\YoutubeAdding\YoutubeAddingExtension;
use Psr\Container\ContainerExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

class WidgetParser implements WidgetParserInterface
{
    /** Widget Generator Hash array */
    const NEED_TO_HASH = [];
    /** Widget Generator Exclude array */
    const EXCLUDE_HASH = [];

    /** @var string[] */
    private $extensions = [
        CrossLinkExtension::SERVICE_NAME,
        NavLinkExtension::SERVICE_NAME,
        AudioArtistsListExtension::SERVICE_NAME,
        AudioTracksListExtension::SERVICE_NAME,
        BreadcrumbsExtension::SERVICE_NAME,
        TreeItemsExtension::SERVICE_NAME,
        TvSeriesListExtension::SERVICE_NAME,
        VideoMoviesListExtension::SERVICE_NAME,
        YoutubeAddingExtension::SERVICE_NAME,
        CategoryBreadcrumbsExtension::SERVICE_NAME,
        ArticleSimpleListExtension::SERVICE_NAME,
    ];
    /** @var ContainerInterface */
    private $container;
    /** @var  InputGetRequestInterface|null */
    private $request;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setRequest(InputGetRequestInterface $request)
    {
        $this->request = $request;
    }

    public function run(string &$content, GetHostInterface $host, Environment $environment, array $data)
    {
        $widgetsInTemplate = [];
        preg_match_all('/\[\%[A-Za-z\:\-\/\_\#\.0-9]+\%\]/', $content, $matches);
        foreach ($matches as $match) {
            $widgetsInTemplate = str_replace(['[', '%', ']'], '', $match);
        }
        $widgetNames = [];
        foreach ($widgetsInTemplate as $widgetPattern) {
            $widgetNames[$widgetPattern] = "/\[\%" . str_replace('/', '\\/', $widgetPattern) . "\%\]/";
        }
        $widgetsMap = [];
        $this->appendExtensions($host, $widgetsMap, $data, $widgetNames);

        if ($widgetNames) {
            $content = preg_replace($widgetNames, $widgetsMap, $content);
        }
    }

    private function appendExtensions(GetHostInterface $host, &$widgetsMap, &$entityData, &$widgetNames)
    {
        foreach ($this->extensions as $extension) {
            try {
                /** @var WidgetExtensionInterface $extension */
                $extension = $this->container->get($extension);

                if ($extension instanceof WidgetNeedRequestInterface && $this->request) {
                    $extension->setRequest($this->request);
                }

                if ($extension) {
                    $extension->appendExtension($host, $widgetsMap, $entityData, $widgetNames);
                    $this->sortOrder($widgetsMap, array_keys($widgetNames));
                }
            } catch (ContainerExceptionInterface $e) {
                //pass
            }
        }
    }

    public function sortOrder(array &$map, array $names)
    {
        $return = [];
        $keys = array_keys($map);
        foreach ($names as $name) {
            $keyIndex = array_search($name, $keys);
            if ($keyIndex !== false) {
                if (isset($keys[$keyIndex]) && isset($map[$keys[$keyIndex]])) {
                    $return[$name] = $map[$keys[$keyIndex]];
                }
            }
        }
        $map = $return;
    }

}