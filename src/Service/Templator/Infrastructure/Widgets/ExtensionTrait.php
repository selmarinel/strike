<?php

namespace App\Service\Templator\Infrastructure\Widgets;


trait ExtensionTrait
{
    public function processCollection(
        array $widgetNames,
        string $widgetPattern,
        int &$count,
        string &$fullName,
        int $defaultCount = 8
    )
    {
        $this->processValue($widgetNames, $widgetPattern, 'count', $fullName, $count, $defaultCount);
    }

    public function processValue(
        array $widgetNames,
        string $widgetPattern,
        string $field,
        string &$fullName,
        &$value,
        $defaultValue = null
    )
    {
        foreach ($widgetNames as $name => $pattern) {
            if (preg_match($widgetPattern, $name, $matches)) {
                if (isset($matches[$field])) {
                    $value = $matches[$field] ?: $defaultValue;
                    $fullName = $name;
                }
            }
        }
    }
}