<?php

namespace App\Service\Templator\Infrastructure\VO;


use App\Service\Templator\Domain\VO\MacrosVOInterface;

class MacrosVO implements MacrosVOInterface
{
    private $template = '';

    private $data = [];

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }
}