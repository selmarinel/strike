<?php

namespace App\Service\Templator\Infrastructure\EventDispatcher;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        $availableExceptions = [
            \Twig_Error_Loader::class,
        ];

        foreach ($availableExceptions as $exception) {
            if ($event->getException() instanceof $exception) {
                return $event->setResponse(new Response('',Response::HTTP_FOUND));
            }
        }
    }
}