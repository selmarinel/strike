<?php

namespace App\Service\Templator\Infrastructure\StatisticCatcher\Extensions\ImageCatcher;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Templator\Domain\StatisticCatcher\Extensions\StatisticCatcherInterface;

class StatisticImageCatcher implements StatisticCatcherInterface
{
    /** @var string */
    private $salt = 'i love to eat pancakes. v2';

    public function __construct()
    {
        if ($salt = getenv('STATISTIC_SALT')) {
            $this->salt = $salt;
        }
    }

    public function appendCatcherToContent(
        GetHostInterface $host,
        GetRequestInterface $request,
        string &$content): void
    {
        $imageSrc = '/0' . md5($this->salt . $host->getHostname()) . '.gif';
        $style = $this->getStyle($host);
        $component = "<img src='{$imageSrc}' style='{$style}'/>";
        $content = str_replace('</body>', "$component</body>", $content);
    }

    private function getStyle(GetHostInterface $host)
    {
        $crypt = $this->hostnameToInt($host->getHostname());
        if ($crypt % 2 == 0) {
            return 'display:none; width: 1px; height: 1px;';
        } else {
            $crypt = $crypt % 5;
            if ($crypt % 2 == 0) {
                return 'visibility: hidden; display: block; width: 0px; height: 0px;';
            }
            return 'position: fixed; top: -1px; left: -1px !important; width: 1px; height: 1px;';
        }
    }

    private function hostnameToInt(string $string): int
    {
        $integer = '';
        foreach (str_split($string) as $char) {
            $integer .= sprintf("%03s", ord($char));
        }
        return (int)$integer;
    }
}