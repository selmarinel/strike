<?php

namespace App\Service\Templator\Infrastructure\StatisticCatcher\Extensions\SSICatcher;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Templator\Domain\StatisticCatcher\Extensions\StatisticCatcherInterface;

class StatisticSSICatcher implements StatisticCatcherInterface
{
    public function appendCatcherToContent(GetHostInterface $host, GetRequestInterface $request, string &$content): void
    {
        $ssiParam = "<!--# include virtual=\"/stat/" . $request->getSlug() . "\" -->";
        $content .= $ssiParam;
    }
}