<?php

namespace App\Service\Templator\Infrastructure;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Templator\Domain\Data\EntityDataPreparationInterface;
use App\Service\Templator\Domain\StatisticCatcher\Extensions\StatisticCatcherInterface;
use App\Service\Templator\Domain\Template\MacrosParserInterface;
use App\Service\Templator\Domain\Widgets\WidgetParserInterface;
use Twig\Environment;

class Handle
{
    /** @var Environment */
    private $twig;
    /** @var MacrosParserInterface */
    private $macrosParser;
    /** @var EntityDataPreparationInterface */
    private $dataPreparation;
    /** @var WidgetParserInterface */
    private $widgetParser;


    public function __construct(
        Environment $twig,
        MacrosParserInterface $templateParser,
        WidgetParserInterface $widgetParser,
        EntityDataPreparationInterface $dataPreparation,
        StatisticCatcherInterface $catcher
    )
    {
        $this->macrosParser = $templateParser;
        $this->widgetParser = $widgetParser;
        $this->dataPreparation = $dataPreparation;
        $this->twig = $twig;
        $this->catcher = $catcher;
    }

    /**
     * @param GetHostInterface $host
     * @param DataObjectInterface $DTO
     * @param InputGetRequestInterface $request
     * @return mixed
     * @throws \Twig_Error_Loader
     */
    public function draw(
        GetHostInterface $host,
        DataObjectInterface $DTO,
        InputGetRequestInterface $request
    )
    {
        $data = $this->dataPreparation->prepare($host, $DTO);
        /** Todo refact */
        $data['__slug'] = $request->getSlug();
        $data['__type'] = $host->getHostType();
        $data['__text'] = isset($data['__text']) ? $data['__text'] : '';
        $template = $this->twig
            ->load("driver/{$host->getHostTypeDriver()}/{$host->getTemplateDriver()}/index.twig");
        try {
            $templateContent = $this->twig
                ->load("driver/{$host->getHostTypeDriver()}/{$host->getTemplateDriver()}/block/{$DTO->getTemplateName()}.twig")
                ->render($data);
        } catch (\Twig_Error_Loader $exception) {
            // when production return default block twig
            if (getenv('APP_ENV') == 'prod' || isset($_GET['__like_a_prod'])) {
                $templateContent = $this->twig
                    ->load("driver/base/block/{$DTO->getTemplateName()}.twig")
                    ->render($data);
            } else {
                // in the other hand throw this exception
                throw $exception;
            }
        }

        $content = $template->render(array_merge(['template' => $templateContent], $data));
        unset($templateContent);

        $this->macrosParser->run(
            $content,
            $host,
            $this->dataPreparation->seo($this->macrosParser->getMacros($host, $DTO))
        );
        $content = $this->twig->createTemplate($content)->render($data);
        $this->widgetParser->setRequest($request);
        $this->widgetParser->run($content, $host, $this->twig, $data);

        $this->catcher->appendCatcherToContent($host, $request, $content);
        return $content;
    }
}