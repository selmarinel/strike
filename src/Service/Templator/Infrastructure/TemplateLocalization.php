<?php

namespace App\Service\Templator\Infrastructure;


use App\Service\Templator\Domain\TemplateLocalizationInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class TemplateLocalization implements TemplateLocalizationInterface
{
    /** @var KernelInterface */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function localize(): array
    {
        $drivers = [];
        $root = $this->kernel->getRootDir();
        $dir = "$root/../templates/driver/";

        foreach (scandir($dir) as $driver) {
            if ($driver == '.' || $driver == '..' || $driver == 'base') {
                continue;
            }

            foreach (scandir($dir . $driver) as $childDir) {
                if ($childDir == '.' || $childDir == '..') {
                    continue;
                }

                if (is_dir($dir . $driver . '/' . $childDir)) {
                    $drivers[$driver][] = $childDir;
                }
            }
        }
        return $drivers;
    }
}