<?php

namespace App\Service\Templator\Domain\StatisticCatcher\Extensions;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\GetRequestInterface;

interface StatisticCatcherInterface
{
    public function appendCatcherToContent(
        GetHostInterface $host,
        GetRequestInterface $request,
        string &$content): void;
}