<?php

namespace App\Service\Templator\Domain\Data;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Templator\Domain\VO\MacrosVOInterface;

interface EntityDataPreparationInterface
{
    public function prepare(GetHostInterface $host, DataObjectInterface $DTO): array;

    public function seo(MacrosVOInterface $templateVO):array;
}