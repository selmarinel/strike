<?php

namespace App\Service\Templator\Domain;


interface TemplateLocalizationInterface
{
    public function localize(): array;
}