<?php

namespace App\Service\Templator\Domain\Template;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DTO\DataObjectInterface;
use App\Service\Templator\Domain\VO\MacrosVOInterface;

interface MacrosParserInterface
{
    public function getMacros(GetHostInterface $host, DataObjectInterface $DTO): MacrosVOInterface;

    public function run(string &$content, GetHostInterface $host, array $data);
}