<?php

namespace App\Service\Templator\Domain\Template\Extensions\SeoGeo\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface SeoGeoInterface
{
    public function getLinks(GetHostInterface $host): string;
}