<?php

namespace App\Service\Templator\Domain\Template\Extensions;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface MacroExtensionInterface
{
    public function appendExtension(GetHostInterface $host, array &$data);
}