<?php

namespace App\Service\Templator\Domain\Template\Extensions\GlobalMetaTags\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface GlobalMetaTagsInterface
{
    public function go(GetHostInterface $host);
}