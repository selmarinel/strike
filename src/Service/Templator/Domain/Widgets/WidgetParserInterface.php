<?php

namespace App\Service\Templator\Domain\Widgets;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use Twig\Environment;

interface WidgetParserInterface
{
    public function run(string &$content, GetHostInterface $host, Environment $environment, array $data);

    public function setRequest(InputGetRequestInterface $request);
}