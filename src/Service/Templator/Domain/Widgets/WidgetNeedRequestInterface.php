<?php

namespace App\Service\Templator\Domain\Widgets;


use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface WidgetNeedRequestInterface
{
    public function setRequest(InputGetRequestInterface $request);
}