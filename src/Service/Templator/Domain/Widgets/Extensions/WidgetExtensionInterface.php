<?php

namespace App\Service\Templator\Domain\Widgets\Extensions;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface WidgetExtensionInterface
{
    public function appendExtension(GetHostInterface $host, array &$widgetsMap, array &$data, array &$widgetNames);
}