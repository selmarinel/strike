<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\NavLinks;

interface NavLinkServiceFactoryInterface
{
    public function getService(bool $isCategory);
}