<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\NavLinks\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface NavLinksInterface
{
    public function getLinks(GetHostInterface $host): array;
}