<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\CategoryBreadcrumbs\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface CategoryBreadcrumbsServiceInterface
{
    public function getBreadcrumbs(GetHostInterface $host);
}