<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\TreeRider\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface TreeRiderInterface
{
    public function process(GetHostInterface $host);
}