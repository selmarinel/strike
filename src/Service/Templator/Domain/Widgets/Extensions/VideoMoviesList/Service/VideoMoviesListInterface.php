<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\VideoMoviesList\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface VideoMoviesListInterface
{
    public function execute(GetHostInterface $host, int $count = 8): array;
}