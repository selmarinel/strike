<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\AudioTracksList\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface AudioTracksListInterface
{
    public function execute(GetHostInterface $host, int $count): array;
}