<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\AudioArtistsList\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface AudioArtistsListInterface
{
    public function execute(GetHostInterface $host, int $count): array;
}