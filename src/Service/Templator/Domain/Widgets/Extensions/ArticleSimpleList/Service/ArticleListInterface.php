<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\ArticleSimpleList\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;

interface ArticleListInterface
{
    public function execute(GetHostInterface $host, InputGetRequestInterface $request, int $count = 12): array;
}