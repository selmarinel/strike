<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\Breadcrumbs\Service;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface BreadcrumbsServiceInterface
{
    /**
     * @param GetHostInterface $host
     * @return mixed
     */
    public function generateLinks(GetHostInterface $host);
}