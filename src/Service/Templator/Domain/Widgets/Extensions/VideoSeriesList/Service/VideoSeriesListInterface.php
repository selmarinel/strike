<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\VideoSeriesList\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface VideoSeriesListInterface
{
    public function execute(GetHostInterface $host, int $count = 8): array;
}