<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\CrossLink\Service;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface CrossLinkServiceInterface
{
    public function getLinks(GetHostInterface $host, int $countRange = 0): array;
}