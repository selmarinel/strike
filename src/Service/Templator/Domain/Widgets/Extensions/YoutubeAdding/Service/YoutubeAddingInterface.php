<?php

namespace App\Service\Templator\Domain\Widgets\Extensions\YoutubeAdding\Service;


use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface YoutubeAddingInterface
{
    public function process(GetHostInterface $host): array;
}