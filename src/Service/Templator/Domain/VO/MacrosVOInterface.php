<?php

namespace App\Service\Templator\Domain\VO;


interface MacrosVOInterface
{
    public function getData(): array;

    public function getTemplate(): string;

    public function setTemplate(string $string);

    public function setData(array $data);
}