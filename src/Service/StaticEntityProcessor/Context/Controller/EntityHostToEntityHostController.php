<?php

namespace App\Service\StaticEntityProcessor\Context\Controller;


use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use App\Service\StaticEntityProcessor\Infrastructure\Repository\EntityToEntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EntityHostToEntityHostController extends Controller implements AdministrateControllerInterface
{
    /** @var EntityToEntityRepository */
    private $entityToEntityRepository;

    public function __construct(EntityToEntityRepository $entityToEntityRepository)
    {
        $this->entityToEntityRepository = $entityToEntityRepository;
    }

    public function getCollection(Request $request)
    {
        return new JsonResponse([
            'collection' => $this->entityToEntityRepository->getCollection(
                ($request->get('limit')) ?: 20,
                ($request->get('offset')) ?: 0,
                $request->get('host_id')
            )
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Service\Administrate\Exception\SaveException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveRelation(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $mainId = $request['main_id'];
        $subordinateId = $request['subordinate_id'];
        $hostId = $request['host_id'];
        $toPosition = isset($request['position']) ? $request['position'] : 0;

        $entityRelation = $this->entityToEntityRepository->addEntityToEntity($mainId, $subordinateId, $hostId,
            $toPosition);
        return new JsonResponse(['entity_to_entity' => $entityRelation->serialize()]);
    }
}