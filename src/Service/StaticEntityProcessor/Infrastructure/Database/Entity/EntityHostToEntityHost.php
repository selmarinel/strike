<?php

namespace App\Service\StaticEntityProcessor\Infrastructure\Database\Entity;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=EntityHostToEntityHostRepository::class)
 * @ORM\Table(indexes={
 *     @Index(name="type_idx", columns={"type"}),
 *     @Index(name="position_idx", columns={"position"}),
 * })
 */
class EntityHostToEntityHost implements \Serializable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var EntityToHost
     *
     * @ORM\ManyToOne(targetEntity=EntityToHost::class)
     * @JoinColumn(name="main_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $main;
    /**
     * @var EntityToHost
     *
     * @ORM\ManyToOne(targetEntity=EntityToHost::class)
     * @JoinColumn(name="subordinate_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subordinate;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $position = 0;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"default":1})
     */
    private $type = Type::RELATION_TYPE_LIST;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return EntityToHost
     */
    public function getMain(): EntityToHost
    {
        return $this->main;
    }

    /**
     * @param EntityToHost $main
     */
    public function setMain(EntityToHost $main): void
    {
        $this->main = $main;
    }

    /**
     * @return EntityToHost
     */
    public function getSubordinate(): EntityToHost
    {
        return $this->subordinate;
    }

    /**
     * @param EntityToHost $subordinate
     */
    public function setSubordinate(EntityToHost $subordinate): void
    {
        $this->subordinate = $subordinate;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function serialize()
    {
        return [
            'main' => $this->getMain()->serialize(),
            'subordinate' => $this->getSubordinate()->serialize(),
            'position' => $this->getPosition(),
        ];
    }

    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}