<?php

namespace App\Service\StaticEntityProcessor\Infrastructure\Database\Repository;

use App\Helper\DoctrineFeatures\UseIndexWalker;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

class EntityHostToEntityHostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntityHostToEntityHost::class);
    }

    public function findById($id)
    {
        /** TODO refact to simple query */
        return $this->find($id);
    }

    public function findMany(int $limit = 10, int $offset = 0, $hostId = null)
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->join('e.main', 'm')
            ->join('e.subordinate', 's')
            ->andWhere('m.host = s.host');
        if ($hostId) {
            $queryBuilder->join('m.host', 'h')
                ->andWhere('h.id = :host_id')
                ->setParameter('host_id', $hostId);
        }
        return $queryBuilder->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     * @param int $offset
     * @param int $limit
     * @param int $type
     * @return mixed
     */
    public function findByMainId(int $id, int $offset = 0, int $limit = 10, int $type = Type::RELATION_TYPE_LIST)
    {
        $sql = "SELECT
  e0_.id             AS id_0,
  e0_.position       AS position_1,
  e0_.type           AS type_2,
  e1_.id             AS id_3,
  e1_.create_ts      AS create_ts_4,
  e1_.entity_slug    AS entity_slug_5,
  e2_.id             AS id_6,
  e2_.create_ts      AS create_ts_7,
  e2_.entity_slug    AS entity_slug_8,
  e3_.id             AS id_9,
  e3_.entity_type    AS entity_type_10,
  e3_.source_id      AS source_id_11,
  e3_.create_ts      AS create_ts_12,
  e3_.score          AS score_13,
  e3_.name           AS name_14,
  e0_.main_id        AS main_id_15,
  e0_.subordinate_id AS subordinate_id_16,
  e1_.host_id        AS host_id_17,
  e1_.entity_id      AS entity_id_18,
  e2_.host_id        AS host_id_19,
  e2_.entity_id      AS entity_id_20
FROM entity_host_to_entity_host e0_
  INNER JOIN entity_to_host e1_ ON e0_.main_id = e1_.id
  INNER JOIN entity_to_host e2_ ON e0_.subordinate_id = e2_.id
  INNER JOIN entity e3_ ON e2_.entity_id = e3_.id
  INNER JOIN (
    SELECT ehteh.id
    FROM entity_host_to_entity_host ehteh
    WHERE ehteh.main_id = :mainId
    AND ehteh.type = :type
    LIMIT :limit OFFSET :offset
    ) AS ei ON ei.id = e0_.id";

        $rsm = new Query\ResultSetMapping();
        $rsm->addEntityResult(EntityHostToEntityHost::class, 'e0_');
        $rsm->addFieldResult('e0_', 'id_0', 'id');
        $rsm->addFieldResult('e0_', 'position_1', 'position');
        $rsm->addFieldResult('e0_', 'type_2', 'type');
        $rsm->addJoinedEntityResult(EntityToHost::class, 'e1_', 'e0_', 'main');
        $rsm->addFieldResult('e1_', 'id_3', 'id');
        $rsm->addJoinedEntityResult(EntityToHost::class, 'e2_', 'e0_', 'subordinate');
        $rsm->addFieldResult('e2_', 'id_6', 'id');
        $rsm->addFieldResult('e2_', 'create_ts_7', 'createTs');
        $rsm->addFieldResult('e2_', 'entity_slug_8', 'entity_slug');
        $rsm->addJoinedEntityResult(Entity::class, 'e3_', 'e2_', 'entity');
        $rsm->addFieldResult('e3_', 'id_9', 'id');
        $rsm->addFieldResult('e3_', 'entity_type_10', 'entity_type');
        $rsm->addFieldResult('e3_', 'source_id_11', 'source_id');
        $rsm->addFieldResult('e3_', 'create_ts_12', 'createTs');
        $rsm->addFieldResult('e3_', 'score_13', 'score');
        $rsm->addFieldResult('e3_', 'name_14', 'name');
        $collection = $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('mainId', $id)
            ->setParameter('type', $type)
            ->setParameter('limit', $limit)
            ->setParameter('offset', $offset)
            ->getResult();

        return $collection;

    }

    public function getCount(int $id, int $type = Type::RELATION_TYPE_LIST): int
    {
        try {
            $count = $this->createQueryBuilder('e')
                ->select('count(e.id)')
                ->join('e.main', 'm')
                ->where('m.id = :id')
                ->andWhere('e.type = :type')
                ->setParameter('type', $type)
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
            return (int)$count;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * @param EntityHostToEntityHost $entityHostToEntityHost
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityHostToEntityHost $entityHostToEntityHost)
    {
        $this->getEntityManager()->persist($entityHostToEntityHost);
        $this->getEntityManager()->flush();
    }
}