<?php


namespace App\Service\StaticEntityProcessor\Infrastructure\DTO\Collection;


use App\Service\EntityProcessor\Domain\DTO\EntityTypesDTO\EntityDTOInterface;
use App\Service\EntityProcessor\Domain\VO\EntityTypesDTOVOInterface;
use App\Service\StaticEntityProcessor\Domain\DTO\Collection\CollectionDTOInterface;

/** @mixin CollectionDTOInterface */
abstract class AbstractCollectionDTO
{
    protected $collection;

    protected $entityType;

    protected $total = 0;

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    protected static $collectionName;

    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param EntityTypesDTOVOInterface $entityTypesDTOVO
     * @param array $collection
     */
    public function setCollection(
        EntityTypesDTOVOInterface $entityTypesDTOVO,
        array $collection
    ) {
        foreach ($collection as $item) {
            $entityDTOClass = $this->entityType;
            /** @var EntityDTOInterface $DTO */
            $DTO = new $entityDTOClass();
            $DTO->setParams($entityTypesDTOVO);
            $DTO->fill($item);
            $this->collection[] = $DTO->serialize();
            unset($DTO);
        }
    }

    /**
     * @return string
     */
    public static function getCollectionName(): string
    {
        return static::$collectionName;
    }

    /**
     * @param EntityTypesDTOVOInterface $entityTypesDTOVO
     * @param array $arguments
     */
    public function fill(EntityTypesDTOVOInterface $entityTypesDTOVO, array $arguments)
    {
        if (isset($arguments['collection'])) {
            $this->setCollection($entityTypesDTOVO, $arguments['collection']);
        }
    }

    public function serialize()
    {
        return [
            'total' => $this->getTotal(),

            'pages' => $this->pages
        ];
    }

    private $pages = [];

    public function setPages(array $pages)
    {
        $this->pages = $pages;
    }
}