<?php

namespace App\Service\StaticEntityProcessor\Infrastructure\DTO\Collection;


use App\Service\EntityProcessor\Infrastructure\DTO\EntityTypesDTO\AudioArtistDTO;
use App\Service\StaticEntityProcessor\Domain\DTO\Collection\CollectionDTOInterface;

class AudioArtistCollectionDTO extends AbstractCollectionDTO implements CollectionDTOInterface
{
    protected $entityType = AudioArtistDTO::class;

    protected static $collectionName = 'Audio Artists List';

    public function serialize(): array
    {
        return array_merge(parent::serialize(), [
            'artists' => $this->getCollection(),
            'title' => 'Artist List'
        ]);
    }
}