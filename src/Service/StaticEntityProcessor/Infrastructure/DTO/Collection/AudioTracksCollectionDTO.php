<?php

namespace App\Service\StaticEntityProcessor\Infrastructure\DTO\Collection;


use App\Service\EntityProcessor\Infrastructure\DTO\EntityTypesDTO\AudioTrackDTO;
use App\Service\StaticEntityProcessor\Domain\DTO\Collection\CollectionDTOInterface;

class AudioTracksCollectionDTO extends AbstractCollectionDTO implements CollectionDTOInterface
{
    protected $entityType = AudioTrackDTO::class;

    protected static $collectionName = 'Audio Tracks List';

    public function serialize(): array
    {
        return array_merge(parent::serialize(), ['tracks' => $this->getCollection()]);
    }

}