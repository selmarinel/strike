<?php

namespace App\Service\StaticEntityProcessor\Infrastructure\Repository;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use Doctrine\ORM\ORMException;

class EntityToEntityRepository
{
    /** @var EntityHostToEntityHostRepository */
    private $entityHostToEntityHostRepository;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;

    public function __construct(
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EntityToHostRepository $entityToHostRepository,

        EntitiesRepository $entitiesRepository,
        HostRepository $hostRepository

    ) {
        $this->entityHostToEntityHostRepository = $entityHostToEntityHostRepository;
        $this->entitiesRepository = $entitiesRepository;
        $this->hostRepository = $hostRepository;
        $this->entityToHostRepository = $entityToHostRepository;
    }

    public function getRepository()
    {
        return $this->entityHostToEntityHostRepository;
    }

    public function getCollection(int $limit = 10, int $offset = 0, $hostId = null)
    {
        $collection = $this->entityHostToEntityHostRepository->findMany($limit, $offset, $hostId);
        $result = [];
        /** @var EntityHostToEntityHost $entityHostToEntityHost */
        foreach ($collection as $entityHostToEntityHost) {
            $result[] = $entityHostToEntityHost->serialize();
        }
        return $result;
    }

    /**
     * @param int $mainId
     * @param int $subordinateId
     * @param int $hostId
     * @param int $toPosition
     * @return EntityHostToEntityHost
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \App\Service\Administrate\Exception\SaveException
     */
    public function addEntityToEntity(int $mainId, int $subordinateId, int $hostId, int $toPosition = 0)
    {
        /** @var EntityToHost $mainEntityToHost */
        $mainEntityToHost = $this->entityToHostRepository
            ->findByHostIdAndEntityId($hostId, $mainId);
        if (!$mainEntityToHost) {
            $host = $this->hostRepository->findById($hostId);
            $entity = $this->entitiesRepository->findById($mainId);
            $mainEntityToHost = $this->entityToHostRepository->createByHostAndEntity($host,$entity);
        }

        /** @var EntityToHost $subordinateEntityToHost */
        $subordinateEntityToHost = $this->entityToHostRepository->findByHostIdAndEntityId($hostId, $subordinateId);
        // check if exists (if need)
        if (!$subordinateEntityToHost) {
            throw new ORMException("EntityToHost with Host $hostId and Entity $subordinateId not exists");
        }
        $entityHostToEntityHost = new EntityHostToEntityHost();
        $entityHostToEntityHost->setMain($mainEntityToHost);
        $entityHostToEntityHost->setSubordinate($subordinateEntityToHost);
        $entityHostToEntityHost->setPosition($toPosition);
        $this->entityHostToEntityHostRepository->save($entityHostToEntityHost);

        return $entityHostToEntityHost;

    }
}