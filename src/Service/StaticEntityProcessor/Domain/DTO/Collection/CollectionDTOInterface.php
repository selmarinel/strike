<?php

namespace App\Service\StaticEntityProcessor\Domain\DTO\Collection;

use App\Service\EntityProcessor\Domain\VO\EntityTypesDTOVOInterface;

interface CollectionDTOInterface
{
    public function serialize(): array;

    public function fill(EntityTypesDTOVOInterface $entityTypesDTOVO, array $parameters);

    public function setTotal(int $total);

    public static function getCollectionName(): string;
}