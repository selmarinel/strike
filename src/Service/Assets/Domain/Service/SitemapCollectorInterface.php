<?php

namespace App\Service\Assets\Domain\Service;

use App\Service\Assets\Domain\VO\SitemapVOInterface;

interface SitemapCollectorInterface
{
    /**
     * @param SitemapVOInterface $assetsHostVO
     * @return mixed
     */
    public function getIndexSitemap(SitemapVOInterface $assetsHostVO);

    public function getSitemaps(SitemapVOInterface $assetsHostVO);
}