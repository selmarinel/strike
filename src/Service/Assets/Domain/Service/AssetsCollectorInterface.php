<?php

namespace App\Service\Assets\Domain\Service;

use App\Service\Assets\Domain\VO\AssetsHostVOInterface;

interface AssetsCollectorInterface
{
    /**
     * @param AssetsHostVOInterface $assetsHostVO
     * @return mixed
     */
    public function getAssetsData(AssetsHostVOInterface $assetsHostVO);
}