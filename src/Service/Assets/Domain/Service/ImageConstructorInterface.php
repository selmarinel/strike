<?php

namespace App\Service\Assets\Domain\Service;


interface ImageConstructorInterface
{
    public function produceImage(string $hash): string;

    public function setSize(int $size);
}