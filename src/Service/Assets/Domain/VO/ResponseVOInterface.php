<?php

namespace App\Service\Assets\Domain\VO;

interface ResponseVOInterface
{
    public function getHost(): string;

    public function getRedirect(): string;

    /**
     * Only for dev env
     * @return string
     */
    public function getRawAsset(): string;

    public function setHost(string $host): void;

    public function setRedirect(string $redirect): void;

    /**
     * Only for dev env
     * @param string $assetRaw
     */
    public function setRawAsset(string $assetRaw): void;
}