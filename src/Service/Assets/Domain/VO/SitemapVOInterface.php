<?php

namespace App\Service\Assets\Domain\VO;

use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

interface SitemapVOInterface
{
    public function setHost(GetHostInterface $host): void;

    public function setFileName(string $filename): void;

    public function setFileFormat(string $format): void;

    public function getHost(): GetHostInterface;

    public function getFileName(): string;

    public function getFileFormat(): string;
}