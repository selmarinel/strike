<?php

namespace App\Service\Assets\Infrastructure\EventDispatcher;

use App\Service\Assets\Infrastructure\Exceptions\FileNotFound;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof FileNotFound) {
            return $event->setResponse(new Response('', $event->getException()->getCode()));
        }
    }
}