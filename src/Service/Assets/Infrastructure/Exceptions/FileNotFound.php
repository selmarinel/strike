<?php

namespace App\Service\Assets\Infrastructure\Exceptions;

class FileNotFound extends \Exception
{
    protected $code = 404;

    protected $message = 'File not found';
}