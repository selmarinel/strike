<?php

namespace App\Service\Assets\Infrastructure\Service;

use App\Service\Assets\Domain\Service\AssetsCollectorInterface;
use App\Service\Assets\Domain\VO\AssetsHostVOInterface;
use App\Service\Assets\Domain\VO\ResponseVOInterface;
use App\Service\Assets\Infrastructure\Exceptions\FileNotFound;
use App\Service\Assets\Infrastructure\VO\ResponseVO;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\HttpKernel\KernelInterface;

class AssetsCollector implements AssetsCollectorInterface
{
    /** @var HostRepository  */
    private $hostRepository;
    /** @var KernelInterface  */
    private $kernel;

    /**
     * AssetsCollector constructor.
     * @param HostRepository $hostRepository
     * @param KernelInterface $kernel
     */
    public function __construct(
        HostRepository $hostRepository,
        KernelInterface $kernel
    ) {
        $this->hostRepository = $hostRepository;
        $this->kernel = $kernel;
    }

    /**
     * @param AssetsHostVOInterface $assetsHostVO
     * @return ResponseVOInterface
     * @throws FileNotFound
     */
    public function getAssetsData(AssetsHostVOInterface $assetsHostVO): ResponseVOInterface
    {
        $hostType = $assetsHostVO->getHost()->getHostTypeDriver();
        $driver = $assetsHostVO->getHost()->getTemplateDriver();

        $file = "{$assetsHostVO->getFileFolder()}/{$assetsHostVO->getFileName()}.{$assetsHostVO->getFileFormat()}";

        $rootDir = realpath($this->kernel->getRootDir() . '/../');
        $filePath = "/templates/driver/{$hostType}/{$driver}/$file";

        if (!file_exists($rootDir . $filePath)) {
            throw new FileNotFound();
        }

        $responseVO = new ResponseVO();

        if (getenv('APP_ENV') === 'dev') {
            $responseVO->setRawAsset(file_get_contents($rootDir . $filePath));
            return $responseVO;
        }

        $responseVO->setHost($assetsHostVO->getHost()->getHostname());
        $responseVO->setRedirect($filePath);

        return $responseVO;
    }
}