<?php

namespace App\Service\Assets\Infrastructure\Service;


use App\Service\Assets\Domain\Service\ImageConstructorInterface;
use Identicon\Identicon;

class ImageConstructor implements ImageConstructorInterface
{
    /** @var int */
    private $size = 200;

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    /**
     * @param string $hash
     * @return string
     */
    public function produceImage(string $hash): string
    {
        $imageValue = $hash ?: 'default';
        $identicon = new Identicon();
        return $identicon->getImageData($imageValue, $this->size);
    }
}