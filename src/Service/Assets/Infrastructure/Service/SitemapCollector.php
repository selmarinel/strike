<?php

namespace App\Service\Assets\Infrastructure\Service;

use App\Service\Assets\Domain\Service\SitemapCollectorInterface;
use App\Service\Assets\Domain\VO\SitemapVOInterface;
use App\Service\Assets\Infrastructure\Exceptions\FileNotFound;
use App\Service\Assets\Infrastructure\VO\ResponseVO;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\HttpKernel\KernelInterface;

class SitemapCollector implements SitemapCollectorInterface
{
    /** @var HostRepository  */
    private $hostRepository;
    /** @var KernelInterface  */
    private $kernel;

    public function __construct(
        HostRepository $hostRepository,
        KernelInterface $kernel
    ) {
        $this->hostRepository = $hostRepository;
        $this->kernel = $kernel;
    }

    public function getIndexSitemap(SitemapVOInterface $sitemapVO)
    {
        /** @var Host $parentHost */
        $parentHost = $this->hostRepository->find($sitemapVO->getHost()->getParentId());

        if (!$parentHost) {
            throw new FileNotFound();
        }

        $domain = $parentHost->getHostname();
        $subdomain = $sitemapVO->getHost()->getHostname();

        $rootDir = realpath($this->kernel->getRootDir() . '/../');
        $filePath = "/shared/sitemap/{$domain}/{$subdomain}/sitemap-index.xml";

        if (!file_exists($rootDir . $filePath)) {
            throw new FileNotFound();
        }

        /** @var ResponseVO $responseVO */
        $responseVO = new ResponseVO();

        if (getenv('APP_ENV') === 'dev') {
            $responseVO->setRawAsset(file_get_contents($rootDir . $filePath));
            return $responseVO;
        }

        $responseVO->setHost($subdomain);
        $responseVO->setRedirect($filePath);

        return $responseVO;
    }

    public function getSitemaps(SitemapVOInterface $sitemapVO)
    {
        /** @var Host $parentHost */
        $parentHost = $this->hostRepository->find($sitemapVO->getHost()->getParentId());

        if (!$parentHost) {
            throw new FileNotFound();
        }

        $domain = $parentHost->getHostname();
        $subdomain = $sitemapVO->getHost()->getHostname();

        $rootDir = realpath($this->kernel->getRootDir() . '/../');
        $filePath = "/shared/sitemap/{$domain}/{$subdomain}/{$sitemapVO->getFileName()}.xml";

        if (!file_exists($rootDir . $filePath)) {
            throw new FileNotFound();
        }

        /** @var ResponseVO $responseVO */
        $responseVO = new ResponseVO();

        if (getenv('APP_ENV') === 'dev') {
            $responseVO->setRawAsset(file_get_contents($rootDir . $filePath));
            return $responseVO;
        }

        $responseVO->setHost($subdomain);
        $responseVO->setRedirect($filePath);

        return $responseVO;
    }
}