<?php

namespace App\Service\Assets\Infrastructure\VO;

use App\Service\Assets\Domain\VO\SitemapVOInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

class SitemapVO implements SitemapVOInterface
{
    /** @var GetHostInterface */
    private $host;
    /** @var  string */
    private $fileName;
    /** @var  string */
    private $fileFormat;

    /**
     * @return GetHostInterface
     */
    public function getHost(): GetHostInterface
    {
        return $this->host;
    }

    /**
     * @param GetHostInterface $host
     */
    public function setHost(GetHostInterface $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileFormat(): string
    {
        return $this->fileFormat;
    }

    /**
     * @param string $fileFormat
     */
    public function setFileFormat(string $fileFormat): void
    {
        $this->fileFormat = $fileFormat;
    }


}