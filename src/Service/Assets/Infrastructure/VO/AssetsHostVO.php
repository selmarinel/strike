<?php

namespace App\Service\Assets\Infrastructure\VO;

use App\Service\Assets\Domain\VO\AssetsHostVOInterface;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;

class AssetsHostVO implements AssetsHostVOInterface
{
    /** @var GetHostInterface */
    private $host;
    /** @var  string */
    private $fileName;
    /** @var  string */
    private $fileFormat;
    /** @var  string */
    private $fileFolder;

    /**
     * @return string
     */
    public function getFileFormat(): string
    {
        return $this->fileFormat;
    }

    /**
     * @param string $fileFormat
     */
    public function setFileFormat(string $fileFormat): void
    {
        $this->fileFormat = $fileFormat;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileFolder(): string
    {
        return $this->fileFolder;
    }

    /**
     * @param string $fileFolder
     */
    public function setFileFolder(string $fileFolder): void
    {
        $this->fileFolder = $fileFolder;
    }

    /**
     * @return GetHostInterface
     */
    public function getHost(): GetHostInterface
    {
        return $this->host;
    }

    /**
     * @param GetHostInterface $host
     */
    public function setHost(GetHostInterface $host): void
    {
        $this->host = $host;
    }
}