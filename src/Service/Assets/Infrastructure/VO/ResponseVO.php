<?php

namespace App\Service\Assets\Infrastructure\VO;

use App\Service\Assets\Domain\VO\ResponseVOInterface;

class ResponseVO implements ResponseVOInterface
{
    /** @var  string */
    private $host;
    /** @var string  */
    private $redirect = '';
    /** @var string  */
    private $additionalUrlPart = '/internal_file';
    /** @var  string */
    private $assetRaw;

    /**
     * @return string
     */
    public function getRedirect(): string
    {
        return $this->additionalUrlPart . $this->redirect;
    }

    /**
     * @param string $redirect
     */
    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getRawAsset(): string
    {
        return $this->assetRaw;
    }

    /**
     * @param string $assetRaw
     */
    public function setRawAsset(string $assetRaw): void
    {
        $this->assetRaw = $assetRaw;
    }
}