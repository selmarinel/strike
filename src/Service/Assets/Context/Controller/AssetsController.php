<?php

namespace App\Service\Assets\Context\Controller;

use App\Service\Assets\Domain\Service\AssetsCollectorInterface;
use App\Service\Assets\Domain\Service\SitemapCollectorInterface;
use App\Service\Assets\Domain\VO\AssetsHostVOInterface;
use App\Service\Assets\Domain\VO\ResponseVOInterface;
use App\Service\Assets\Domain\VO\SitemapVOInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Processor\Domain\VO\Request\InputGetRequestInterface;
use App\Service\Processor\Domain\VO\Request\InputSetRequestInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AssetsController extends Controller
{
    /**
     * @param Request $request
     * @param AssetsHostVOInterface $assetsHostVO
     * @param AssetsCollectorInterface $assetsCollector
     * @param InputGetRequestInterface|InputSetRequestInterface $processorRequest
     * @param UrlParserServiceInterface $urlParserService
     * @return Response
     */
    public function index(
        Request $request,
        AssetsHostVOInterface $assetsHostVO,
        AssetsCollectorInterface $assetsCollector,
        InputGetRequestInterface $processorRequest,
        UrlParserServiceInterface $urlParserService
    ) {
        $processorRequest->setHostname($request->getHost());
        $processorRequest->setScheme($request->getScheme() ?? 'http');

        $host = $urlParserService->processRequest($processorRequest, true);

        $assetsHostVO->setHost($host);
        $assetsHostVO->setFileFolder($request->get('file_folder'));
        $assetsHostVO->setFileName($request->get('file_name'));
        $assetsHostVO->setFileFormat($request->get('_format'));

        /** @var ResponseVOInterface $assetsResponse */
        $assetsResponse = $assetsCollector->getAssetsData($assetsHostVO);

        if (getenv('APP_ENV') === 'dev') {
            return new Response($assetsResponse->getRawAsset());
        }

        $response = new Response();
        $response->headers->set('X-Accel-Redirect', $assetsResponse->getRedirect());
        $response->headers->set('X-Accel-Host', $assetsResponse->getHost());
        return $response;
    }

    /**
     * @param Request $request
     * @param InputGetRequestInterface $processorRequest
     * @param UrlParserServiceInterface $urlParserService
     * @param SitemapVOInterface $sitemapVO
     * @param SitemapCollectorInterface $sitemapCollector
     * @return Response
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostSlugException
     */
    public function indexSitemap(
        Request $request,
        InputGetRequestInterface $processorRequest,
        UrlParserServiceInterface $urlParserService,
        SitemapVOInterface $sitemapVO,
        SitemapCollectorInterface $sitemapCollector
    ) {
        $processorRequest->setHostname($request->getHost());
        $processorRequest->setScheme($request->getScheme() ?? 'http');

        $host = $urlParserService->processRequest($processorRequest, true);

        $sitemapVO->setHost($host);

        /** @var ResponseVOInterface $assetsResponse */
        $assetsResponse = $sitemapCollector->getIndexSitemap($sitemapVO);

        if (getenv('APP_ENV') === 'dev') {
            $response = new Response($assetsResponse->getRawAsset());
            $response->headers->set('Content-Type', 'xml');
            return $response;
        }

        $response = new Response();
        $response->headers->set('X-Accel-Redirect', $assetsResponse->getRedirect());
        $response->headers->set('X-Accel-Host', $assetsResponse->getHost());
        $response->headers->set('Content-Type', 'xml');
        return $response;
    }

    public function sitemap(
        Request $request,
        InputGetRequestInterface $processorRequest,
        UrlParserServiceInterface $urlParserService,
        SitemapVOInterface $sitemapVO,
        SitemapCollectorInterface $sitemapCollector
    ) {
        $processorRequest->setHostname($request->getHost());
        $processorRequest->setScheme($request->getScheme() ?? 'http');

        $host = $urlParserService->processRequest($processorRequest, true);

        $sitemapVO->setHost($host);
        $sitemapVO->setFileName($request->get('file_name'));

        /** @var ResponseVOInterface $assetsResponse */
        $assetsResponse = $sitemapCollector->getSitemaps($sitemapVO);

        if (getenv('APP_ENV') === 'dev') {
            $response = new Response($assetsResponse->getRawAsset());
            $response->headers->set('Content-Type', 'xml');
            return $response;
        }

        $response = new Response();
        $response->headers->set('X-Accel-Redirect', $assetsResponse->getRedirect());
        $response->headers->set('X-Accel-Host', $assetsResponse->getHost());
        $response->headers->set('Content-Type', 'xml');
        return $response;
    }
}