<?php

namespace App\Service\Metric\Domain\VO;


interface ArrayAbleInterface
{
    public function toArray(): array;
}