<?php

namespace App\Service\Metric\Domain\Service\MetricOutSource;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

interface PreparationInterface
{
    public function prepareStaticUrls(Host $host): array;

    public function prepareSomeContentUrls(Host $host):array;
}