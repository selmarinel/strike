<?php

namespace App\Service\Metric\Domain\Service\MetricOutSource;


use App\Service\Metric\Infrastructure\Service\MetricOutSource\VO\MetricRequestVO;

interface MetricFetcherInterface
{
    public function fetchUrls(MetricRequestVO $metricRequestVO): array;
}