<?php

namespace App\Service\Metric\Domain\Service;


use App\Service\Metric\Infrastructure\VO\MetricCollection;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;

interface MeasureInterface
{
    public function measurePeriod(GetPaginateInterface $paginate,bool $isWeek = false): MetricCollection;

    public function measureHost(int $hostId, bool $isWeek = false): MetricCollection;
}