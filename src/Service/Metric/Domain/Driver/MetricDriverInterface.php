<?php

namespace App\Service\Metric\Domain\Driver;


use App\Service\Metric\Infrastructure\VO\MeasurePeriodVO;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;

interface MetricDriverInterface
{
    public function getPeriodMetric(
        MeasurePeriodVO $measurePeriodVO,
        GetPaginateInterface $paginate
    );

    public function getHostMetric(
        MeasurePeriodVO $measurePeriodVO,
        int $hostId,
        bool $byWeek = false
    );

    public function getTotalErrorsMetric(MeasurePeriodVO $measurePeriodVO);
}