<?php

namespace App\Service\Metric\Infrastructure\Driver\Doctrine\Mysql;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Metric\Domain\Driver\MetricDriverInterface;
use App\Service\Metric\Infrastructure\VO\MeasurePeriodVO;
use App\Service\Metric\Infrastructure\VO\MetricDayHostVO;
use App\Service\Metric\Infrastructure\VO\MetricDayVO;
use App\Service\Metric\Infrastructure\VO\MetricWeekHostVO;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class MySQLDriver implements MetricDriverInterface
{
    /** @var EntityManagerInterface */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param MeasurePeriodVO $measurePeriodVO
     * @param GetPaginateInterface $paginate
     * @return array
     */
    public function getPeriodMetric(
        MeasurePeriodVO $measurePeriodVO,
        GetPaginateInterface $paginate
    )
    {
        $sql = $this->getSQLForMetric();
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('id', MetricDayVO::FIELD_ID);
        $rsm->addScalarResult('host_id', MetricDayVO::FIELD_HOST_ID);
        $rsm->addScalarResult('hostname', MetricDayVO::FIELD_HOSTNAME);
        $rsm->addScalarResult('error_status', MetricDayVO::FIELD_ERROR_STATUS);
        $rsm->addScalarResult('error_title', MetricDayVO::FIELD_ERROR_TITLE);
        $rsm->addScalarResult('error_length', MetricDayVO::FIELD_ERROR_SIZE);


        $collection = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter('from', $measurePeriodVO->getFrom()->format("Y-m-d"))
            ->setParameter('from_y', $measurePeriodVO->getFromPast()->format("Y-m-d"))
            ->setParameter('to', $measurePeriodVO->getTo()->format("Y-m-d"))
            ->setParameter('to_y', $measurePeriodVO->getToPast()->format("Y-m-d"))
            ->setParameter("limit", $paginate->getLimit())
            ->setParameter('offset', $paginate->getOffset())
            ->setParameter('status', Type::HOST_STATUS_ACTIVE)
            ->getArrayResult();

        uasort($collection, function ($itemA, $itemB) {
            $errorsA = $itemA['error_status'] + $itemA['error_title'] + $itemA['error_size'];
            $errorsB = $itemB['error_status'] + $itemB['error_title'] + $itemB['error_size'];

            if ($errorsA == $errorsB) {
                return 0;
            }
            return ($errorsA < $errorsB) ? 1 : -1;
        });
        return $collection;
    }

    /**
     * @param MeasurePeriodVO $measurePeriodVO
     * @return array
     */
    public function getTotalErrorsMetric(MeasurePeriodVO $measurePeriodVO)
    {
        $sql = $this->getCountOfErrorsForMetric();
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('error_status', MetricDayVO::FIELD_ERROR_STATUS);
        $rsm->addScalarResult('error_title', MetricDayVO::FIELD_ERROR_TITLE);
        $rsm->addScalarResult('error_length', MetricDayVO::FIELD_ERROR_SIZE);

        $collection = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter('from', $measurePeriodVO->getFrom()->format("Y-m-d"))
            ->setParameter('from_y', $measurePeriodVO->getFromPast()->format("Y-m-d"))
            ->setParameter('to', $measurePeriodVO->getTo()->format("Y-m-d"))
            ->setParameter('to_y', $measurePeriodVO->getToPast()->format("Y-m-d"))
            ->setParameter('status', Type::HOST_STATUS_ACTIVE)
            ->getArrayResult();
        $errors = array_shift($collection);

        return [
            'status' => $errors[MetricDayVO::FIELD_ERROR_STATUS],
            'title' => $errors[MetricDayVO::FIELD_ERROR_TITLE],
            'size' => $errors[MetricDayVO::FIELD_ERROR_SIZE],
        ];
    }

    /**
     * @param MeasurePeriodVO $measurePeriodVO
     * @param int $hostId
     * @param bool $byWeek
     * @return array
     */
    public function getHostMetric(
        MeasurePeriodVO $measurePeriodVO,
        int $hostId,
        bool $byWeek = false
    )
    {
        $sql = $this->getSQLForHost($byWeek);
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('id', MetricDayHostVO::FIELD_ID);
        $rsm->addScalarResult('host_id', MetricDayHostVO::FIELD_HOST_ID);
        $rsm->addScalarResult('link', MetricDayHostVO::FIELD_LINK);
        $rsm->addScalarResult('today_status', MetricDayHostVO::STATUS_TODAY);
        $rsm->addScalarResult('yesterday_status', MetricDayHostVO::STATUS_YESTERDAY);
        $rsm->addScalarResult('today_size', MetricDayHostVO::SIZE_TODAY);
        $rsm->addScalarResult('yesterday_size', MetricDayHostVO::SIZE_YESTERDAY);
        $rsm->addScalarResult('today_time', MetricDayHostVO::TIME_TODAY);
        $rsm->addScalarResult('yesterday_time', MetricDayHostVO::TIME_YESTERDAY);
        $rsm->addScalarResult('today_title', MetricDayHostVO::TITLE_TODAY);
        $rsm->addScalarResult('yesterday_title', MetricDayHostVO::TITLE_YESTERDAY);
        $rsm->addScalarResult('created', MetricWeekHostVO::FIELD_CREATED);
        $rsm->addScalarResult('created_past', MetricWeekHostVO::FIELD_PAST_CREATED);

        return $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter('from', $measurePeriodVO->getFrom()->format("Y-m-d"))
            ->setParameter('from_y', $measurePeriodVO->getFromPast()->format("Y-m-d"))
            ->setParameter('to', $measurePeriodVO->getTo()->format("Y-m-d"))
            ->setParameter('to_y', $measurePeriodVO->getToPast()->format("Y-m-d"))
            ->setParameter('hostId', $hostId)
            ->getArrayResult();
    }

    /**
     * @param bool $week
     * @return string
     */
    private function getSQLForHost(bool $week = false)
    {
        return "SELECT h.id, h.host_id, h.link,l.created,l2.created AS created_past, l.status AS today_status," .
            " l2.status AS yesterday_status, l.content_length AS today_size," .
            " l2.content_length AS yesterday_size, l.time AS today_time," .
            " l2.time AS yesterday_time, l.title AS today_title, l2.title AS yesterday_title" .
            " FROM metric_host_log l LEFT JOIN metric_host_log l2 " .
            " ON l.metric_host_id = l2.metric_host_id AND l2.created < l.created" .
            " AND l2.created BETWEEN :from_y AND :to_y" .
            " INNER JOIN metric_host h ON l.metric_host_id = h.id " .
            " WHERE l.created BETWEEN :from AND :to AND h.host_id = :hostId" .
            ((!$week) ? " GROUP BY h.id " : "") .
            " ORDER BY h.id";
    }

    /**
     * @return string
     */
    private function getSQLForMetric()
    {
        return "SELECT h.id, h.host_id, h2.hostname," .
            " SUM(IF(l.status != 200, 1, 0)) AS error_status," .
            " SUM(IF(l.title = '' || l.title != l2.title, 1, 0)) AS error_title," .
            " SUM(IF(l.content_length = 0 || (1 - (l.content_length / l2.content_length)) > 0.05, 1, 0)) AS error_length," .
            " SUM(IF(l.status != 200, 1, 0)) + SUM(IF(l.title = '' || l.title != l2.title, 1, 0)) + " .
            " SUM(IF(l.content_length = 0 || (1 - (l.content_length / l2.content_length)) > 0.05, 1, 0)) AS error_total," .
            " l.status AS today_status, l2.status AS yesterday_status, l.content_length AS today_size," .
            " l2.content_length AS yesterday_size, l.time AS today_time, l2.time AS yesterday_time," .
            " l.title AS today_title, l2.title AS yesterday_title, l.created, l2.created" .
            " FROM metric_host_log l LEFT JOIN metric_host_log l2 ON l.metric_host_id = l2.metric_host_id" .
            " AND l2.created < l.created AND l2.created BETWEEN :from_y AND :to_y" .
            " INNER JOIN metric_host h ON l.metric_host_id = h.id" .
            " INNER JOIN host h2 ON h.host_id = h2.id AND h2.status = :status" .
            " WHERE l.created BETWEEN :from AND :to GROUP BY h.host_id " .
            " ORDER BY error_total DESC, h.id " .
            "LIMIT :limit OFFSET :offset";
    }

    /**
     * @return string
     */
    private function getCountOfErrorsForMetric()
    {
        return "SELECT SUM(IF(l.status != 200 || l.status != l2.status, 1, 0)) AS error_status," .
            " SUM(IF(l.title = '' || l.title != l2.title, 1, 0)) AS error_title," .
            " SUM(IF(l.content_length = 0 || (1 - (l.content_length / l2.content_length)) > 0.05, 1, 0)) AS error_length" .
            " FROM metric_host_log l LEFT JOIN metric_host_log l2 ON l.metric_host_id = l2.metric_host_id" .
            " AND l2.created < l.created AND l2.created BETWEEN :from_y AND :to_y" .
            " INNER JOIN metric_host h ON l.metric_host_id = h.id" .
            " INNER JOIN host h2 ON h.host_id = h2.id AND h2.status = :status" .
            " WHERE l.created BETWEEN :from AND :to " .
            " ORDER BY SUM(IF(l.status != 200 || l.status != l2.status, 1, 0)) +" .
            " SUM(IF(l.title != l2.title, 1, 0)) +" .
            " SUM(IF((1 - (l.content_length / l2.content_length)) > 0.05, 1, 0)) DESC, h.id ";
    }
}