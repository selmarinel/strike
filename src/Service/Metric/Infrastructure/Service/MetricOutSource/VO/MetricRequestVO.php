<?php

namespace App\Service\Metric\Infrastructure\Service\MetricOutSource\VO;


class MetricRequestVO
{
    private $abuseLevel = 1;
    private $offset = 0;
    private $limit = 1000;

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getAbuseLevel(): int
    {
        return $this->abuseLevel;
    }

    /**
     * @param int $abuseLevel
     */
    public function setAbuseLevel(int $abuseLevel)
    {
        $this->abuseLevel = $abuseLevel;
    }

}