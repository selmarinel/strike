<?php

namespace App\Service\Metric\Infrastructure\Service\MetricOutSource;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Metric\Domain\Service\MetricOutSource\MetricFetcherInterface;
use App\Service\Metric\Domain\Service\MetricOutSource\PreparationInterface;
use App\Service\Metric\Infrastructure\Service\MetricOutSource\VO\MetricRequestVO;

class MetricFetcher implements MetricFetcherInterface
{
    /** @var PreparationInterface */
    private $preparation;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(HostRepository $hostRepository, PreparationInterface $preparation)
    {
        $this->hostRepository = $hostRepository;
        $this->preparation = $preparation;
    }

    public function fetchUrls(MetricRequestVO $metricRequestVO): array
    {
        $hosts = $this->hostRepository->findOnlyChildren(
            $metricRequestVO->getAbuseLevel(),
            $metricRequestVO->getLimit(),
            $metricRequestVO->getOffset());
        $urls = [];
        /** @var Host $host */
        foreach ($hosts as $host) {
            $urls = array_merge(
                $urls,
                $this->preparation->prepareStaticUrls($host)
            );
        }
        return $urls;
    }
}