<?php

namespace App\Service\Metric\Infrastructure\Service\MetricOutSource;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Metric\Domain\Service\MetricOutSource\PreparationInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Domain\VO\Slug\GetSlugInterface;

class Preparation implements PreparationInterface
{
    /** @var UriSlugParserInterface */
    private $uriSlugParser;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $linkGenerator;
    /** @var PrepareHostAndEntityInterface */
    private $preparation;

    public function __construct(
        UriSlugParserInterface $parser,
        EntityToHostRepository $entityToHostRepository,
        EntityLinkGeneratorInterface $linkGenerator,
        PrepareHostAndEntityInterface $prepareHostAndEntity
    )
    {
        $this->uriSlugParser = $parser;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->linkGenerator = $linkGenerator;
        $this->preparation = $prepareHostAndEntity;
    }

    private const AVAILABLE_STATIC_PAGES = [
        Type::HOST_TYPE_AUDIO => [
            Type::ROOT,
            Type::COLLECTION_AUDIO_ARTISTS,
            Type::COLLECTION_AUDIO_TRACKS,
        ],
        Type::HOST_TYPE_VIDEO => [
            Type::ROOT,
            Type::COLLECTION_VIDEO_SERIES,
            Type::COLLECTION_MOVIE_FILMS,
        ]
    ];

    private const AVAILABLE_CONTENT_PAGES = [
        Type::HOST_TYPE_AUDIO => [
            Type::AUDIO_TRACK,
//            Type::AUDIO_ARTIST,
//            Type::AUDIO_ALBUM,
//            Type::AUDIO_GENRE,
            Type::DEEZER_AUDIO_TRACK,
//            Type::DEEZER_AUDIO_ARTIST,
//            Type::DEEZER_AUDIO_ALBUM,
//            Type::DEEZER_AUDIO_GENRE
        ],
        Type::HOST_TYPE_VIDEO => [
            Type::VIDEO_EPISODE,
//            Type::VIDEO_SERIES,
//            Type::VIDEO_SEASON,
//            Type::VIDEO_MOVIES_GENRE,
            Type::MOVIE_FILM,
//            Type::VIDEO_SERIES_GENRE
        ]
    ];

    public function prepareStaticUrls(Host $host): array
    {
        $urls = [];
        $slugData = $this->uriSlugParser->parseSlug($host->getData());
        $types = self::AVAILABLE_STATIC_PAGES[$host->getType()];
        /** @var GetSlugInterface $slug */
        foreach ($slugData as $slug) {
            if (in_array($slug->getSlugType(), $types)) {
                $urls[] = [
                    'period' => '1d',
                    'url' => 'http://' . $host->getHostname() . '/' . $slug->getSlug()
                ];
            }
        }
        return $urls;
    }

    public function prepareSomeContentUrls(Host $host): array
    {
        $urls = [];

        foreach (self::AVAILABLE_CONTENT_PAGES[$host->getType()] as $type) {
            /** @var EntityToHost $entityToHost */
            $entityToHost = $this->entityToHostRepository
                ->createQueryBuilder('eh')
                ->join('eh.entity', 'e')
                ->where('eh.host = :host')
                ->andWhere('eh.entity_slug != :empty')
                ->andWhere('e.entity_type = :entityType')
                ->setParameter('host', $host->getParent())
                ->setParameter('empty', '')
                ->setParameter('entityType',$type)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
            if(!$entityToHost){
                continue;
            }
            $entityToHost->setHost($host);
            $this->preparation->prepareFromEntityToHost($entityToHost);
            $urls[] = [
                'period' => '1d',
                'url' => $this->linkGenerator->generateLink($this->preparation->getEntityDAO(), $this->preparation->getHostDAO())
            ];
        }
        return $urls;
    }
}