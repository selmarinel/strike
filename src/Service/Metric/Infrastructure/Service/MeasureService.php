<?php

namespace App\Service\Metric\Infrastructure\Service;

use App\Service\Base\Infrastructure\Repository\Metric\Host\MetricHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Metric\Domain\Driver\MetricDriverInterface;
use App\Service\Metric\Domain\Service\MeasureInterface;
use App\Service\Metric\Infrastructure\VO\MeasurePeriodVO;
use App\Service\Metric\Infrastructure\VO\MetricCollection;
use App\Service\Metric\Infrastructure\VO\MetricDayHostVO;
use App\Service\Metric\Infrastructure\VO\MetricDayVO;
use App\Service\Metric\Infrastructure\VO\MetricWeekHostVO;
use App\Service\Processor\Domain\VO\Request\GetPaginateInterface;
use App\Service\Processor\Infrastructure\VO\Request;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MeasureService implements MeasureInterface
{
    /** @var MetricDriverInterface */
    private $driver;

    /**
     * MeasureService constructor.
     * @param MetricDriverInterface $metricDriver
     */
    public function __construct(MetricDriverInterface $metricDriver)
    {
        $this->driver = $metricDriver;
    }

    /**
     * @param GetPaginateInterface $paginate
     * @param bool $isWeek
     * @return MetricCollection
     */
    public function measurePeriod(GetPaginateInterface $paginate, bool $isWeek = false): MetricCollection
    {
        $period = new MeasurePeriodVO($isWeek);
        $collection = $this->driver->getPeriodMetric($period, $paginate);

        $result = new MetricCollection();
        foreach ($collection as $item) {
            $result->addItem(new MetricDayVO($item));
        }
        return $result;
    }

    /**
     * @param int $hostId
     * @param bool $isWeek
     * @return MetricCollection
     */
    public function measureHost(int $hostId, bool $isWeek = false): MetricCollection
    {
        if ($isWeek) {
            $result = new MetricCollection();
            //make trash ugar and sodomy
            $period = new MeasurePeriodVO(false);
            for ($i = 0; $i < 7; $i++) {
                $period->setFrom((new \DateTime())->modify("-{$i} days"));
                $period->setTo((new \DateTime())->modify("-{$i} days"));
                $period->setFromPast((new \DateTime())->modify("-1 week -{$i} days"));
                $period->setToPast((new \DateTime())->modify("-1 week -{$i} days"));
                $collection = $this->driver->getHostMetric($period, $hostId);
                foreach ($collection as $item) {
                    $result->addItem(new MetricWeekHostVO($item));
                }
            }
            return $result;
        }

        $period = new MeasurePeriodVO($isWeek);
        $collection = $this->driver->getHostMetric($period, $hostId, $isWeek);

        $measureVOClass = MetricDayHostVO::class;
        if ($isWeek) {
            $measureVOClass = MetricWeekHostVO::class;
        }

        $result = new MetricCollection();
        foreach ($collection as $item) {
            $result->addItem(new $measureVOClass($item));
        }
        return $result;
    }
}