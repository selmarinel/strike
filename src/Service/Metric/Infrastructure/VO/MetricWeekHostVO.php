<?php

namespace App\Service\Metric\Infrastructure\VO;


use App\Service\Metric\Domain\VO\ArrayAbleInterface;

class MetricWeekHostVO extends MetricDayHostVO implements ArrayAbleInterface
{
    const FIELD_CREATED = 'created';
    const FIELD_PAST_CREATED = 'created_past';

    /** @var \DateTimeInterface */
    protected $created;

    /** @var \DateTimeInterface */
    protected $createdPast;

    public function __construct(array $dataToFill)
    {
        parent::__construct($dataToFill);
        $this->created = new \DateTime($dataToFill[self::FIELD_CREATED]);
        if ($dataToFill[self::FIELD_PAST_CREATED]) {
            $this->createdPast = new \DateTime($dataToFill[self::FIELD_PAST_CREATED]);
        }
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'link' => $this->getLink(),
            'present' => [
                'date' => $this->getCreated()->format("Y-m-d"),
                'size' => $this->getSizeToday(),
                'title' => $this->getTitleToday(),
                'time' => $this->getTimeToday(),
                'status' => $this->getStatusToday()
            ],
            'past' => [
                'date' => ($this->getCreatedPast()) ? $this->getCreatedPast()->format("Y-m-d") : null,
                'size' => $this->getSizeYesterday(),
                'title' => $this->getTitleYesterday(),
                'time' => $this->getTimeYesterday(),
                'status' => $this->getStatusYesterday()
            ]
        ];
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param \DateTimeInterface $created
     */
    public function setCreated(\DateTimeInterface $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedPast(): ?\DateTimeInterface
    {
        return $this->createdPast;
    }

    /**
     * @param \DateTimeInterface $createdPast
     */
    public function setCreatedPast(\DateTimeInterface $createdPast): void
    {
        $this->createdPast = $createdPast;
    }
}