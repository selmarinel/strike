<?php

namespace App\Service\Metric\Infrastructure\VO;


use App\Service\Metric\Domain\VO\ArrayAbleInterface;

class MetricDayHostVO implements ArrayAbleInterface
{
    const FIELD_ID = 'id';
    const FIELD_LINK = 'link';
    const FIELD_HOST_ID = 'host_id';

    const STATUS_TODAY = 'status_today';
    const STATUS_YESTERDAY = 'status_yesterday';

    const SIZE_TODAY = 'size_today';
    const SIZE_YESTERDAY = 'size_yesterday';

    const TITLE_TODAY = 'title_today';
    const TITLE_YESTERDAY = 'title_yesterday';

    const TIME_TODAY = 'time_today';
    const TIME_YESTERDAY = 'time_yesterday';

    protected $id;

    protected $link;

    protected $hostId;

    protected $statusToday = 0;

    protected $statusYesterday = 0;

    protected $sizeToday = 0;

    protected $sizeYesterday = 0;

    protected $titleToday = '';

    protected $titleYesterday = '';

    protected $timeToday = 0;

    protected $timeYesterday = 0;

    public function __construct(array $dataToFill)
    {
        $this->setId($dataToFill[self::FIELD_ID]);
        $this->setLink($dataToFill[self::FIELD_LINK]);
        $this->setHostId((int)$dataToFill[self::FIELD_HOST_ID]);

        $this->setSizeToday((int)$dataToFill[self::SIZE_TODAY]);
        $this->setSizeYesterday((int)$dataToFill[self::SIZE_YESTERDAY]);

        $this->setStatusToday((int)$dataToFill[self::STATUS_TODAY]);
        $this->setStatusYesterday((int)$dataToFill[self::STATUS_YESTERDAY]);

        $this->setTitleToday((string)$dataToFill[self::TITLE_TODAY]);
        $this->setTitleYesterday((string)$dataToFill[self::TITLE_YESTERDAY]);

        $this->setTimeToday((int)$dataToFill[self::TIME_TODAY]);
        $this->setTimeYesterday((int)$dataToFill[self::TIME_YESTERDAY]);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'link' => $this->getLink(),
            'today' => [
                'size' => $this->getSizeToday(),
                'title' => $this->getTitleToday(),
                'time' => $this->getTimeToday(),
                'status' => $this->getStatusToday()
            ],
            'yesterday' => [
                'size' => $this->getSizeYesterday(),
                'title' => $this->getTitleYesterday(),
                'time' => $this->getTimeYesterday(),
                'status' => $this->getStatusYesterday()
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getHostId()
    {
        return $this->hostId;
    }

    /**
     * @param mixed $hostId
     */
    public function setHostId($hostId): void
    {
        $this->hostId = $hostId;
    }

    /**
     * @return int
     */
    public function getStatusToday(): int
    {
        return $this->statusToday;
    }

    /**
     * @param int $statusToday
     */
    public function setStatusToday(int $statusToday): void
    {
        $this->statusToday = $statusToday;
    }

    /**
     * @return int
     */
    public function getStatusYesterday(): int
    {
        return $this->statusYesterday;
    }

    /**
     * @param int $statusYesterday
     */
    public function setStatusYesterday(int $statusYesterday): void
    {
        $this->statusYesterday = $statusYesterday;
    }

    /**
     * @return int
     */
    public function getSizeToday(): int
    {
        return $this->sizeToday;
    }

    /**
     * @param int $sizeToday
     */
    public function setSizeToday(int $sizeToday): void
    {
        $this->sizeToday = $sizeToday;
    }

    /**
     * @return int
     */
    public function getSizeYesterday(): int
    {
        return $this->sizeYesterday;
    }

    /**
     * @param int $sizeYesterday
     */
    public function setSizeYesterday(int $sizeYesterday): void
    {
        $this->sizeYesterday = $sizeYesterday;
    }

    /**
     * @return string
     */
    public function getTitleToday(): string
    {
        return $this->titleToday;
    }

    /**
     * @param string $titleToday
     */
    public function setTitleToday(string $titleToday): void
    {
        $this->titleToday = $titleToday;
    }

    /**
     * @return string
     */
    public function getTitleYesterday(): string
    {
        return $this->titleYesterday;
    }

    /**
     * @param string $titleYesterday
     */
    public function setTitleYesterday(string $titleYesterday): void
    {
        $this->titleYesterday = $titleYesterday;
    }

    /**
     * @return int
     */
    public function getTimeToday(): int
    {
        return $this->timeToday;
    }

    /**
     * @param int $timeToday
     */
    public function setTimeToday(int $timeToday): void
    {
        $this->timeToday = $timeToday;
    }

    /**
     * @return int
     */
    public function getTimeYesterday(): int
    {
        return $this->timeYesterday;
    }

    /**
     * @param int $timeYesterday
     */
    public function setTimeYesterday(int $timeYesterday): void
    {
        $this->timeYesterday = $timeYesterday;
    }
}