<?php

namespace App\Service\Metric\Infrastructure\VO;


class MeasurePeriodVO
{
    /** @var \DateTimeInterface */
    private $from;
    /** @var \DateTimeInterface */
    private $to;
    /** @var \DateTimeInterface */
    private $fromPast;
    /** @var \DateTimeInterface */
    private $toPast;

    public function __construct(bool $isWeek)
    {
        $this->from = (new \DateTime());
        $this->to = (new \DateTime());

        $this->fromPast = (new \DateTime())->modify('-1 day');
        $this->toPast = (new \DateTime())->modify('-1 day');

        if ($isWeek) {
            $this->from = (new \DateTime())->modify('-1 week +1 day');

            $this->fromPast = (clone $this->from)->modify('-1 week -1day');
            $this->toPast = (clone $this->to)->modify('-1 week');
        }
    }

    /**
     * @param \DateTimeInterface $from
     */
    public function setFrom(\DateTimeInterface $from): void
    {
        $this->from = $from;
    }

    /**
     * @param \DateTimeInterface $to
     */
    public function setTo(\DateTimeInterface $to): void
    {
        $this->to = $to;
    }

    /**
     * @param \DateTimeInterface $fromPast
     */
    public function setFromPast(\DateTimeInterface $fromPast): void
    {
        $this->fromPast = $fromPast;
    }

    /**
     * @param \DateTimeInterface $toPast
     */
    public function setToPast(\DateTimeInterface $toPast): void
    {
        $this->toPast = $toPast;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getFrom(): \DateTimeInterface
    {
        return $this->from;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTo(): \DateTimeInterface
    {
        return $this->to;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getFromPast(): \DateTimeInterface
    {
        return $this->fromPast;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getToPast(): \DateTimeInterface
    {
        return $this->toPast;
    }
}