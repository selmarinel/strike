<?php

namespace App\Service\Metric\Infrastructure\VO;

use App\Service\Metric\Domain\VO\ArrayAbleInterface;

class MetricCollection
{
    /** @var array */
    private $collection = [];

    public function __construct(array $items = [])
    {
        $this->collection = $items;
    }

    public function addItem($item)
    {
        $this->collection[] = $item;
    }

    public function map(callable $callback)
    {
        $keys = array_keys($this->collection);
        $items = array_map($callback, $this->collection, $keys);
        return new static(array_combine($keys, $items));
    }

    public function toArray()
    {
        return array_map(function ($value) {
            return $value instanceof ArrayAbleInterface ? $value->toArray() : $value;
        }, $this->collection);
    }
}