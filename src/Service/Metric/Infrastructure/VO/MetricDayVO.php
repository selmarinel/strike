<?php

namespace App\Service\Metric\Infrastructure\VO;


use App\Service\Metric\Domain\VO\ArrayAbleInterface;

class MetricDayVO implements ArrayAbleInterface
{
    const FIELD_ID = 'id';
    const FIELD_HOST_ID = 'host_id';
    const FIELD_HOSTNAME = 'hostname';

    const FIELD_ERROR_STATUS = 'error_status';
    const FIELD_ERROR_TITLE = 'error_title';
    const FIELD_ERROR_SIZE = 'error_size';

    private $id = 0;

    private $hostId = 0;

    private $hostname = '';

    private $errorStatus = 0;

    private $errorTitle = 0;

    private $errorSize = 0;

    public function __construct(array $dataToFill)
    {
        $this->setHostId($dataToFill[self::FIELD_HOST_ID]);
        $this->setHostname($dataToFill[self::FIELD_HOSTNAME]);

        $this->setErrorStatus((int)$dataToFill[self::FIELD_ERROR_STATUS]);
        $this->setErrorSize((int)$dataToFill[self::FIELD_ERROR_SIZE]);
        $this->setErrorTitle((int)$dataToFill[self::FIELD_ERROR_TITLE]);


    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname): void
    {
        $this->hostname = $hostname;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getErrorStatus(): int
    {
        return $this->errorStatus;
    }

    /**
     * @param int $errorStatus
     */
    public function setErrorStatus(int $errorStatus): void
    {
        $this->errorStatus = $errorStatus;
    }

    /**
     * @return int
     */
    public function getErrorTitle(): int
    {
        return $this->errorTitle;
    }

    /**
     * @param int $errorTitle
     */
    public function setErrorTitle(int $errorTitle): void
    {
        $this->errorTitle = $errorTitle;
    }

    /**
     * @return int
     */
    public function getErrorSize(): int
    {
        return $this->errorSize;
    }

    /**
     * @param int $errorSize
     */
    public function setErrorSize(int $errorSize): void
    {
        $this->errorSize = $errorSize;
    }

    /**
     * @return int
     */
    public function getHostId(): int
    {
        return $this->hostId;
    }

    /**
     * @param int $hostId
     */
    public function setHostId(int $hostId): void
    {
        $this->hostId = $hostId;
    }

    public function toArray(): array
    {
        return [
            'host_id' => $this->getHostId(),
            'hostname' => $this->getHostname(),
            'total_errors' => $this->getErrorTitle() + $this->getErrorSize() + $this->getErrorStatus(),
            'errors' => [
                'size' => $this->getErrorSize(),
                'title' => $this->getErrorTitle(),
                'status' => $this->getErrorStatus()
            ],
        ];
    }
}