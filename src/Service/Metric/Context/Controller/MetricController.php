<?php

namespace App\Service\Metric\Context\Controller;


use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Metric\Host\MetricHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Metric\Domain\Service\MeasureInterface;
use App\Service\Metric\Domain\Service\MetricOutSource\MetricFetcherInterface;
use App\Service\Metric\Infrastructure\Service\MetricOutSource\VO\MetricRequestVO;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Processor\Infrastructure\VO\Request as RequestVO;

class MetricController extends Controller implements AdministrateControllerInterface
{
    /**
     * todo remove logic from controller
     * @param Request $request
     * @param MetricFetcherInterface $metricFetcher
     * @return JsonResponse
     * @internal param HostRepository $hostRepository
     * @internal param UriSlugParserInterface $parser
     */
    public function sitesAction(Request $request, MetricFetcherInterface $metricFetcher)
    {
        $metricVO = new MetricRequestVO();
        $metricVO->setLimit($request->get('limit') ?: 100);
        $metricVO->setOffset($request->get('offset') ?: 0);
        $metricVO->setAbuseLevel($request->get('abuse') ?: 1);
        $urls = $metricFetcher->fetchUrls($metricVO);
        return new JsonResponse($urls);
    }

    /**
     * @param Request $request
     * @param MeasureInterface $measure
     * @param MetricHostRepository $repository
     * @return JsonResponse
     */
    public function getDayByDay(Request $request, MeasureInterface $measure, MetricHostRepository $repository)
    {
        $requestVO = new RequestVO;
        $requestVO->setLimit((int)$request->get('limit') ?: 25);
        $requestVO->setPage((int)$request->get('page') ?? 0);
        return new JsonResponse([
            'total' => $repository->getTotalMeasuredHosts(),
            'data' => $measure->measurePeriod($requestVO)->toArray(),
        ]);
    }

    /**
     * @param Request $request
     * @param MeasureInterface $measure
     * @param MetricHostRepository $repository
     * @return JsonResponse
     */
    public function getWeekByWeek(Request $request, MeasureInterface $measure, MetricHostRepository $repository)
    {
        $requestVO = new RequestVO;
        $requestVO->setLimit((int)$request->get('limit') ?: 25);
        $requestVO->setPage((int)$request->get('page') ?? 0);

        return new JsonResponse([
            'total' => $repository->getTotalMeasuredHosts(),
            'data' => $measure->measurePeriod($requestVO, true)->toArray(),
        ]);
    }

    /**
     * @param int $hostId
     * @param MeasureInterface $measure
     * @return JsonResponse
     */
    public function getHostByDay(int $hostId, MeasureInterface $measure)
    {
        return new JsonResponse($measure->measureHost($hostId)->toArray());
    }

    /**
     * @param int $hostId
     * @param MeasureInterface $measure
     * @return JsonResponse
     */
    public function getHostByWeek(int $hostId, MeasureInterface $measure)
    {
        return new JsonResponse($measure->measureHost($hostId, true)->toArray());
    }
}