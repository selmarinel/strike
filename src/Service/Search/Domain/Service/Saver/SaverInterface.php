<?php

namespace App\Service\Search\Domain\Service\Saver;


use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

interface SaverInterface
{
    public function getEntitySearchEntity(): Entity;

    public function getEntityToHost(Entity $entity, Host $host, string $slug);

    public function appendSearchResults(SearchEntity $searchEntity, EntityToHost $entityToHost, array $results);

    public function createSearchEntity(Host $host, string $query);

    public function remove(Host $host, string $query): bool;

    public function commit();
}