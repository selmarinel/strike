<?php

namespace App\Service\Search\Domain\Service\Parser\Youtube\Fetcher;


use App\Service\Search\Infrastructure\Exception\Fetcher\FetcherNotFoundException;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;
use App\Service\Search\Infrastructure\Service\VO\Youtube\ResponseYoutubeItemVO;

interface FetcherInterface
{
    /**
     * @param RequestFetcherYouTubeVO $requestFetcherYouTubeVO
     * @return ResponseYoutubeItemVO[]
     * @throws FetcherNotFoundException
     */
    public function fetchData(RequestFetcherYouTubeVO $requestFetcherYouTubeVO): array;
}