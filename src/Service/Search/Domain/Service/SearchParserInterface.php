<?php

namespace App\Service\Search\Domain\Service;


use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;

interface SearchParserInterface
{
    public function parse(RequestFetcherYouTubeVO $requestFetcherYouTubeVO, SearchEntity $searchEntity, bool $save = false);

    public function remove(RequestFetcherYouTubeVO $requestFetcherYouTubeVO, Host $host, bool $save = false);

    public function addWithoutYoutubeSearch(RequestFetcherYouTubeVO $requestFetcherYouTubeVO, Host $host, bool $save = false);

    public function save();
}