<?php

namespace App\Service\Search\Domain\Service\Searcher\Driver;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;

interface SearchDriverInterface
{
    public function connect();

    /** todo change to VO */
    public function prepareRecord(EntityToHost $entityToHost, array &$params);

    public function updateRecord(string $id, array $data);

    public function deleteRecord(string $id);

    public function saveRecords(array $params);

    public function search(array $query);

    public function searchByName(string $name, int $hostId);


}