<?php

namespace App\Service\Search\Domain\Service\Searcher;
use App\Service\Search\Domain\Service\Searcher\VO\GetSearchRequestInterface;

interface SearchInterface
{
    /**
     * @param string $query
     * @param GetSearchRequestInterface $request
     * @return \App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost[]|array
     */
    public function search(GetSearchRequestInterface $request);
}