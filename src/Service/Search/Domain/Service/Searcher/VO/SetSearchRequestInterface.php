<?php

namespace App\Service\Search\Domain\Service\Searcher\VO;


interface SetSearchRequestInterface
{
    public function setQuery(string $query): void;

    public function setHostId(?int $hostId): void;

    public function setType(?int $type): void;
}