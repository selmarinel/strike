<?php

namespace App\Service\Search\Domain\Service\Searcher\VO;


use App\Service\Processor\Domain\VO\Request\GetRequestInterface;

interface GetSearchRequestInterface extends GetRequestInterface
{
    public function getQuery(): string;

    public function getHostId(): ?int;

    public function getType(): ?int;

    public function getHostname(): string;

    public function getRequestUri(): string;
}