<?php
/**
 * Created by PhpStorm.
 * User: yuriiastahov
 * Date: 11.07.18
 * Time: 16:38
 */

namespace App\Service\Search\Domain\Service\Searcher\Provider;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Search\Domain\Service\Searcher\VO\GetSearchRequestInterface;

interface ProviderInterface
{
    /**
     * @param GetSearchRequestInterface $request
     * @param GetHostInterface $host
     * @return EntityToHost[]
     */
    public function search(GetSearchRequestInterface $request, GetHostInterface $host): array;

    public function searchByName(string $name, GetHostInterface $host): array;
}