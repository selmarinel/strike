<?php

namespace App\Service\Search\Context\Controller;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Search\Domain\Service\Searcher\SearchInterface;
use App\Service\Search\Infrastructure\Service\Searcher\VO\SearchRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function search(
        Request $request,
        SearchInterface $fetcher,
        RequestPreparationInterface $requestPreparation
    )
    {
        $processorRequest = $requestPreparation->prepareFromHttpRequest($request);
        $searchRequest = new SearchRequest($processorRequest);

        if (!$request->get('query')) {
            return new JsonResponse(['error' => 'Query is empty'], JsonResponse::HTTP_NOT_FOUND);
        }

        $searchRequest->setType($request->get('type'));
        $searchRequest->setQuery((string)$request->get('query'));
        $result = $fetcher->search($searchRequest);

        return new JsonResponse(['result' => array_map(function (EntityToHost $item) {
            return [
                'name' => $item->getEntity()->getName(),
                'id' => $item->getEntity()->getId()
            ];
        }, $result)]);
    }
}