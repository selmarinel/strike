<?php

namespace App\Service\Search\Context\Controller;

use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Search\Domain\Service\SearchParserInterface;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class ProcessCSVController extends Controller
{
    public function migrate(Request $request, SearchParserInterface $handler, HostRepository $hostRepository, KernelInterface $kernel, Filesystem $filesystem)
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            if (!$file) {
                return new JsonResponse(['status' => 'ko', 'message' => 'not find file'], JsonResponse::HTTP_NOT_FOUND);
            }
            if ($file->getClientMimeType() != 'text/csv') {
                return new JsonResponse(['status' => 'ko', 'message' => 'unsupported format'], JsonResponse::HTTP_BAD_REQUEST);
            }
            if (!file_exists($file->getRealPath())) {
                return new JsonResponse(['status' => 'ko', 'message' => 'file not in tmp'], JsonResponse::HTTP_BAD_REQUEST);
            }

            $md5 = md5_file($file->getRealPath());

            $filename = '/tmp/' . $md5 . '.csv';

            if (file_exists($filename)) {
                return new JsonResponse(['status' => 'ok', 'message' => 'File is processing'], JsonResponse::HTTP_OK);
            }

            if (file_exists($filename . '.error')) {
                unlink(file_exists($filename) . '.error');

                $filesystem->dumpFile($filename, file_get_contents($file->getRealPath()), true);

                $process = new Process(
                    'php '.$kernel->getRootDir() . '/../bin/console app:process_csv:search '.$filename
                );
                $process->setTimeout(null);
                $process->start();
                return new JsonResponse(
                    ['status' => 'ok','message' => 'There was problem with this file processing, starting new process'],
                    JsonResponse::HTTP_OK
                );
            } else {
                $filesystem->dumpFile($filename, file_get_contents($file->getRealPath()), true);

                $process = new Process(
                    'php ' . $kernel->getRootDir() . '/../bin/console app:process_csv:search ' . $filename
                );
                $process->setTimeout(null);
                $process->start();
            }



            return new JsonResponse(
                [
                    'status' => 'ok',
                    'message' => 'Process started with PID ' . $process->getPid()
                ],
                JsonResponse::HTTP_OK
            );
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['status' => 'ko', 'error' => $exception->getMessage()],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
    }

    public function delete(Request $request, SearchParserInterface $handler, HostRepository $hostRepository)
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            $fiO = new \SplFileObject($file->getPath() . '/' . $file->getFilename());
            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }
                list($query, $domain) = explode(',', $line);
                $host = $hostRepository->findOneBy(['hostname' => $domain]);
                if (!$host) {
                    continue;
                }
                $request = new RequestFetcherYouTubeVO();
                $request->setQuery($query);
                $handler->remove($request, $host);
            }
            $handler->save();
            return new JsonResponse(['status' => 'deleted'], JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'ko'], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}