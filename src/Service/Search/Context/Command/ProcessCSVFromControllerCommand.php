<?php

namespace App\Service\Search\Context\Command;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ProcessCSVFromControllerCommand extends Command
{
    /** @var HostRepository */
    private $hostRepository;

    /** @var Filesystem $filesystem */
    private $filesystem;

    /** @var int $limit */
    private $limit = 5000;
    /** @var int $count */
    private $count = 0;

    private $hostMap = [];
    /** @var EntityManagerInterface  */
    private $manager;
    /** @var SearchEntityRepository */
    private $searchEntityRepository;
    private $uniqueQueries = [];

    public function __construct(
        EntityManagerInterface $manager,
        Filesystem $filesystem
    ) {
        $this->manager = $manager;
        $this->manager->getConfiguration()->setSQLLogger(null);

        foreach ($this->manager->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                $this->manager->getEventManager()->removeEventListener($hash, $listener);
            }
        }

        $this->manager->getClassMetadata(SearchEntity::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(Host::class)->setLifecycleCallbacks([]);

        $this->hostRepository = $this->manager->getRepository(Host::class);
        $this->filesystem = $filesystem;
        $this->searchEntityRepository = $this->manager->getRepository(SearchEntity::class);
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('app:process_csv:search')
            ->addArgument('file', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();

        $filePath = $input->getArgument('file');

        $fiO = new \SplFileObject($filePath);
        while (!$fiO->eof()) {
            $line = trim($fiO->fgets());
            if (empty($line)) {
                continue;
            }

            try {
                list($query, $domain) = explode(',', $line);
            } catch (\Exception $e) {
                $output->writeln("<error>{$e->getMessage()} on line {$line}</error>");
                continue;
            }

            if (isset($this->hostMap[$domain])) {
                $host = $this->hostMap[$domain];
            } else {
                /** @var Host $host */
                $host = $this->hostRepository->findOneBy(['hostname' => $domain]);
                $this->hostMap[$domain] = $host;
            }


            if (!$host) {
                $output->writeln("<info>$domain Not found</info>");
                continue;
            }

            $output->writeln("<comment>Process <info>$query</info> On <info>$domain</info> HOST</comment>");

            try {
                $this->process($query, $host);
            } catch (\Exception $e) {
                $output->writeln($e);
                $this->manager->flush();
                $this->manager->clear(SearchEntity::class);
            }

            unset($line, $domain, $query, $host);
        }

        $this->manager->flush();

        unlink($filePath);
    }

    private function process(string $query, Host $host)
    {
        $hash = md5(SlugMakerService::makeSlug($query) . $host->getId());
        /** @var SearchEntity $searchEntity */
        $searchEntity = $this->searchEntityRepository->findOneBy([
            'hash' => $hash,
        ]);

        if ($searchEntity) {
            $this->manager->detach($searchEntity);
            unset($searchEntity, $hash);
            return;
        }

        if (!in_array($hash, $this->uniqueQueries)) {
            $this->uniqueQueries[] = $hash;

            $searchEntity = new SearchEntity();
            $searchEntity->setHost($host);
            $searchEntity->setQuery($query);
            $searchEntity->setResults([]);
            $searchEntity->setHash($hash);
            $searchEntity->setStatus(SearchEntity::UNPROCESSED);
            $this->manager->persist($searchEntity);
        }

        $this->count += 1;

        if ($this->limit === $this->count) {
            dump("FLUSHING");
            $this->count = 0;
            $this->manager->flush();
            $this->manager->clear(SearchEntity::class);
            $this->uniqueQueries = [];
            gc_collect_cycles();
        }
    }
}