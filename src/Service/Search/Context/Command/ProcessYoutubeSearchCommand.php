<?php

namespace App\Service\Search\Context\Command;

use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Search\Domain\Service\SearchParserInterface;
use App\Service\Search\Infrastructure\Logger\SearchLog;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessYoutubeSearchCommand extends Command
{
    /** @var SearchEntityRepository */
    private $repository;
    /** @var SearchParserInterface */
    private $handler;
    /** @var ContainerInterface */
    private $container;
    /** @var array */
    private $tokens;
    private $limit = 10;
    /** @var int $count */
    private $count = 0;

    public function __construct(
        SearchParserInterface $handler,
        ContainerInterface $container,
        SearchEntityRepository $repository
    )
    {
        $this->handler = $handler;
        $this->repository = $repository;
        $this->container = $container;
        $this->tokens = $this->container->getParameter('youtube_token');
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('app:process_youtube:search')
            ->setAliases(["a:p:s"])
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL, 'wip')
            ->addOption('unsearch', 'x', InputOption::VALUE_OPTIONAL, 'Process not search type')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Limit', 1000);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int)$input->getArgument('limit');

        $type = $input->getOption('type') ?: null;

        /** @var SearchEntity[] $searchEntities */
        $searchEntities = $this->getEntities(
            $limit,
            (bool)$input->getOption('unsearch'),
            $type
        );

        foreach ($searchEntities as $searchEntity) {
            $output->writeln("<comment>Process <info>[{$searchEntity->getQuery()}]</info> On " .
                "<info>[{$searchEntity->getHost()->getHostname()}]</info> HOST</comment>");

            $this->process($searchEntity->getQuery(), $searchEntity, $output);
        }

        $this->handler->save();
    }

    private function getEntities(int $limit, bool $unSearch = false, int $type = null)
    {
        $builder = $this->repository->createQueryBuilder('s')
            ->where('s.status = :status')
            ->setParameter('status', SearchEntity::UNPROCESSED);
        if ($unSearch) {
            $builder->andWhere('s.entityToHost is not null');
            if ($type) {
                $builder->join('s.entityToHost', 'eth')
                    ->join('eth.entity', 'e')
                    ->andWhere('e.entity_type = :type')
                    ->setParameter('type', $type);
            }
        }
        return $builder->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    private function process(string $query, SearchEntity $searchEntity, OutputInterface $output)
    {
        chronicle(SearchLog::create(
            SearchLog::ACTION_START,
            [
                'query' => $query,
            ],
            ''));
        try {
            $request = new RequestFetcherYouTubeVO();
            $request->setQuery($query);

            if (empty($this->tokens)) {
                $this->handler->save();
                $output->writeln("<error>Valid YOUTUBE token not found! Please check</error>");

                chronicle(SearchLog::create(
                    SearchLog::ACTION_FAIL,
                    [
                        'query' => $query
                    ],
                    'Valid YOUTUBE token not found!'
                ));
                die();
            }

            $request->setToken($this->tokens[0]);

            $this->handler->parse($request, $searchEntity);
            $this->count += 1;

            if ($this->limit === $this->count) {
                $this->count = 0;
                $this->handler->save();
            }

        } catch (ClientException $e) {
            if ($e->getCode() == 403) {
                $this->handler->save();
                unset($this->tokens[0]);
                if (!empty($this->tokens)) {
                    $this->tokens = array_values($this->tokens);
                    chronicle(SearchLog::create(
                        SearchLog::ACTION_RETRY,
                        [
                            'query' => $query
                        ],
                        'Valid YOUTUBE token not found!'
                    ));
                    $this->process($searchEntity->getQuery(), $searchEntity, $output);
                } else {
                    dd($e->getMessage());
                }
            }
        }
    }
}