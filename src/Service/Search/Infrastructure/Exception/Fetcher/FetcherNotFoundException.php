<?php
/**
 * Created by PhpStorm.
 * User: yuriiastahov
 * Date: 10.07.18
 * Time: 11:07
 */

namespace App\Service\Search\Infrastructure\Exception\Fetcher;


class FetcherNotFoundException extends FetcherException
{
    protected $code = 404;

    protected $message = 'Fetcher on Search Not found';
}