<?php

namespace App\Service\Search\Infrastructure\Exception\Fetcher;


class FetcherException extends \Exception
{
    protected $code = 400;

    protected $message = 'Fetcher on Search Exception';
}