<?php

namespace App\Service\Search\Infrastructure\EventDispatcher;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Search\Infrastructure\Service\Searcher\Driver\ElasticSearchDriver;
use Doctrine\ORM\Event\LifecycleEventArgs;

class SaveIndexer
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof EntityToHost) {
            unset($entity);
            return;
        }
        try {
            $driver = new ElasticSearchDriver();
            $params = [];
            $driver->prepareRecord($entity, $params);
            $driver->saveRecords($params);
        } catch (\Exception $exception) {
            unset($driver, $entity);
            return;
        }
    }
}