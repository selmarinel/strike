<?php

namespace App\Service\Search\Infrastructure\Service\Saver;

use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Search\SearchEntityRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Search\Domain\Service\Parser\Youtube\Fetcher\FetcherInterface;
use App\Service\Search\Domain\Service\Saver\SaverInterface;
use App\Service\Search\Infrastructure\Service\Hash\HashGenerator;
use App\Service\Search\Infrastructure\Service\VO\Youtube\ResponseYoutubeItemVO;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class Saver implements SaverInterface
{
    /** @var FetcherInterface */
    private $fetcher;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var SearchEntityRepository */
    private $searchEntityRepository;
    /** @var EntityManagerInterface */
    private $_em;

    public function __construct(
        EntitiesRepository $entitiesRepository,
        EntityToHostRepository $entityToHostRepository,
        SearchEntityRepository $searchEntityRepository,
        EntityManagerInterface $entityManager,
        FetcherInterface $fetcher)
    {
        $this->searchEntityRepository = $searchEntityRepository;
        $this->entitiesRepository = $entitiesRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->_em = $entityManager;
        $this->fetcher = $fetcher;

        $this->_em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * @return Entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getEntitySearchEntity(): Entity
    {
        $searchEntity = $this->entitiesRepository->findOneBy([
            'entity_type' => Type::SEARCH_TYPE
        ]);
        if (!$searchEntity) {
            $searchEntity = new Entity();
            $searchEntity->setSourceId('');
            $searchEntity->setEntityType(Type::SEARCH_TYPE);
            $searchEntity->setName('');
            $searchEntity->setScore(0);
            $this->_em->persist($searchEntity);
            $this->_em->flush();
        }
        return $searchEntity;
    }

    /**
     * @param Entity $entity
     * @param Host $host
     * @param string $slug
     * @return EntityToHost
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getEntityToHost(Entity $entity, Host $host, string $slug)
    {
        $entityToHost = $this->entityToHostRepository->findOneBy([
            'entity_slug' => $slug,
            'host' => $host,
            'entity' => $entity
        ]);

        if (!$entityToHost) {
            $entityToHost = new EntityToHost();
            $entityToHost->setHost($host);
            $entityToHost->setEntity($entity);
            $entityToHost->setEntitySlug($slug);
            $this->_em->persist($entityToHost);
            $this->_em->flush();
        }

        return $entityToHost;
    }

    /**
     * @param SearchEntity $searchEntity
     * @param EntityToHost $entityToHost
     * @param ResponseYoutubeItemVO[] $results
     * @return SearchEntity
     */
    public function appendSearchResults(SearchEntity $searchEntity, EntityToHost $entityToHost, array $results)
    {
        $searchEntity->setEntityToHost($entityToHost);
        $searchEntity->setResults($results);
        $searchEntity->setStatus(SearchEntity::PROCESSED);
        $this->_em->persist($searchEntity);
        return $searchEntity;
    }

    public function createSearchEntity(Host $host, string $query)
    {
        $searchEntity = $this->searchEntityRepository->findOneBy([
            'host' => $host,
            'query' => $query
        ]);

        if (!$searchEntity) {
            $searchEntity = new SearchEntity();
            $searchEntity->setHost($host);
            $searchEntity->setQuery($query);
            $searchEntity->setResults([]);
            $searchEntity->setStatus(SearchEntity::UNPROCESSED);
            $searchEntity->setHash(HashGenerator::generate($searchEntity));
            $this->_em->persist($searchEntity);
        }

        return $searchEntity;
    }

    /**
     * @param Host $host
     * @param string $query
     * @return bool
     */
    public function remove(Host $host, string $query): bool
    {
        /** @var SearchEntity $searchEntity */
        $searchEntity = $this->searchEntityRepository->createQueryBuilder('se')
            ->join('se.entityToHost', 'eh')
            ->where('eh.host = :host')
            ->setParameter('host', $host)
            ->andWhere('se.query = :query')
            ->setParameter('query', $query)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$searchEntity) {
            return false;
        }
        $this->_em->remove($searchEntity->getEntityToHost());
        return true;
    }

    public function commit()
    {
        $this->_em->flush();
    }
}