<?php

namespace App\Service\Search\Infrastructure\Service\VO\Youtube;


class RequestFetcherYouTubeVO
{
    /** @var string */
    private $part = 'snippet';
    /** @var int */
    private $maxCount = 12;
    /** @var string */
    private $token = '';
    /** @var string */
    private $type = 'video';
    /** @var string */
    private $query = '';

    /**
     * @return string
     */
    public function getURLToFetch(): string
    {
        return "https://www.googleapis.com/youtube/v3/search?" .
            "type={$this->getType()}&" .
            "part={$this->getPart()}&" .
            "q={$this->getQuery()}&" .
            "key={$this->getToken()}&" .
            "maxResults={$this->getMaxCount()}&" .
            "videoEmbeddable=true&" .
            "videoSyndicated=true&" .
            "order=relevance";
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return str_replace('"', '', $this->query);
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query): void
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getPart(): string
    {
        return $this->part;
    }

    /**
     * @param string $part
     */
    public function setPart(string $part): void
    {
        $this->part = $part;
    }

    /**
     * @return int
     */
    public function getMaxCount(): int
    {
        return $this->maxCount;
    }

    /**
     * @param int $maxCount
     */
    public function setMaxCount(int $maxCount): void
    {
        $this->maxCount = $maxCount;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}