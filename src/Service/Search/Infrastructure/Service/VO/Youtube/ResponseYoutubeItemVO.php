<?php

namespace App\Service\Search\Infrastructure\Service\VO\Youtube;


class ResponseYoutubeItemVO
{
    /** @var string */
    private $videoId = '';
    /** @var string */
    private $title = '';
    /** @var string */
    private $description = '';
    /** @var string */
    private $src = '';

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getVideoId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'src' => $this->getSrc()
        ];
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId(string $videoId): void
    {
        $this->videoId = $videoId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * @param array|string $thumbnails
     */
    public function setSrc($thumbnails): void
    {
        if (is_array($thumbnails)) {
            $this->src = isset($thumbnails['high']['url']) ?
                $thumbnails['high']['url'] :
                $thumbnails['default']['url'];
        } elseif (is_string($thumbnails)) {
            $this->src = $thumbnails;
        }
    }

}