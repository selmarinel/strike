<?php

namespace App\Service\Search\Infrastructure\Service\Searcher\VO;


use App\Service\Processor\Domain\VO\Request\GetRequestInterface;
use App\Service\Search\Domain\Service\Searcher\VO\GetSearchRequestInterface;
use App\Service\Search\Domain\Service\Searcher\VO\SetSearchRequestInterface;

class SearchRequest implements GetSearchRequestInterface, SetSearchRequestInterface
{
    /** @var GetRequestInterface */
    private $request;

    public function __construct(GetRequestInterface $request)
    {
        $this->request = $request;
    }

    /** @var string */
    private $query = '';
    /** @var int|null */
    private $hostId = 0;
    /** @var int|null */
    private $type = null;

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query): void
    {
        $this->query = $query;
    }

    /**
     * @return int|null
     */
    public function getHostId(): ?int
    {
        return $this->hostId;
    }

    /**
     * @param int|null $hostId
     */
    public function setHostId(?int $hostId): void
    {
        $this->hostId = $hostId;
    }

    /**
     * @return null|int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param null|int $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->request->getHostName();
    }

    /**
     * @return string
     */
    public function getRequestUri(): string
    {
        return $this->request->getRequestUri();
    }

    public function getScheme(): string
    {
        return $this->request->getScheme();
    }

    public function getSlug(): string
    {
        return $this->request->getSlug();
    }

    public function getGeo(): string
    {
        return $this->request->getGeo();
    }

    public function getIsBot(): bool
    {
        return $this->request->getIsBot();
    }
}