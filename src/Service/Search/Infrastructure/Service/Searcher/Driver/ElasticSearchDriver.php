<?php

namespace App\Service\Search\Infrastructure\Service\Searcher\Driver;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Search\Domain\Service\Searcher\Driver\SearchDriverInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchDriver implements SearchDriverInterface
{
    /** @var false|string */
    private $host;
    /** @var false|string */
    private $index;
    /** @var false|string */
    private $type;
    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->host = getenv('ELASTIC_SEARCH_URL');
        $this->index = getenv('ELASTIC_INDEX');
        $this->type = getenv('ELASTIC_TYPE');
    }

    public function getClient()
    {
        if (!$this->client) {
            $this->connect();
        }
        return $this->client;
    }

    public function connect()
    {
        $this->client = ClientBuilder::create()->setHosts([$this->host])->build();
        return $this;
    }

    public function prepareRecord(EntityToHost $entityToHost, array &$params)
    {
        $params['body'][] = [
            'index' => [
                '_index' => $this->index,
                '_type' => $this->type
            ]
        ];
        $params['body'][] = [
            'id' => $entityToHost->getId(),
            'name' => $entityToHost->getEntity()->getName(),
            'host_id' => $entityToHost->getHost()->getId(),
            'type' => $entityToHost->getEntity()->getEntityType()
        ];
    }

    public function updateRecord(string $id, array $data): self
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id,
            'body' => [
                'doc' => $data
            ]
        ];
        $this->getClient()->update($params);
        return $this;
    }

    public function deleteRecord(string $id): self
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id,
        ];
        $this->getClient()->delete($params);
        return $this;
    }

    public function saveRecords(array $params): self
    {
        $this->getClient()->bulk($params);
        return $this;
    }

    public function search(array $query)
    {
        $params = [
            'index' => 'app',
            'type' => 'entity',
            'body' => [
                'query' => $query
            ]
        ];
        return $this->getClient()->search($params);
    }

    public function searchByName(string $name, int $hostId)
    {
        $query = ['bool' => [
            'must' => [
                ['match' => ['name' => $name]],
            ],
            'filter' => ['term' => ['host_id' => $hostId]]
        ]];
        return $this->search($query);
    }
}