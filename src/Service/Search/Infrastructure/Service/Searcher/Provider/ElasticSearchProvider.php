<?php

namespace App\Service\Search\Infrastructure\Service\Searcher\Provider;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Search\Domain\Service\Searcher\Provider\ProviderInterface;
use App\Service\Search\Domain\Service\Searcher\VO\GetSearchRequestInterface;
use App\Service\Search\Infrastructure\Service\Searcher\Driver\ElasticSearchDriver;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Elasticsearch\Common\Exceptions\NoNodesAvailableException;

class ElasticSearchProvider implements ProviderInterface
{
    /**
     * @var EntityToHostRepository
     */
    private $entityToHostRepository;

    public function __construct(EntityToHostRepository $entityToHostRepository)
    {
        $this->entityToHostRepository = $entityToHostRepository;
    }

    /**
     * @param GetSearchRequestInterface $request
     * @param GetHostInterface $host
     * @return EntityToHost[]
     */
    public function search(GetSearchRequestInterface $request, GetHostInterface $host): array
    {
        $hostId = $host->getHostId();
        if ($host->getAbuseLevel() <= 1) {
            $hostId = $host->getParentId();
        }
        $query = ['bool' => [
            'must' => [
                ['match' => ['name' => $request->getQuery()]],
            ],
            'filter' => ['term' => ['host_id' => $hostId]]
        ]];

        if ($request->getType()) {
            $query['bool']['must'][] = ['match' => ['type' => $request->getType()]];
        }
        return $this->getEntitiesToHost($query);
    }

    public function searchByName(string $name, GetHostInterface $host): array
    {
        $hostId = $host->getHostId();
        if ($host->getAbuseLevel() <= 1) {
            $hostId = $host->getParentId();
        }
        $query = ['bool' => [
            'must' => [
                ['match' => ['name' => $name]],
            ],
            'filter' => ['term' => ['host_id' => $hostId]]
        ]];
        return $this->getEntitiesToHost($query);

    }

    private function getEntitiesToHost(array $query)
    {
        $client = new ElasticSearchDriver();
        try {
            $results = $client->connect()->search($query);
            $hits = $results['hits']['hits'];
            $ids = [];
            foreach ($hits as $hit) {
                $ids[] = $hit['_source']['id'];
            }
            return $this->entityToHostRepository->createQueryBuilder('eh')
                ->select('eh, e')
                ->join('eh.entity', 'e')
                ->where('eh.id in (:ids)')->setParameter('ids', $ids)
                ->getQuery()->getResult();
        } catch (NoNodesAvailableException $exception) {
            return [];
        } catch (Missing404Exception $exception){
            return [];
        }
    }
}