<?php

namespace App\Service\Search\Infrastructure\Service\Searcher;

use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Search\Domain\Service\Searcher\SearchInterface;
use App\Service\Search\Domain\Service\Searcher\Provider\ProviderInterface;
use App\Service\Search\Domain\Service\Searcher\VO\GetSearchRequestInterface;
use App\Service\Search\Domain\Service\Searcher\VO\SetSearchRequestInterface;

class Search implements SearchInterface
{
    /** @var UrlParserServiceInterface */
    private $parser;
    /** @var ProviderInterface */
    private $provider;

    public function __construct(
        ProviderInterface $provider,
        UrlParserServiceInterface $urlParserService
    )
    {
        $this->provider = $provider;
        $this->parser = $urlParserService;
    }

    /**
     * @param GetSearchRequestInterface|SetSearchRequestInterface $request
     * @return \App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost[]|array
     * @internal param string $query
     */
    public function search(GetSearchRequestInterface $request): array
    {
        $host = $this->parser->processRequest($request);
        $request->setHostId($host->getHostId());
        return $this->provider->search($request, $host);
    }
}