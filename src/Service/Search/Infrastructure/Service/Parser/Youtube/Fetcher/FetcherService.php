<?php

namespace App\Service\Search\Infrastructure\Service\Parser\Youtube\Fetcher;


use App\Service\Search\Domain\Service\Parser\Youtube\Fetcher\FetcherInterface;
use App\Service\Search\Infrastructure\Exception\Fetcher\FetcherNotFoundException;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;
use App\Service\Search\Infrastructure\Service\VO\Youtube\ResponseYoutubeItemVO;
use GuzzleHttp\ClientInterface;

class FetcherService implements FetcherInterface
{
    /** @var ClientInterface */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestFetcherYouTubeVO $requestFetcherYouTubeVO
     * @return ResponseYoutubeItemVO[]
     * @throws FetcherNotFoundException
     */
    public function fetchData(RequestFetcherYouTubeVO $requestFetcherYouTubeVO): array
    {
        $response = $this->client->request('GET', $requestFetcherYouTubeVO->getURLToFetch());
        $response = json_decode($response->getBody()->getContents(), true);
        if (!isset($response['items'])) {
            throw new FetcherNotFoundException;
        }
        $result = [];
        foreach ($response['items'] as $item) {
            if (!isset($item['id'])) {
                continue;
            }
            if (!isset($item['id']['videoId'])) {
                continue;
            }
            if (!$item['id']['videoId']) {
                continue;
            }

            $itemVO = new ResponseYoutubeItemVO();
            $itemVO->setTitle($item['snippet']['title']);
            $itemVO->setVideoId($item['id']['videoId']);
            $itemVO->setSrc($item['snippet']['thumbnails']);
            $itemVO->setDescription($item['snippet']['description']);
            $result[] = $itemVO;
            unset($itemVO);
        }
        return $result;
    }
}