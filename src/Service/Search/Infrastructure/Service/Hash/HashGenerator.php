<?php

namespace App\Service\Search\Infrastructure\Service\Hash;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;

class HashGenerator
{
    public static function generate(SearchEntity $entity)
    {
        $hostId = 0;
        if ($entity->getHost()) {
            $hostId = $entity->getHost()->getId();
        }
        $entityHostId = 0;
        if ($entity->getEntityToHost()) {
            $entityHostId = $entity->getEntityToHost()->getId();
        }
        return self::simpleGenerate($entity->getQuery(), $hostId, $entityHostId);
    }

    public static function simpleGenerate(string $text, int $hostId = null, int $entityHostId = null)
    {
        return md5(SlugMakerService::makeSlug($text) . $hostId . $entityHostId);
    }
}