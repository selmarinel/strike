<?php

namespace App\Service\Search\Infrastructure\Service;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Search\Domain\Service\SearchParserInterface;
use App\Service\Search\Domain\Service\Parser\Youtube\Fetcher\FetcherInterface;
use App\Service\Search\Domain\Service\Saver\SaverInterface;
use App\Service\Search\Infrastructure\Logger\SearchLog;
use App\Service\Search\Infrastructure\Service\VO\Youtube\RequestFetcherYouTubeVO;

class Handler implements SearchParserInterface
{
    /** @var FetcherInterface */
    private $fetcher;
    /** @var SaverInterface */
    private $service;

    public function __construct(FetcherInterface $fetcher, SaverInterface $searchService)
    {
        $this->fetcher = $fetcher;
        $this->service = $searchService;
    }

    /**
     * @param RequestFetcherYouTubeVO $requestFetcherYouTubeVO
     * @param SearchEntity $searchEntity
     * @param bool $save
     */
    public function parse(
        RequestFetcherYouTubeVO $requestFetcherYouTubeVO,
        SearchEntity $searchEntity,
        bool $save = false
    )
    {
        // #offtopic
        // If search entity has entityToHost - it's mean that search entity Has
        // related entity such as search or another
        // it's mean that, code doesn't need to create new entity and overwrite it
        $entityToHost = $searchEntity->getEntityToHost();

        if (!$entityToHost) {
            $entity = $this->service->getEntitySearchEntity();
            $slug = SlugMakerService::makeSlug($requestFetcherYouTubeVO->getQuery());
            $entityToHost = $this->service->getEntityToHost($entity, $searchEntity->getHost(), $slug);
        }

        $data = $this->fetcher->fetchData($requestFetcherYouTubeVO);
        $this->service->appendSearchResults($searchEntity, $entityToHost, $data);

        if (empty($data)) {
            chronicle(SearchLog::create(
                SearchLog::ACTION_EMPTY_RESULT,
                [
                    'query' => $searchEntity->getQuery(),
                    'entity_to_host' => [
                        'id' => $entityToHost->getId(),
                        'slug' => $entityToHost->getEntitySlug()
                    ]
                ], ''
            ));
        } else {
            chronicle(SearchLog::create(
                SearchLog::ACTION_SUCCESS,
                [
                    'query' => $searchEntity->getQuery(),
                    'entity_to_host' => [
                        'id' => $entityToHost->getId(),
                        'slug' => $entityToHost->getEntitySlug()
                    ]
                ], ''
            ));
        }

        if ($save) {
            $this->service->commit();
        }
    }

    public function remove(RequestFetcherYouTubeVO $requestFetcherYouTubeVO, Host $host, bool $save = false)
    {
        $this->service->remove($host, $requestFetcherYouTubeVO->getQuery());
        if ($save) {
            $this->service->commit();
        }
    }

    public function addWithoutYoutubeSearch(
        RequestFetcherYouTubeVO $requestFetcherYouTubeVO,
        Host $host,
        bool $save = false
    )
    {
        $this->service->createSearchEntity($host, $requestFetcherYouTubeVO->getQuery());
        if ($save) {
            $this->service->commit();
        }
    }

    public function save()
    {
        $this->service->commit();
    }
}