<?php

namespace App\Service\Search\Infrastructure\Logger;


use Logger\Message\StrikeLogMessage;

class SearchLog extends StrikeLogMessage
{
    protected $service = 'search';

    const ACTION_START = 'TRY_TO_SEARCH';
    const ACTION_SUCCESS = 'SUCCESS';
    const ACTION_FAIL = 'FAIL';
    const ACTION_RETRY = 'RETRY';
    const ACTION_EMPTY_RESULT = 'EMPTY';

    public static function create(string $action, $data, $message = '')
    {
        $log = new self();
        $log->setType($action);
        $log->setData(self::processData($data));
        $log->setMessage($message);
        return $log;
    }

    public static function processData($data): string
    {
        if ($data instanceof \Exception) {
            $data = [
                'message' => $data->getMessage(),
                'code' => $data->getCode(),
                'class' => get_class($data),
            ];
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        } elseif (is_string($data)) {
            return $data;
        } elseif (is_array($data)) {
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        }
        return 'Unsupported Data type';
    }
}