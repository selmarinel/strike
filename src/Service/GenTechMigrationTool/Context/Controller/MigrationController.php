<?php

namespace App\Service\GenTechMigrationTool\Context\Controller;


use App\Service\Administrate\Domain\Controller\AdministrateControllerInterface;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\ArtistMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\MovieMigrationService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\TrackMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\VideoMigrateService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MigrationController extends Controller implements AdministrateControllerInterface
{
    /**
     * @param Request $request
     * @param MigrateVOInterface $migrateVO
     * @return JsonResponse
     */
    public function migrate(
        Request $request,
        MigrateVOInterface $migrateVO
    ) {
        $request = json_decode($request->getContent(), true);

        $migrateVO->setHostId($request['host_id']);
        $migrateVO->setTitle($request['title']);

        if (!isset($request['entity_type_id'])) {
            return new JsonResponse([], JsonResponse::HTTP_BAD_REQUEST);
        }

        switch ($request['entity_type_id']) {
            case Type::AUDIO_ARTIST:
                $service = $this->container->get(ArtistMigrateService::class);
                break;
            case Type::AUDIO_TRACK:
                $service = $this->container->get(TrackMigrateService::class);
                break;
            case Type::VIDEO_SERIES:
                $service = $this->container->get(VideoMigrateService::class);
                break;
            case Type::MOVIE_FILM:
                $service = $this->container->get(MovieMigrationService::class);
                break;
            default:
                return new JsonResponse(['unsuported type'], 400);
        }

        $service->migrateContent($migrateVO);
        return new JsonResponse([], 200);
    }
}