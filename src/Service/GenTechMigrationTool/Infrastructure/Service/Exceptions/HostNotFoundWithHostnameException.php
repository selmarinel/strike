<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions;


class HostNotFoundWithHostnameException extends IncorrectDataInServiceException
{

}