<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions;


use Symfony\Component\HttpFoundation\Response;

class IncorrectDataInServiceException extends \Exception
{
    protected $code = Response::HTTP_BAD_REQUEST;
}