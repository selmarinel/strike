<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Migration;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\EntityProcessor\Infrastructure\Service\Pigeon\Exception\PigeonException;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrateInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use Doctrine\ORM\ORMException;

/**
 * Class ArtistMigrateTrashService
 * @package App\Service\GenTechMigrationTool
 */
class TrackMigrateService implements MigrateInterface, MigrationInterface
{

    /** @var MigrateConstructorInterface */
    private $constructor;
    /** @var TransmitterInterface */
    private $transmitter;

    public function __construct(TransmitterInterface $transmitter, MigrateConstructorInterface $constructor)
    {
        $this->transmitter = $transmitter;
        $this->constructor = $constructor;
    }

    /**
     * @param TransmitterParametersVOInterface $transmitterParametersVO
     * @param MigrateVOInterface $migrateVO
     * @param string $receiveUrl
     * @throws \Exception
     */
    public function migrate(
        TransmitterParametersVOInterface $transmitterParametersVO,
        MigrateVOInterface $migrateVO,
        string $receiveUrl
    ) {
        $this->migrateContent($migrateVO);
    }

    /**
     * @param MigrateVOInterface $migrateVO
     * @return null|void
     * @throws PigeonException
     * @throws \Exception
     */
    public function migrateContent(MigrateVOInterface $migrateVO)
    {
        $this->constructor->host($migrateVO->getHostId());
        $track = $this->transmitter->fetch(Type::AUDIO_TRACK, $migrateVO);

        if (!$track['artist_id']) {
            throw new PigeonException;
        }
        $artist = $this->transmitter->fetchById(Type::AUDIO_ARTIST, $track['artist_id']);
        if (!isset($artist)) {
            return null;
        }
        $artistMigrateService = new ArtistMigrateService($this->transmitter, $this->constructor);
        $migrateVO->setId($track['artist_id']);
        $artistMigrateService->migrateContent($migrateVO);
    }

}