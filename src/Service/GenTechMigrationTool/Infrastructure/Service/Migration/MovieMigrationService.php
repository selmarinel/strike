<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Migration;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrateType\MovieMigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\FatalException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\IncorrectDataInServiceException;
use App\Service\GenTechMigrationTool\Infrastructure\Traits\MigrateConsoleTrait;

/**
 * Class MovieMigrateService
 * @package App\Service\GenTechMigrationTool\Infrastructure\Service\Migration
 */
class MovieMigrationService implements MovieMigrationInterface, MigrationInterface, NeedToTimeLogMigrateInterface
{
    use MigrateConsoleTrait;

    /** @var TransmitterInterface */
    private $transmitter;
    /** @var MigrateConstructorInterface */
    private $constructor;

    public function __construct(
        TransmitterInterface $transmitter,
        MigrateConstructorInterface $constructor
    )
    {
        $this->transmitter = $transmitter;
        $this->constructor = $constructor;
    }

    /**
     * @param TransmitterParametersVOInterface $transmitterParametersVO
     * @param MigrateVOInterface $migrateVO
     * @param string $receiveUrl
     * @throws \Exception
     *
     * @deprecated
     */
    public function migrate(
        TransmitterParametersVOInterface $transmitterParametersVO,
        MigrateVOInterface $migrateVO,
        string $receiveUrl
    )
    {
        $this->migrateContent($migrateVO);
    }

    /**
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     */
    public function migrateContent(MigrateVOInterface $migrateVO)
    {
        $this->constructor->host($migrateVO->getHostId());
        try {
            if ($migrateVO->getId()) {
                $film = $this->transmitter->fetchById(Type::MOVIE_FILM, $migrateVO->getId());
            } else {
                $film = $this->transmitter->fetch(Type::MOVIE_FILM, $migrateVO);
            }
            $this->notice("Movie title [{$film['title_ru']}]");
            $entity = $this->constructor->saveEntity(
                $film['video_id'],
                Type::MOVIE_FILM,
                $film['title_ru'],
                round($film['rating_imdb'] * 100));
            $this->constructor->bindTagsToEntity($entity);
//            $this->constructor->saveToTree($entity);
            $this->constructor->manualSaveToTree($entity, null, null, 0, 1, 2);
        } catch (\Exception $exception) {
            throw new FatalException($exception->getMessage());
        }
        if (!$film['title_ru']) {
            throw new IncorrectDataInServiceException('invalid film name');
        }
        try {
            $entityToHost = $this->constructor->saveEntityToHost($entity, SlugMakerService::makeSlug($film['title_ru']));
            $this->constructor->saveToList($entityToHost, Type::COLLECTION_MOVIE_FILMS);
            $this->constructor->confirmMigration();
        } catch (\Exception $exception){
            throw new FatalException($exception->getMessage());
        }
    }
}