<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Migration;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrateInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Traits\MigrateConsoleTrait;
use Doctrine\ORM\ORMException;

/**
 * Class ArtistMigrateTrashService
 * @package App\Service\GenTechMigrationTool
 */
class ArtistMigrateService implements MigrationInterface, MigrateInterface, NeedToTimeLogMigrateInterface
{
    use MigrateConsoleTrait;

    /** @var TransmitterInterface */
    private $transmitter;
    /** @var MigrateConstructorInterface */
    private $constructor;

    public function __construct(TransmitterInterface $transmitter, MigrateConstructorInterface $constructor)
    {
        $this->transmitter = $transmitter;
        $this->constructor = $constructor;
    }

    /**
     * @param TransmitterParametersVOInterface $transmitterParametersVO
     * @param string $receiveUrl
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     *
     * @deprecated
     */
    public function migrate(
        TransmitterParametersVOInterface $transmitterParametersVO,
        MigrateVOInterface $migrateVO,
        string $receiveUrl
    ) {
        $this->migrateContent($migrateVO);
    }


    /**
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     */
    public function migrateContent(MigrateVOInterface $migrateVO)
    {
        $tree = [];
        $this->constructor->host($migrateVO->getHostId());

        if ($migrateVO->getId()) {
            $artist = $this->transmitter->fetchById(Type::AUDIO_ARTIST, $migrateVO->getId());
        } else {
            $artist = $this->transmitter->fetch(Type::AUDIO_ARTIST, $migrateVO);
        }
        if (!$artist['name']) {
            throw new \Exception('invalid artist name');
        }
        $this->notice("Artist name [{$artist['name']}]");

        $tree['artist'] = [
            'level' => 0,
            'lft' => 1,
            'rft' => 2
        ];
        //get albums
        if (!$albums = $this->transmitter->fetchSubItems(
            Type::AUDIO_ARTIST,
            $artist['id'],
            Type::AUDIO_ALBUM
        )) {
            throw new \Exception('Empty albums. skipping');
        }
        $this->notice("Artist [{$artist['name']}] has <info>[" . count($albums) . "]</info> albums");
        //albums run
        foreach ($albums as $album) {
            $tracks = $this->transmitter->fetchSubItems(
                Type::AUDIO_ALBUM,
                $album['id'],
                Type::AUDIO_TRACK
            );
            if (!$tracks) {
                continue;
            }
            $this->notice("Album [{$album['name']}]has <info>[" . count($tracks) . "]</info> tracks");
            $artist['albums'][$album['id']] = $album;
            $tree['album'][$album['id']] = [
                'level' => 1,
                'lft' => $tree['artist']['rft'],
                'rft' => $tree['artist']['rft'] + 1
            ];

            $tree['artist']['rft'] = $tree['artist']['rft'] + 2;
            foreach ($tracks as $track) {
                $tree['track'][$track['id']] = [
                    'level' => 2,
                    'lft' => $tree['album'][$album['id']]['rft'],
                    'rft' => $tree['album'][$album['id']]['rft'] + 1,
                ];

                $rft = $tree['album'][$album['id']]['rft'];

                $tree['album'][$album['id']]['rft'] = $tree['album'][$album['id']]['rft'] + 2;

                foreach ($tree['album'] as $albumId => $albumPosition) {
                    if ($albumPosition['lft'] > $rft) {
                        $tree['album'][$albumId]['lft'] = $tree['album'][$albumId]['lft'] + 2;
                        $tree['album'][$albumId]['rft'] = $tree['album'][$albumId]['rft'] + 2;
                    }
                }
                $tree['artist']['rft'] = $tree['artist']['rft'] + 2;
                $artist['albums'][$album['id']]['tracks'][$track['id']] = $track;
            }
        }
        if (empty($artist['albums'])) {
            throw new \Exception('All albums are empty');
        }
        $this->saveToDB($artist, $tree);
    }

    private function saveToDB(array $artist, array $tree)
    {
        $entityArtist = $this->constructor->saveEntity(
            (int)$artist['id'],
            Type::AUDIO_ARTIST,
            $artist['name'],
            (int)$artist['total_listenings']
        );
        $this->constructor->bindTagsToEntity($entityArtist);
//        $artistTree = $this->constructor->saveToTree($entityArtist);
        $artistTree = $this->constructor->manualSaveToTree(
            $entityArtist,
            null,
            null,
            $tree['artist']['level'],
            $tree['artist']['lft'],
            $tree['artist']['rft']
        );
        $entityToHostArtist = $this->constructor->saveEntityToHost(
            $entityArtist,
            SlugMakerService::makeSlug($artist['name']));
        $this->constructor->saveToList($entityToHostArtist, Type::COLLECTION_AUDIO_ARTISTS);
        foreach ($artist['albums'] as $album) {
            $entityAlbum = $this->constructor->saveEntity(
                (int)$album['id'],
                Type::AUDIO_ALBUM,
                $album['name']
            );
//            $albumTree = $this->constructor->saveToTree($entityAlbum, $artistTree, $artistTree);
            $albumTree = $this->constructor->manualSaveToTree(
                $entityAlbum,
                $artistTree,
                $artistTree,
                $tree['album'][$album['id']]['level'],
                $tree['album'][$album['id']]['lft'],
                $tree['album'][$album['id']]['rft']
            );

            $this->constructor->saveEntityToHost($entityAlbum, SlugMakerService::makeSlug($album['name']));
            foreach ($album['tracks'] as $track) {
                $entityTrack = $this->constructor->saveEntity(
                    (int)$track['id'],
                    Type::AUDIO_TRACK,
                    $track['title'],
                    $track['total_listening']
                );
                $this->constructor->setTrackLyric(
                    $entityTrack,
                    $this->transmitter->fetchById(
                        Type::AUDIO_LYRIC,
                        $entityTrack->getSourceId()
                    ));
//                $this->constructor->saveToTree($entityTrack, $artistTree, $albumTree);
                $this->constructor->manualSaveToTree(
                    $entityTrack,
                    $artistTree,
                    $albumTree,
                    $tree['track'][$track['id']]['level'],
                    $tree['track'][$track['id']]['lft'],
                    $tree['track'][$track['id']]['rft']
                );
                $entityToHostTrack = $this->constructor->saveEntityToHost(
                    $entityTrack,
                    SlugMakerService::makeSlug($track['title'])
                );
                $this->constructor->saveToList($entityToHostTrack, Type::COLLECTION_AUDIO_TRACKS);
            }
        }
        $this->constructor->confirmMigration();
    }
}