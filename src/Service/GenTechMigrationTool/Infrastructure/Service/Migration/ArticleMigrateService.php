<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Migration;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Traits\MigrateConsoleTrait;

class ArticleMigrateService implements MigrationInterface, NeedToTimeLogMigrateInterface
{
    use MigrateConsoleTrait;

    /** @var TransmitterInterface */
    private $transmitter;
    /** @var MigrateConstructorInterface */
    private $constructor;

    public function __construct(TransmitterInterface $transmitter, MigrateConstructorInterface $constructor)
    {
        $this->transmitter = $transmitter;
        $this->constructor = $constructor;
    }

    /**
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     */
    public function migrateContent(MigrateVOInterface $migrateVO)
    {
        $this->constructor->host($migrateVO->getHostId());
        $article = $this->transmitter->fetchById(Type::NEWS_ARTICLE, $migrateVO->getId());

        $this->notice("Article [{$article['title']}]");
        $this->saveToDB($article);
    }

    private function saveToDB(array $article)
    {
        $entityArticle = $this->constructor->saveEntity(
            (int)$article['id'],
            Type::NEWS_ARTICLE,
            $article['title'],
            $this->getScore($article)
        );
        $entityArticleToHost = $this->constructor->saveEntityToHost(
            $entityArticle, $article['slug']
        );

        foreach ($article['categories'] as $category) {
            $entityCategory = $this->constructor->saveEntity(
                (int)$category['id'],
                Type::COLLECTION_NEWS_CATEGORY,
                $category['name'],
                0
            );
            $entityCategoryToHost = $this->constructor->saveEntityToHost(
                $entityCategory,
                SlugMakerService::makeSlug($category['name']
                )
            );
            $this->constructor->joinEntityHosts($entityArticleToHost, $entityCategoryToHost, Type::RELATION_TYPE_LIST);
        }

        foreach ($article['relations'] as $relation) {
            $entityRelation = $this->constructor->saveEntity(
                (int)$relation['id'],
                Type::NEWS_ARTICLE,
                $relation['title'],
                $this->getScore($relation)
            );
            $entityRelationToHost = $this->constructor->saveEntityToHost(
                $entityRelation, $relation['slug']
            );

            $this->constructor->joinEntityHosts($entityRelationToHost, $entityArticleToHost, Type::RELATION_TYPE_RELATE);
        }
        $this->constructor->confirmMigration();
    }

    private function getScore(array $article): int
    {
        $date = new \DateTime($article['created']);
        return $date->getTimestamp();
    }
}