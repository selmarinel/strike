<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Migration;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;

use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;

use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrateInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Traits\MigrateConsoleTrait;

class VideoMigrateService implements MigrationInterface, MigrateInterface, NeedToTimeLogMigrateInterface
{
    use MigrateConsoleTrait;

    /** @var MigrateConstructorInterface */
    private $constructor;
    /** @var TransmitterInterface */
    private $transmitter;

    public function __construct(TransmitterInterface $transmitter, MigrateConstructorInterface $constructor)
    {
        $this->transmitter = $transmitter;
        $this->constructor = $constructor;
    }

    /**
     * @param TransmitterParametersVOInterface $transmitterParametersVO
     * @param string $receiveUrl
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     *
     * @deprecated
     */
    public function migrate(
        TransmitterParametersVOInterface $transmitterParametersVO,
        MigrateVOInterface $migrateVO,
        string $receiveUrl
    ) {
        $this->migrateContent($migrateVO);
    }

    /**
     * @param MigrateVOInterface $migrateVO
     * @throws \Exception
     */
    public function migrateContent(MigrateVOInterface $migrateVO)
    {
        $this->constructor->host($migrateVO->getHostId());
        $series = $this->transmitter->fetch(Type::VIDEO_SERIES, $migrateVO);
        $this->notice("Series title [{$series['title_ru']}]");

        $tree['series'] = [
            'level' => 0,
            'lft' => 1,
            'rft' => 2
        ];

        if (!isset($series['seasons']) || empty($series['seasons'])) {
            throw new \Exception('Series has not seasons');
        }

        $this->notice("Series [{$series['title_ru']}] has <info>[" .
            count($series['seasons']) . "]</info> seasons");

        foreach ($series['seasons'] as $number => $season) {
            if (!isset($season['episodes']) || empty($season['episodes'])) {
                continue;
            }
            $this->notice("Episode [{$number}] has <info>[" .
                count($season['episodes']) . "]</info> episodes");

            $tree['season'][$number] = [
                'level' => 1,
                'lft' => $tree['series']['rft'],
                'rft' => $tree['series']['rft'] + 1
            ];
            $tree['series']['rft'] = $tree['series']['rft'] + 2;

            foreach ($season['episodes'] as $episode) {

                $tree['episode'][$episode['video_id']] = [
                    'level' => 2,
                    'lft' => $tree['season'][$number]['rft'],
                    'rft' => $tree['season'][$number]['rft'] + 1,
                ];
                $rft = $tree['season'][$number]['rft'];
                $tree['season'][$number]['rft'] = $tree['season'][$number]['rft'] + 2;

                foreach ($tree['season'] as $seasonId => $seasonPosition) {
                    if ($seasonPosition['lft'] > $rft) {
                        $tree['season'][$seasonId]['lft'] = $tree['season'][$seasonId]['lft'] + 2;
                        $tree['season'][$seasonId]['rft'] = $tree['season'][$seasonId]['rft'] + 2;
                    }
                }
                $tree['series']['rft'] = $tree['series']['rft'] + 2;
            }

        }
        $this->saveToDB($series, $tree);
    }

    private function saveToDB(array $series, array $tree)
    {
        $entitySeries = $this->constructor->saveEntity(
            (int)$series['id'],
            Type::VIDEO_SERIES,
            $series['title_ru']
        );
        $seriesTree = $this->constructor->manualSaveToTree(
            $entitySeries,
            null,
            null,
            $tree['series']['level'],
            $tree['series']['lft'],
            $tree['series']['rft']
        );
        $this->constructor->bindTagsToEntity($entitySeries);
        $entityToHostSeries = $this->constructor->saveEntityToHost(
            $entitySeries,
            SlugMakerService::makeSlug($series['title_ru'])
        );
        $this->constructor->saveToList($entityToHostSeries, Type::COLLECTION_VIDEO_SERIES);

        foreach ($series['seasons'] as $number => $season) {
            if (!isset($season['episodes']) || empty($season['episodes'])) {
                continue;
            }

            $entitySeason = $this->constructor->saveEntity(
                $series['id'] . '/' . $number,
                Type::VIDEO_SEASON,
                $series['title_ru'] . ' - ' . $number . ' сезон'
            );
            $seasonTree = $this->constructor->manualSaveToTree(
                $entitySeason,
                $seriesTree,
                $seriesTree,
                $tree['season'][$number]['level'],
                $tree['season'][$number]['lft'],
                $tree['season'][$number]['rft']
            );
            $this->constructor->saveEntityToHost(
                $entitySeason,
                SlugMakerService::makeSlug($series['title_ru'] . ' - ' . $number . ' сезон'
                ));

            foreach ($season['episodes'] as $episode) {
                $entityEpisode = $this->constructor->saveEntity(
                    $episode['video_id'],
                    Type::VIDEO_EPISODE,
                    $series['title_ru'] . ' - ' .
                    $episode['season'] . ' сезон - ' . $episode['episode'] . ' эпизод'
                );
                $this->constructor->manualSaveToTree(
                    $entityEpisode,
                    $seriesTree,
                    $seasonTree,
                    $tree['episode'][$episode['video_id']]['level'],
                    $tree['episode'][$episode['video_id']]['lft'],
                    $tree['episode'][$episode['video_id']]['rft']
                );
                $this->constructor->saveEntityToHost(
                    $entityEpisode,
                    SlugMakerService::makeSlug($series['title_ru'] . ' - ' .
                        $episode['season'] . ' сезон - ' . $episode['episode'] . ' эпизод'));
            }

            $this->constructor->confirmMigration();
        }
    }
}