<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\Constructor;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Processor\TagRepository;
use App\Service\EntityProcessor\Infrastructure\DAO\HostDAO;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;

class MigrateConstructor implements MigrateConstructorInterface
{
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityHostToEntityHostRepository */
    private $entityToEntityRepository;
    /** @var EntityNestedTreeRepository */
    private $tree;
    /** @var TagRepository */
    private $tagRepository;
    /** @var ReceiveMapInterface */
    private $map;

    /** @var Host */
    private $host;

    public function __construct(
        EntitiesRepository $entitiesRepository,
        HostRepository $hostRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityHostToEntityHostRepository $entityToEntityRepository,
        EntityNestedTreeRepository $tree,
        TagRepository $tagRepository,
        ReceiveMapInterface $map,
        EntityManagerInterface $manager
    )
    {
        $this->entitiesRepository = $entitiesRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->hostRepository = $hostRepository;
        $this->entityToEntityRepository = $entityToEntityRepository;
        $this->tree = $tree;
        $this->tagRepository = $tagRepository;
        $this->entityManager = $manager;
        $this->map = $map;
    }

    public function confirmMigration()
    {
        $this->entityManager->flush();
    }

    public function setHost(Host $host)
    {
        $this->host = $host;
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function host(int $id)
    {
        if ($this->host && $this->host->getId() === $id) {
            return;
        }
        $this->host = $this->hostRepository->findById($id);
        if (!$this->host) {
            throw new \Exception('Invalid host');
        }
    }

    /**
     * @param string $id
     * @param int $type
     * @param string $name
     * @param int $score
     * @return Entity
     */
    public function saveEntity(string $id, int $type, string $name, int $score = 0): Entity
    {
        $entity = $this->entitiesRepository->findOneBy([
            'entity_type' => $type,
            'source_id' => $id
        ]);
        if (!$entity) {
            $entity = new Entity();
            $entity->setEntityType($type);
            $entity->setSourceId($id);
            $entity->setScore($score);
        }
        $entity->setName($name);
        $this->entityManager->persist($entity);
        return $entity;
    }

    /**
     * @param Entity $entity
     * @param string $slug
     * @param bool $duplicate
     * @return EntityToHost
     */
    public function saveEntityToHost(Entity $entity, string $slug, bool $duplicate = false): EntityToHost
    {
        $entityToHost = $this->entityToHostRepository->findOneBy([
            'host' => $this->host,
            'entity' => $entity,
        ]);
        if (!$entityToHost) {
            $entityToHost = new EntityToHost();
            $entityToHost->setEntitySlug($slug);
            $entityToHost->setEntity($entity);
            $entityToHost->setHost($this->host);
        }
        $this->entityManager->persist($entityToHost);

        if ($duplicate) {
            $this->duplicateEntitiesToChild($entity, $slug);
        }
        return $entityToHost;
    }

    /**
     * @param EntityToHost $entityToHost
     * @param int $type
     * @return EntityHostToEntityHost
     */
    public function saveToList(EntityToHost $entityToHost, int $type): EntityHostToEntityHost
    {
        $listEntity = $this->entitiesRepository->findOneBy([
            'entity_type' => $type
        ]);
        if (!$listEntity) {
            $listEntity = new Entity();
            $listEntity->setEntityType($type);
            $listEntity->setSourceId(0);
            $listEntity->setScore(0);
            $this->entityManager->persist($listEntity);
            $this->entityManager->flush($listEntity);
        }

        $mainETH = $this->entityToHostRepository->findOneBy([
            'entity' => $listEntity,
            'host' => $entityToHost->getHost()
        ]);
        if (!$mainETH) {
            $data = $entityToHost->getHost()->getData();
            $slugTypes = $data[HostDAO::SLUG_TYPES];
            $config = [];
            foreach ($slugTypes as $slugType) {
                if ($slugType['slug_type'] == $type) {
                    $config = $slugType;
                }
            }

            $mainETH = new EntityToHost();
            $mainETH->setEntity($listEntity);
            $mainETH->setHost($entityToHost->getHost());
            $mainETH->setEntitySlug($config['pattern']['slug']);
            $this->entityManager->persist($mainETH);
            $this->entityManager->flush($mainETH);
        }

        $entityToEntity = $this->entityToEntityRepository->findOneBy([
            'main' => $mainETH,
            'subordinate' => $entityToHost
        ]);

        if (!$entityToEntity) {
            $entityToEntity = new EntityHostToEntityHost();
            $entityToEntity->setMain($mainETH);
            $entityToEntity->setSubordinate($entityToHost);
            $entityToEntity->setPosition(0);
            $entityToEntity->setType(Type::RELATION_TYPE_LIST);
            $this->entityManager->persist($entityToEntity);
        }
        return $entityToEntity;
    }

    public function joinEntityHosts(EntityToHost $entityToHost, EntityToHost $toThis, int $type): EntityHostToEntityHost
    {
        $entityToEntity = $this->entityToEntityRepository->findOneBy([
            'main' => $toThis,
            'subordinate' => $entityToHost
        ]);

        if (!$entityToEntity) {
            $entityToEntity = new EntityHostToEntityHost();
            $entityToEntity->setMain($toThis);
            $entityToEntity->setSubordinate($entityToHost);
            $entityToEntity->setPosition(0);
            $entityToEntity->setType($type);
            $this->entityManager->persist($entityToEntity);
        }
        return $entityToEntity;
    }

    /**
     * @param Entity $entity
     * @param EntityNestedTree|null $root
     * @param EntityNestedTree|null $parent
     * @return EntityNestedTree
     */
    public function saveToTree(
        Entity $entity,
        EntityNestedTree $root = null,
        EntityNestedTree $parent = null
    ): EntityNestedTree
    {
        $byEntity = $this->tree->findByEntity($entity);

        if (!$byEntity) {
            $byEntity = new EntityNestedTree();
            $byEntity->setRoot(is_null($root) ? null : $byEntity);
            $byEntity->setEntity($entity);
            if ($parent) {
                $byEntity->setParent($parent);
            }
            $this->entityManager->persist($byEntity);
        }
        return $byEntity;
    }

    /**
     * Not working correctly
     *
     * @param Entity $entity
     * @param EntityNestedTree|null $root
     * @param EntityNestedTree|null $parent
     * @param int $lvl
     * @param int $lft
     * @param int $rft
     * @return EntityNestedTree|null|object
     */
    public function manualSaveToTree(
        Entity $entity,
        EntityNestedTree $root = null,
        EntityNestedTree $parent = null,
        int $lvl,
        int $lft,
        int $rft
    )
    {
        /** @var EntityNestedTree $byEntity */
        $byEntity = $this->tree->findByEntity($entity);

        if ($byEntity) {
            if (
                $byEntity->getLevel() == $lvl &&
                $byEntity->getLft() == $lft &&
                $byEntity->getRgt() == $rft
            ) {
                return $byEntity;
            }
        }

        if (!$byEntity) {
            $byEntity = new EntityNestedTree();
            $byEntity->setEntity($entity);
            if ($parent) {
                $byEntity->setParent($parent);
            }
            $byEntity->setRoot($byEntity);
            if ($root) {
                $byEntity->setRoot($root);
            }
        }
        $byEntity->setLevel($lvl);
        $byEntity->setLft($lft);
        $byEntity->setRgt($rft);

        $this->entityManager->persist($byEntity);

        return $byEntity;
    }

    public function bindTagsToEntity(Entity $entity)
    {
        $entityDTOClass = Type::MAP_DTO[$entity->getEntityType()];
        $tagDTOClass = Type::MAP_TAG[$entity->getEntityType()];

        $dto = new $entityDTOClass();
        $map = $this->map->map($dto);
        $client = new Client(['auth' => [$map->getUsername(), $map->getPassword()]]);
        $response = $client->get($map->getHost() . "{$entity->getSourceId()}/genres");
        $response = json_decode($response->getBody()->getContents(), true);

        $genres = $response['records'];
        /** @var DTOTagInterface|DTOInterface $tagDTO */
        $tagDTO = new $tagDTOClass();
        $tagEntities = $this->entitiesRepository->createQueryBuilder('e')
            ->where('e.entity_type = :type')
            ->setParameter('type', $tagDTO->getEntityType())
            ->andWhere('e.source_id in (:ids)')
            ->setParameter('ids', $genres)
            ->getQuery()
            ->getResult();

        /** @var Entity $tagEntity */
        foreach ($tagEntities as $tagEntity) {
            $entityTagToHost = $this->entityToHostRepository->findOneBy([
                'entity' => $tagEntity,
                'host' => $this->host
            ]);
            if (!$entityTagToHost) {
                $entityTagToHost = new EntityToHost();
                $entityTagToHost->setEntity($tagEntity);
                $entityTagToHost->setHost($this->host);
                $entityTagToHost->setEntitySlug(SlugMakerService::makeSlug($tagEntity->getName()));
                $this->entityManager->persist($entityTagToHost);
            }
            unset($entityTagToHost);
        }

        $tags = $this->tagRepository->createQueryBuilder('t')
            ->where('t.entity in (:entities)')
            ->setParameter('entities', $tagEntities)
            ->getQuery()
            ->getResult();
        $entity->setTags($tags);
        $this->entityManager->persist($entity);
    }

    public function setTrackLyric(Entity $track, array $lyricData = null)
    {
        if (empty($lyricData)) {
            return;
        }
        $entityLyric = $this->entitiesRepository->findOneBy([
            'entity_type' => Type::AUDIO_LYRIC,
            'source_id' => $track->getSourceId()
        ]);
        if (!$entityLyric) {
            $entityLyric = new Entity();
            $entityLyric->setEntityType(Type::AUDIO_LYRIC);
            $entityLyric->setSourceId($track->getSourceId());
            $entityLyric->setScore(0);
            $entityLyric->setName('Текст песни ' . $track->getName());
            $this->entityManager->persist($entityLyric);
        }
        /** @var EntityToHost $trackEntityToHost */
        $trackEntityToHost = $this->entityToHostRepository->findOneBy([
            'entity' => $track,
            'host' => $this->host
        ]);

        if (!$trackEntityToHost) {
            return;
        }
        $slug = SlugMakerService::makeSlug($track->getName());
        $entityToHost = $this->entityToHostRepository->findOneBy([
            'host' => $this->host,
            'entity' => $entityLyric,
            'entity_slug' => $slug
        ]);
        if (!$entityToHost) {
            $entityToHost = new EntityToHost();
            $entityToHost->setEntitySlug($slug);
            $entityToHost->setEntity($entityLyric);
            $entityToHost->setHost($this->host);
            $this->entityManager->persist($entityToHost);
        }
        $entityHostToEntityHost = $this->entityToEntityRepository->findBy([
            'main' => $trackEntityToHost,
            'subordinate' => $entityToHost
        ]);
        if (!$entityHostToEntityHost) {
            $entityHostToEntityHost = new EntityHostToEntityHost();
            $entityHostToEntityHost->setMain($trackEntityToHost);
            $entityHostToEntityHost->setSubordinate($entityToHost);
            $entityHostToEntityHost->setType(Type::RELATION_TYPE_RELATE);
            $this->entityManager->persist($entityHostToEntityHost);
        }
    }

    //todo make adding to sitemaps

    /**
     * @param Entity $entity
     * @param string $slug
     */
    private function duplicateEntitiesToChild(Entity $entity, string $slug)
    {
        foreach ($this->host->getChildren() as $host) {
            $entityToHost = $this->entityToHostRepository->findOneBy([
                'host' => $this->host,
                'entity' => $entity,
                'entity_slug' => $slug
            ]);
            if (!$entityToHost) {
                $entityToHost = new EntityToHost();
                $entityToHost->setHost($host);
                $entityToHost->setEntity($entity);
                $entityToHost->setEntitySlug($slug);
            }
            $this->entityManager->persist($entityToHost);
        }
    }
}