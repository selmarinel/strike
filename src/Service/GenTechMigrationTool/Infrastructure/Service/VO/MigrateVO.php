<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\VO;


use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;

class MigrateVO implements MigrateVOInterface
{
    /** @var int */
    private $hostId = 0;
    /** @var string */
    private $title = '';
    /** @var  string */
    private $id = '';

    /**
     * @return int
     */
    public function getHostId(): int
    {
        return $this->hostId;
    }

    /**
     * @param int $hostId
     */
    public function setHostId(int $hostId): void
    {
        $this->hostId = $hostId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}