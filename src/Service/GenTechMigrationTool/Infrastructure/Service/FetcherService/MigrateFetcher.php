<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\GenTechMigrationTool\Domain\Service\FetcherService\MigrateFetcherInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\Decorator\ArticleFetchVO;
use App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\VO\FetchVO;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use GuzzleHttp\Client;

class MigrateFetcher implements MigrateFetcherInterface
{
    /** @var ReceiveMapInterface */
    private $map;
    /** @var  Client */
    private $client;
    private $entitiesRepository;

    public function __construct(
        ReceiveMapInterface $map,
        EntitiesRepository $entitiesRepository
    )
    {
        $this->map = $map;
        $this->entitiesRepository = $entitiesRepository;
    }

    public function prepareClient(DTOInterface $DTO): MigrateFetcherInterface
    {
        $uri = $this->map->map($DTO);

        $this->client = new Client([
            'auth' => [
                $uri->getUsername(),
                $uri->getPassword()
            ],
            'base_uri' => $uri->getHost()
        ]);
        return $this;
    }

    public function fetchIds(DTOInterface $DTO, FetchVO $fetchVO): array
    {
        switch ($DTO->getEntityType()) {
            case Type::NEWS_ARTICLE:
                $fetchVO = new ArticleFetchVO($fetchVO);
                $fetchVO->setEntitiesRepository($this->entitiesRepository);

                $urlToFetch = "fetch/last?limit={$fetchVO->getLimit()}&date={$fetchVO->getDate()}";
                $field = 'data';
                break;
            default:
                $urlToFetch = "random?limit={$fetchVO->getLimit()}";
                $field = ['records'];
        }

        $result = $this->client->get($urlToFetch);
        $result = json_decode($result->getBody()->getContents(), true);
        return $result[$field];
    }
}