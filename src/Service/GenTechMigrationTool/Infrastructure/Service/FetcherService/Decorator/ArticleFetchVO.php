<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\Decorator;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\VO\FetchVO;

class ArticleFetchVO extends FetchVO
{
    private $fetcherVO = null;
    /** @var EntitiesRepository */
    private $entitiesRepository;

    public function __construct(FetchVO $fetchVO)
    {
        $this->fetcherVO;
    }

    public function getDate(): int
    {
        return $this->fetchLastArticleDate();
    }

    /**
     * @param EntitiesRepository $entitiesRepository
     */
    public function setEntitiesRepository(EntitiesRepository $entitiesRepository)
    {
        $this->entitiesRepository = $entitiesRepository;
    }

    /**
     * @return int
     */
    private function fetchLastArticleDate(): int
    {
        $timestamp = 0;
        $entity = $this->entitiesRepository->createQueryBuilder('e')
            ->where('e.entity_type = :entityType')
            ->setParameter('entityType', Type::NEWS_ARTICLE)
            ->orderBy('e.score', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if ($entity) {
            $timestamp = $entity->getScore() ?: 0;
        }
        return $timestamp;

    }
}