<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\VO;


class FetchVO
{
    /** @var int */
    private $limit = 12;
    /** @var int */
    private $date = 0;

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate(int $date)
    {
        $this->date = $date;
    }
}