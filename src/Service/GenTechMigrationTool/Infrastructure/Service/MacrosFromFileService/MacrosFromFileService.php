<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Service\MacrosFromFileService;


use App\Service\Base\Infrastructure\Repository\Templator\MacrosRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\GenTechMigrationTool\Domain\Service\MacrosFromFileService\MacrosFromFileInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\HostNotFoundWithHostnameException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MacrosToFileVo;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;

class MacrosFromFileService implements MacrosFromFileInterface
{
    /** @var MacrosRepository */
    private $macrosRepository;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(MacrosRepository $macrosRepository, HostRepository $hostRepository)
    {
        $this->macrosRepository = $macrosRepository;
        $this->hostRepository = $hostRepository;
    }

    /**
     * @param MacrosToFileVo $macrosToFileVo
     * @throws HostNotFoundWithHostnameException
     */
    public function process(MacrosToFileVo $macrosToFileVo)
    {
        $macros = $this->macrosRepository->createQueryBuilder('t')
            ->join('t.host', 'h')
            ->where('h.hostname = :host_name')
            ->andWhere('t.entity_type = :entityType')
            ->setParameter('host_name', $macrosToFileVo->getDomain())
            ->setParameter('entityType', $macrosToFileVo->getEntityType())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$macros) {

            $host = $this->hostRepository->findByHostName($macrosToFileVo->getDomain());
            if (!$host) {
                throw new HostNotFoundWithHostnameException("Host with hostname {$macrosToFileVo->getDomain()} not found");
            }
            $macros = new Macros();
            $macros->setEntityType($macrosToFileVo->getEntityType());
            $macros->setHost($host);
        }

        $macros->setSeoData([
            'seo_h1' => $macrosToFileVo->getH1(),
            'seo_description' => $macrosToFileVo->getDescription(),
            'seo_title' => $macrosToFileVo->getTitle(),
        ]);
        $this->macrosRepository->save($macros);
    }
}