<?php

namespace App\Service\GenTechMigrationTool\Infrastructure\Traits;


use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Trait MigrateConsoleTrait
 * @package App\Service\GenTechMigrationTool\Infrastructure\Traits
 * @mixin NeedToTimeLogMigrateInterface
 */
trait MigrateConsoleTrait
{
    /** @var OutputInterface */
    private $output;
    /** @var \DateTimeInterface */
    private $begin;
    /** @var \DateTimeInterface */
    private $end;


    /**
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $outputStyle = new OutputFormatterStyle('red', 'yellow', ['bold']);
        $output->getFormatter()->setStyle('warning', $outputStyle);
        $outputStyle = new OutputFormatterStyle('blue', null, ['bold']);
        $output->getFormatter()->setStyle('notice', $outputStyle);

        $this->output = $output;

    }

    public function startMeasuring()
    {
        $this->begin = new \DateTime();
    }

    public function endMeasuring()
    {
        $this->end = new \DateTime();
    }

    public function measurePeriod($format = '%s seconds')
    {
        if ($this->begin && $this->end && $this->output) {
            $diff = $this->end->diff($this->begin);
            $interval = $diff->format($format);
            if ($interval > 5) {
                $this->output->writeln("<warning>Working {$interval}</warning>");
            } else {
                $this->output->writeln("<info>Working {$interval}</info>");
            }
        }
    }

    public function notice(string $notice)
    {
        $this->output->writeln("<notice>{$notice}</notice>");
    }
}