<?php

namespace App\Service\GenTechMigrationTool\Domain\Service\FetcherService;

use App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\VO\FetchVO;
use App\Service\Processor\Domain\DTO\DTOInterface;

interface MigrateFetcherInterface
{
    public function prepareClient(DTOInterface $DTO): MigrateFetcherInterface;

    public function fetchIds(DTOInterface $DTO, FetchVO $fetchVO):array;
}