<?php

namespace App\Service\GenTechMigrationTool\Domain\Service;


use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;

interface MigrationInterface
{
    public function migrateContent(MigrateVOInterface $migrateVO);
}