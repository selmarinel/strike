<?php

namespace App\Service\GenTechMigrationTool\Domain\Service;


use App\Service\EntityProcessor\Domain\VO\Transmitter\TransmitterParametersVOInterface;
use App\Service\GenTechMigrationTool\Domain\VO\MigrateVOInterface;

/**
 * Interface MigrateInterface
 * @package App\Service\GenTechMigrationTool\Domain\Service
 * @deprecated
 */
interface MigrateInterface
{
    public function migrate(
        TransmitterParametersVOInterface $transmitterParametersVO,
        MigrateVOInterface $migrateVO,
        string $receiveUrl
    );
}