<?php

namespace App\Service\GenTechMigrationTool\Domain\Service\Constructor;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use Doctrine\ORM\EntityManagerInterface;

interface MigrateConstructorInterface
{
    public function confirmMigration();

    public function setHost(Host $host);

    public function host(int $id);

    public function saveEntity(string $id, int $type, string $name, int $score = 0): Entity;

    public function saveEntityToHost(Entity $entity, string $slug, bool $duplicate = false): EntityToHost;

    public function saveToList(EntityToHost $entityToHost, int $type): EntityHostToEntityHost;

    public function joinEntityHosts(EntityToHost $entityToHost, EntityToHost $toThis, int $type): EntityHostToEntityHost;

    public function saveToTree(
        Entity $entity,
        EntityNestedTree $root = null,
        EntityNestedTree $parent = null
    ): EntityNestedTree;

    public function bindTagsToEntity(Entity $entity);

    public function setTrackLyric(Entity $track, array $lyricData = null);

    public function manualSaveToTree(
        Entity $entity,
        EntityNestedTree $root = null,
        EntityNestedTree $parent = null,
        int $lvl,
        int $lft,
        int $rft
    );
}