<?php

namespace App\Service\GenTechMigrationTool\Domain\Service;


use Symfony\Component\Console\Output\OutputInterface;

interface NeedToTimeLogMigrateInterface
{
    public function setOutput(OutputInterface $output): void;

    public function startMeasuring();

    public function endMeasuring();

    public function measurePeriod($format = '%s seconds');
}