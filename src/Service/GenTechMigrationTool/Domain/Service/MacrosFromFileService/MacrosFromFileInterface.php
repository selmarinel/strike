<?php

namespace App\Service\GenTechMigrationTool\Domain\Service\MacrosFromFileService;


use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\HostNotFoundWithHostnameException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MacrosToFileVo;

interface MacrosFromFileInterface
{
    /**
     * @param MacrosToFileVo $macrosToFileVo
     * @throws HostNotFoundWithHostnameException
     */
    public function process(MacrosToFileVo $macrosToFileVo);
}