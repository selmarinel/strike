<?php

namespace App\Service\GenTechMigrationTool\Domain\VO;


interface MigrateVOInterface
{
    public function setHostId(int $hostId);

    public function getHostId(): int;

    public function setTitle(string $title);

    public function getTitle(): string;

    public function getId(): string;

    public function setId(string $id): void;
}