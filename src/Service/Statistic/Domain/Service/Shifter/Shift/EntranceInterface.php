<?php

namespace App\Service\Statistic\Domain\Service\Shifter\Shift;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

interface EntranceInterface
{
    public function run(Host $host, EntityToHost $entityToHost, string $fromLink);
}