<?php

namespace App\Service\Statistic\Domain\Service\Scavenger\Adapter;


use App\Service\Statistic\Infrastructure\Service\Scavenger\DTO\AbuseDTO;

interface ScavengerAdapterInterface
{
    public function morph(AbuseDTO $abuseDTO);

    public function save(AbuseDTO $abuseDTO);

    public function search(array $parameters);

    public function calculateCountOfAbusesOnHost(int $hostId): int;

    /**
     * @param int $hostId
     * @return AbuseDTO[]|array
     */
    public function searchByHostId(int $hostId): array;
}