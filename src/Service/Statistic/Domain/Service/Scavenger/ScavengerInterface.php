<?php

namespace App\Service\Statistic\Domain\Service\Scavenger;

use Symfony\Component\HttpFoundation\Request;

interface ScavengerInterface
{
    public function saveFromRequest(Request $request);
}