<?php

namespace App\Service\Statistic\Domain\Service;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

interface ShifterInterface
{
    public function shift(Host $host, string $zone);
}