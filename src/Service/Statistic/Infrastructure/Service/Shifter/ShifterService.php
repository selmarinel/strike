<?php

namespace App\Service\Statistic\Infrastructure\Service\Shifter;


use App\Service\Base\Infrastructure\Entity\Redirect\Redirect;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Redirect\RedirectRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use App\Service\Statistic\Domain\Service\Scavenger\Adapter\ScavengerAdapterInterface;
use App\Service\Statistic\Domain\Service\ShifterInterface;
use App\Service\Statistic\Infrastructure\Database\Entity\ShiftLogEntity;
use App\Service\Statistic\Infrastructure\Database\Repository\ShiftLogRepository;
use App\Service\Statistic\Infrastructure\Service\Scavenger\DTO\AbuseDTO;
use App\Service\Statistic\Infrastructure\Service\Shifter\Exceptions\AbusesIsEmpty;
use App\Service\Statistic\Infrastructure\Service\Shifter\Helper\DomainNameTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShifterService implements ShifterInterface
{
    use DomainNameTrait;

    const GREEN_ZONE = 1;
    const CONCERNED_ZONE = 2;
    const DANGER_ZONE = 3;

    const DANGER_PERCENT = 10;

    /** @var ScavengerAdapterInterface */
    private $adapter;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var RedirectRepository */
    private $redirectRepository;
    /** @var EntityLinkGeneratorInterface */
    private $generator;
    /** @var PrepareHostAndEntityInterface */
    private $prepareHostAndEntity;
    /** @var ContainerInterface */
    private $container;
    /** @var ShiftLogRepository */
    private $shiftLogRepository;
    /** @var EntityNestedTreeRepository */
    private $treeRepository;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var EntityHostToEntityHostRepository  */
    private $entityHostToEntityHostRepository;

    public function __construct(
        HostRepository $hostRepository,
        EntityToHostRepository $entityToHostRepository,
        EntitiesRepository $entitiesRepository,
        RedirectRepository $redirectRepository,
        EntityNestedTreeRepository $treeRepository,
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        ShiftLogRepository $shiftLogRepository,
        EntityLinkGeneratorInterface $linkGenerator,
        PrepareHostAndEntityInterface $prepareHostAndEntity,
        ScavengerAdapterInterface $scavengerAdapter,
        ContainerInterface $container
    ) {
        $this->entitiesRepository = $entitiesRepository;
        $this->entityHostToEntityHostRepository = $entityHostToEntityHostRepository;
        $this->hostRepository = $hostRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->redirectRepository = $redirectRepository;
        $this->shiftLogRepository = $shiftLogRepository;
        $this->treeRepository = $treeRepository;
        $this->generator = $linkGenerator;
        $this->prepareHostAndEntity = $prepareHostAndEntity;
        $this->adapter = $scavengerAdapter;
        $this->container = $container;
    }

    /**
     * @param EntityNestedTree $node
     * @param Host $host
     * @param array $collection
     * @return EntityToHost[]
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function processRoot(EntityNestedTree $node, Host $host, array &$collection = [])
    {
        $children = $node->getChildren();
        /** @var EntityNestedTree $child */
        foreach ($children as $child) {
            if (empty($child->getChildren())) {
                continue;
            }
            /** @var EntityToHost $entityToHost */
            $entityToHost = $this->entityToHostRepository->findOneBy([
                'host' => $host,
                'entity' => $child->getEntity()
            ]);
            if ($entityToHost) {
                $collection[] = $entityToHost;
            }
            $this->processRoot($child, $host, $collection);
        }
        return $collection;
    }

    /**
     * @param Host $host
     * @param Entity $entity
     * @param AbuseDTO $abuseDTO
     * @return array
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function greenShift(Host $host, Entity $entity, AbuseDTO $abuseDTO)
    {
        $return = [];
        $acceptorHost = $this->prepareHost($host);
        /** @var EntityNestedTree $node */
        $node = $this->treeRepository->findOneBy(['entity' => $entity]);
        $root = $node->getRoot();
        if ($root !== $node->getId()) {
            $node = $this->treeRepository->find($root);
        }
        $collection = [];
        $collection = $this->processRoot($node, $host, $collection);

        foreach ($collection as $entityToHost) {
            $this->prepareHostAndEntity->prepareFromEntityToHost($entityToHost);
            $fromLink = $this->generator->generateLink(
                $this->prepareHostAndEntity->getEntityDAO(),
                $this->prepareHostAndEntity->getHostDAO());

            $redirectEntity = $this->redirectRepository->findOneBy([
                'redirectFrom' => $fromLink
            ]);

            if($redirectEntity){
                $return[] = $redirectEntity;
                continue;
            }

            $entityToHostShifter = $this->prepareEntityToHost($acceptorHost, $entityToHost);
            $this->prepareLog(
                $entityToHost->getEntity(),
                $host,
                $acceptorHost,
                $abuseDTO->getDate()
            );

            $this->prepareHostAndEntity->prepareFromEntityToHost($entityToHostShifter);
            $toLink = $this->generator->generateLink(
                $this->prepareHostAndEntity->getEntityDAO(),
                $this->prepareHostAndEntity->getHostDAO());

            $redirectEntity = new Redirect();
            $redirectEntity->setRedirectFrom($fromLink);
            $redirectEntity->setRedirectTo($toLink);
            $this->redirectRepository->save($redirectEntity);
            $return[] = $redirectEntity;

            $this->entityToHostRepository->remove($entityToHost);
        }
        return $return;
    }

    /**
     * @param Host $host
     * @param Entity $entity
     * @param AbuseDTO $abuseDTO
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function concernedShift(Host $host, Entity $entity, AbuseDTO $abuseDTO)
    {
        $return = [];

        /** @var EntityNestedTree $node */
        $node = $this->treeRepository->findOneBy(['entity' => $entity]);
        $root = $node->getRoot();
        if ($root !== $node->getId()) {
            $node = $this->treeRepository->find($root);
        }
        $collection = [];
        $collection = $this->processRoot($node, $host, $collection);

        foreach ($collection as $entityToHost) {
            $this->prepareHostAndEntity->prepareFromEntityToHost($entityToHost);
            $fromLink = $this->generator->generateLink(
                $this->prepareHostAndEntity->getEntityDAO(),
                $this->prepareHostAndEntity->getHostDAO());

            $redirectEntity = $this->redirectRepository->findOneBy([
                'redirectFrom' => $fromLink
            ]);
            if($redirectEntity){
                $return[] = $redirectEntity;
                continue;
            }

            $shiftLog = $this->shiftLogRepository->createQueryBuilder('sl')
                ->where('sl.entity = :entity')
                ->andWhere('sl.hostTo = :host')
                ->setParameter('entity', $entityToHost->getEntity())
                ->setParameter('host', $host)
                ->orderBy('sl.date', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

            if ($shiftLog) {
                if ($abuseDTO->getDate()->getTimestamp() === $shiftLog->getDate()->getTimeStamp()) {
                    continue;
                }
                $diff = $abuseDTO->getDate()->diff($shiftLog->getDate());
                if ($diff->m > 3) {
                    $this->prepareLog($entityToHost->getEntity(), $host, $host, $abuseDTO->getDate());
                    continue;
                } else {
                    $acceptorHost = $this->prepareHost($host);
                    $entityToHostShifter = $this->prepareEntityToHost($acceptorHost, $entityToHost);

                    $this->prepareLog(
                        $entityToHost->getEntity(),
                        $host,
                        $acceptorHost,
                        $abuseDTO->getDate());

                    $this->prepareHostAndEntity->prepareFromEntityToHost($entityToHostShifter);
                    $toLink = $this->generator->generateLink(
                        $this->prepareHostAndEntity->getEntityDAO(),
                        $this->prepareHostAndEntity->getHostDAO());
                    $redirectEntity = new Redirect();
                    $redirectEntity->setRedirectFrom($fromLink);
                    $redirectEntity->setRedirectTo($toLink);
                    $this->redirectRepository->save($redirectEntity);
                    $return[] = $redirectEntity;
                }
            }
        }
        return $return;
    }

    /**
     * @param Host $host
     * @return void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function dangerShift(Host $host)
    {
        $countOfAbuse = $this->adapter->calculateCountOfAbusesOnHost($host->getId());

        $countOfContent = count($this->entityToHostRepository->createQueryBuilder('eh')
            ->where('eh.host = :host')
            ->setParameter('host', $host->getParent())
            ->getQuery()
            ->getResult());

        $percent = ($countOfAbuse / $countOfContent) * 100;
        if ($percent >= self::DANGER_PERCENT) {
            $acceptorHost = $this->prepareHost($host);

            $data = $host->getData();
            $data['redirect'] = $acceptorHost->getId();
            $host->setData($data);
            $this->hostRepository->save($host);

            $allEntitiesOnHost = $this->entityToHostRepository->findBy([
                'host' => $host
            ]);
            /** @var EntityToHost $entityToHost */
            foreach ($allEntitiesOnHost as $entityToHost){
                $newEntityToHost = new EntityToHost();
                $newEntityToHost->setEntity($entityToHost->getEntity());
                $newEntityToHost->setHost($acceptorHost);
                $newEntityToHost->setEntitySlug($entityToHost->getEntitySlug());
                $this->entityToHostRepository->save($newEntityToHost);
                $this->entityToHostRepository->remove($entityToHost);
            }

        }
    }


    /**
     * @param Host $host
     * @param string $zone
     * @return array
     * @throws AbusesIsEmpty
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @see '../shift.feature'
     */
    public function shift(Host $host, string $zone)
    {
        $abuseCollection = $this->adapter->searchByHostId($host->getId());

        if (empty($abuseCollection)) {
            throw new AbusesIsEmpty;
        }

        if ($zone === self::DANGER_ZONE) {
            $this->dangerShift($host);
            return [];
        }

        $result = [];
        foreach ($abuseCollection as $abuseDTO) {
            /** @var Entity $entityWithAbuse */
            if ($zone == self::GREEN_ZONE) {
                $entityWithAbuse = $this->entitiesRepository->findById($abuseDTO->getEntityId());
                $result = $this->greenShift($host, $entityWithAbuse, $abuseDTO);
                continue;
            }

            if ($zone == self::CONCERNED_ZONE) {
                $entityWithAbuse = $this->entitiesRepository->findById($abuseDTO->getEntityId());
                $result = $this->concernedShift($host, $entityWithAbuse, $abuseDTO);
                continue;
            }
        }
        return $result;
    }

    /**
     * @param Host $host
     * @return Host
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function prepareHost(Host $host): Host
    {
        $acceptorHost = $this->hostRepository->findOneBy([
            'abuse_level' => $host->getAbuseLevel() + 1,
            'parent' => $host->getParent(),
            'geo' => $host->getGeo(),
            'type' => $host->getType()
        ]);

        if (!$acceptorHost) {
            $acceptorHost = new Host();
            $acceptorHost->setType($host->getType());
            $acceptorHost->setGeo($host->getGeo());
            $acceptorHost->setParent($host->getParent());
            $acceptorHost->setData($host->getData());
            $acceptorHost->setDriver($host->getDriver());
            $acceptorHost->setAbuseLevel($host->getAbuseLevel() + 1);
            $acceptorHost->setHostname($this->generateHostname($host));
            $this->hostRepository->save($acceptorHost);
        }
        return $acceptorHost;
    }

    /**
     * @param Host $host
     * @param EntityToHost $entityToHost
     * @return EntityToHost|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function prepareEntityToHost(Host $host, EntityToHost $entityToHost)
    {
        $entityToHostShifter = $this->entityToHostRepository->findOneBy([
            'host' => $host,
            'entity' => $entityToHost->getEntity()
        ]);

        if (!$entityToHostShifter) {
            $entityToHostShifter = new EntityToHost();
            $entityToHostShifter->setHost($host);
            $entityToHostShifter->setEntity($entityToHost->getEntity());
            $entityToHostShifter->setEntitySlug($entityToHost->getEntitySlug());
            $this->entityToHostRepository->save($entityToHostShifter);
        }
        return $entityToHostShifter;
    }

    /**
     * @param Entity $entity
     * @param Host $fromHost
     * @param Host $toHost
     * @param \DateTimeInterface $dateTime
     * @return ShiftLogEntity|mixed
     */
    private function prepareLog(Entity $entity, Host $fromHost, Host $toHost, \DateTimeInterface $dateTime)
    {
        $shiftLog = $this->shiftLogRepository->findOneBy([
            'entity' => $entity,
            'hostTo' => $toHost,
            'date' => $dateTime
        ]);

        if (!$shiftLog) {
            $shiftLog = new ShiftLogEntity();
            $shiftLog->setEntity($entity);
            $shiftLog->setHostTo($toHost);
            $shiftLog->setHostFrom($fromHost);
            $shiftLog->setDate($dateTime);
            $this->shiftLogRepository->save($shiftLog);
        }
        return $shiftLog;
    }


}