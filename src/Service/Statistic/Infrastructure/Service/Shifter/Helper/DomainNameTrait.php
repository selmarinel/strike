<?php

namespace App\Service\Statistic\Infrastructure\Service\Shifter\Helper;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;

trait DomainNameTrait
{
    protected function generateHostname(Host $host)
    {
        $hostname = $host->getParent()->getHostname();
        $a = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 2)), 0, 2);
        return "$a.$hostname";
    }
}