<?php

namespace App\Service\Statistic\Infrastructure\Service\Shifter\Exceptions;


class AbusesIsEmpty extends ShifterException
{
    protected $message = 'Abuses is empty';
}