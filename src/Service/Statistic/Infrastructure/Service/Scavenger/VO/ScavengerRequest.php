<?php

namespace App\Service\Statistic\Infrastructure\Service\Scavenger\VO;


use Symfony\Component\HttpFoundation\Request;

class ScavengerRequest
{
    /**
     * @param Request $request
     * @return self[]
     * @throws \Exception
     */
    public static function createFromHttpRequest(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        if (!isset($request['items'])) {
            throw new \Exception('Invalid request');
        }
        $result = [];
        foreach ($request['items'] as $item) {
            $scavengerRequest = new self();
            $scavengerRequest->setDate($item['date']);
            $scavengerRequest->setLink($item['link']);
            $result[] = $scavengerRequest;
            unset($scavengerRequest);
        }
        return $result;
    }

    private $link = '';
    private $date = 'now';

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return new \DateTime($this->date);
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }
}