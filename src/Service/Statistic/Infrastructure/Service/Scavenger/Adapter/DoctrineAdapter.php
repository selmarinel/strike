<?php

namespace App\Service\Statistic\Infrastructure\Service\Scavenger\Adapter;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Statistic\Domain\Service\Scavenger\Adapter\ScavengerAdapterInterface;
use App\Service\Statistic\Infrastructure\Database\Entity\AbuseEntity;
use App\Service\Statistic\Infrastructure\Database\Repository\AbuseEntityRepository;
use App\Service\Statistic\Infrastructure\Service\Scavenger\DTO\AbuseDTO;

class DoctrineAdapter implements ScavengerAdapterInterface
{

    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var AbuseEntityRepository */
    private $abuseEntityRepository;

    public function __construct(
        AbuseEntityRepository $abuseEntityRepository,
        HostRepository $hostRepository,
        EntityToHostRepository $entityToHostRepository
    ) {
        $this->abuseEntityRepository = $abuseEntityRepository;
        $this->hostRepository = $hostRepository;
        $this->entityToHostRepository = $entityToHostRepository;
    }

    /**
     * @param AbuseDTO $dto
     * @return AbuseEntity
     */
    public function morph(AbuseDTO $dto): AbuseEntity
    {
        /** @var Host $host */
        $host = $this->hostRepository->findById($dto->getHostId());
        /** @var EntityToHost $entityToHost */
        $entityToHost = $this->entityToHostRepository->findByHostIdAndEntityId(
            $dto->getHostId(), $dto->getEntityId()
        );

        $entity = new AbuseEntity();
        $entity->setDate($dto->getDate());
        $entity->setEntity($entityToHost->getEntity());
        $entity->setHost($host);
        return $entity;
    }

    /**
     * @param AbuseDTO $dto
     * @return AbuseEntity
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function save(AbuseDTO $dto): AbuseEntity
    {
        $entity = $this->abuseEntityRepository->createQueryBuilder('ae')
            ->join('ae.host', 'h')
            ->join('ae.entity', 'e')
            ->where('h.id = :hostId')
            ->andWhere('e.id = :entityId')
            ->setParameter('hostId', $dto->getHostId())
            ->setParameter('entityId', $dto->getEntityId())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$entity) {
            $entity = $this->morph($dto);
            $this->abuseEntityRepository->persist($entity);
            $this->abuseEntityRepository->flush();
        }
        return $entity;

    }

    /**
     * @param array $parameters
     * @return AbuseDTO[]
     */
    public function search(array $parameters): array
    {
        $queryBuilder = $this->abuseEntityRepository->createQueryBuilder('ae')
            ->select('ae, h, eh')
            ->join('ae.host', 'h')
            ->join('ae.entityToHost', 'eh');
        if (isset($parameters['host_id'])) {
            $queryBuilder->andWhere('h.id = :hostId')
                ->setParameter('hostId', $parameters['host_id']);
        }
        if (isset($parameters['entity_to_host_id'])) {
            $queryBuilder->andWhere('eh.id = :entityToHostId')
                ->setParameter('entityToHostId', $parameters['entity_to_host_id']);
        }
        if (isset($parameters['from'])) {
            $queryBuilder->andWhere('ae.date >= :fromDate')
                ->setParameter('fromDate', new \DateTime($parameters['from']));
        }
        if (isset($parameters['to'])) {
            $queryBuilder->andWhere('ae.date <= :toDate')
                ->setParameter('toDate', new \DateTime($parameters['to']));
        }

        $collection = $queryBuilder
            ->orderBy('ae.date')
            ->getQuery()
            ->getResult();

        $result = [];
        /** @var AbuseEntity $item */
        foreach ($collection as $item) {
            $dto = new AbuseDTO();
            $dto->setDate($item->getDate());
            $dto->setHostId($item->getHost()->getId());
            $dto->setEntityId($item->getEntity()->getId());
            $result[] = $dto;
            unset($dto);
        }
        return $result;
    }

    /**
     * @param int $hostId
     * @return AbuseDTO[]|array
     */
    public function searchByHostId(int $hostId): array
    {
        $collection = $this->abuseEntityRepository->createQueryBuilder('ae')
            ->join('ae.host', 'h')
            ->where('h.id = :hostId')
            ->setParameter('hostId', $hostId)
            ->getQuery()
            ->getResult();
        return $this->convertCollectionToDTOs($collection);
    }

    /**
     * @param AbuseEntity[] $collection
     * @return AbuseDTO[]
     */
    private function convertCollectionToDTOs(array $collection): array
    {
        $result = [];
        foreach ($collection as $item) {
            $dto = new AbuseDTO();
            $dto->setDate($item->getDate());
            $dto->setEntityId($item->getEntity()->getId());
            $dto->setHostId($item->getHost()->getId());
            $result[] = $dto;
            unset($dto);
        }
        return $result;
    }

    /**
     * @param int $hostId
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function calculateCountOfAbusesOnHost(int $hostId): int
    {
        return $this->abuseEntityRepository
            ->createQueryBuilder('ae')
            ->select('count(ae.id)')
            ->join('ae.host', 'h')
            ->where('h.id = :host_id')
            ->setParameter('host_id', $hostId)
            ->getQuery()
            ->getSingleScalarResult();
    }
}