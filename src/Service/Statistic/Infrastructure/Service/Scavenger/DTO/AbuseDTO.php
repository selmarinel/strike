<?php

namespace App\Service\Statistic\Infrastructure\Service\Scavenger\DTO;


class AbuseDTO
{
    /** @var \DateTimeInterface */
    private $date;
    /** @var int */
    private $entityId;
    /** @var int */
    private $hostId;

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     */
    public function setDate(\DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getHostId(): int
    {
        return $this->hostId;
    }

    /**
     * @param int $hostId
     */
    public function setHostId(int $hostId): void
    {
        $this->hostId = $hostId;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }
}