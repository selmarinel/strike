<?php

namespace App\Service\Statistic\Infrastructure\Service\Scavenger;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;

use App\Service\Processor\Domain\Service\RequestParser\RequestPreparationInterface;
use App\Service\Processor\Domain\Service\UrilParser\UrlParserServiceInterface;
use App\Service\Statistic\Domain\Service\Scavenger\Adapter\ScavengerAdapterInterface;
use App\Service\Statistic\Domain\Service\Scavenger\ScavengerInterface;
use App\Service\Statistic\Infrastructure\Service\Scavenger\DTO\AbuseDTO;
use App\Service\Statistic\Infrastructure\Service\Scavenger\VO\ScavengerRequest;
use Symfony\Component\HttpFoundation\Request;

class ScavengerService implements ScavengerInterface
{
    /** @var UrlParserServiceInterface */
    private $parser;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var ScavengerAdapterInterface */
    private $adapter;
    /** @var RequestPreparationInterface  */
    private $preparation;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        UrlParserServiceInterface $urlParserService,
        RequestPreparationInterface $requestPreparation,
        ScavengerAdapterInterface $adapter
    ) {
        $this->parser = $urlParserService;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->adapter = $adapter;
        $this->preparation = $requestPreparation;
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function saveFromRequest(Request $request)
    {
        $items = ScavengerRequest::createFromHttpRequest($request);

        foreach ($items as $scavengerRequest) {
            try {
                $requestVO = $this->preparation->prepareFromString($scavengerRequest->getLink());
                //find host and all data
                $hostDAO = $this->parser->processRequest($requestVO, true);

                $dto = new AbuseDTO();
                $dto->setEntityId($hostDAO->getEntityId());
                $dto->setDate($scavengerRequest->getDate());
                $dto->setHostId($hostDAO->getHostId());

                // save entity by adapter save method
                // for future changes (click house)
                $this->adapter->save($dto);
            } catch (\Exception $exception) {
                continue;
            } finally {
                unset($dto, $hostDAO);
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        return $this->adapter->search($request);
    }
}
