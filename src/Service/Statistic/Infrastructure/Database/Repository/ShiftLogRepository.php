<?php

namespace App\Service\Statistic\Infrastructure\Database\Repository;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Statistic\Infrastructure\Database\Entity\ShiftLogEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ShiftLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = ShiftLogEntity::class;
        parent::__construct($registry, $entityClass);
    }

    public function persist(ShiftLogEntity $entity)
    {
        $this->_em->persist($entity);
    }

    public function flush()
    {
        $this->_em->flush();
    }

    public function save(ShiftLogEntity $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        return $entity;
    }

    public function findLastByEntityAndHost(Entity $entity, Host $host)
    {
        return $shiftLog = $this->createQueryBuilder('shl')
            ->where('shl.entity = :entity')
            ->andWhere('shl.hostTo = :host')
            ->setParameter('entity', $entity)
            ->setParameter('host', $host)
            ->orderBy('shl.date')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }
}
