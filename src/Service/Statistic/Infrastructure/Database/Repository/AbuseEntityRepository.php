<?php

namespace App\Service\Statistic\Infrastructure\Database\Repository;

use App\Service\Statistic\Infrastructure\Database\Entity\AbuseEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class AbuseEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = AbuseEntity::class;
        parent::__construct($registry, $entityClass);
    }

    public function persist(AbuseEntity $entity)
    {
        $this->_em->persist($entity);
    }

    public function flush()
    {
        $this->_em->flush();
    }

    public function save(AbuseEntity $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        return $entity;
    }


}
