<?php

namespace App\Service\Statistic\Infrastructure\Database\Entity;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Service\Statistic\Infrastructure\Database\Repository\AbuseEntityRepository;

/**
 * Class AbuseEntity
 * @package App\Service\Statistic\Infrastructure\Database\Scavenger
 * @ORM\Entity(repositoryClass=AbuseEntityRepository::class)
 * @ORM\Table(
 *     indexes={
 *      @Index(name="host_and_entity_search",columns={"host_id","entity_id"})
 *      },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_link_on_date_abuse", columns={"entity_id","host_id", "date"})
 *     })
 */
class AbuseEntity
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Entity
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;

    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost(Host $host): void
    {
        $this->host = $host;
    }

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $date;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $dateTime)
    {
        $this->date = $dateTime;
    }
}