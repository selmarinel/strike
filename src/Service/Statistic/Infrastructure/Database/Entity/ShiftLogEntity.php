<?php

namespace App\Service\Statistic\Infrastructure\Database\Entity;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Class ShiftLogEntity
 * @package App\Service\Statistic\Infrastructure\Database\Scavenger
 * @ORM\Entity(repositoryClass=ShiftLogEntityRepository::class)
 * @ORM\Table(
 *     indexes={
 *      @Index(name="entity_id_host_id_search",columns={"entity_id","host_id_to"}),
 *      },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="unique_link_on_date_abuse", columns={"entity_id", "host_id_to", "date"})
 *     })
 */
class ShiftLogEntity
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var Entity
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $entity;

    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id_from", referencedColumnName="id", onDelete="CASCADE")
     */
    private $hostFrom;

    /**
     * @var Host
     * @ORM\ManyToOne(targetEntity=Host::class)
     * @ORM\JoinColumn(name="host_id_to", referencedColumnName="id", onDelete="CASCADE")
     */
    private $hostTo;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $date;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return Host
     */
    public function getHostTo(): Host
    {
        return $this->hostTo;
    }

    /**
     * @param Host $hostTo
     */
    public function setHostTo(Host $hostTo)
    {
        $this->hostTo = $hostTo;
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Host
     */
    public function getHostFrom(): Host
    {
        return $this->hostFrom;
    }

    /**
     * @param Host $hostFrom
     */
    public function setHostFrom(Host $hostFrom)
    {
        $this->hostFrom = $hostFrom;
    }

    /**
     * @param \DateTimeInterface $date
     */
    public function setDate(\DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }
}