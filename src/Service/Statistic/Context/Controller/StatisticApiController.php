<?php

namespace App\Service\Statistic\Context\Controller;


use App\Service\Statistic\Domain\Service\Scavenger\ScavengerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StatisticApiController extends Controller
{
    /**
     * @param Request $request
     * @param ScavengerInterface $scavengerService
     * @return JsonResponse
     */
    public function parseAction(Request $request, ScavengerInterface $scavengerService)
    {
        try {
            $scavengerService->saveFromRequest($request);
        } catch (\Exception $e) {
            return new JsonResponse('ko', JsonResponse::HTTP_BAD_REQUEST);
        }
        return new JsonResponse('ok', JsonResponse::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param ScavengerInterface $scavengerService
     * @return JsonResponse
     */
    public function searchAction(Request $request, ScavengerInterface $scavengerService)
    {
        return new JsonResponse($scavengerService->search($request), JsonResponse::HTTP_OK);
    }
}