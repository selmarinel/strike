<?php

namespace App\EventDispatcher;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ResponsesExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (getenv('APP_ENV') === 'prod') {
            $event->allowCustomResponseCode();
            $exception = $event->getException();
            if ($exception instanceof NotFoundHttpException) {
                $response = new Response();
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $event->setResponse($response);
            } else {
                $response = new Response();
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $event->setResponse($response);
            }
        }
    }
}