<?php

namespace App\EventDispatcher;


use App\Service\Processor\Infrastructure\Exception\Host\HostRedirectGeoException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class LogExceptionsListener
{
    private $path = '/tmp/strike.exceptions.log';

    public function __construct(ContainerInterface $container)
    {
        $this->path = $container->getParameter('log.dir');
    }

    /**
     * @return string
     */
    private function getPath(): string
    {
        return $this->path;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $date = (new \DateTime())->format("Y-m-d H:i:s");

        $exception = $event->getException();
        $exceptionMessage = $exception->getMessage();
        $exceptionClass = get_class($exception);
        $file = $exception->getFile();
        $line = $exception->getLine();
        $serverName = $_SERVER['HTTP_HOST'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $url = $_SERVER['REQUEST_URI'];

        $prev = ($exception->getPrevious()) ? "Previous {$exception->getPrevious()->getMessage()}\n" : "";

        $data = [
            'date' => $date,
            'serverName' => $serverName,
            'url' => $url,
            'exceptionClass' => $exceptionClass,
            '__file' => $file,
            '__line' => (string)$line,
            'exceptionMessage' => $exceptionMessage,
            'prev' => $prev,
            'userAgent' => $userAgent,
            '__level' => $this->getExceptionLevel($exception)
        ];
        if (getenv('APP_ENV') == 'dev') {
            $this->logToFile($data);
        }
        if (getenv('APP_ENV') == 'prod') {
            $this->logToJson($data);
        }

        unset($exception, $exceptionMessage, $exceptionClass, $file, $line);
    }

    private function logToFile(array $data)
    {
        $message = "\n" .
            "\033[0;34m[{$data['date']}]\033[1;37m Server in {$data['serverName']}\n" .
            "URL [{$data['url']}] \n" .
            "Exception [{$data['exceptionClass']}] \n" .
            "In file {$data['__file']} \n" .
            "On Line {$data['__line']} \n" .
            "Message \"{$data['exceptionMessage']}\"\n" .
            "{$data['prev']}" .
            "\033[0;36m[USER_AGENT]\033[1;37m:{$data['userAgent']}\n".
            "WARNING LEVEL [{$data['__level']}]\n"
        ;

        file_put_contents($this->getPath(), $message, FILE_APPEND);
    }

    public function getExceptionLevel(\Exception $exception)
    {
        if($exception instanceof HostRedirectGeoException){
            return 'LOW';
        }
        return 'MIDDLE';
    }

    private function logToJson(array $data)
    {
        $message = json_encode($data);
        file_put_contents($this->getPath(), $message . "\n", FILE_APPEND);
    }
}