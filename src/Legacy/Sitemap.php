<?php

namespace App\Legacy;

/**
 * Sitemap
 *
 * Helps to build UI elements for the application
 */
class Sitemap
{

    private $compress;
    private $page = 'index';
    private $index = 1;
    private $count = 1;
    public $max_count = 40000;
    private $urls = array();


    private $base_url;
    private $file_path;
    private $site_path;

    /**
     * Sitemap constructor.
     * @param bool $compress
     * @param string $base_url host with schema where url
     * @param string $file_path real path where store sitemaps
     * @param string $site_path where
     */
    public function __construct(string $base_url, string $file_path, $compress = true, $site_path = '/sitemap/')
    {
        $this->base_url = $base_url;
        $this->file_path = $file_path;
        $this->site_path = $site_path;


        if (!is_dir($this->file_path)) {
            mkdir($this->file_path, 0777, true);
        }

        $this->compress = ($compress) ? '.gz' : '';
    }

    public function page($name)
    {
        $this->save();
        $this->page = $name;
        $this->index = 1;
    }

    public function url($url, $lastmod = '', $changefreq = '', $priority = '', $raw_lastmod = false)
    {
        $url = htmlspecialchars($this->base_url . $url);
        if (!$raw_lastmod) {
            $lastmod = (!empty($lastmod)) ? date('Y-m-d', strtotime($lastmod)) : false;
        }
        $changefreq = (!empty($changefreq) && in_array(strtolower($changefreq), array('always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never'))) ? strtolower($changefreq) : false;
        $priority = (!empty($priority) && is_numeric($priority) && abs($priority) <= 1) ? round(abs($priority), 1) : false;
        if (!$lastmod && !$changefreq && !$priority) {
            $this->urls[] = $url;
        } else {
            $url = array('loc' => $url);
            if ($lastmod !== false) $url['lastmod'] = $lastmod;
            if ($changefreq !== false) $url['changefreq'] = $changefreq;
            if ($priority !== false) $url['priority'] = ($priority < 1) ? $priority : '1.0';
            $this->urls[] = $url;
        }
        if ($this->count == $this->max_count) {
            $this->save();
        } else {
            $this->count++;
        }
    }

    public function close()
    {
        $this->save();
        //todo: maybe later
        //$this->ping_search_engines();
    }

    private function save()
    {
        if (empty($this->urls)) return;
        $file = "sitemap-{$this->page}-{$this->index}.xml{$this->compress}";
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
        foreach ($this->urls as $url) {
            $xml .= '  <url>' . "\n";
            if (is_array($url)) {
                foreach ($url as $key => $value) $xml .= "    <{$key}>{$value}</{$key}>\n";
            } else {
                $xml .= "    <loc>{$url}</loc>\n";
            }
            $xml .= '  </url>' . "\n";
        }
        $xml .= '</urlset>' . "\n";
        $this->urls = array();
        if (!empty($this->compress)) $xml = gzencode($xml, 9);
        $fp = fopen($this->file_path . $file, 'wb');
        fwrite($fp, $xml);
        fclose($fp);
        $this->index++;
        $this->count = 1;
        $num = $this->index; // should have already been incremented
        while (file_exists($this->file_path . "sitemap-{$this->page}-{$num}.xml{$this->compress}")) {
            unlink($this->file_path . "sitemap-{$this->page}-{$num}.xml{$this->compress}");
            $num++;
        }
        $this->index($file);
    }

    private $index_list = [];

    private function index($file)
    {
        $sitemaps = &$this->index_list;
        $index = "sitemap-index.xml{$this->compress}";
        if (file_exists($this->file_path . $index)) {
            $xml = (!empty($this->compress)) ? gzfile($this->file_path . $index) : file($this->file_path . $index);
            $tags = $this->xml_tag(implode('', $xml), array('sitemap'));

            foreach ($tags as $xml) {
                $loc = str_replace($this->base_url, '', $this->xml_tag($xml, 'loc'));
                $lastmod = $this->xml_tag($xml, 'lastmod');
                $lastmod = ($lastmod) ? date('Y-m-d', strtotime($lastmod)) : date('Y-m-d');
                if (file_exists($this->file_path . $loc)) $sitemaps[$loc] = $lastmod;
            }
        }
        $sitemaps[$file] = date('Y-m-d');

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
        foreach ($sitemaps as $loc => $lastmod) {
            $xml .= '  <sitemap>' . "\n";
            $xml .= '    <loc>' . $this->base_url . $this->site_path . $loc . '</loc>' . "\n";
            $xml .= '    <lastmod>' . $lastmod . '</lastmod>' . "\n";
            $xml .= '  </sitemap>' . "\n";
        }
        $xml .= '</sitemapindex>' . "\n";
        if (!empty($this->compress)) $xml = gzencode($xml, 9);
        $fp = fopen($this->file_path . $index, 'wb');
        fwrite($fp, $xml);
        fclose($fp);
    }

    private function xml_tag($xml, $tag, &$end = '')
    {
        if (is_array($tag)) {
            $tags = array();
            while ($value = $this->xml_tag($xml, $tag[0], $end)) {
                $tags[] = $value;
                $xml = substr($xml, $end);
            }
            return $tags;
        }
        $pos = strpos($xml, "<{$tag}>");
        if ($pos === false) return false;
        $start = strpos($xml, '>', $pos) + 1;
        $length = strpos($xml, "</{$tag}>", $start) - $start;
        $end = strpos($xml, '>', $start + $length) + 1;
        return ($end !== false) ? substr($xml, $start, $length) : false;
    }

    public function ping_search_engines()
    {
        $sitemap = $this->base_url . $this->site_path . 'sitemap-index.xml' . $this->compress;

        $engines = array();
        $engines['www.google.com'] = '/webmasters/tools/ping?sitemap=' . urlencode($sitemap);
        $engines['www.bing.com'] = '/webmaster/ping.aspx?siteMap=' . urlencode($sitemap);
        foreach ($engines as $host => $path) {
            if ($fp = fsockopen($host, 80)) {
                $send = "HEAD $path HTTP/1.1\r\n";
                $send .= "HOST: $host\r\n";
                $send .= "CONNECTION: Close\r\n\r\n";
                fwrite($fp, $send);
                $http_response = fgets($fp, 128);
                fclose($fp);
                list($response, $code) = explode(' ', $http_response);
                if ($code != 200) trigger_error("{$host} ping was unsuccessful.<br />Code: {$code}<br />Response: {$response}");
            }
        }
    }

    public function __destruct()
    {
        $this->save();
    }

}

?>