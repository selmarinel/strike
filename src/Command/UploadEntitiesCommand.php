<?php

namespace App\Command;


use App\Helper\ConsoleColorsTrait;
use App\Service\Base\Infrastructure\Entity\Tag;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MigrateVO;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\DTO\DTOTagInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadEntitiesCommand extends Command
{
    use ConsoleColorsTrait;

    /** @var TransmitterInterface */
    private $transmitter;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var ReceiveMapInterface */
    private $map;

    public function __construct(
        TransmitterInterface $transmitter,
        EntityManagerInterface $entityManager,
        ReceiveMapInterface $map
    )
    {
        $this->transmitter = $transmitter;
        $this->entityManager = $entityManager;
        $this->map = $map;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('upload:entities')->setAliases(['u:e']);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appendPack($output);

        $limit = 1000;
        $offset = 0;
        $continue = true;
        //movies

        while ($continue) {
            $output->writeln("Start Fetching [{$offset}]");
            $films = $this->transmitter->fetchAllWithLimitAndOffset(Type::MOVIE_FILM, $limit, $offset);
            if (!$films) {
                $continue = false;
                continue;
            }
            $offset += $limit;
            $output->writeln("catch [" . count($films) . "]");

            foreach ($films as $id) {

                $entity = $this->entityManager
                    ->getRepository(Entity::class)
                    ->findOneBy([
                        'entity_type' => Type::MOVIE_FILM,
                        'source_id' => $id
                    ]);

                if ($entity) {
                    unset($entity);
                    continue;
                }
                $this->memory($output);

                $migrateVO = new MigrateVO();
                $migrateVO->setId($id);
                $film = $this->transmitter->fetch(Type::MOVIE_FILM, $migrateVO);
                $output->writeln("Process <comment>{$film['title_ru']}</comment>");

                $entity = $this->saveEntity($id, Type::MOVIE_FILM, $film['title_ru']);

                $this->entityManager->persist($entity);
                unset($migrateVO, $film, $entity);
                gc_collect_cycles();

            }
            $this->entityManager->flush();


            unset($films);
            gc_collect_cycles();
        }

    }

    /**
     * @param $id
     * @param $type
     * @param $name
     * @return Entity
     */
    private function saveEntity($id, $type, $name): Entity
    {
        $entity = new Entity();
        $entity->setEntityType($type);
        $entity->setSourceId($id);
        $entity->setScore(0);
        $entity->setName($name);

        $entityDTOClass = Type::MAP_DTO[$entity->getEntityType()];
        $tagDTOClass = Type::MAP_TAG[$entity->getEntityType()];

        $dto = new $entityDTOClass();
        $map = $this->map->map($dto);

        $client = new Client(['auth' => [$map->getUsername(), $map->getPassword()]]);
        $response = $client->get($map->getHost() . "{$entity->getSourceId()}/genres");
        $response = json_decode($response->getBody()->getContents(), true);
        $genres = $response['records'];

        /** @var DTOTagInterface|DTOInterface $tagDTO */
        $tagDTO = new $tagDTOClass();
        $tagEntities = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(Entity::class, 'e')
            ->where('e.entity_type = :type')
            ->setParameter('type', $tagDTO->getEntityType())
            ->andWhere('e.source_id in (:ids)')
            ->setParameter('ids', $genres)
            ->getQuery()
            ->getResult();

        $tags = $this->entityManager->createQueryBuilder()
            ->select('t')
            ->from(Tag::class, 't')
            ->where('t.entity in (:entities)')
            ->setParameter('entities', $tagEntities)
            ->getQuery()
            ->getResult();

        $entity->setTags($tags);
        $this->saveTree($entity);
        return $entity;
    }

    private function saveTree(Entity $entity)
    {
        $entityNestedTree = new EntityNestedTree();
        $entityNestedTree->setEntity($entity);
        $entityNestedTree->setLevel(0);
        $entityNestedTree->setParent(null);
        $entityNestedTree->setRoot(null);
        $entityNestedTree->setLft(1);
        $entityNestedTree->setRgt(2);
        $this->entityManager->persist($entityNestedTree);
    }

    private function memory(OutputInterface $output)
    {
        $mu = number_format(memory_get_usage(true) / 1000000, 2);
        $output->writeln("use [<green>$mu</green> MB]");
        unset($mu);
    }
}