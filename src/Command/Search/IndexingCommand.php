<?php

namespace App\Command\Search;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Search\Infrastructure\Service\Searcher\Driver\ElasticSearchDriver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IndexingCommand extends Command
{
    /** @var ContainerInterface */
    private $container;
    /** @var EntityToHostRepository */
    private $repository;

    public function __construct(ContainerInterface $container, EntityToHostRepository $entityToHostRepository)
    {
        $this->container = $container;
        $this->repository = $entityToHostRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('app:elastic:indexing')
            ->setAliases(['a:e:i'])
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Count of indexes', 5000);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Start indexing");
        $client = new ElasticSearchDriver();
        $client->connect();

        $limit = $input->getOption('limit') ?? 5000;
        $count = $this->repository->createQueryBuilder('eh')
            ->select('count(eh.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $output->writeln("<comment>Will process {$count} records</comment>");

        $progressBar = new ProgressBar($output, $count);
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
        unset($hosts, $count);

        $offset = 0;
        $ehs = $this->repository->createFindQuery()->setMaxResults($limit)->setFirstResult($offset)->getQuery()->getResult();
        while (!empty($ehs)) {
            $params = [];
            /** @var EntityToHost $eh */
            foreach ($ehs as $eh) {
                $client->prepareRecord($eh, $params);
                $progressBar->advance();
            }
            $client->saveRecords($params);
            unset($params, $ehs);
            $offset = $offset + $limit;
            $ehs = $this->repository->createFindQuery()->setMaxResults($limit)->setFirstResult($offset)->getQuery()->getResult();
        }
        $progressBar->finish();
    }
}