<?php
/**
 * Created by PhpStorm.
 * User: yuriiastahov
 * Date: 26.09.18
 * Time: 14:24
 */

namespace App\Command\Search;


use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class ParallelSearchBindingCommand extends Command
{
    use HostFetcherTrait;

    /** @var HostRepository */
    private $hostRepository;
    /** @var KernelInterface */
    private $kernel;

    const MAX_PROCESSES = 5;
    /** @var  OutputInterface */
    private $out;

    public function __construct(HostRepository $hostRepository, KernelInterface $kernel)
    {
        $this->hostRepository = $hostRepository;
        $this->kernel = $kernel;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('parallel:search:bind')
            ->setAliases(['entity:bind:search', 'e:b:s', 'ebi'])
            ->addArgument('hostType', InputArgument::REQUIRED)
            ->addArgument('entityType', InputArgument::REQUIRED);

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $hostType = $input->getArgument('hostType');
        $entityType = $input->getArgument('entityType');
        $hosts = $this->findOnlyParentsHosts($hostType);
        $limit = 1000;
        $processes = [];
        $processed = 0;
        $this->out = $output;

        foreach ($hosts as $host) {
            $this->startProcess($processes, $processed, $host, $entityType, $limit);
            if ($processed < self::MAX_PROCESSES) {
                continue;
            } else {
                foreach ($processes as $index => $process) {
                    $process->wait();
                    $processed--;
                }
                $processes = [];
                $processed = 0;
                continue;
            }
        }
    }

    private function startProcess(array &$processes, &$processed, Host $host, int &$entityType, int &$limit)
    {
        $this->out->writeln("Start process <info>[{$host->getHostname()}]</info>");
        $processNew = $this->createProcess($host->getId(), $entityType, $limit);
        $processes[] = $processNew;
        $processed += 1;
    }

    private function createProcess(int $hostId, int $entityType, int $limit)
    {
        $dir = $this->kernel->getRootDir() . '/..';
        $process = new Process("{$dir}/bin/console s:b:e {$hostId} {$entityType} --limit={$limit}");
        $process->setTimeout(null);
        $process->start(function ($type, $buffer) {
            $this->out->write($buffer);
        });
        return $process;
    }
}