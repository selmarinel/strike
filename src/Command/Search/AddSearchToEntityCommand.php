<?php

namespace App\Command\Search;


use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\Search\Infrastructure\Service\Hash\HashGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AddSearchToEntityCommand extends Command
{
    /** @var EntityManagerInterface */
    private $manager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->manager = $entityManager;
        $this->manager->getConfiguration()->setSQLLogger(null);

        foreach ($this->manager->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                $this->manager->getEventManager()->removeEventListener($hash, $listener);
            }
        }

        $this->manager->getClassMetadata(EntityToHost::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(EntityNestedTree::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(Host::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(SearchEntity::class)->setLifecycleCallbacks([]);
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('search:bind:entity')
            ->setAliases(['s:b:e'])
            ->addArgument('host', InputArgument::REQUIRED, 'host Id')
            ->addArgument('type', InputArgument::REQUIRED, 'entity Type')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit', 1000)
            ->addOption('again', 'a', InputOption::VALUE_OPTIONAL, 'Parse Again', false);
    }

    const SQL = 'SELECT e.name 
        FROM entity e 
        JOIN entity_nested_tree tree ON tree.entity_id = e.id 
        JOIN entity_nested_tree tree_child ON tree_child.root = tree.id AND tree_child.entity_id = :entityId
        LIMIT 1';

    public function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();
        $limit = $input->getOption('limit') ?? 1000;
        $offset = 0;
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('name', 'name');
        $hostId = $input->getArgument('host');
        $type = $input->getArgument('type');

        $entityToHosts = $this->getEntityToHosts($offset, $limit, $hostId, $type);
        $output->writeln("<comment>Found</comment> [" . count($entityToHosts) . "] on [{$hostId}]");
        /** @var EntityToHost $entityToHost */
        foreach ($entityToHosts as $entityToHost) {
            $parentEntity = $this
                ->manager
                ->createNativeQuery(self::SQL, $rsm)
                ->setParameter('entityId', $entityToHost->getEntity()->getId())
                ->getOneOrNullResult();

            if (!$parentEntity) {
                $this->manager->detach($parentEntity);
                unset($parentEntity);
                gc_collect_cycles();
                continue;
            }
            $searchQuery = $this->generateQueryByType($parentEntity, $entityToHost->getEntity());

            $hash = HashGenerator::simpleGenerate($searchQuery, $entityToHost->getHost()->getId(), $entityToHost->getId());
            $search = $this->manager->createQueryBuilder()
                ->select('s')
                ->from(SearchEntity::class, 's')
                ->where('s.hash = :hash')
                ->setParameter('hash', $hash)
                ->setMaxResults(1)
                ->getQuery()
                ->getScalarResult();
            if ($search) {
                unset($search);
                gc_collect_cycles();
                continue;
            }
            $search = new SearchEntity();
            $search->setHost($entityToHost->getHost());
            $search->setEntityToHost($entityToHost);
            $search->setStatus(SearchEntity::UNPROCESSED);
            $search->setQuery($searchQuery);
            $search->setResults([]);
            $search->setHash($hash);
            $this->manager->persist($search);
            gc_collect_cycles();
        }
        $output->writeln('<info>' . round(memory_get_usage(true) / 1000000) . ' MB use</info>');
        $this->manager->flush();
        $this->manager->clear(SearchEntity::class);
    }

    const TYPES_NEED_PARENT_MAP = [
        Type::AUDIO_TRACK,
        Type::AUDIO_ALBUM,
        Type::DEEZER_AUDIO_TRACK,
        Type::DEEZER_AUDIO_ALBUM,
    ];

    private function generateQueryByType(array $parentEntity, Entity $entity)
    {
        if (in_array($entity->getEntityType(), self::TYPES_NEED_PARENT_MAP)) {
            return $parentEntity['name'] . ' - ' . $entity->getName();
        }
        return $entity->getName();

    }

    private function getEntityToHosts(int &$offset, int &$limit, int &$hostId, int &$type)
    {
        $sql = 'SELECT e0_.id AS id_0 ' .
            'FROM entity_to_host e0_ ' .
            'INNER JOIN host h1_ ON e0_.host_id = h1_.id ' .
            'INNER JOIN entity e2_ ON e0_.entity_id = e2_.id ' .
            'WHERE h1_.id = :hostId AND e2_.entity_type = :type ' .
            'AND ! EXISTS(SELECT * FROM search_entity se WHERE se.entity_host_id = e0_.id) ' .
            'LIMIT :limit';

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_0', 'id_0');

        $collection = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter('hostId', $hostId)
            ->setParameter('type', $type)
            ->setParameter('limit', $limit)
            ->getArrayResult();

        $ids = [];
        foreach ($collection as $item) {
            $ids[] = $item['id_0'];
        }

        return $this->manager->createQueryBuilder()
            ->select('eh,e,h')
            ->from(EntityToHost::class, 'eh')
            ->join('eh.host', 'h')
            ->join('eh.entity', 'e')
            ->where('eh.id in (:ids)')
            ->setParameter('ids', $ids)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

    }
}