<?php
/**
 * Created by PhpStorm.
 * User: selmarinel
 * Date: 26.06.18
 * Time: 14:22
 */

namespace App\Command\FunctionalTestCommand;


use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\Base\Infrastructure\Entity\Metric\Host\MetricHost;
use App\Service\Base\Infrastructure\Entity\Metric\Host\MetricHostLog;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Metric\Host\MetricHostLogRepository;
use App\Service\Base\Infrastructure\Repository\Metric\Host\MetricHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestPagesCommand extends Command
{
    use HostFetcherTrait;

    /** @var UriSlugParserInterface */
    private $parser;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $generator;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var PrepareHostAndEntityInterface */
    private $prepare;
    /** @var MetricHostRepository */
    private $mhr;
    /** @var EntityManagerInterface */
    private $manager;
    /** @var ClientInterface */
    private $client;
    /** @var MetricHostLogRepository */
    private $log;
    /** @var OutputInterface */
    private $output;

    public function __construct(
        HostRepository $hostRepository,
        EntityToHostRepository $entityToHostRepository,
        UriSlugParserInterface $uriSlugParser,
        PrepareHostAndEntityInterface $prepareHostAndEntity,
        EntityLinkGeneratorInterface $generator,

        MetricHostRepository $metricHostRepository,
        MetricHostLogRepository $logRepository,

        ClientInterface $client,

        EntityManagerInterface $manager
    )
    {
        $this->hostRepository = $hostRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->parser = $uriSlugParser;
        $this->prepare = $prepareHostAndEntity;
        $this->generator = $generator;

        $this->mhr = $metricHostRepository;
        $this->log = $logRepository;

        $this->manager = $manager;
        $this->client = $client;

        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('test:functional:pages')
            ->setAliases(['t:f:p'])
            ->addOption("hostname", 'o', InputOption::VALUE_OPTIONAL)
            ->addOption("host_type", 't', InputOption::VALUE_OPTIONAL)
//            ->addOption("enable_entities", 'b', InputOption::VALUE_OPTIONAL)
            ->addOption("abuse_level",
                'a',
                InputOption::VALUE_OPTIONAL,
                'Abuse level (1-4, all)',
                1);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \App\Service\Processor\Infrastructure\Exception\Host\HostSlugException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new OutputFormatterStyle('red', 'yellow', array('bold', 'underscore'));
        $output->getFormatter()->setStyle('fire', $outputStyle);
        $outputStyle = new OutputFormatterStyle('yellow', 'blue', array('bold'));
        $output->getFormatter()->setStyle('kk', $outputStyle);

        $this->output = $output;

        $hostType = $input->getOption('host_type') ?? null;
        $abuseLevel = $input->getOption('abuse_level') ?? null;
        $hostname = $input->getOption('hostname') ?? null;

        if ($abuseLevel == 'all') {
            $abuseLevel = null;
        }
        $hosts = $this->findOnlyParentsHosts($hostType, $hostname);
        $date = new \DateTime();

        foreach ($hosts as $parentHost) {
            $output->writeln("<info>START</info> Parsing children of host <comment>{$parentHost->getHostname()}</comment>");

            foreach ($parentHost->getChildren() as $host) {
                $output->writeln("<info>Process {$host->getHostname()}</info>");

                if ($host->getAbuseLevel() >= 2) {
                    $someDataOnSite = $this->entityToHostRepository->createQueryBuilder('eh')
                        ->join('eh.host', 'h')
                        ->join('eh.entity', 'e')
                        ->where('h.id = :host_id')
                        ->andWhere('e.entity_type NOT IN (:entityTypes)')
                        ->andWhere('h.abuse_level < :abuse_level')
                        ->setParameter('host_id', $host->getId())
                        ->setParameter('entityTypes', [
                            Type::ROOT,
                            Type::COLLECTION_AUDIO_ARTISTS,
                            Type::COLLECTION_AUDIO_TRACKS,
                            Type::COLLECTION_VIDEO_SERIES,
                            Type::COLLECTION_MOVIE_FILMS
                        ])
                        ->setParameter('abuse_level', 2)
                        ->setMaxResults(1)
                        ->getQuery();

                    if (empty($someDataOnSite->getResult())) {
                        $output->writeln("This site is empty. <comment>Skipping...</comment>");
                        continue;
                    }
                }

                $this->go($host, $date);
                $slugData = $this->parser->parseSlug($host->getData());
                $types = self::AvailablePages[$host->getType()];

                foreach ($slugData as $slug) {
                    if (in_array($slug->getSlugType(), $types)) {
                        $this->go($host, $date, $slug->getSlug());
                    }
                }
            }
            $this->manager->flush();
        }
    }


    private function go(
        Host $host,
        \DateTimeInterface $date,
        string $slug = ''
    )
    {
        $hostname = "http://{$host->getHostname()}" . (($slug) ? "/{$slug}" : '');
        $startTime = microtime(true);
        try {
            $response = $this->client->request('GET', $hostname, [
                'headers' => [
                    'User-Agent' => 'googlebot'
                ]
            ]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
        } catch (ConnectException $exception) {
            $response = $exception->getResponse();
        } catch (GuzzleException $exception) {
            $response = '';
        }
        $finishTime = microtime(true);

        $metricHost = $this->mhr->findOneBy([
            'host' => $host,
            'link' => $hostname
        ]);

        if (!$metricHost) {
            $metricHost = new MetricHost();
            $metricHost->setHost($host);
            $metricHost->setLink($hostname);
            $this->manager->persist($metricHost);
        }

        $metricHostLog = $this->log->findOneBy([
            'created' => $date,
            'metricHost' => $metricHost,
        ]);

        if (!$metricHostLog) {
            $metricHostLog = new MetricHostLog();
            $metricHostLog->setMetricHost($metricHost);
            $metricHostLog->setContentLength(($response) ? $response->getBody()->getSize() : 0);
            $metricHostLog->setStatus(($response) ? $response->getStatusCode() : 42);
            $metricHostLog->setCreated($date);
            $metricHostLog->setTime(round(($finishTime - $startTime) * 1000));
            $metricHostLog->setTitle($this->pageTitle(($response) ? $response->getBody()->getContents() : '') ?? '');
            $this->manager->persist($metricHostLog);
        }

        $format = "kk";
        if ($metricHostLog->getStatus() != 200) {
            $format = "fire";
        }
        $this->output->writeln("<comment> slug [/{$slug}]" .
            " status <{$format}>[{$metricHostLog->getStatus()}]</{$format}> " .
            " time [{$metricHostLog->getTime()}] " .
            " size [{$metricHostLog->getContentLength()}]</comment>");
        $this->output->writeln("Uri <info>{$hostname}</info>");
    }

    private function pageTitle($content)
    {
        $matches = [];
        if (preg_match('/<title>(.*?)<\/title>/', $content, $matches)) {
            return $matches[1];
        } else {
            return '';
        }
    }

    const AvailablePages = [
        Type::HOST_TYPE_AUDIO => [
            Type::COLLECTION_AUDIO_ARTISTS,
            Type::COLLECTION_AUDIO_TRACKS,
        ],
        Type::HOST_TYPE_VIDEO => [
            Type::COLLECTION_VIDEO_SERIES,
            Type::COLLECTION_MOVIE_FILMS,
        ]
    ];
}