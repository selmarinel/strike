<?php

namespace App\Command\Cron\Sitemap;
/**
 * @deprecated
 * Class Constructor
 * @package App\Command\Cron\Sitemap
 */
class Constructor extends \GenmediaSitemapGenerator\Constructor
{
    const DATA_PROVIDERS_PATH = __DIR__ . '/';
    const DATA_PROVIDERS_FILE_POSTFIX = 'DataProvider';
    const SITEMAP_ENTITY = 'App\\Command\\Cron\\Sitemap\\Entity';
    //todo: refactor sitemap generator to work with namespace

    protected static $sitemap_items = [
        self::SITEMAP_ENTITY,
    ];

}