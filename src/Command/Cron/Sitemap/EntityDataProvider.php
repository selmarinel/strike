<?php
//todo: refactor sitemap generator to work with namespace
namespace App\Command\Cron\Sitemap;


/**
 * @deprecated
 * Class clips_data_provider
 */
class EntityDataProvider extends \GenmediaSitemapGenerator\AbstractDataProvider
{

    /** @var bool */
    protected $is_index = true;

    /** @var array method for calls */
    protected $other_data_methods = [
    ];

    /** @var array */
    protected $last_mod_update = [
        'get_all_data' => 3, // update every 3 days
    ];
    /**
     * @var array static data from command.. sorry about that
     */
    protected static $data = [];

    /**
     * @return array
     */
    public static function getData(): array
    {
        return self::$data;
    }

    /**
     * @param array $data
     */
    public static function setData(array &$data): void
    {
        self::$data = $data;
    }

    /**
     * @return array
     */
    public function getAllData()
    {

        if (empty(self::getData())){
            return [];
        }

        return $this->formatData(self::getData(), date('Y-m-d'), 'monthly', '0.8');
    }

}