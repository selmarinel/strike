<?php

namespace App\Command\Cron\Sitemap;

use App\Command\CommandTraits\HostFetcherTrait;
use App\Legacy\Sitemap;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostInvalidConfigurationException;
use App\Service\Processor\Infrastructure\Traits\Parser\PrepareDTOTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GenerateSitemap extends Command
{
    use PrepareDTOTrait, HostFetcherTrait;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityLinkGeneratorInterface */
    private $entityLinkGenerator;
    /** @var HostRepository */
    private $hostRepository;

    /** @var ContainerInterface */
    private $container;
    /** @var OutputInterface */
    private $out;
    /** @var PrepareHostAndEntityInterface */
    private $prepareHostAndEntity;
    /** @var \DateTime */
    private $modified;
    /** @var EntityManagerInterface */
    private $em;
    /** @var  int */
    private $limit = 10000;


    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container,
        EntityLinkGeneratorInterface $linkGenerator,
        PrepareHostAndEntityInterface $prepareHostAndEntity
    )
    {
        $this->entityLinkGenerator = $linkGenerator;
        $this->container = $container;
        $this->prepareHostAndEntity = $prepareHostAndEntity;

        $entityManager->getConfiguration()->setSQLLogger(null);

        foreach ($entityManager->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                $entityManager->getEventManager()->removeEventListener($hash, $listener);
            }
        }

        $entityManager->getClassMetadata(EntityToHost::class)->setLifecycleCallbacks([]);
        $entityManager->getClassMetadata(Host::class)->setLifecycleCallbacks([]);

        $this->entityToHostRepository = $entityManager->getRepository(EntityToHost::class);
        $this->hostRepository = $entityManager->getRepository(Host::class);

        $this->modified = $this->setTime();
        $this->em = $entityManager;
        parent::__construct();
    }

    /**
     * @return \DateTime
     */
    private function setTime()
    {
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));
        return $modifiedAt;
    }

    protected function configure()
    {
        $this->setName('cron:generate:sitemaps')
            ->setDescription('Command for generating sitemaps')
            ->setAliases(['c:g:s'])
            ->addOption('host', 'o', InputOption::VALUE_OPTIONAL, '', '')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit of entities', 10000);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        gc_enable();
        $this->limit = $input->getOption('limit') ?? 10000;
        $this->out = $output;
        $this->out->writeln("<info>Start sitemap generation</info>");
        $hostToProcess = $input->getOption('host');
        foreach ($this->findOnlyChildrenHosts() as $host) {
            if ($host->getParent() == null || $host->getAbuseLevel() !== 1) {
                continue;
            }//skip parents
            if ($hostToProcess) {
                if ($hostToProcess !== $host->getHostname()) {
                    continue;
                }
            }
            $this->processHost($host);

            gc_collect_cycles();
        }
    }

    /**
     * @param Host $host
     * @return bool
     * @throws \Exception
     */
    private function processHost(Host $host): bool
    {
        $this->out->writeln("<info>" . sprintf("host-id: %s (%s)", $host->getId(), $host->getHostname()) . "</info>");

        $offset = 0;

        $schema = 'http://';//todo add support for secure site;
        $siteMap = new Sitemap($schema . $host->getHostname(),
            $this->getFolderToSave() . '/' . $host->getParent()->getHostname() . '/' . $host->getHostname() . '/',
            false,
            '/sitemap/'
        );

        while (true) {
            $this->out->writeln("<info>[Processing]</info> | [{$offset}] | [<comment>{$this->getMemoryUsage()} MB</comment>]");
            /** @var @var $entityToHosts EntityToHost[] $entityToHosts */
            try {
                foreach ($this->process($host, $offset) as $data) {
                    $entity = new Entity();
                    $entity->setId($data['entity_id']);
                    $entity->setEntityType($data['entity_type']);
                    $entity->setSourceId('');

                    $entityToHost = new EntityToHost();
                    $entityToHost->setHost($host);
                    $entityToHost->setEntitySlug($data['entity_slug']);
                    $entityToHost->setEntity($entity);

                    $this->prepareHostAndEntity->prepareFromEntityToHost($entityToHost);
                    $hostDAO = $this->prepareHostAndEntity->getHostDAO();
                    $entityDAO = $this->prepareHostAndEntity->getEntityDAO();
                    try {
                        $link = $this->entityLinkGenerator->generateLink($entityDAO, $hostDAO);
                    } catch (HostInvalidConfigurationException $exception) {
                        $this->out->writeln("<error>{$exception->getMessage()}</error>");
                        $this->out->writeln("<info>{$entityDAO->getEntitySlug()}</info>");
                        continue;
                    }
                    $correlate = intval(crc32($host->getHostname()) / 10000000);
                    $modified = clone $this->modified;
                    $modified->setTimestamp($entityToHost->getCreateTs() - $correlate);

                    $siteMap->url(parse_url($link)['path'], $modified->format('Y-m-d\TH:i:s\Z'));

                    unset($link, $hostDAO, $entityDAO, $modified, $entityToHost);
                }
            } catch (\Exception $exception) {
                break;
            }
            $offset += $this->limit;
        }
        $siteMap->close();

        unset ($siteMap);
        gc_collect_cycles();
        return true;
    }

    private function process(Host $host, &$offset)
    {
        $sql = "SELECT e0_.id, " .
            " e0_.entity_slug AS entity_slug, " .
            " h1_.hostname    AS hostname, " .
            " h1_.data        AS data, " .
            " e2_.id          AS entity_id, " .
            " e2_.entity_type AS entity_type " .
            " FROM entity_to_host e0_ " .
            " INNER JOIN entity e2_ ON e0_.entity_id = e2_.id " .
            " INNER JOIN host h1_ ON e0_.host_id = h1_.id " .
            " JOIN ( SELECT ee.id, ee.entity_id FROM entity_to_host ee " .
            " WHERE ee.host_id IN (:hostId,:parentHostId) " .
            "LIMIT {$offset}, {$this->limit}) AS t ON t.id = e0_.id AND t.entity_id = e0_.entity_id";
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('entity_slug', 'entity_slug');
        $rsm->addScalarResult('hostname', 'hostname');
        $rsm->addScalarResult('data', 'data');
        $rsm->addScalarResult('entity_id', 'entity_id');
        $rsm->addScalarResult('entity_type', 'entity_type');
        $entityToHosts = $this->em->createNativeQuery($sql, $rsm)
            ->setParameter('hostId', $host->getId())
            ->setParameter('parentHostId', $host->getParentId())
            ->getScalarResult();
        if (!$entityToHosts) {
            throw new \Exception('empty', 1);
        }
        foreach ($entityToHosts as $entityToHost) {
            yield $entityToHost;
        }
    }

    private function getMemoryUsage()
    {
        $usage = number_format(memory_get_usage() / 1000000, 2);
        return $usage;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getFolderToSave(): string
    {
        $kernel = $this->container->get('kernel');
        $path = realpath($kernel->getRootDir() . '/../') . '/shared/sitemap/';

        if (!file_exists($path) && mkdir($path, 0777, true)) {
            throw new \Exception(sprintf("Cant create dir %s", $path));
        }

        return $path;
    }

}
