<?php

namespace App\Command\Cron\CrossLinks;


use App\Service\Base\Domain\Repository\Seo\EntityCrossLinkRepositoryInterface;
use App\Service\Base\Infrastructure\Entity\Seo\EntityCrossLink;
use App\Service\Base\Infrastructure\Repository\Seo\EntityCrossLinkRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Prophecy\Argument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCrossLinksCommand extends Command
{
    const COUNT = 10;

    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityCrossLinkRepositoryInterface|EntityCrossLinkRepository */
    private $entityCrossLinkRepository;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        EntityCrossLinkRepositoryInterface $entityCrossLinkRepository,
        HostRepository $repository
    )
    {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityCrossLinkRepository = $entityCrossLinkRepository;
        $this->hostRepository = $repository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cron:generate:crosslinks')
            ->setAliases(['c:g:cl'])
            ->setDescription("Command for generating cross links")
            ->addArgument('host', InputArgument::REQUIRED, 'Host id or [all]')
            ->addArgument('type', InputArgument::OPTIONAL, 'Host type');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new OutputFormatterStyle('black', 'white', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $outputStyle);

        $hosts = $this->hostRepository->createQueryBuilder('h')
            ->where('h.parent is null');
        if ($input->getArgument('host') != 'all') {
            $hosts->andWhere('h.id = :id')
                ->setParameter('id', $input->getArgument('host'));
        }
        if ($input->getArgument('type')) {
            $hosts->andWhere('h.type = :type')
                ->setParameter('type', $input->getArgument('type'));
        }
        $hosts = $hosts->getQuery()
            ->getResult();

        if (empty($hosts)) {
            $output->writeln('<error>Host not found</error>');
        }

        $limit = 1000;

        /** @var Host $host */
        foreach ($hosts as $host) {
            $this->memory($output);
            $output->writeln("<info>Process host {$host->getId()} {$host->getHostname()}</info>");
            /** @var EntityToHost[] $entityToHosts */
            $go = true;
            $offset = 0;
            while ($go) {
                $this->memory($output);

                $entityToHosts = $this->entityToHostRepository
                    ->createQueryBuilder('e')
                    ->where('e.host = :host')
                    ->setParameter('host', $host)
                    ->setFirstResult($offset)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult();

                $offset = $offset + $limit;

                if (!empty($entityToHosts)) {
                    $ids = $this->getAllIds($entityToHosts);
                    /** @var EntityToHost $entityToHost */
                    foreach ($entityToHosts as $entityToHost) {
                        $key = $this->generateKey($entityToHost);
                        $entityCross = $this->entityCrossLinkRepository->find($key);

                        if (!$entityCross) {
                            $entityCross = new EntityCrossLink();
                            $entityCross->setId($key);
                        }
                        $entityCross->setLinks([]);

                        for ($i = 1; $i <= self::COUNT; $i++) {
                            $id = $this->getId($ids);
                            $entityCross->appendLink($id);
                            unset($id);
                        }
//                $output->writeln("<info> --- Prepare To save --- </info>");
                        $this->entityCrossLinkRepository->prepareToSave($entityCross);
                        unset($entityCross, $key);
                    }
                    $this->entityCrossLinkRepository->flush();
                    $output->writeln("<fire>Saved {$offset}+ records</fire>");
                } else {
                    $go = false;
                }

                gc_collect_cycles();
                unset($entityToHost, $ids);
            }
            gc_collect_cycles();
        }

    }

    private function memory(OutputInterface $output)
    {
        $usage = memory_get_usage(true) / 1000000;
        $output->writeln("<info>Use {$usage} MB</info>");
        unset($usage);
    }

    private function getId(array $ids)
    {
        $item = array_rand($ids, 1);
        return $ids[$item];
    }

    /**
     * @param EntityToHost[] $entityToHosts
     * @return array
     */
    public function getAllIds(array $entityToHosts)
    {
        $ids = [];
        foreach ($entityToHosts as $entityToHost) {
            $ids[] = $entityToHost->getId();
        }
        return $ids;
    }

    /**
     * @param EntityToHost $entityToHost
     * @return string
     */
    private function generateKey(EntityToHost $entityToHost)
    {
        $hostId = ($entityToHost->getHost()->getParentId()) ?: $entityToHost->getHost()->getId();
        return md5($hostId . $entityToHost->getEntitySlug());
    }
}