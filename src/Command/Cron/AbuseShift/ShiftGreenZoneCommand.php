<?php

namespace App\Command\Cron\AbuseShift;

use App\Service\Statistic\Infrastructure\Service\Shifter\ShifterService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShiftGreenZoneCommand extends AbstractShiftCommand
{
    const ABUSE_LEVEL = 1;

    protected function configure()
    {
        $this->setName('cron:abuse:greenzone')
            ->setAliases(['c:a:gz'])
            ->setDescription('Transfer content on hosts with A*B*U*S*E L*E*V*E*L [1]');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Start shift on [Green Zone]</info>");
        $hosts = $this->hostRepository->findOnlyChildren(self::ABUSE_LEVEL);

        foreach ($hosts as $host) {
            $output->writeln("<comment>Process host {$host->getHostname()}</comment>");
            $output->writeln($this->processHost(ShifterService::GREEN_ZONE, $host));
        }

        $output->writeln("<info>Finish shift on [Green Zone]</info>");
    }

}