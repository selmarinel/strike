<?php

namespace App\Command\Cron\AbuseShift;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Statistic\Domain\Service\ShifterInterface;
use App\Service\Statistic\Infrastructure\Service\Shifter\Exceptions\AbusesIsEmpty;
use Symfony\Component\Console\Command\Command;

abstract class AbstractShiftCommand extends Command
{
    /** @var HostRepository */
    protected $hostRepository;
    /** @var ShifterInterface */
    protected $shifter;

    protected const ABUSE_LEVEL = 0;

    public function __construct(
        HostRepository $hostRepository,
        ShifterInterface $shifter
    ) {
        $this->hostRepository = $hostRepository;
        $this->shifter = $shifter;
        parent::__construct();
    }

    protected function processHost(string $zone, Host $host)
    {
        try {
            $this->shifter->shift($host, $zone);
            if (!empty($result)) {
                return "<info>Complete</info>";
            }
            return "<info>No need to create redirects</info>";

        } catch (AbusesIsEmpty $exception) {
            return "<comment>{$exception->getMessage()}</comment>";
        }

    }
}