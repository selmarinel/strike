<?php

namespace App\Command\Cron\AbuseShift;

use App\Service\Statistic\Infrastructure\Service\Shifter\ShifterService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShiftDangerZoneCommand extends AbstractShiftCommand
{
    const ABUSE_LEVEL = 3;

    protected function configure()
    {
        $this->setName('cron:abuse:dangerzone')
            ->setAliases(['c:a:dz'])
            ->setDescription('Transfer content on hosts with A*B*U*S*E L*E*V*E*L [3]');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Start shift|transfer on [Danger Zone]</info>");
        $hosts = $this->hostRepository->findOnlyChildren(self::ABUSE_LEVEL);

        foreach ($hosts as $host) {
            $output->writeln("<comment>Process host {$host->getHostname()}</comment>");
            $output->writeln($this->processHost(ShifterService::DANGER_ZONE, $host));
            //todo
        }

        $output->writeln("<info>Finish shift|transfer on [Danger Zone]</info>");

    }

}