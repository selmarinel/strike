<?php

namespace App\Command\Cron\AbuseShift;

use App\Service\Statistic\Infrastructure\Service\Shifter\ShifterService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShiftConcernedZoneCommand extends AbstractShiftCommand
{
    const ABUSE_LEVEL = 2;

    protected function configure()
    {
        $this->setName('cron:abuse:yellowzone')
            ->setAliases(['c:a:yz'])
            ->setDescription('Transfer content on hosts with A*B*U*S*E L*E*V*E*L [2]');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Start shift on [Concerned Zone]</info>");
        $hosts = $this->hostRepository->findOnlyChildren(self::ABUSE_LEVEL);

        foreach ($hosts as $host) {
            $output->writeln("<comment>Process host {$host->getHostname()}</comment>");
            $output->writeln($this->processHost(ShifterService::CONCERNED_ZONE, $host));
            //todo
        }

        $output->writeln("<info>Finish shift on [Concerned Zone]</info>");

    }

}