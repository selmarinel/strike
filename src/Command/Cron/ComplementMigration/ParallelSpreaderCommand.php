<?php

namespace App\Command\Cron\ComplementMigration;


use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ParallelSpreaderCommand extends Command
{
    use HostFetcherTrait;

    /** @var HostRepository */
    private $hostRepository;

    /** @var KernelInterface */
    private $kernel;

    public function __construct(KernelInterface $kernel, HostRepository $hostRepository)
    {
        $this->kernel = $kernel;
        $this->hostRepository = $hostRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName("cron:parallel:spreader")
            ->setAliases(['c:p:s'])
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, '', 100);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int)$input->getOption('limit') ?? 100;
        //movies
        $hosts = $this->findOnlyParentsHosts(
            Type::HOST_TYPE_VIDEO,
            'all'
        );
        $processed = 0;

        $toProcess = [];
        $i = 0;
        foreach ($hosts as $host) {
            $i++;
            $toProcess[] = $host;
            if ($i > 10) {
                continue;
            }
        }
        $processes = [];

        foreach ($toProcess as $host) {

            $output->writeln("[Start] to {$host->getHostname()} movies");
            $processNew = $this->createProcess($host->getHostname(), $limit, 'movies');
            $processes[] = $processNew;
            $processed += 1;

            $output->writeln("[Start] to {$host->getHostname()} series");
            $processNew = $this->createProcess($host->getHostname(), $limit, 'series');
            $processes[] = $processNew;

            $processed += 1;

            if ($processed < 6) {
                continue;
            } else {
                foreach ($processes as $index => $process) {
                    $process->wait(function () use (&$processes, &$processed, $index) {
                        unset($processed[$index]);
                        $processed = 0;
                    });
                }
            }
        }
    }

    private function createProcess($hostname, $limit, $type)
    {
        $dir = $this->kernel->getRootDir() . '/..';

        $process = new Process("{$dir}/bin/console c:c:s {$type} {$hostname} --limit={$limit}");
        $process->setIdleTimeout(60 * 10);
        $process->start();
        return $process;
    }
}