<?php

namespace App\Command\Cron\ComplementMigration;


use App\Command\CommandTraits\ContentToTypeTrait;
use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class SynchronyComplementCommand extends Command
{
    use HostFetcherTrait, ContentToTypeTrait;

    /** @var HostRepository */
    private $hostRepository;
    /** @var KernelInterface */
    private $kernel;
    /** @var  OutputInterface */
    private $out;

    public function __construct(KernelInterface $kernel, HostRepository $hostRepository)
    {
        $this->kernel = $kernel;
        $this->hostRepository = $hostRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('cron:synchrony:complement')
            ->setAliases(['c:s:c', 'riot'])
            ->addArgument('content', InputArgument::REQUIRED)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'count of content', 12);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $content = $input->getArgument('content');
        $limit = (int)$input->getOption('limit');
        $this->out = $output;
        $hosts = $this->findOnlyParentsHosts($this->prepareHostType($content));
        foreach ($hosts as $host) {
            $startTime = time();
            $output->writeln("Start Process [<info>{$host->getHostname()}</info>]");
            try {
                $complete = $this->startProcess($content, $host->getHostname(), $limit);
            } catch (\Exception $exception) {
                $complete = false;
            }
            if ($complete) {
                $output->writeln("Process ended with status <comment>SUCCESS</comment>");
            } else {
                $output->writeln("Process ended with status <error>FAIL</error>");
            }
            $finishTime = time();
            $diffTime = $finishTime - $startTime;
            $output->writeln("Process works [<comment>{$diffTime}</comment> seconds]");
            gc_collect_cycles();
            unset($complete, $memoryUsage, $startTime, $diffTime, $finishTime);
        }
    }

    private function startProcess(string $type, string $hostname, int $limit)
    {
        $dir = $this->kernel->getRootDir() . '/..';
        $command = "{$dir}/bin/console c:m:c {$type} {$hostname} --limit={$limit}";
        $process = new Process($command);
        $process->setTimeout(null);
        $process->run();
        if ($process->isSuccessful()) {
            return true;
        }
        return false;
    }
}