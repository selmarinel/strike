<?php

namespace App\Command\Cron\ComplementMigration;


use App\Command\CommandTraits\ContentToTypeTrait;
use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\GenTechMigrationTool\Domain\Service\FetcherService\MigrateFetcherInterface;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Domain\Service\NeedToTimeLogMigrateInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\FatalException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Exceptions\IncorrectDataInServiceException;
use App\Service\GenTechMigrationTool\Infrastructure\Service\FetcherService\VO\FetchVO;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\ArticleMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\ArtistMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\DeezerArtistMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\MovieMigrationService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\VideoMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MigrateVO;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Infrastructure\DTO\Article\Article;
use App\Service\Processor\Infrastructure\DTO\Audio\Artist;
use App\Service\Processor\Infrastructure\DTO\Deezer\DeezerArtistDTO;
use App\Service\Processor\Infrastructure\DTO\Movie\Film;
use App\Service\Processor\Infrastructure\DTO\Video\Series;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ComplementCommand extends Command
{
    use HostFetcherTrait, ContentToTypeTrait;

    /** @var HostRepository */
    private $hostRepository;
    /** @var ContainerInterface */
    private $container;
    /** @var OutputInterface */
    private $output;
    /** @var MigrateFetcherInterface */
    private $migrateFetcher;

    public function __construct(
        ContainerInterface $container,
        HostRepository $hostRepository,
        MigrateFetcherInterface $migrateFetcher)
    {
        $this->hostRepository = $hostRepository;
        $this->container = $container;
        $this->migrateFetcher = $migrateFetcher;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cron:migration:content')
            ->setAliases(['c:m:c'])
            ->setDescription("Commmand for adding new entitites to hosts, this is cron command, which get random ids from API and save to DB")
            ->addArgument('content', InputArgument::REQUIRED)
            ->addArgument('host', InputArgument::REQUIRED)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'count of content', 12)
            ->addOption('exclude', 'x', InputOption::VALUE_OPTIONAL, 'exclude', '')
            ->addOption('back', 'b', InputOption::VALUE_OPTIONAL, 'in back order', 0);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new OutputFormatterStyle('magenta', null, ['bold', 'underscore']);
        $output->getFormatter()->setStyle('host', $outputStyle);

        $this->output = $output;
        $content = $input->getArgument('content');
        $limit = (int)$input->getOption('limit');

        $back = (bool)$input->getOption('back');

        $hosts = $this->findOnlyParentsHosts(
            $this->prepareHostType($content),
            $input->getArgument('host')
        );
        if ($back) {
            $hosts = array_reverse($hosts);
        }
        $excludeHost = $input->getOption('exclude') ?? '';

        foreach ($hosts as $host) {
            $this->output->writeln("<info>[START]</info> <host>Start process {$host->getHostname()}</host>");
            if ($host->getHostname() == $excludeHost) {
                $output->writeln("<comment>Skipping this host");
                continue;
            }
            if ($host->getId() == $excludeHost) {
                $output->writeln("<comment>Skipping this host");
                continue;
            }
            $this->migrateOneHost($host, $content, $limit);
            gc_collect_cycles();
        }
        exit();
    }

    /**
     * @param Host $host
     * @param string $content
     * @param int $limit
     * @throws \Exception
     */
    private function migrateOneHost(Host $host, string $content, int $limit = 12)
    {
        $service = $this->prepareService($content);
        if ($service instanceof NeedToTimeLogMigrateInterface) {
            $service->setOutput($this->output);
        }
        $dto = $this->prepareDTO($content);

        $fetchVO = new FetchVO();
        $fetchVO->setLimit($limit);

        $indexes = $this->migrateFetcher->prepareClient($dto)->fetchIds($dto, $fetchVO);

        foreach ($indexes as $index) {
            $this->memory();
            $migrateVO = new MigrateVO();
            $migrateVO->setHostId($host->getId());

            if (is_array($index)) {
                if (isset($index['id'])) {
                    $index = $index['id'];
                } else {
                    $this->output->writeln('Invalid response');
                    continue;
                }
            }
            $this->output->writeln("Process $index index");
            $migrateVO->setId($index);
            try {
                if ($service instanceof NeedToTimeLogMigrateInterface) {
                    $service->startMeasuring();
                }
                $service->migrateContent($migrateVO);
                if ($service instanceof NeedToTimeLogMigrateInterface) {
                    $service->endMeasuring();
                    $service->measurePeriod();
                }
            } catch (FatalException $exception) {
                $this->output->writeln("<error>Catch Exception {$exception->getMessage()}</error>");
            } catch (IncorrectDataInServiceException $exception) {
                $this->output->writeln("<error> Invalid response data</error>");
            } catch (\Exception $exception) {
                $this->output->writeln("<error>F.A.T.A.L. {$exception->getMessage()}</error>");
            }
            unset($migrateVO);
            gc_collect_cycles();
        }
        unset($dto, $uri, $indexes, $service);
    }

    private function memory()
    {
        $usage = memory_get_usage(true) / 1000000;
        $this->output->writeln("<info>Use {$usage} MB</info>");
        unset($usage);
    }

    /**
     * @param string $content
     * @return DTOInterface
     * @throws \Exception
     */
    private function prepareDTO(string $content)
    {
        switch ($content) {
            case 'series':
                return new Series();
            case 'movies':
                return new Film();
            case 'artists':
                return new Artist();
            case 'deezer':
                return new DeezerArtistDTO();
            case 'article':
                return new Article;
            default:
                throw new \Exception('unsuported content type');
        }
    }

    /**
     * @param string $content
     * @return MigrationInterface
     * @throws \Exception
     */
    private function prepareService(string $content)
    {
        switch ($content) {
            case 'artists':
                return $this->container->get(ArtistMigrateService::class);
            case 'series':
                return $this->container->get(VideoMigrateService::class);
            case 'movies':
                return $this->container->get(MovieMigrationService::class);
            case 'deezer':
                return $this->container->get(DeezerArtistMigrateService::class);
            case 'article':
                return $this->container->get(ArticleMigrateService::class);
            default:
                throw new \Exception('unsuported content type');
        }
    }
}