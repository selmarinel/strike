<?php

namespace App\Command\Cron\ComplementMigration;


use App\Command\CommandTraits\ContentToTypeTrait;
use App\Command\CommandTraits\HostFetcherTrait;
use App\Helper\ConsoleColorsTrait;
use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\DAO\HostDAO;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\GenTechMigrationTool\Domain\Service\Constructor\MigrateConstructorInterface;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SpreaderCommand extends Command
{
    use HostFetcherTrait, ContentToTypeTrait, ConsoleColorsTrait;

    /** @var EntityManagerInterface */
    private $manager;
    /** @var HostRepository */
    private $hostRepository;
    /** @var  OutputInterface */
    private $out;
    /** @var InputInterface */
    private $in;
    /** @var EntitiesRepository */
    private $entityRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityNestedTreeRepository */
    private $tree;
    /** @var EntityHostToEntityHostRepository */
    private $entityToEntityRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        MigrateConstructorInterface $constructor
    )
    {
        $this->manager = $entityManager;
        $this->manager->getConfiguration()->setSQLLogger(null);

        foreach ($this->manager->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                $this->manager->getEventManager()->removeEventListener($hash, $listener);
            }
        }

        $this->manager->getClassMetadata(EntityToHost::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(EntityNestedTree::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(EntityHostToEntityHost::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(Entity::class)->setLifecycleCallbacks([]);
        $this->manager->getClassMetadata(Host::class)->setLifecycleCallbacks([]);

        $this->hostRepository = $this->manager->getRepository(Host::class);
        $this->entityRepository = $this->manager->getRepository(Entity::class);
        $this->entityToHostRepository = $this->manager->getRepository(EntityToHost::class);
        $this->tree = $this->manager->getRepository(EntityNestedTree::class);
        $this->entityToEntityRepository = $this->manager->getRepository(EntityHostToEntityHost::class);
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('cron:complement:spreader')
            ->setAliases(['c:c:s'])
            ->addArgument('content', InputArgument::REQUIRED)
            ->addArgument('host', InputArgument::REQUIRED)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'count of content', 12);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appendPack($output);
        $output->writeln("<green>CRON Started</green>");
        $content = $input->getArgument('content');
        $limit = (int)$input->getOption('limit');
        $this->out = $output;
        $this->in = $input;
        $output->writeln("Start find hosts");
        $hosts = $this->findOnlyParentsHosts(
            $this->prepareHostType($content),
            $input->getArgument('host')
        );
        $entityType = $this->getEntityTypeByContent($input->getArgument('content'));
        $output->writeln("Start count content");
        $counts = $this->getContentCount($entityType);

        try {
            foreach ($hosts as $host) {
                $output->writeln("<green>Process host {$host->getHostname()}</green>");
                if (isset($counts[$host->getId()])) {
                    if ($counts[$host->getId()] > 1) {
                        $output->writeln("<warning>{$host->getHostname()} has too many content. skipping</warning>");
                        continue;
                    }
                }
                $this->processOneHost($host, $entityType, $limit);
                gc_collect_cycles();
            }
            return 0;
        } catch (\Exception $exception) {
            $output->writeln("<warning>{$exception->getMessage()}</warning>");
            return 1;
        }
    }

    private function getEntityTypeByContent(string $content)
    {
        switch ($content) {
            case 'series' :
                return Type::VIDEO_SERIES;
            case 'movies':
                return Type::MOVIE_FILM;
            default:
                throw new \Exception('Invalid content type');
        }
    }

    private function getContentCount(int $entityType)
    {
        $sql = "SELECT COUNT(DISTINCT e.id) AS count, h.id AS host_id, h.parent_id
          FROM entity_nested_tree ent
          JOIN entity e ON e.id = ent.entity_id
          JOIN entity_to_host eth ON eth.entity_id = ent.entity_id
          JOIN host h ON eth.host_id = h.id
          WHERE ent.level = 0
            AND e.entity_type = :entityType
          GROUP BY h.id
          HAVING h.parent_id is null";
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('count', 'count');
        $rsm->addScalarResult('host_id', 'host_id');

        $result = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter('entityType', $entityType)
            ->getResult();
        $countOfResults = count($result);
        $totalContent = 0;
        foreach ($result as $item) {
            $totalContent += $item['count'];
        }
        $average = ($countOfResults) ? $totalContent / $countOfResults : 0;
        $counts = [];
        foreach ($result as $item) {
            $percent = ($average) ? abs(1 - $item['count'] / $average) : 0;
            $counts[$item['host_id']] = $percent;
        }
        return $counts;
    }

    const SQL = "SELECT entity.id AS entity_id
        FROM entity_nested_tree ent0
          JOIN entity ON ent0.entity_id = entity.id
        WHERE ent0.entity_id NOT IN (SELECT e.id
                                     FROM entity_nested_tree ent
                                       JOIN entity e ON e.id = ent.entity_id
                                       JOIN entity_to_host eth ON eth.entity_id = ent.entity_id
                                       JOIN host h ON eth.host_id = h.id
                                     WHERE ent.level = 0
                                           AND h.parent_id IS NULL
                                           AND e.entity_type = :entityType
                                           AND h.id = :hostId)
              AND entity.entity_type = :entityType AND ent0.level = :level ORDER BY RAND() LIMIT :limit";

    private function processOneHost(Host $host, int $entityType, $limit)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('entity_id', 'entity_id');

        $result = $this->manager->createNativeQuery(self::SQL, $rsm)
            ->setParameter('entityType', $entityType)
            ->setParameter('hostId', $host->getId())
            ->setParameter('level', 0)
            ->setParameter('limit', $limit)
            ->getScalarResult();
        unset($rsm);
        $type = $this->getListTypeByContent($this->in->getArgument('content'));
        $p = $this->appendProgressbar($this->out, count($result));
        try {
            $manager = clone $this->manager;
            foreach ($result as $item) {
                $p->advance();
                /** @var EntityNestedTree $treeNode */
                $treeNode = $this->tree->findByEntityId($item['entity_id']);
                $entityHost = $this->saveEntityToHost($treeNode->getEntity(), $host);
                $this->saveToList($entityHost, $manager, $type);
                $manager->persist($entityHost);
                unset($entityHost);
                $children = $this->tree->children($treeNode);
                /** @var EntityNestedTree $child */
                foreach ($children as $child) {
                    $entityHost = $this->saveEntityToHost($child->getEntity(), $host);
                    $manager->persist($entityHost);
                    unset($entityHost);
                }
                unset($item);
            }
            $manager->flush();
            $p->finish();
            $this->out->writeln('');
            unset($p, $manager);

        } catch (\Exception $exception) {
            $this->out->writeln('');
            $this->out->writeln("<warning>{$exception->getMessage()}</warning>");
            sleep(5);
        }
    }

    /**
     * @param Entity $entity
     * @param Host $host
     * @return EntityToHost|null|object
     */
    private function saveEntityToHost(Entity $entity, Host $host)
    {
        $slug = SlugMakerService::makeSlug($entity->getName());

        $entityToHost = $this->entityToHostRepository->findOneBy([
            'entity' => $entity,
            'host' => $host,
            'entity_slug' => $slug
        ]);
        if ($entityToHost) {
            return $entityToHost;
        }
        $entityToHost = new EntityToHost();
        $entityToHost->setHost($host);
        $entityToHost->setEntity($entity);
        $entityToHost->setEntitySlug($slug);
        return $entityToHost;
    }

    private function getListTypeByContent(string $content)
    {
        switch ($content) {
            case 'series' :
                return Type::COLLECTION_VIDEO_SERIES;
            case 'movies':
                return Type::COLLECTION_MOVIE_FILMS;
            default:
                throw new \Exception('Invalid content type');
        }
    }

    private function saveToList(EntityToHost $entityToHost, EntityManagerInterface $entityManager, int &$type)
    {
        $listEntity = $this->entityRepository->findOneBy([
            'entity_type' => $type
        ]);
        if (!$listEntity) {
            $listEntity = new Entity();
            $listEntity->setEntityType($type);
            $listEntity->setSourceId(0);
            $listEntity->setScore(0);
            $this->entityRepository->save($listEntity);
        }

        $mainETH = $this->entityToHostRepository->findOneBy([
            'entity' => $listEntity,
            'host' => $entityToHost->getHost()
        ]);
        if (!$mainETH) {
            $data = $entityToHost->getHost()->getData();
            $slugTypes = $data[HostDAO::SLUG_TYPES];
            $config = [];
            foreach ($slugTypes as $slugType) {
                if ($slugType['slug_type'] == $type) {
                    $config = $slugType;
                }
            }

            $mainETH = new EntityToHost();
            $mainETH->setEntity($listEntity);
            $mainETH->setHost($entityToHost->getHost());
            $mainETH->setEntitySlug($config['pattern']['slug']);
            $this->entityToHostRepository->save($mainETH);
        }

        $entityToEntity = $this->entityToEntityRepository->findOneBy([
            'main' => $mainETH,
            'subordinate' => $entityToHost
        ]);

        if (!$entityToEntity) {
            $entityToEntity = new EntityHostToEntityHost();
            $entityToEntity->setMain($mainETH);
            $entityToEntity->setSubordinate($entityToHost);
            $entityToEntity->setPosition(0);
            $entityToEntity->setType(Type::RELATION_TYPE_LIST);
            $entityManager->persist($entityToEntity);
        }
    }
}