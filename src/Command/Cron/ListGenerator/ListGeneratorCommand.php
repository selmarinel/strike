<?php

namespace App\Command\Cron\ListGenerator;

use App\Command\CommandTraits\HostFetcherTrait;
use App\Service\Administrate\Exception\SaveException;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Repository\EntityToEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ListGeneratorCommand extends Command
{
    use HostFetcherTrait;

    /** @var EntityToEntityRepository */
    private $entityToEntityRepository;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var HostRepository */
    private $hostRepository;
    /** @var  int */
    private $maxResult;


    public function __construct(
        EntitiesRepository $entitiesRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityToEntityRepository $entityToEntityRepository,
        HostRepository $hostRepository
    ) {
        $this->entitiesRepository = $entitiesRepository;
        $this->entityToEntityRepository = $entityToEntityRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->hostRepository = $hostRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cron:generate:list')
            ->setAliases(['c:g:l'])
            ->setDescription("Command for generating lists")
            ->addArgument('entity_type', InputArgument::REQUIRED)
            ->addArgument('host', InputArgument::REQUIRED)
            ->addArgument('max_result', InputArgument::OPTIONAL, 'Results number to set to db', 12);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Start List Generator Command</info>");
        $this->maxResult = $input->getArgument('max_result');

        $type = $input->getArgument('entity_type');
        $host = $input->getArgument('host');

        switch ($type) {
            case 'artists':
                $type = Type::COLLECTION_AUDIO_ARTISTS;
                $typeToFind = Type::AUDIO_ARTIST;
                $hostType = Type::HOST_TYPE_AUDIO;
                break;
            case 'tracks':
                $type = Type::COLLECTION_AUDIO_TRACKS;
                $typeToFind = Type::AUDIO_TRACK;
                $hostType = Type::HOST_TYPE_AUDIO;
                break;
            case 'series':
                $type = Type::COLLECTION_VIDEO_SERIES;
                $typeToFind = Type::VIDEO_SERIES;
                $hostType = Type::HOST_TYPE_VIDEO;
                break;
            case 'movies':
                $type = Type::COLLECTION_MOVIE_FILMS;
                $typeToFind = Type::MOVIE_FILM;
                $hostType = Type::HOST_TYPE_VIDEO;
                break;
            default:
                $output->writeln("<error>Not supported type</error>");
                return;
        }

        $hosts = $this->findOnlyParentsHosts($hostType, $host);

        foreach ($hosts as $host) {
            $this->migrateOneHost($host, $type, $typeToFind, $output);
        }

    }

    /**
     * @param Host $host
     * @param int $type
     * @param int $typeToFind
     * @param OutputInterface $output
     */
    private function migrateOneHost(Host $host, int $type, int $typeToFind, OutputInterface $output)
    {
        $output->writeln("<info>Process {$host->getHostname()}</info>");

        $collection = $this->entityToEntityRepository->getRepository()
            ->createQueryBuilder('e')
            ->join('e.subordinate', 's')
            ->join('s.host', 'h')
            ->join('s.entity', 'entity')
            ->where('h.id = :host_id')
            ->setParameter("host_id", $host->getId())
            ->andWhere('entity.entity_type = :et')
            ->setParameter('et', $typeToFind)
            ->getQuery()
            ->getResult();

        $output->writeln("Already in base " . count($collection) . " relations");
        $ids = [];
        /** @var EntityHostToEntityHost $value */
        foreach ($collection as $value) {
            $ids [] = $value->getSubordinate()->getEntity()->getId();
        }

        $entitiesToHost = $this->entityToHostRepository
            ->createQueryBuilder('eh')
            ->join('eh.entity', 'e')
            ->join('eh.host', 'h')
            ->where('e.entity_type = :et')
            ->andWhere('h.id = :host_id')
            ->orderBy('e.score', 'DESC')
            ->setParameter('et', $typeToFind)
            ->setParameter('host_id', $host->getId());

        if (!empty($ids)) {
            $entitiesToHost->andWhere('e.id not in (:ids)')
                ->setParameter('ids', $ids);
        }
        $entitiesToHost = $entitiesToHost
            ->setMaxResults($this->maxResult)
            ->getQuery()
            ->getResult();

        $output->writeln('Process ' . count($entitiesToHost) . ' entities');

        $entities = [];
        /** @var EntityToHost $entityToHost */
        foreach ($entitiesToHost as $entityToHost) {
            $entities[] = $entityToHost->getEntity();
        }
        try {
            $mainEntity = $this->entitiesRepository->createQueryBuilder('e')
                ->where('e.entity_type = :et')
                ->setParameter('et', $type)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $mainEntity = null;
        }
        if (!$mainEntity) {
            $mainEntity = new Entity();
            $mainEntity->setEntityType($type);
            $mainEntity->setSourceId(0);
            $mainEntity->setScore(0);
            try {
                $this->entitiesRepository->getEntityManager()->persist($mainEntity);
                $this->entitiesRepository->getEntityManager()->flush();
            } catch (ORMException $e) {
                $output->writeln("<error>Error {$e->getMessage()}</error>");
            }
        }

        /** @var Entity $entity */
        foreach ($entities as $entity) {
            try {
                $recourse = $this->entityToEntityRepository->addEntityToEntity(
                    $mainEntity->getId(),
                    $entity->getId(),
                    $host->getId()
                );
                $output->writeln("<info>{$recourse->getSubordinate()->getId()}</info>");
            } catch (ORMException $e) {
                $output->writeln("<error>ORM Error {$e->getMessage()}</error>");
            } catch (SaveException $e) {
                $output->writeln("<error>Save Error {$e->getMessage()}</error>");
            } catch (\Exception $e) {
                $output->writeln("<error>Exception {$e->getMessage()}</error>");
            }
        }
    }
}