<?php

namespace App\Command\Cron\Parser;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ParserTaskProducer extends Command
{
    /** @var ContainerInterface  */
    private $container;
    /** @var HostRepository  */
    private $hostRepository;
    /** @var  OutputInterface */
    private $out;

    public function __construct(ContainerInterface $container, HostRepository $hostRepository)
    {
        $this->container = $container;
        $this->hostRepository = $hostRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cron:lumen:parser')
            ->setDescription('Cron for setting queue to parse Lumen Database')
            ->setAliases(['c:l:p']);


        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;

        foreach ($this->hostRepository->findAll() as $host) {
            /** @var Host $host */
            if ($host->getParent() == null) {
                continue;
            }

            $this->processHost($host);
        }
    }

    private function processHost(Host $host)
    {
        $message = [
            'type' => 'lumen',
            'link' => [
                'term' => $host->getHostname(),
                'per_page' => '40',
                'base_url' => 'https://www.lumendatabase.org/notices/search'
            ],
            'pattern' => $host->getHostname()
        ];

        $this->out->writeln("<info>Sending {$host->getHostname()} to AMPQ for LumenDatabase parse</info>");
        $publisher = $this->container->get('old_sound_rabbit_mq.parsing_producer');
        $publisher->setContentType('application/json')->publish(json_encode($message), 'lumen');
    }
}