<?php
/**
 * Created by PhpStorm.
 * User: vladislavpistun
 * Date: 28.09.18
 * Time: 15:44
 */

namespace App\Command\Fix;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Search\SearchEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FixSearchEntityCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $limit = 10000;

    private $offset = 0;

    protected function configure()
    {
        $this->setName("fix:search_entity");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();

        /** @var EntityManagerInterface $manager */
        $manager = $this->container->get('doctrine.orm.entity_manager');
        $manager->getConfiguration()->setSQLLogger(null);

        while (true) {
            /** @var SearchEntity[] $searchEntities */
            $searchEntities = $manager->getRepository(SearchEntity::class)
                ->createQueryBuilder('s')
                ->setMaxResults($this->limit)
                ->setFirstResult($this->offset)
                ->getQuery()
                ->getResult();

            if (empty($searchEntities)) {
                break;
            }

            foreach ($searchEntities as $searchEntity) {
                $hash = md5(SlugMakerService::makeSlug($searchEntity->getQuery() . $searchEntity->getHost()->getId()));

                $searchEntity->setHash($hash);

                $manager->persist($searchEntity);
            }

            $manager->flush();
            $manager->clear();
            gc_collect_cycles();

            $this->offset = $this->offset + $this->limit;
            dump("Flushing {$this->offset}");
        }

        dump("LAST FLUSH");
        $manager->flush();
        $manager->clear();
        gc_collect_cycles();

        while (true) {
            $result = $manager->getConnection()->fetchAll(
                "SELECT id, hash, COUNT(*) FROM search_entity GROUP BY hash HAVING COUNT(*) > 1"
            );

            if (empty($result)) {
                break;
            }

            $id = [];

            foreach ($result as $row) {
                $id[] = $row['id'];
            }

            $manager->getConnection()->executeQuery("DELETE FROM search_entity WHERE id IN (". implode(',', $id) .")");
        }
    }
}