<?php

namespace App\Command\Fix;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\ParserFactory;
use App\Service\Templator\Domain\TemplateLocalizationInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixHostDataAddTypeCommand extends Command
{
    /** @var HostRepository */
    private $repository;
    /** @var TemplateLocalizationInterface */
    private $localization;

    /**
     * @param HostRepository $hostRepository
     * @param TemplateLocalizationInterface $localization
     */

    public function __construct(HostRepository $hostRepository, TemplateLocalizationInterface $localization)
    {
        $this->repository = $hostRepository;
        $this->localization = $localization;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('fix:add:type')
            ->setDescription('Command for adding new type to host, if type exists - not process this host')
            ->setAliases(['f:a:t'])
            ->addArgument('host_type', InputArgument::REQUIRED)
            ->addArgument('type', InputArgument::REQUIRED)
            ->addOption('remove', 'r', InputOption::VALUE_OPTIONAL, 'remove', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hosts = $this->repository->findBy([
            'type' => (int)$input->getArgument('host_type'),
            'parent' => null
        ]);

        /** @var Host $host */
        $type = $input->getArgument('type');
        foreach ($hosts as $host) {
            $output->writeln("Process host {$host->getHostname()}");
            $data = $host->getData();
            $slugTypes = $data['slug_types'];

            $st = [];
            foreach ($slugTypes as $slugType) {
                $st[] = $slugType['slug_type'];
            }
            //todo
            if (!in_array($type, $st)) {
                $parse = ParserFactory::createTypedParser(false, $type);
                $parse->setSeparator('/');
                $parse->setTypeSlug($this->repository->randomizeSlug($type));
                //todo change to normal functionality
                $parse->setOrder([
                    'slug_type',
                    'entity_slug',
                    'id'
                ]);

                $slugTypes[] = [
                    'id' => rand(42, 424242),
                    SlugTypeDTO::SLUG_TYPE => $type,
                    SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
                    SlugTypeDTO::STATIC_FIELD => false
                ];

                $data['slug_types'] = $slugTypes;
                $host->setData($data);
                try {
                    $this->repository->save($host);
                } catch (OptimisticLockException $e) {
                    $output->writeln("<error>{$e->getMessage()}</error>");
                } catch (ORMException $e) {
                    $output->writeln("<error>{$e->getMessage()}</error>");
                }
            } elseif ((bool)$input->getOption('remove')) {
                //find
                foreach ($data['slug_types'] as $i => $datum){
                    if(isset($datum['slug_type']) && $datum['slug_type'] == $type){
                        $output->writeln('Remove');
                        unset($data['slug_types'][$i]);
                    }
                }
                $host->setData($data);
                $this->repository->save($host);
            }
        }
    }

}