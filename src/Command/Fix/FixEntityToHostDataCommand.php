<?php

namespace App\Command\Fix;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixEntityToHostDataCommand extends Command
{
    /** @var HostRepository $hostRepository */
    private $hostRepository;
    /** @var EntityToHostRepository $entityToHostRepository */
    private $entityToHostRepository;
    /** @var  OutputInterface */
    private $output;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        HostRepository $hostRepository,
        EntityToHostRepository $entityToHostRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->hostRepository = $hostRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('fix:entityToHost:id')
            ->setDescription("Fix entityTo host, add to parent, after complement command")
            ->setAliases(['f:eh:i']);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Host[] $parentHosts */
        $parentHosts = $this->hostRepository->findBy(['parent' => null]);
        $this->output = $output;

        foreach ($parentHosts as $parentHost) {
            $hosts = $parentHost->getChildren();
            $keys = [];
            $output->writeln("<info>Start process. {$parentHost->getHostname()}</info>");

            foreach ($hosts as $host) {
                $output->writeln(" --- Start process http://{$host->getHostname()}");
                $this->fixOneHost($host, $keys);
            }

            $output->writeln("Prepare to save. {$parentHost->getHostname()}");
            $this->entityManager->flush();
            $output->writeln("<comment>Save complete Successfully</comment>");
        }
    }

    private function fixOneHost(Host $host, array &$keys)
    {
        /** @var EntityToHost[] $entityToHosts */
        $entityToHosts = $this->entityToHostRepository->createQueryBuilder('eh')
            ->join('eh.entity', 'e')
            ->where('e.entity_type IN (:types)')
            ->andWhere('eh.host = :host')
            ->setParameter('host', $host)
            ->setParameter('types', [
                Type::AUDIO_TRACK,
                Type::AUDIO_ARTIST,
                Type::AUDIO_ALBUM,
                Type::VIDEO_EPISODE,
                Type::VIDEO_SERIES,
                Type::VIDEO_SEASON,
                Type::MOVIE_FILM,
                Type::AUDIO_GENRE,
                Type::VIDEO_SERIES_GENRE,
                Type::VIDEO_MOVIES_GENRE
            ])->getQuery()->getResult();

        foreach ($entityToHosts as $entityToHost) {
            $parentEntityToHost = $this->entityToHostRepository->findOneBy([
                'entity' => $entityToHost->getEntity(),
                'host' => $host->getParent(),
                'entity_slug' => $entityToHost->getEntitySlug()
            ]);

            if (!$parentEntityToHost) {

                $key = "{$host->getParent()->getId()}-{$entityToHost->getEntity()->getId()}-{$entityToHost->getEntitySlug()}";

                if (!array_key_exists($key, $keys)) {
//                    $this->output->writeln(
//                        "<info>Exists on children {$host->getHostname()} - entity {$entityToHost->getEntity()->getEntityType()},
//                        but misses on parent {$host->getParent()->getHostname()}</info>"
//                    );

                    $parentEntityToHost = new EntityToHost();
                    $parentEntityToHost->setHost($host->getParent());
                    $parentEntityToHost->setEntity($entityToHost->getEntity());
                    $parentEntityToHost->setEntitySlug($entityToHost->getEntitySlug());
                    $this->entityManager->persist($parentEntityToHost);
                    $keys[$key] = true;

                }
            }
        }
    }
}