<?php

namespace App\Command\Fix;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixChildrenHostDataCommand extends Command
{
    /** @var HostRepository */
    private $repository;

    public function __construct(HostRepository $hostRepository)
    {
        $this->repository = $hostRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName("fix:hosts:data")
            ->setAliases(['f:h:d'])
            ->addArgument('host', InputArgument::REQUIRED)
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $input->getArgument('host');

        if ($host == 'all') {
            $query = $this->repository->createQueryBuilder('h')
                ->where('h.parent is null');
            if ($input->getOption('type')) {
                $query->andWhere('h.type = :type')
                    ->setParameter('type', $input->getOption('type'));
            }


            $hostEntities = $query->getQuery()->getResult();
            /** @var Host $hostEntity */
            foreach ($hostEntities as $hostEntity) {
                $output->writeln("<comment>Process parent host {$hostEntity->getHostname()}</comment>");
                $this->processOneHost($hostEntity, $output);
            }
        } else {
            $hostEntity = $this->repository->findById((int)$host);
            $output->writeln("<comment>Process parent host {$hostEntity->getHostname()}</comment>");
            $this->processOneHost($hostEntity, $output);
        }
    }

    /**
     * @param Host $host
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function processOneHost(Host $host, OutputInterface $output)
    {
        $children = $host->getChildren();
        /** @var Host $childHost */
        foreach ($children as $childHost) {
            if ($childHost->getData() == $host->getData()) {
                $output->writeln("<info>Data is similar</info>");
                continue;
            }
            $childHost->setData($host->getData());
            $this->repository->simpleSave($childHost);
        }
    }
}