<?php

namespace App\Command\Fix;


use App\Service\Administrate\Infrastructure\Repository\Host\HostRepository;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Templator\MacrosRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixDeezerMacrosFromAudioMacrosCommand extends Command
{
    /** @var MacrosRepository */
    private $macrosRepository;
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->macrosRepository = $entityManager->getRepository(Macros::class);
        $this->hostRepository = $entityManager->getRepository(Host::class);
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('fix:deezer:audio')
            ->setAliases(['f:d:a']);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = 1000;
        $offset = 0;
        $countOfNew = 0;
        do {
            $output->writeln("Process {$offset}");
            $macros = $this->getMacroses($limit, $offset);
            /** @var Macros $macro */
            $output->writeln('<info>'.memory_get_usage() / 1000000 . '</info> MBytes');
            foreach ($macros as $key => $macro) {

                if (!isset(self::MAP[$macro->getEntityType()])) {
                    continue;
                }
                $type = self::MAP[$macro->getEntityType()];
                $newMacro = $this->macrosRepository->findOneBy([
                    'host' => $macro->getHost(),
                    'entity_type' => $type
                ]);

                if (!$newMacro) {
                    $countOfNew++;
                    $newMacro = new Macros();
                    $newMacro->setHost($macro->getHost());
                    $newMacro->setEntityType($type);
                }

                $newMacro->setSeoData($macro->getSeoData());
                $this->entityManager->persist($newMacro);
//                unset($newMacro, $macro, $macros[$key]);
                gc_collect_cycles();
            }
            $output->writeln("Saving...");
            $this->entityManager->flush();
            $output->writeln("<info>Saved</info>");
            $offset += $limit;
        } while (!empty($macros));
        $output->writeln("Count of new [<comment>{$countOfNew}</comment>]");
    }

    const MAP = [
        Type::AUDIO_ARTIST => Type::DEEZER_AUDIO_ARTIST,
        Type::AUDIO_ALBUM => Type::DEEZER_AUDIO_ALBUM,
        Type::AUDIO_TRACK => Type::DEEZER_AUDIO_TRACK,
        Type::AUDIO_GENRE => Type::DEEZER_AUDIO_GENRE,
    ];

    private function getMacroses(int $limit, int $offset)
    {
        return $this->macrosRepository->createQueryBuilder('m')
            ->join('m.host', 'h')
            ->where('h.type = :type')
            ->setParameter('type', Type::HOST_TYPE_AUDIO)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }
}