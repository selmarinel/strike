<?php

namespace App\Command\Fix;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FixEntityNameCommand extends Command
{
    /** @var EntitiesRepository */
    private $repository;
    /** @var ContainerInterface */
    private $container;

    public function __construct(
        EntitiesRepository $repository,
        ContainerInterface $container
    ) {
        $this->container = $container;
        $this->repository = $repository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('fix:entities:names')
            ->setDescription("Fix entity names, this bug is with artist entity")
            ->setAliases(['f:e:n']);
//
        $this
            ->addArgument('type', InputArgument::OPTIONAL, 'Type', 2)
            ->addArgument('offset', InputArgument::OPTIONAL, 'Offset', 0)
            ->addArgument('limit', InputArgument::OPTIONAL, 'Limit', 1000000);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entities = $this->repository->createQueryBuilder('e')
            ->where('e.entity_type = :ent')
            ->setParameter('ent', $input->getArgument('type'))
            ->setFirstResult($input->getArgument('offset'))
            ->setMaxResults($input->getArgument('limit'))
            ->getQuery()->getResult();

        /** @var Entity $entity */
        foreach ($entities as $entity) {

            $name = preg_replace('%[^A-Za-zА-Яа-я0-9]%', '', $entity->getName());

            $output->writeln("<info>Process entity {$entity->getId()} {$name}</info>");

            $transmitter = $this->container->get('app.service.entity_processor.infrastructure.service.pigeon.transmitter');

            $result = $transmitter->fetchById(
                $entity->getEntityType(),
                $entity->getSourceId()
            );

            if (!$result) {
                $output->writeln("<error>Invalid data in base</error>");
                continue;
            }

            if (!isset($result['name'])) {
                $output->writeln("<comment>No exists name for type {$entity->getEntityType()} </comment>");
                continue;
            }

            if ($entity->getName() != $result['name']) {
                $output->writeln("<comment>Whoops! </comment>");
                $output->writeln("<info>Entity name {$entity->getName()} not equal {$result['name']}</info>");
                $entity->setName($result['name']);
                $this->repository->save($entity);
                continue;
            }

            $output->writeln("<comment>Pass</comment>");
        }
    }
}