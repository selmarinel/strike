<?php

namespace App\Command\Fix;

use App\Service\Base\Infrastructure\Entity\Redirect\Redirect;
use App\Service\Base\Infrastructure\Repository\Redirect\RedirectRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\DAO\Host\GetHostInterface;
use App\Service\Processor\Domain\DAO\Host\SetHostInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\PrepareHostAndEntityInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixDoublesETHCommand extends Command
{
    /** @var EntityLinkGeneratorInterface */
    private $generator;
    /** @var RegistryInterface */
    private $registry;
    /** @var RedirectRepository */
    private $redirect;
    /** @var EntityToHostRepository */
    private $ehRepository;
    /** @var PrepareHostAndEntityInterface */
    private $prepare;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(
        RegistryInterface $registry,
        EntityLinkGeneratorInterface $generator,
        RedirectRepository $redirectRepository,
        EntityToHostRepository $entityToHostRepository,
        PrepareHostAndEntityInterface $prepare,
        HostRepository $hostRepository
    )
    {
        $this->registry = $registry;
        $this->generator = $generator;
        $this->redirect = $redirectRepository;
        $this->ehRepository = $entityToHostRepository;
        $this->prepare = $prepare;
        $this->hostRepository = $hostRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('fix:entity_to_host:slug')
            ->setAliases(['f:eh:s']);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $sql = "SELECT DISTINCT
  eh0.id AS original,
  eh1.id AS duble,
  h.hostname AS hostname,
  eh0.entity_slug AS original_slug,
  eh1.entity_slug AS duble_slug
FROM
  (SELECT
     _eh.id,
     _eh.host_id,
     _eh.entity_id,
     _eh.entity_slug
   FROM entity_to_host _eh
   GROUP BY _eh.host_id, _eh.entity_id)
  eh0
  JOIN host h ON eh0.host_id = h.id
  JOIN entity e ON eh0.entity_id = e.id

  LEFT JOIN entity_to_host eh1 ON eh1.entity_id = eh0.entity_id
                                  AND eh1.host_id = eh0.host_id

                                  AND eh0.entity_slug != eh1.entity_slug
                                  AND eh1.id != eh0.id

WHERE eh1.entity_slug IS NOT NULL";
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('original', 'original');
        $rsm->addScalarResult('duble', 'duble');
        $rsm->addScalarResult('original_slug', 'original_slug');
        $rsm->addScalarResult('duble_slug', 'duble_slug');
        $rsm->addScalarResult('hostname', 'hostname');

        /** @var EntityManager $manager */
        $manager = $this->registry->getManager();
        $collection = $manager->createNativeQuery($sql, $rsm)->getResult();
        foreach ($collection as $model) {
            try {
                /** @var EntityToHost $ehOriginal */
                $ehOriginal = $this->ehRepository->find($model['original']);
                $ehDuble = $this->ehRepository->find($model['duble']);
                $children = $this->hostRepository->findChildren($ehOriginal->getHost()->getId());
                /** @var Host $host */
                foreach ($children as $host) {
                    /** @var SetHostInterface|GetHostInterface $hostDao */
                    $this->prepare->prepareFromEntityToHost($ehOriginal);
                    $hostDao = $this->prepare->getHostDAO();
                    $hostDao->setHostId($host->getId());
                    $hostDao->setHostname($host->getHostname());
                    $originalLink = $this->generator->generateLink($this->prepare->getEntityDAO(), $this->prepare->getHostDAO());
                    $this->prepare->prepareFromEntityToHost($ehOriginal);

                    /** @var EntityToHost $ehDuble */
                    $this->prepare->prepareFromEntityToHost($ehDuble);
                    /** @var SetHostInterface|GetHostInterface $hostDao */
                    $hostDao = $this->prepare->getHostDAO();
                    $hostDao->setHostId($host->getId());
                    $hostDao->setHostname($host->getHostname());

                    $dubleLink = $this->generator->generateLink($this->prepare->getEntityDAO(), $this->prepare->getHostDAO());

                    $output->writeln("<comment>{$dubleLink}</comment> -> <info>{$originalLink}</info>");

                    $redirect = $this->redirect->findBy([
                        'redirectFrom' => $dubleLink,
                        'redirectTo' => $originalLink
                    ]);
                    if ($redirect) {
                        $output->writeln('Redirect already exists');
                        continue;
                    }
                    $redirect = new Redirect();
                    $redirect->setRedirectFrom($dubleLink);
                    $redirect->setRedirectTo($originalLink);
                    $this->redirect->save($redirect);
                    $output->writeln("<comment>Saved</comment>");
                }
            } catch (\Exception $exception) {
                $output->writeln("HOST {$model['hostname']} ||| SLUG {$model['original_slug']}");
                $output->writeln("<error>{$exception->getMessage()}</error>");
            }
        }

    }
}