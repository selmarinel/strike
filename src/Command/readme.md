# CRON COMMANDS
---

**Content migration**

`php bin/console c:m:c {content_type} {host} {limit|12}`

1. content_type - `series`, `movies`, `artists`
2. host - `all` or by id
3. limit - default(`12`)

**Generating cross links**

`php bin/console c:g:c {host} {host_type}`

1. host - `all` or by id
2. host_type - `1` - Audio host, `2` - Video host

**Generating lists**

`php bin/console c:g:l {entity_type} {host} {max_result}`

1. content_type - `series`, `movies`, `artists`, `tracks`
2. host - `all` or by id
3. max_result - default(`12`)

**Sending tasks to rabbitmq to parse lumen database**

`php bin/console c:l:p`

**Generating sitemaps**

`php bin/console c:g:s`

**Abuse processing first level**

`php bin/console c:a:gz`

**Abuse processing second level**

`php bin/console c:a:yz`

**Abuse processing third level**

`php bin/console c:a:dz`

# FIX COMMANDS
---

**Fix entity names**

`php bin/console f:e:n {type|2} {offset|0} {limit|100}`

1. type - default(`2` - artists)
2. offset - default(`0`)
3. limit - default(`100`)

**Add new type to hosts**

`php bin/console f:a:t {host_type} {type}`

1. host_type - `1` - Audio host, `2` - Video host
2. type - content type

# MIGRATION COMMANDS
---

**Migration hosts**

`php bin/console m:h {path_to_csv}`

1. path_to_csv - file_path to csv file with domains data

**Migration macroses**

`php bin/console m:m {path_to_csv} {separator|ę}`

1. path_to_csv - file_path to csv file with macroses data
2. separator - default(`ę`), separator for csv lines

**Migration content**

`php bin/console m:t {content_type} {host_type} {path_to_csv} -l{limit|18} -s{separator|,}`

1. content_type - (`artist` - Audio Artist, `series` - Video Series, `movie` - Movies)
2. host_type - (`1` - [audio] Host, `2` - [video series] Host, `3` - [video movies] Host)
3. path_to_csv - file_path to csv file with macroses data
4. limit - default(`18`)
5. separator - default(`,`), separator for csv lines

**Migration lyrics**

`php bin/console m:l`

**Migration tags**

`php bin/console m:c:t {type}`

1. type - `audio`, `video`, `movie`

**Migration bind tags**

`php bin/console m:b:t {content_type}`

1. content_type - (`1` - track, `2` - artists, `3` - album, `13` - series, `21` - movie)

**Migration create genres pages**

`php bin/console m:t:p {host}`

1. host - `all` or by id

# AUDIO
---

**Create audio domains**

`php bin/console m:h storages/csv/audio/domains.csv `

**Create audio macroses**

`php bin/console m:m storages/csv/audio/seo.csv `

**Fill audio domain with artists content**

`php bin/console m:t artist 1 storages/csv/audio/internal_artists.csv` 

**Create artists list**

`php bin/console c:g:l artists all`

**Create tracks list**

`php bin/console c:g:l tracks all`

**_create audio genres_**

`php bin/console m:c:t audio`

**_bind audio genres to artists_**

`php bin/console m:b:t 2`

**_create genres pages_**

`php bin/console m:t:p all`


> For artist name fix use
> `php bin/console f:e:n 2 0 100000`

# VIDEO
------------

**Create video domains**

`php bin/console m:h storages/csv/video/domains.csv `

**Create macroses for video domains**

`php bin/console m:m storages/csv/video/seo.csv `

**Fill video domains with series**

`php bin/console m:t series 2 storages/csv/video/initial_series.csv` 

**Fill video domains with movies**

`php bin/console m:t movie 2 storages/csv/movie/initial_series.csv`

**Fill series lists**

`php bin/console c:g:l series all`

**Fill movies lists**

`php bin/console c:g:l movies all`

**Create series genres**

`php bin/console m:c:t video`

**Bind video genres to series**

`php bin/console m:b:t 13`

**Create video genres pages** 

`php bin/console m:t:p all`

**Create movies genres**

`php bin/console m:c:t movie`

**Bind movie genres to movies**

`php bin/console m:b:t 21`

**Create movie genres pages** 

`php bin/console m:t:p all`