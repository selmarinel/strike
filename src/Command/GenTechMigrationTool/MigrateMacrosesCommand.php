<?php
/**
 * Created by PhpStorm.
 * User: vladislavpistun
 * Date: 10.05.18
 * Time: 19:08
 */

namespace App\Command\GenTechMigrationTool;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\PattedDecorator\Infrastructure\Database\Repository\MacrosRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateMacrosesCommand extends Command
{
    /** @var MacrosRepository */
    private $macrosRepository;
    /** @var HostRepository */
    private $hostRepository;

    public function __construct(
        MacrosRepository $macrosRepository,
        HostRepository $hostRepository,
        $name = null
    )
    {
        $this->macrosRepository = $macrosRepository;
        $this->hostRepository = $hostRepository;
        parent::__construct($name);
    }

    private const DESCRIPTION = '
 This is an automatic message from A-L-E-R-T-R-A for device B-R-A-I-N-F-*-C-K
==============================================================================
...
- Last modified 30.08.2018/ Modify available arrays/ Add collections and root page
    to available types to create macros
    ';

    protected function configure(): void
    {
        $this
            ->setName('migrate:macroses')
            ->setDescription(self::DESCRIPTION)
            ->addArgument('import_path', InputArgument::REQUIRED, 'Path to csv for import')
            ->addArgument('separator', InputArgument::OPTIONAL, 'Separator', 'ę')
            ->setAliases(['m:m']);
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): void
    {
        $file = $input->getArgument('import_path');
        $fails = [];
        if ($file && file_exists($file)) {
            $output->writeln(sprintf('Now we start parsing file: %s', $file));
            $fiO = new \SplFileObject($file);
            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }
                list($domain, $entityType, $title, $h1, $desc) = explode(
                    $input->getArgument('separator'),
                    $line
                );
                if (!in_array($entityType, array_keys(Type::MAP_DTO + Type::MAP_COLLECTIONS))) {
                    $output->writeln("<error>Not supporting entity_type</error>");
                }

                /** @var Macros $macros */
                $macros = $this->macrosRepository->createQueryBuilder('t')
                    ->join('t.host', 'h')
                    ->where('h.hostname = :host_name')
                    ->andWhere('t.entity_type = :entityType')
                    ->setParameter('host_name', $domain)
                    ->setParameter('entityType', $entityType)
                    ->getQuery()
                    ->getOneOrNullResult();
                if (!$macros) {
                    $output->writeln("<info>Macros not found with name {$domain}</info>");
                    $output->writeln("<comment>Try to create</comment>");

                    $host = $this->hostRepository->findByHostName($domain);
                    if (!$host) {
                        if (!isset($fails[$domain])) {
                            $fails[$domain] = 1;
                        } else {
                            $fails[$domain] += 1;
                        }
                        $output->writeln("<error>Host with name </error><comment>$domain</comment><error> not found</error>");
                        continue;
                    }
                    $macros = new Macros();
                    $macros->setEntityType($entityType);
                    $macros->setHost($host);
                }

                $macros->setSeoData([
                    'seo_h1' => $h1,
                    'seo_description' => $desc,
                    'seo_title' => $title,
//                        'seo_text' => $seoText,
                ]);

                $this->macrosRepository->save($macros);

                $output->writeln("<info>Template found and updated with name {$domain}</info>");
            }
        }
        if (!empty($fails)) {
            foreach ($fails as $fail => $count) {
                $output->writeln("Fail <info>$fail</info> <comment>$count</comment> times");
            }
        }
    }
}