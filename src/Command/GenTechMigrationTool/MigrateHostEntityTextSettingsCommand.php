<?php

namespace App\Command\GenTechMigrationTool;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateHostEntityTextSettingsCommand extends Command
{
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntityManagerInterface  */
    private $_em;

    public function __construct(
        HostRepository $hostRepository,
        EntityManagerInterface $manager,
        $name = null
    ) {
        $this->_em = $manager;
        $this->_em->getConfiguration()->setSQLLogger(null);
        $this->hostRepository = $hostRepository;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setName('migrate:entity_text_settings')
            ->addArgument('import_path', InputArgument::REQUIRED, 'Path to csv for import')
            ->addArgument('separator', InputArgument::OPTIONAL, 'Separator', ',')
            ->setAliases(['m:ets']);
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): void
    {
        $file = $input->getArgument('import_path');
        $fails = [];
        if ($file && file_exists($file)) {
            $output->writeln(sprintf('Now we start parsing file: %s', $file));
            $fiO = new \SplFileObject($file);
            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }
                list($domain, $settings) = explode(
                    $input->getArgument('separator'),
                    $line
                );
                if (empty($settings)) {
                    $output->writeln("<error>Settings not set for $domain</error>");
                    continue;
                }

                /** @var Host $host */
                $host = $this->hostRepository->findOneBy(['hostname' => $domain]);

                if (!$host) {
                    $output->writeln("<error>Host not found $domain</error>");
                    continue;
                }

                $host->setTextGenerateSettings(explode('+', $settings));

                /** @var Host[] $childrenHosts */
                $childrenHosts = $this->hostRepository->createQueryBuilder('h')
                    ->join('h.parent', 'p')
                    ->where('p.id = :parentId')
                    ->setParameter('parentId', $host->getId())
                    ->getQuery()
                    ->getResult();

                foreach ($childrenHosts as $childrenHost) {
                    $output->writeln("<info>Host found and updated {$childrenHost->getHostname()}</info>");
                    $childrenHost->setTextGenerateSettings(explode('+', $settings));
                    $this->_em->persist($childrenHost);
                }

                $this->_em->persist($host);

                $output->writeln("<info>Host found and updated {$domain}</info>");
            }

            $this->_em->flush();
        }
        if (!empty($fails)) {
            foreach ($fails as $fail => $count) {
                $output->writeln("Fail <info>$fail</info> <comment>$count</comment> times");
            }
        }
    }
}