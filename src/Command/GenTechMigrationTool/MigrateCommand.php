<?php

namespace App\Command\GenTechMigrationTool;


use App\Helper\ConsoleColorsTrait;
use App\Service\Base\Infrastructure\Entity\Type;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityProcessor\Infrastructure\VO\Transmitter\TransmitterParametersVO;
use App\Service\GenTechMigrationTool\Domain\Service\MigrationInterface;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\ArtistMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\MovieMigrationService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\Migration\VideoMigrateService;
use App\Service\GenTechMigrationTool\Infrastructure\Service\VO\MigrateVO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

class MigrateCommand extends Command
{
    use ConsoleColorsTrait;

    /** @var HostRepository */
    private $hostRepository;
    /** @var EntitiesRepository */
    private $entityRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var ContainerInterface */
    private $container;

    public function __construct(
        HostRepository $hostRepository,
        EntitiesRepository $entityRepository,
        EntityToHostRepository $entityToHostRepository,
        ContainerInterface $container
    )
    {
        $this->hostRepository = $hostRepository;
        $this->entityRepository = $entityRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('migrate:content:tool')
            ->setDescription('AUTOMATIC Tool for migrate different content to different hosts')
            ->addArgument('content_type', InputArgument::REQUIRED, "Type of content \n" .
                "'artist' - Audio Artist \n" .
                "'series' - Video Series \n" .
                "'movie' - Movies \n" .
                "")
            ->addArgument('host_type', InputArgument::REQUIRED, "Type of host \n" .
                "1 - [audio] Host \n" .
                "2 - [video series] Host \n" .
                "3 - [video movies] Host \n" .
                "")
            ->addArgument('import_path', InputArgument::REQUIRED, 'Path to csv for import')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit', 18)
            ->addOption('separator', 's', InputOption::VALUE_OPTIONAL, 'Separator', ',')
            ->addOption('hostname', null, InputOption::VALUE_OPTIONAL, 'what host want to process')
            ->addOption('use_process', 'up', InputOption::VALUE_OPTIONAL, 'use subprocess', false)
            ->setAliases(['m:t']);
    }

    const AVAILABLE_HOST_TYPES = [
        Type::HOST_TYPE_AUDIO,
        Type::HOST_TYPE_VIDEO,
        Type::HOST_TYPE_MOVIE
    ];

    /**
     * @param string $contentType
     * @return bool|int
     */
    private function getTypeFromContentType(string $contentType)
    {
        switch ($contentType) {
            case 'artist':
                return Type::AUDIO_ARTIST;
            case 'series':
                return Type::VIDEO_SERIES;
            case 'movie':
                return Type::MOVIE_FILM;
            default:
                return false;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appendPack($output);
        if (!$content = $this->processFile($input, $output)) {
            $output->writeln('<error>File not found</error>');
            die();
        }
        if (!$contentType = $this->getTypeFromContentType($input->getArgument('content_type'))) {
            $output->writeln("<error>Unsupported Content Type</error>\n" .
                "<info>Available types are:\n" .
                "'artist' - Audio Artist \n" .
                "'series' - Video Series \n" .
                "'movie' - Movies</info>");
            die();
        }

        $hostType = (int)$input->getArgument('host_type');

        if ($input->getOption('hostname') != null) {
            $hosts = $this->hostRepository->findBy(['hostname' => $input->getOption('hostname')]);
        } elseif (in_array($hostType, self::AVAILABLE_HOST_TYPES)) {
            $hosts = $this->hostRepository->findBy(['type' => $hostType, 'parent' => null]);
        } else {
            $output->writeln("<error>Invalid host Type</error>");
            die();
        }
        /** @var Host $host */
        foreach ($hosts as $host) {
            if ($input->getOption('use_process') !== false) {
                $this->newSubProcess($input, $output, $host->getHostname());
            } else {
                $output->writeln("<green> ▒▒▒ Start process {$host->getHostname()} ▒▒▒ </green>");
                /** @var Host $host */
                $this->migrateOneHost($host, $content, $contentType, $input, $output);
                $output->writeln("<success> ☺ Process complete</success> \n");
            }
        }

        $output->writeln('<info>Success</info>');

    }

    /**
     * @param int $contentType
     * @return MigrationInterface|bool
     */
    private function getMigrateService(int $contentType)
    {
        switch ($contentType) {
            case Type::AUDIO_ARTIST:
                return $this->container->get(ArtistMigrateService::class);
            case Type::VIDEO_SERIES:
                return $this->container->get(VideoMigrateService::class);
            case Type::MOVIE_FILM:
                return $this->container->get(MovieMigrationService::class);
            default:
                return false;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array|bool
     */
    private function processFile(InputInterface $input, OutputInterface $output)
    {
        $content = [];
        $fileToProcess = $input->getArgument('import_path');
        if ($fileToProcess && file_exists($fileToProcess)) {
            $output->writeln(sprintf('Now we start parsing file: %s', $fileToProcess));
            $fiO = new \SplFileObject($fileToProcess);

            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }

                list($series_id) = explode(
                    $input->getOption('separator') ?? ',',
                    $line
                );
                $content[] = $series_id;
            }
            return $content;
        }

        return false;

    }

    /**
     * @param Host $host
     * @param array $content
     * @param int $contentType
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function migrateOneHost(
        Host $host,
        array $content,
        int $contentType,
        InputInterface $input,
        OutputInterface $output
    )
    {
        $count = $this->entityToHostRepository->countByTypeAndHost($contentType, $host);

        if ($count >= (int)$input->getOption('limit')) {
            $output->writeln("<comment>Limit for entities {$host->getHostname()} is full!</comment>");
            unset($count);
            return;
        }

        $countEntityToFetch = (int)$input->getOption('limit') - (int)$count;

        $migrateVO = new MigrateVO();
        $migrateVO->setHostId($host->getId());
        $contentKeysToProcess = array_rand($content, $countEntityToFetch);

        if (!is_array($contentKeysToProcess)) {
            $contentKeysToProcess = [$contentKeysToProcess];
        }

        $progressBar = $this->appendProgressbar($output, count($contentKeysToProcess));
        $transmitterParametersVO = new TransmitterParametersVO();

        if (!$migrateService = $this->getMigrateService($contentType)) {
            $output->writeln("<error>Invalid Content Type</error>");
            return;
        };

        foreach ($contentKeysToProcess as $index => $item) {
            $progressBar->advance();

            if ($entityToHost = $this->entityToHostRepository->findOneBySourceIdAndType($item, $contentType)) {
                $output->writeln("<error>Entity on host already exists</error>");
                unset($content[$item]);
                continue;
            }
            $migrateVO->setId($content[$item]);
            $migrateVO->setTitle($content[$item]);
            try {
                $migrateService->migrateContent($migrateVO);
            } catch (\Exception $e) {

                $output->writeln("<error>Error {$e->getMessage()}</error>");
            }

            unset($content[$item]);
        }

        $progressBar->finish();
        unset($progressBar, $entityToHost, $transmitterParametersVO, $migrateVO, $count, $contentKeysToProcess);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param $hostname
     */
    private function newSubProcess(InputInterface $input, OutputInterface $output, $hostname)
    {

        $p = new Process(array_merge([
            'bin/console'
        ], array_values($input->getArguments()), [
            '--hostname=' . $hostname
        ]));
        $p->start();

        while ($p->isRunning()) {
            $output->write($p->getIncrementalOutput());
            $output->write($p->getIncrementalErrorOutput());
            usleep(10000);
        }
        if ($p->isSuccessful()) {
            $output->writeln($p->getOutput());
        } else {
            $output->writeln(sprintf('<error>%s</error>', ($p->getErrorOutput())));
        }

    }
}