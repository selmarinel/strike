<?php

namespace App\Command\GenTechMigrationTool;

use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Domain\Service\Pigeon\TransmitterInterface;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateLyricsCommand extends Command
{
    /** @var HostRepository */
    private $hostRepository;
    /** @var EntitiesRepository */
    private $entityRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var EntityHostToEntityHostRepository */
    private $entityHostToEntityHostRepository;
    /** @var TransmitterInterface */
    private $transmitter;
    /** @var EntityManagerInterface */
    private $manager;

    public function __construct(
        HostRepository $hostRepository,
        EntitiesRepository $entitiesRepository,
        TransmitterInterface $transmitter,
        EntityToHostRepository $entityToHostRepository,
        EntityHostToEntityHostRepository $entityHostToEntityHostRepository,
        EntityManagerInterface $managerRegistry
    ) {
        $this->hostRepository = $hostRepository;
        $this->entityRepository = $entitiesRepository;
        $this->entityToHostRepository = $entityToHostRepository;
        $this->entityHostToEntityHostRepository = $entityHostToEntityHostRepository;
        $this->transmitter = $transmitter;
        $this->manager = $managerRegistry;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('migrate:lyrics')
            ->setDescription('Command for migrating lyrics')
            ->setAliases(['m:l']);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $tracks = $this->entityRepository->findBy([
            'entity_type' => Type::AUDIO_TRACK
        ]);

        $hosts = $this->hostRepository->createQueryBuilder('h')
            ->where('h.type = :type')
            ->andWhere('h.abuse_level <= :level')
            ->setParameter('type', Type::HOST_TYPE_AUDIO)
            ->setParameter('level', 1)
            ->getQuery()
            ->getResult();

        foreach ($tracks as $track) {
            /** @var $track Entity */
            $output->writeln("<comment>Process entity {$track->getId()} {$track->getName()}</comment>");

            $fetchedData = $this->transmitter->fetchById(
                Type::AUDIO_LYRIC,
                $track->getSourceId()
            );

            if (empty($fetchedData)) {
                continue;
            }

            $entity = $this->entityRepository->findOneBy([
                'entity_type' => Type::AUDIO_LYRIC,
                'source_id' => $track->getSourceId()
            ]);

            if (!$entity) {
                $entity = new Entity();
                $entity->setEntityType(Type::AUDIO_LYRIC);
                $entity->setSourceId($track->getSourceId());
                $entity->setScore(0);
                $entity->setName('Текст песни ' . $track->getName());
                $this->manager->persist($entity);
//                $this->entityRepository->getEntityManager()->persist($entity);
//                $this->entityRepository->getEntityManager()->flush();
            }

            /** @var Host $host */
            foreach ($hosts as $host) {
                $trackEntityToHost = $this->entityToHostRepository->findByHostIdAndEntityId(
                    $host->getId(),
                    (int)$track->getId()
                );

                if (!$trackEntityToHost) {
                    continue;
                }

                $slug = SlugMakerService::makeSlug($track->getName());
                $entityToHost = $this->entityToHostRepository->findOneBy([
                    'host' => $host,
                    'entity' => $entity,
                    'entity_slug' => $slug
                ]);
                if (!$entityToHost) {
                    $entityToHost = new EntityToHost();
                    $entityToHost->setEntitySlug($slug);
                    $entityToHost->setEntity($entity);
                    $entityToHost->setHost($host);
                    $this->manager->persist($entityToHost);
                }

                $entityHostToEntityHost = $this->entityHostToEntityHostRepository->findBy([
                    'main' => $trackEntityToHost,
                    'subordinate' => $entityToHost
                ]);

                if (!$entityHostToEntityHost) {
                    $entityHostToEntityHost = new EntityHostToEntityHost();
                    $entityHostToEntityHost->setMain($trackEntityToHost);
                    $entityHostToEntityHost->setSubordinate($entityToHost);
                    $entityHostToEntityHost->setType(Type::RELATION_TYPE_RELATE);
                    $this->manager->persist($entityHostToEntityHost);
                }

                $output->writeln("<info>Done entity to host for {$host->getHostname()}</info>");

            }
            $output->writeln("<info>Done entity {$track->getId()} {$track->getName()}</info>");
            $this->manager->flush();
        }

        $output->writeln("<info>Success</info>");
    }
}