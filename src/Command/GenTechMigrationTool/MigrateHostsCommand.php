<?php

namespace App\Command\GenTechMigrationTool;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\ParserFactory;
use App\Service\Templator\Domain\TemplateLocalizationInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateHostsCommand extends Command
{
    /** @var HostRepository */
    private $repository;
    /** @var TemplateLocalizationInterface */
    private $localization;

    /**
     * @param HostRepository $hostRepository
     * @param TemplateLocalizationInterface $localization
     */

    public function __construct(HostRepository $hostRepository, TemplateLocalizationInterface $localization)
    {
        $this->repository = $hostRepository;
        $this->localization = $localization;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('migrate:hosts')
            ->setDescription('Command for migartion hosts')
            ->addArgument('import_path', InputArgument::REQUIRED, 'Path to csv for import')
            ->setAliases(['m:h']);
    }

    private function getDataByType($type)
    {
        switch ($type) {
            case Type::HOST_TYPE_AUDIO:
                return $this->getAudioData();
            case Type::HOST_TYPE_VIDEO:
                return $this->getVideoData();
            case Type::HOST_TYPE_MOVIE:
                return $this->getMovieData();
        }
    }

    private function getVideoData()
    {
        $parse = ParserFactory::createParser();
        $slugs = [];
        $parse->setSeparator('/');
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::VIDEO_SERIES));
        $parse->setOrder([
            'slug_type',
            'entity_slug',
            'id'
        ]);

        $slugs[] = [
            'id' => 1,
            SlugTypeDTO::SLUG_TYPE => Type::VIDEO_SERIES,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];

        $parse->setTypeSlug($this->repository->randomizeSlug(Type::MOVIE_FILM));
        $parse->setOrder([
            'slug_type',
            'entity_slug',
            'id'
        ]);

        $slugs[] = [
            'id' => 11,
            SlugTypeDTO::SLUG_TYPE => Type::MOVIE_FILM,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];

        $parse->setTypeSlug($this->repository->randomizeSlug(Type::VIDEO_EPISODE));
        $slugs[] = [
            'id' => 2,
            SlugTypeDTO::SLUG_TYPE => Type::VIDEO_EPISODE,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::VIDEO_SEASON));

        $slugs[] = [
            'id' => 3,
            SlugTypeDTO::SLUG_TYPE => Type::VIDEO_SEASON,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];
        $parse = ParserFactory::createParser(true);
        $parse->setTypeSlug('');
        $slugs[] = [
            'id' => 6,
            SlugTypeDTO::SLUG_TYPE => Type::ROOT,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::COLLECTION_VIDEO_SERIES));
        $slugs[] = [
            'id' => 7,
            SlugTypeDTO::SLUG_TYPE => Type::COLLECTION_VIDEO_SERIES,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::COLLECTION_MOVIE_FILMS));
        $slugs[] = [
            'id' => 2,
            SlugTypeDTO::SLUG_TYPE => Type::COLLECTION_MOVIE_FILMS,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        return [
            'slug_types' => $slugs
        ];
    }

    private function getAudioData()
    {
        $parse = ParserFactory::createParser();
        $slugs = [];
        $parse->setSeparator('/');
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::AUDIO_TRACK));
        $parse->setOrder([
            'slug_type',
            'entity_slug',
            'id'
        ]);

        $slugs[] = [
            'id' => 1,
            SlugTypeDTO::SLUG_TYPE => Type::AUDIO_TRACK,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::AUDIO_ARTIST));
        $slugs[] = [
            'id' => 2,
            SlugTypeDTO::SLUG_TYPE => Type::AUDIO_ARTIST,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];

        $parse->setTypeSlug($this->repository->randomizeSlug(Type::AUDIO_ALBUM));
        $slugs[] = [
            'id' => 3,
            SlugTypeDTO::SLUG_TYPE => Type::AUDIO_ALBUM,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];

        $parse = ParserFactory::createParser(true);
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::COLLECTION_AUDIO_ARTISTS));
        $slugs[] = [
            'id' => 4,
            SlugTypeDTO::SLUG_TYPE => Type::COLLECTION_AUDIO_ARTISTS,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];

        $parse->setTypeSlug($this->repository->randomizeSlug(Type::COLLECTION_AUDIO_TRACKS));
        $slugs[] = [
            'id' => 5,
            SlugTypeDTO::SLUG_TYPE => Type::COLLECTION_AUDIO_TRACKS,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        $parse->setTypeSlug('');
        $slugs[] = [
            'id' => 6,
            SlugTypeDTO::SLUG_TYPE => Type::ROOT,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        return [
            'slug_types' => $slugs
        ];
    }

    private function getMovieData()
    {
        $parse = ParserFactory::createParser();
        $slugs = [];
        $parse->setSeparator('/');
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::MOVIE_FILM));
        $parse->setOrder([
            'slug_type',
            'entity_slug',
            'id'
        ]);

        $slugs[] = [
            'id' => 1,
            SlugTypeDTO::SLUG_TYPE => Type::MOVIE_FILM,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => false
        ];

        $parse = ParserFactory::createParser(true);
        $parse->setTypeSlug($this->repository->randomizeSlug(Type::COLLECTION_MOVIE_FILMS));
        $slugs[] = [
            'id' => 2,
            SlugTypeDTO::SLUG_TYPE => Type::COLLECTION_MOVIE_FILMS,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];

        $parse->setTypeSlug('');
        $slugs[] = [
            'id' => 3,
            SlugTypeDTO::SLUG_TYPE => Type::ROOT,
            SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
            SlugTypeDTO::STATIC_FIELD => true
        ];
        return [
            'slug_types' => $slugs
        ];
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('import_path');

        if ($file && file_exists($file)) {
            $output->writeln(sprintf('Now we start parsing file: %s', $file));
            $fiO = new \SplFileObject($file);

            while (!$fiO->eof()) {
                $line = trim($fiO->fgets());

                if (empty($line)) {
                    continue;
                }
                list($domain, $type, $driver) = explode(',', $line);

                $host = $this->repository->findOneBy(['hostname' => $domain]);
                if (!$host) {
                    $host = new Host();
                    $host->setType((int)$type);
                    $host->setHostname($domain);
                    $host->setAbuseLevel(0);
                    $host->setData($this->getDataByType($type));

                } else {
                    $output->writeln('Host exists ' . $domain);
                }

                $drivers = $this->localization->localize(Type::HOST_TYPE_DRIVER_MAP[$type]);
                //set driver
                if (!$driver) {
                    if (in_array($domain, $drivers)) {
                        $driver = $domain;
                    } else {
                        $key = array_rand($drivers, 1);
                        $driver = $drivers[$key];
                    }
                }
                $host->setDriver($driver);

                try {
                    $this->repository->save($host);
                    $output->writeln("Host $domain created");
                } catch (ORMException $e) {
                    $output->writeln("<error>{$e->getMessage()}</error>");
                    continue;
                }

                $subDomains = ['ru', 'ua', 'kz', 'a', 'b', 'c', 'd', 'e'];

                foreach ($subDomains as $subDomain) {
                    $subHostname = $subDomain . "." . $host->getHostname();
                    $subHost = $this->repository->findOneBy(['hostname' => $subHostname]);
                    if (!$subHost) {
                        $subHost = new Host();
                        $subHost->setType((int)$type);
                        $subHost->setParent($host);
                        $subHost->setHostname($subHostname);
                        $subHost->setDriver($host->getDriver());
                        $subHost->setGeo($this->repository->getGeoFromSubDomain($subDomain));
                        $subHost->setAbuseLevel($this->repository->getAbuseLevelFromSubDomain($subDomain));
                        $subHost->setData($host->getData());
                        try {
                            $this->repository->save($subHost);
                            $output->writeln("Host {$subHost->getHostname()} created");
                        } catch (ORMException $e) {
                            $output->writeln("<error>{$e->getMessage()}</error>");
                            continue;
                        }
                    } else {
                        $output->writeln("Host {$subHost->getHostname()} exists");
                    }

                }
            }
        }
    }
}