<?php

namespace App\Command\Deezer;

use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeezerTrackCacheWarmer extends Command
{
    private const NUM_OF_ATTEMPTS = 3;
    private const URL = "http://95.216.170.185/api/v1/tracks/%d/listen";
    /** @var HostRepository  */
    private $hostRepository;
    /** @var EntityManagerInterface  */
    private $manager;
    /** @var Client  */
    private $client;

    public function __construct(
        HostRepository $hostRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->hostRepository = $hostRepository;
        $this->manager = $entityManager;
        $this->client = new Client();

        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('deezer:track:cache:warmer')
            ->setAliases(['d:t:c:w'])
            ->setDescription('This script warm track cache on deezer')
            ->addArgument('file', InputArgument::REQUIRED)
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit', 1000);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('file');

        if (!file_exists($filePath)) {
            return $output->writeln("<error>File not found</error>");
        }

        $file = new \SplFileObject($filePath);

        while (!$file->eof()) {
            $line = trim($file->fgets());

            if (empty($line)) {
                continue;
            }

            list($domain) = explode(
                ',',
                $line
            );

            $host = $this->hostRepository->findByHostName($domain);

            if (!$host) {
                $output->writeln("Host not found [{$domain}]");
                continue;
            }

            $output->writeln("<info>Processing host: [{$host->getHostname()}]</info>");
            $limit = $input->getOption("limit");
            $offset = 0;

            while (true) {
                /** @var Entity[] $entities */
                $entities = $this->getEntities($host->getId(), $limit, $offset);

                if(empty($entities)) {
                    break;
                }

                foreach ($entities as $entity) {
                    $output->writeln("<comment>Processing entity: [{$entity->getName()}]</comment>");

                    $attempts = 0;
                    do {
                        try {
                            $result = $this->client->request(
                                'GET',
                                sprintf(self::URL, (int)$entity->getSourceId()),
                                ['timeout' => 350]
                            );
                            unset($result);
                        } catch (\Exception $e) {
                            $attempts++;
                            if ($attempts <= 3) {
                                $output->writeln("<error>Restarting process for: [{$entity->getName()}]</error>");
                                sleep(3);
                            } else {
                                var_dump($e->getMessage());
                                $output->writeln("<error>ERROR for: [{$entity->getName()}]</error>");
                            }

                            continue;
                        }

                        break;
                    } while ($attempts <= self::NUM_OF_ATTEMPTS);

                    $this->manager->detach($entity);
                }

                $offset = $offset + $limit;
                $this->manager->clear();
                unset($entities);
            }
        }
    }

    private function getEntities(int $hostId, int $limit, int $offset = 0)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Entity::class, "en");
        $rsm->addFieldResult('en', 'id', 'id');
        $rsm->addFieldResult('en', 'source_id', 'source_id');
        $rsm->addFieldResult('en', 'name', 'name');

        $sql = "SELECT e.id, e.source_id, e.name " .
            " FROM entity e " .
            " INNER JOIN entity_to_host eh ON e.id = eh.entity_id" .
            " WHERE e.entity_type = :type AND eh.host_id = :host" .
            " LIMIT :limit OFFSET :offset";

        $entities = $this->manager->createNativeQuery($sql, $rsm)
            ->setParameter("type", Type::DEEZER_AUDIO_TRACK)
            ->setParameter('host', $hostId)
            ->setParameter('limit', $limit)
            ->setParameter('offset', $offset)
            ->getResult();

        return $entities;
    }
}