<?php

namespace App\Command\CommandTraits;

use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;

/**
 * Trait HostFetcherTrait
 * @package App\Command\CommandTraits
 * @property HostRepository $hostRepository
 */
trait HostFetcherTrait
{
    /**
     * @param int|null $hostType
     * @param string|null $hostIdOrName
     * @param int|null $abuseLevel
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function prepareHostsQuery(int $hostType = null, string $hostIdOrName = null, int $abuseLevel = null)
    {
        $query = $this->hostRepository->createQueryBuilder('h');
        if ($hostType) {
            $query->andWhere('h.type = :host_type')
                ->setParameter('host_type', $hostType);
        }
        if ($hostIdOrName) {
            if (is_numeric($hostIdOrName)) {
                $query->andWhere('h.id = :host_id')
                    ->setParameter('host_id', $hostIdOrName);

            } elseif (is_string($hostIdOrName)) {
                if ($hostIdOrName != 'all') {
                    $query->andWhere('h.hostname = :hostname')
                        ->setParameter('hostname', $hostIdOrName);
                }
            }
        }
        if ($abuseLevel) {
            $query->andWhere('h.abuse_level = :abuse_level')
                ->setParameter('abuse_level', $abuseLevel);
        }
        return $query;
    }

    /**
     * @param int|null $hostType
     * @param string|null $hostIdOrName
     * @param int|null $abuseLevel
     * @return Host[]
     */
    public function findOnlyChildrenHosts(int $hostType = null, string $hostIdOrName = null, int $abuseLevel = null)
    {
        return $this->prepareHostsQuery($hostType, $hostIdOrName, $abuseLevel)
            ->andWhere('h.parent IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int|null $hostType
     * @param string|null $hostIdOrName
     * @param int|null $abuseLevel
     * @return Host[]
     */
    public function findOnlyParentsHosts(int $hostType = null, string $hostIdOrName = null, int $abuseLevel = null)
    {
        return $this->prepareHostsQuery($hostType, $hostIdOrName, $abuseLevel)
            ->andWhere('h.parent IS NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int|null $hostType
     * @param string|null $hostIdOrName
     * @param int|null $abuseLevel
     * @return Host[]
     */
    public function findAllHosts(int $hostType = null, string $hostIdOrName = null, int $abuseLevel = null)
    {
        return $this->prepareHostsQuery($hostType, $hostIdOrName, $abuseLevel)
            ->getQuery()
            ->getResult();
    }
}