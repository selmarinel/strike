<?php

namespace App\Command\CommandTraits;


use App\Service\Base\Infrastructure\Entity\Type;

trait ContentToTypeTrait
{
    /**
     * @param string $contentType
     * @return int
     * @throws \Exception
     */
    public function prepareHostType(string $contentType)
    {
        switch ($contentType) {
            case 'series':
                return Type::HOST_TYPE_VIDEO;
            case 'movies':
                return Type::HOST_TYPE_VIDEO;
            case 'artists':
                return Type::HOST_TYPE_AUDIO;
            case 'deezer':
                return Type::HOST_TYPE_AUDIO;
            case 'article':
                return Type::HOST_TYPE_NEWS;
            default:
                throw new \Exception('unsuported content type');
        }
    }
}