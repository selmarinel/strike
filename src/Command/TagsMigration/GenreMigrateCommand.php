<?php

namespace App\Command\TagsMigration;


use App\Service\Base\Infrastructure\Entity\Tag;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Processor\TagRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Infrastructure\DTO\Audio\Tag\Genre as AudioGenre;
use App\Service\Processor\Infrastructure\DTO\Deezer\Tag\DeezerGenreDTO;
use App\Service\Processor\Infrastructure\DTO\Video\Tag\Genre as VideoGenre;
use App\Service\Processor\Infrastructure\DTO\Movie\Tag\Genre as MovieGenre;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenreMigrateCommand extends Command
{
    /** @var ReceiveMapInterface */
    private $map;
    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var TagRepository */
    private $tagRepository;

    public function __construct(
        ReceiveMapInterface $map,
        EntitiesRepository $entitiesRepository,
        TagRepository $tagRepository
    )
    {
        $this->map = $map;
        $this->tagRepository = $tagRepository;
        $this->entitiesRepository = $entitiesRepository;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('migrate:create:tags')
            ->setAliases(['m:c:t'])
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                "Type of tags\n - audio;\n - video;\n - movie"
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('type')) {
            case 'audio':
                $dto = new AudioGenre();
                $entityType = Type::AUDIO_GENRE;
                break;
            case 'video':
                $dto = new VideoGenre();
                $entityType = Type::VIDEO_SERIES_GENRE;
                break;
            case 'movie':
                $dto = new MovieGenre();
                $entityType = Type::VIDEO_MOVIES_GENRE;
                break;
            case 'deezer':
                $dto = new DeezerGenreDTO();
                $entityType = Type::DEEZER_AUDIO_GENRE;
                break;
            default:
                throw new \Exception('Not supported type');
        }

        $map = $this->map->map($dto);
        $guzzle = new Client(['auth' => [$map->getUsername(), $map->getPassword()]]);
        $response = $guzzle->get($map->getHost() . "all");
        $response = json_decode($response->getBody()->getContents(), true);
        if (isset($response['records'])) {
            foreach ($response['records'] as $oneTag) {
                $output->writeln("<info>Begin process {$oneTag['id']}</info>");
                $entity = $this->entitiesRepository->findOneBy([
                    'source_id' => $oneTag['id'],
                    'entity_type' => $entityType
                ]);
                if (!$entity) {
                    $output->writeln("Try to create new tag entity");

                    $name = $oneTag['name'];

                    if (!$name && isset($oneTag['name_ru'])) {
                        $name = $oneTag['name_ru'];
                    }

                    if (!$name) {
                        $output->writeln("<error>Genre does not have name</error>");
                        continue;
                    }
                    $output->writeln("<comment>Create new tag entity [{$name}]</comment>");

                    $entity = new Entity();
                    $entity->setName($name);
                    $entity->setEntityType($entityType);
                    $entity->setSourceId($oneTag['id']);
                    $entity->setScore(0);
                    $this->entitiesRepository->save($entity);
                }
                $output->writeln("Tag entity [{$entity->getName()}] created");

                $tag = $this->tagRepository->findOneBy([
                    'entity' => $entity
                ]);

                if (!$tag) {
                    $output->writeln("Try to create new Tag");
                    $tag = new Tag();
                    $tag->setName($entity->getName());
                    $tag->setEntity($entity);
                    $this->tagRepository->save($tag);
                }
                $output->writeln("Tag [{$tag->getName()}] created");
            }
        }

    }
}