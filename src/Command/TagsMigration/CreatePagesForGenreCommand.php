<?php

namespace App\Command\TagsMigration;


use App\Helper\SlugMakerService;
use App\Service\Base\Infrastructure\Entity\Tag;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Processor\TagRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\EntityProcessor\Infrastructure\Service\UriRunner\ParserFactory;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreatePagesForGenreCommand extends Command
{
    /** @var TagRepository */
    private $tagRepository;
    /** @var EntityToHostRepository */
    private $entityToHostRepository;
    /** @var HostRepository */
    private $hostRepository;
    /** @var UriSlugParserInterface */
    private $parser;

    public function __construct(
        EntityToHostRepository $entityToHostRepository,
        TagRepository $tagRepository,
        HostRepository $hostRepository,
        UriSlugParserInterface $parser
    ) {
        $this->entityToHostRepository = $entityToHostRepository;
        $this->tagRepository = $tagRepository;
        $this->hostRepository = $hostRepository;
        $this->parser = $parser;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('migrate:tags:pages')
            ->setAliases(['m:t:p'])
            ->addArgument('host', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('host') == 'all') {
            $hosts = $this->hostRepository->findBy(['parent' => null]);
            /** @var Host $host */
            foreach ($hosts as $host) {
                $this->processOneHost($host, $output);
            }
        } else {
            $host = $this->hostRepository->findById((int)$input->getArgument('host'));
            $this->processOneHost($host, $output);
        }
    }

    /**
     * @param Host $host
     * @param OutputInterface $output
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function processOneHost(Host $host, OutputInterface $output)
    {
        $output->writeln("Process host {$host->getHostname()}");

        $collection = $this->entityToHostRepository->createQueryBuilder('eh')
            ->select('eh')
            ->join('eh.entity', 'e')
            ->join('e.tags', 't')
            ->where('eh.host = :host')
            ->setParameter('host', $host)
            ->getQuery()
            ->getResult();

        if (empty($collection) && $host->getParentId()) {
            $collection = $this->entityToHostRepository->createQueryBuilder('eh')
                ->select('eh')
                ->join('eh.entity', 'e')
                ->join('e.tags', 't')
                ->where('eh.host = :host')
                ->setParameter('host', $host->getParent())
                ->getQuery()
                ->getResult();
        }

        /** @var EntityToHost $entityToHost */
        $tags = [];

        foreach ($collection as $entityToHost) {
            $a = $entityToHost->getEntity()->getTags()->map(function (Tag $tag) {
                return $tag;
            });

            /** @var Tag $tag */
            foreach ($a as $tag) {
                $tags[$tag->getId()] = $tag;
            }
        }

        foreach ($tags as $tag) {
            $output->writeln("<comment>Prepare to save {$tag->getName()}</comment>");
            $entityToHost = $this->entityToHostRepository->findOneBy([
                'entity' => $tag->getEntity(),
                'host' => $host
            ]);
            if (!$entityToHost) {
                $output->writeln("<info>S*A*V*I*N*G {$tag->getName()}</info>");
                $entityToHost = new EntityToHost();
                $entityToHost->setEntity($tag->getEntity());
                $entityToHost->setHost($host);
                $entityToHost->setEntitySlug(SlugMakerService::makeSlug($tag->getName()));
                $this->entityToHostRepository->save($entityToHost);
            }
            $output->writeln("Save complete {$entityToHost->getId()}");
        }

        $data = $host->getData();
        //append information for this page for host
        $slugTypes = $data['slug_types'];
        $st = [];
        foreach ($slugTypes as $slugType) {
            $st[] = $slugType['slug_type'];
        }
        $this->addConfigToHost($host, $st, $slugTypes);
    }

    /**
     * @param Host $host
     * @param array $slugTypeNames
     * @param array $slugTypes
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addConfigToHost(Host $host, array $slugTypeNames, array $slugTypes)
    {
        if ($host->getType() == Type::HOST_TYPE_AUDIO) {
            if (!in_array(Type::AUDIO_GENRE, $slugTypeNames)) {
                $parse = ParserFactory::createParser();
                $parse->setSeparator('/');
                $parse->setTypeSlug($this->hostRepository->randomizeSlug(Type::AUDIO_GENRE)); //todo
                $parse->setOrder([
                    'slug_type',
                    'entity_slug',
                    'id'
                ]);

                $slugTypes[] = [
                    'id' => 42,
                    SlugTypeDTO::SLUG_TYPE => Type::AUDIO_GENRE,
                    SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
                    SlugTypeDTO::STATIC_FIELD => false
                ];
            }
        }

        if ($host->getType() == Type::HOST_TYPE_VIDEO) {
            if (!in_array(Type::VIDEO_SERIES_GENRE, $slugTypeNames)) {
                $parse = ParserFactory::createParser();
                $parse->setSeparator('/');
                $parse->setTypeSlug($this->hostRepository->randomizeSlug(Type::VIDEO_SERIES_GENRE)); //todo
                $parse->setOrder([
                    'slug_type',
                    'entity_slug',
                    'id'
                ]);

                $slugTypes[] = [
                    'id' => 43,
                    SlugTypeDTO::SLUG_TYPE => Type::VIDEO_SERIES_GENRE,
                    SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
                    SlugTypeDTO::STATIC_FIELD => false
                ];
            }
            if (!in_array(Type::VIDEO_MOVIES_GENRE, $slugTypeNames)) {
                $parse = ParserFactory::createParser();
                $parse->setSeparator('/');
                $parse->setTypeSlug($this->hostRepository->randomizeSlug(Type::VIDEO_MOVIES_GENRE)); //todo
                $parse->setOrder([
                    'slug_type',
                    'entity_slug',
                    'id'
                ]);

                $slugTypes[] = [
                    'id' => 44,
                    SlugTypeDTO::SLUG_TYPE => Type::VIDEO_MOVIES_GENRE,
                    SlugTypeDTO::PATTERN_FIELD => $parse->getPatternArray(),
                    SlugTypeDTO::STATIC_FIELD => false
                ];
            }
        }
        $data['slug_types'] = $slugTypes;
        $host->setData($data);
        $this->hostRepository->save($host);
    }
}