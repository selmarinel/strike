<?php

namespace App\Command\TagsMigration;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Base\Infrastructure\Repository\Processor\TagRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntitiesRepository;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Infrastructure\DTO\Audio\Album;
use App\Service\Processor\Infrastructure\DTO\Audio\Artist;
use App\Service\Processor\Infrastructure\DTO\Audio\Track;
use App\Service\Processor\Infrastructure\DTO\Deezer\DeezerArtistDTO;
use App\Service\Processor\Infrastructure\DTO\Movie\Film;
use App\Service\Processor\Infrastructure\DTO\Video\Series;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BindEntityToTagCommand extends Command
{

    /** @var EntitiesRepository */
    private $entitiesRepository;
    /** @var TagRepository */
    private $tagRepository;
    /** @var ReceiveMapInterface */
    private $map;

    public function __construct(
        ReceiveMapInterface $map,
        EntitiesRepository $entitiesRepository,
        TagRepository $tagRepository
    ) {
        $this->map = $map;
        $this->entitiesRepository = $entitiesRepository;
        $this->tagRepository = $tagRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('migrate:bind:tags')
            ->setAliases(['m:b:t'])
            ->addArgument('type', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $type = (int)$input->getArgument('type');

        switch ($type) {
            case Type::AUDIO_TRACK:
                $entity = new Track();
                $tagType = Type::AUDIO_GENRE;
                break;
            case Type::AUDIO_ARTIST:
                $entity = new Artist();
                $tagType = Type::AUDIO_GENRE;
                break;
            case Type::AUDIO_ALBUM:
                $entity = new Album();
                $tagType = Type::AUDIO_GENRE;
                break;
            case Type::VIDEO_SERIES:
                $entity = new Series();
                $tagType = Type::VIDEO_SERIES_GENRE;
                break;
            case Type::MOVIE_FILM:
                $entity = new Film();
                $tagType = Type::VIDEO_MOVIES_GENRE;
                break;
            default:
                throw new \Exception("not supported content type");
        }

        $entities = $this->entitiesRepository->findBy([
            'entity_type' => (int)$type
        ]);
        $map = $this->map->map($entity);
        $client = new Client(['auth' => [$map->getUsername(), $map->getPassword()]]);
        /** @var Entity $entity */
        foreach ($entities as $entity) {
            $output->writeln("Process {$entity->getSourceId()} {$entity->getName()}");
            $response = $client->get($map->getHost() . "{$entity->getSourceId()}/genres");
            $response = json_decode($response->getBody()->getContents(), true);
            $genres = $response['records'];
            $tagEntities = $this->entitiesRepository->createQueryBuilder('e')
                ->where('e.entity_type = :type')
                ->setParameter('type', $tagType)
                ->andWhere('e.source_id in (:ids)')
                ->setParameter('ids', $genres)
                ->getQuery()
                ->getResult();
            $tags = $this->tagRepository->createQueryBuilder('t')
                ->where('t.entity in (:entities)')
                ->setParameter('entities', $tagEntities)
                ->getQuery()
                ->getResult();

            $entity->setTags($tags);
            $this->entitiesRepository->save($entity);
        }
    }
}