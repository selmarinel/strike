<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181101153017 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX type_idx ON entity (entity_type)');
        $this->addSql('CREATE INDEX type_idx ON entity_host_to_entity_host (type)');
        $this->addSql('CREATE INDEX position_idx ON entity_host_to_entity_host (position)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX type_idx ON entity');
        $this->addSql('DROP INDEX type_idx ON entity_host_to_entity_host');
        $this->addSql('DROP INDEX position_idx ON entity_host_to_entity_host');
    }
}
