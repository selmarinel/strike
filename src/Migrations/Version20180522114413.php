<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180522114413 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_cross_link DROP FOREIGN KEY FK_C1F73739A38EFE26');
        $this->addSql('DROP INDEX IDX_C1F73739A38EFE26 ON entity_cross_link');
        $this->addSql('ALTER TABLE entity_cross_link DROP entity_to_host_id, CHANGE id id VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_cross_link ADD entity_to_host_id BIGINT UNSIGNED DEFAULT NULL, CHANGE id id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE entity_cross_link ADD CONSTRAINT FK_C1F73739A38EFE26 FOREIGN KEY (entity_to_host_id) REFERENCES entity_to_host (id)');
        $this->addSql('CREATE INDEX IDX_C1F73739A38EFE26 ON entity_cross_link (entity_to_host_id)');
    }
}
