<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180419140739 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX widget_name_unique ON widgets');
        $this->addSql('ALTER TABLE widgets ADD host_id INT DEFAULT NULL, ADD entity_type INT NOT NULL');
        $this->addSql('ALTER TABLE widgets ADD CONSTRAINT FK_9D58E4C11FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9D58E4C11FB8D185 ON widgets (host_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_entity_type_host_name ON widgets (entity_type, host_id, name)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE widgets DROP FOREIGN KEY FK_9D58E4C11FB8D185');
        $this->addSql('DROP INDEX IDX_9D58E4C11FB8D185 ON widgets');
        $this->addSql('DROP INDEX unique_entity_type_host_name ON widgets');
        $this->addSql('ALTER TABLE widgets DROP host_id, DROP entity_type');
        $this->addSql('CREATE UNIQUE INDEX widget_name_unique ON widgets (name)');
    }
}
