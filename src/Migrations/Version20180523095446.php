<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180523095446 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE host_has_widgets DROP FOREIGN KEY FK_83DDA7E41FB8D185');
        $this->addSql('DROP TABLE host_has_widgets');
        $this->addSql('DROP TABLE widgets');
        $this->addSql('ALTER TABLE macros RENAME INDEX idx_6f287d8e1fb8d185 TO IDX_F47550E71FB8D185');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE host_has_widgets (host_id BIGINT UNSIGNED NOT NULL, widget_id INT NOT NULL, INDEX IDX_83DDA7E41FB8D185 (host_id), INDEX IDX_83DDA7E4FBE885E2 (widget_id), PRIMARY KEY(host_id, widget_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE widgets (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, twig_path VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, entity_type INT NOT NULL, UNIQUE INDEX unique_entity_type (entity_type, name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE host_has_widgets ADD CONSTRAINT FK_83DDA7E41FB8D185 FOREIGN KEY (host_id) REFERENCES widgets (id)');
        $this->addSql('ALTER TABLE host_has_widgets ADD CONSTRAINT FK_83DDA7E4FBE885E2 FOREIGN KEY (widget_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE macros RENAME INDEX idx_f47550e71fb8d185 TO IDX_6F287D8E1FB8D185');
    }
}
