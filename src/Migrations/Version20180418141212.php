<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180418141212 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree DROP FOREIGN KEY FK_5B370E9B79066886');
        $this->addSql('DROP INDEX IDX_5B370E9B79066886 ON entity_nested_tree');
        $this->addSql('ALTER TABLE entity_nested_tree CHANGE lft lft INT NOT NULL, CHANGE rft rft INT NOT NULL, CHANGE root_id root INT DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree CHANGE lft lft INT DEFAULT NULL, CHANGE rft rft INT DEFAULT NULL, CHANGE root root_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE entity_nested_tree ADD CONSTRAINT FK_5B370E9B79066886 FOREIGN KEY (root_id) REFERENCES entity_nested_tree (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_5B370E9B79066886 ON entity_nested_tree (root_id)');
    }
}
