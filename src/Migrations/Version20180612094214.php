<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180612094214 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shift_log_entity (id INT UNSIGNED AUTO_INCREMENT NOT NULL, entity_id BIGINT UNSIGNED DEFAULT NULL, host_id_from INT DEFAULT NULL, host_id_to INT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_2F6D5CE581257D5D (entity_id), INDEX IDX_2F6D5CE55C69143 (host_id_from), INDEX IDX_2F6D5CE5B4986630 (host_id_to), INDEX entity_id_host_id_search (entity_id, host_id_to), UNIQUE INDEX unique_link_on_date_abuse (entity_id, host_id_to, date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shift_log_entity ADD CONSTRAINT FK_2F6D5CE581257D5D FOREIGN KEY (entity_id) REFERENCES entity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shift_log_entity ADD CONSTRAINT FK_2F6D5CE55C69143 FOREIGN KEY (host_id_from) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shift_log_entity ADD CONSTRAINT FK_2F6D5CE5B4986630 FOREIGN KEY (host_id_to) REFERENCES host (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE shift_log_entity');
    }
}
