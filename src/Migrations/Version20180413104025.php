<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413104025 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entity_nested_tree (id INT AUTO_INCREMENT NOT NULL, root_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, entity_id INT NOT NULL, lft INT DEFAULT NULL, level INT DEFAULT NULL, rft INT DEFAULT NULL, INDEX IDX_5B370E9B79066886 (root_id), INDEX IDX_5B370E9B727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entity_nested_tree ADD CONSTRAINT FK_5B370E9B79066886 FOREIGN KEY (root_id) REFERENCES entity_nested_tree (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entity_nested_tree ADD CONSTRAINT FK_5B370E9B727ACA70 FOREIGN KEY (parent_id) REFERENCES entity_nested_tree (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree DROP FOREIGN KEY FK_5B370E9B79066886');
        $this->addSql('ALTER TABLE entity_nested_tree DROP FOREIGN KEY FK_5B370E9B727ACA70');
        $this->addSql('DROP TABLE entity_nested_tree');
    }
}
