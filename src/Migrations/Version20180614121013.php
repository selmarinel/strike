<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180614121013 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entities_to_tags (entity_id BIGINT UNSIGNED NOT NULL, tag_id INT UNSIGNED NOT NULL, INDEX IDX_1146335581257D5D (entity_id), INDEX IDX_11463355BAD26311 (tag_id), PRIMARY KEY(entity_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT UNSIGNED AUTO_INCREMENT NOT NULL, entity_id BIGINT UNSIGNED DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX unique_entity_tag (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entities_to_tags ADD CONSTRAINT FK_1146335581257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE entities_to_tags ADD CONSTRAINT FK_11463355BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B78381257D5D FOREIGN KEY (entity_id) REFERENCES entity (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entities_to_tags DROP FOREIGN KEY FK_11463355BAD26311');
        $this->addSql('DROP TABLE entities_to_tags');
        $this->addSql('DROP TABLE tag');
    }
}
