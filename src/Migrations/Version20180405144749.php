<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180405144749 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entity (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, entity_type INT NOT NULL, source_id VARCHAR(255) DEFAULT NULL, create_ts INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity_to_host (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, entity_id BIGINT UNSIGNED DEFAULT NULL, create_ts INT NOT NULL, entity_slug VARCHAR(255) NOT NULL, INDEX IDX_BB0430111FB8D185 (host_id), INDEX IDX_BB04301181257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE host (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(255) NOT NULL, geo VARCHAR(2) NOT NULL, parent_id INT DEFAULT NULL, abuse_level SMALLINT NOT NULL, data JSON NOT NULL, INDEX hostname_idx (hostname), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB0430111FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB04301181257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB04301181257D5D');
        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB0430111FB8D185');
        $this->addSql('DROP TABLE entity');
        $this->addSql('DROP TABLE entity_to_host');
        $this->addSql('DROP TABLE host');
    }
}
