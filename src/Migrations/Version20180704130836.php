<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180704130836 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree ADD CONSTRAINT FK_5B370E9B16F4F95B FOREIGN KEY (root) REFERENCES entity_nested_tree (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_5B370E9B16F4F95B ON entity_nested_tree (root)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree DROP FOREIGN KEY FK_5B370E9B16F4F95B');
        $this->addSql('DROP INDEX IDX_5B370E9B16F4F95B ON entity_nested_tree');
    }
}
