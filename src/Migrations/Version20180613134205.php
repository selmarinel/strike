<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180613134205 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE abuse_entity DROP FOREIGN KEY FK_EBA8AA77A38EFE26');
        $this->addSql('DROP INDEX IDX_EBA8AA77A38EFE26 ON abuse_entity');
        $this->addSql('DROP INDEX unique_link_on_date_abuse ON abuse_entity');
        $this->addSql('ALTER TABLE abuse_entity DROP entity_to_host_id');
        $this->addSql('CREATE UNIQUE INDEX unique_link_on_date_abuse ON abuse_entity (entity_id, host_id, date)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX unique_link_on_date_abuse ON abuse_entity');
        $this->addSql('ALTER TABLE abuse_entity ADD entity_to_host_id BIGINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE abuse_entity ADD CONSTRAINT FK_EBA8AA77A38EFE26 FOREIGN KEY (entity_to_host_id) REFERENCES entity_to_host (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_EBA8AA77A38EFE26 ON abuse_entity (entity_to_host_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_link_on_date_abuse ON abuse_entity (entity_to_host_id, date)');
    }
}
