<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180802151851 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE search_entity ADD host_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE search_entity ADD CONSTRAINT FK_D7CA2B771FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_D7CA2B771FB8D185 ON search_entity (host_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE search_entity DROP FOREIGN KEY FK_D7CA2B771FB8D185');
        $this->addSql('DROP INDEX IDX_D7CA2B771FB8D185 ON search_entity');
        $this->addSql('ALTER TABLE search_entity DROP host_id');
    }
}
