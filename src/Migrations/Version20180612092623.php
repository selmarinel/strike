<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180612092623 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE abuse_entity ADD entity_id BIGINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE abuse_entity ADD CONSTRAINT FK_EBA8AA7781257D5D FOREIGN KEY (entity_id) REFERENCES entity (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_EBA8AA7781257D5D ON abuse_entity (entity_id)');
        $this->addSql('CREATE INDEX host_and_entity_search ON abuse_entity (host_id, entity_id)');
        $this->addSql('ALTER TABLE abuse_entity RENAME INDEX host_search TO IDX_EBA8AA771FB8D185');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE abuse_entity DROP FOREIGN KEY FK_EBA8AA7781257D5D');
        $this->addSql('DROP INDEX IDX_EBA8AA7781257D5D ON abuse_entity');
        $this->addSql('DROP INDEX host_and_entity_search ON abuse_entity');
        $this->addSql('ALTER TABLE abuse_entity DROP entity_id');
        $this->addSql('ALTER TABLE abuse_entity RENAME INDEX idx_eba8aa771fb8d185 TO host_search');
    }
}
