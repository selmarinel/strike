<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180424165801 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entity_host_to_entity_host (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, main_id BIGINT UNSIGNED DEFAULT NULL, subordinate_id BIGINT UNSIGNED DEFAULT NULL, position INT NOT NULL, entity_type INT NOT NULL, INDEX IDX_56CA8265627EA78A (main_id), INDEX IDX_56CA82655A373861 (subordinate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entity_host_to_entity_host ADD CONSTRAINT FK_56CA8265627EA78A FOREIGN KEY (main_id) REFERENCES entity_to_host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entity_host_to_entity_host ADD CONSTRAINT FK_56CA82655A373861 FOREIGN KEY (subordinate_id) REFERENCES entity_to_host (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE entity_host_to_entity_host');
    }
}
