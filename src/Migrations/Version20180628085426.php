<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180628085426 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE metric_host (id INT UNSIGNED AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, link VARCHAR(255) NOT NULL, INDEX IDX_405D41EA1FB8D185 (host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE metric_host_log (id INT UNSIGNED AUTO_INCREMENT NOT NULL, metric_host_id INT UNSIGNED DEFAULT NULL, status INT NOT NULL, content_length INT NOT NULL, time INT NOT NULL, title VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_7307551D3E8CC30B (metric_host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE metric_host ADD CONSTRAINT FK_405D41EA1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE metric_host_log ADD CONSTRAINT FK_7307551D3E8CC30B FOREIGN KEY (metric_host_id) REFERENCES metric_host (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE metric_host_log DROP FOREIGN KEY FK_7307551D3E8CC30B');
        $this->addSql('DROP TABLE metric_host');
        $this->addSql('DROP TABLE metric_host_log');
    }
}
