<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180607104326 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE abuse_entity (id INT UNSIGNED AUTO_INCREMENT NOT NULL, entity_to_host_id BIGINT UNSIGNED DEFAULT NULL, host_id INT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_EBA8AA77A38EFE26 (entity_to_host_id), INDEX host_search (host_id), UNIQUE INDEX unique_link_on_date_abuse (entity_to_host_id, date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abuse_entity ADD CONSTRAINT FK_EBA8AA77A38EFE26 FOREIGN KEY (entity_to_host_id) REFERENCES entity_to_host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE abuse_entity ADD CONSTRAINT FK_EBA8AA771FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE abuse_entity');
    }
}
