<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180418091808 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB0430111FB8D185');
        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB04301181257D5D');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB0430111FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB04301181257D5D FOREIGN KEY (entity_id) REFERENCES entity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entity_nested_tree CHANGE entity_id entity_id BIGINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE entity_nested_tree ADD CONSTRAINT FK_5B370E9B81257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('CREATE INDEX IDX_5B370E9B81257D5D ON entity_nested_tree (entity_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entity_nested_tree DROP FOREIGN KEY FK_5B370E9B81257D5D');
        $this->addSql('DROP INDEX IDX_5B370E9B81257D5D ON entity_nested_tree');
        $this->addSql('ALTER TABLE entity_nested_tree CHANGE entity_id entity_id INT NOT NULL');
        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB0430111FB8D185');
        $this->addSql('ALTER TABLE entity_to_host DROP FOREIGN KEY FK_BB04301181257D5D');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB0430111FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE entity_to_host ADD CONSTRAINT FK_BB04301181257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
    }
}
