<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180427064514 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE host_has_widgets (host_id BIGINT UNSIGNED NOT NULL, widget_id INT NOT NULL, INDEX IDX_83DDA7E41FB8D185 (host_id), INDEX IDX_83DDA7E4FBE885E2 (widget_id), PRIMARY KEY(host_id, widget_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE host_has_widgets ADD CONSTRAINT FK_83DDA7E41FB8D185 FOREIGN KEY (host_id) REFERENCES widgets (id)');
        $this->addSql('ALTER TABLE host_has_widgets ADD CONSTRAINT FK_83DDA7E4FBE885E2 FOREIGN KEY (widget_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE widgets DROP FOREIGN KEY FK_9D58E4C11FB8D185');
        $this->addSql('DROP INDEX unique_entity_type_host_name ON widgets');
        $this->addSql('DROP INDEX IDX_9D58E4C11FB8D185 ON widgets');
        $this->addSql('ALTER TABLE widgets DROP host_id');
        $this->addSql('CREATE UNIQUE INDEX unique_entity_type ON widgets (entity_type, name)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE host_has_widgets');
        $this->addSql('DROP INDEX unique_entity_type ON widgets');
        $this->addSql('ALTER TABLE widgets ADD host_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE widgets ADD CONSTRAINT FK_9D58E4C11FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX unique_entity_type_host_name ON widgets (entity_type, host_id, name)');
        $this->addSql('CREATE INDEX IDX_9D58E4C11FB8D185 ON widgets (host_id)');
    }
}
