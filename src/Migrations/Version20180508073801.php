<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Service\Base\Infrastructure\Entity\Type;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180508073801 extends AbstractMigration
{
    private function widgets()
    {
        yield [
            'type' => Type::AUDIO_TRACK,
            'path' => 'blocks/widget/track/entity',
            'name' => 'entity'
        ];
        yield [
            'type' => Type::AUDIO_TRACK,
            'path' => 'blocks/widget/track/player',
            'name' => 'audio_track_player'
        ];

        yield [
            'type' => Type::AUDIO_ARTIST,
            'path' => 'blocks/widget/artist/entity',
            'name' => 'entity'
        ];
        yield [
            'type' => Type::AUDIO_ARTIST,
            'path' => 'blocks/widget/artist/list/albums',
            'name' => 'artist_albums_ul_li'
        ];

        yield [
            'type' => Type::AUDIO_ALBUM,
            'path' => 'blocks/widget/album/entity',
            'name' => 'entity'
        ];
        yield [
            'type' => Type::AUDIO_ALBUM,
            'path' => 'blocks/widget/album/list/tracks',
            'name' => 'album_tracks_ul_li'
        ];
        yield [
            'type' => Type::COLLECTION_AUDIO_TRACKS,
            'path' => 'collection/tracks/list',
            'name' => 'audio_tracks_list'
        ];
        yield [
            'type' => Type::COLLECTION_AUDIO_ARTISTS,
            'path' => 'collection/artists/list',
            'name' => 'audio_artists_list'
        ];
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        foreach ($this->widgets() as $widget){
            $this->addSql("INSERT INTO `widgets` (`name`, `twig_path`, `entity_type`) VALUES ('{$widget['name']}','{$widget['path']}',{$widget['type']})");
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE `widgets`');
    }
}
