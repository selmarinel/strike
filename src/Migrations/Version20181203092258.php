<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181203092258 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE host_geo_redirect (id INT UNSIGNED AUTO_INCREMENT NOT NULL, host_from_id INT DEFAULT NULL, host_to_id INT DEFAULT NULL, INDEX IDX_77C942BA4F32F53A (host_from_id), INDEX IDX_77C942BA93570796 (host_to_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE host_geo_redirect ADD CONSTRAINT FK_77C942BA4F32F53A FOREIGN KEY (host_from_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE host_geo_redirect ADD CONSTRAINT FK_77C942BA93570796 FOREIGN KEY (host_to_id) REFERENCES host (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE host_geo_redirect');
    }
}
