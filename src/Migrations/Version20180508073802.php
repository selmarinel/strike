<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Service\Base\Infrastructure\Entity\Type;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180508073802 extends AbstractMigration
{
    private function widgets()
    {
        yield [
            'type' => Type::COLLECTION_AUDIO_ARTISTS,
            'path' => 'collection/artists/list.semantic',
            'name' => 'audio_artists_list_semantic'
        ];
        yield [
            'type' => Type::COLLECTION_AUDIO_TRACKS,
            'path' => 'collection/tracks/list.semantic',
            'name' => 'audio_tracks_list_semantic'
        ];
        yield [
            'type' => Type::AUDIO_ARTIST,
            'path' => 'blocks/widget/artist/entity.semantic',
            'name' => 'entity_semantic'
        ];
        yield [
            'type' => Type::AUDIO_ARTIST,
            'path' => 'blocks/widget/artist/list/albums.semantic',
            'name' => 'artist_albums_ul_li_semantic'
        ];
        yield [
            'type' => Type::AUDIO_ALBUM,
            'path' => 'blocks/widget/album/entity.semantic',
            'name' => 'entity_semantic'
        ];
        yield [
            'type' => Type::AUDIO_ALBUM,
            'path' => 'blocks/widget/album/list/tracks.semantic',
            'name' => 'album_tracks_ul_li_semantic'
        ];
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        foreach ($this->widgets() as $widget){
            $this->addSql("INSERT INTO `widgets` (`name`, `twig_path`, `entity_type`) VALUES ('{$widget['name']}','{$widget['path']}',{$widget['type']})");
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE `widgets`');
    }
}
