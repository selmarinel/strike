<?php

namespace App\Helper;


use Ausi\SlugGenerator\SlugGenerator;

class SlugMakerService
{
    /**
     * @param string $string
     * @return string
     */
    public static function makeSlug(string $string)
    {
        $generator = new SlugGenerator;
        $string = str_replace(['ь'], '_', $string);
        return $generator->generate(trim($string), ['delimiter' => '_']);
    }
}