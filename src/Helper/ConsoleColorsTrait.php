<?php

namespace App\Helper;


use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

trait ConsoleColorsTrait
{
    public function appendPack(OutputInterface $output)
    {
        $outputStyle = new OutputFormatterStyle('cyan', null, array('bold'));
        $output->getFormatter()->setStyle('green', $outputStyle);
        $outputStyle = new OutputFormatterStyle('green', null, ['bold']);
        $output->getFormatter()->setStyle('success', $outputStyle);
        $outputStyle = new OutputFormatterStyle('blue','yellow');
        $output->getFormatter()->setStyle('warning',$outputStyle);
    }

    public function appendProgressbar(OutputInterface $output, int $count)
    {
        $progressBar = new ProgressBar($output, $count);
        $progressBar->setFormat('verbose');
        $progressBar->setBarCharacter('<comment>░</comment>');
        $progressBar->setProgressCharacter('<info>▒</info>');
        $progressBar->setEmptyBarCharacter(' ');
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
        $progressBar->start();
        return $progressBar;
    }
}