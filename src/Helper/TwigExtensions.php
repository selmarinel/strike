<?php

namespace App\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtensions extends AbstractExtension
{

    private $rootDir = '';

    public function __construct(   ContainerInterface $container )
    {
        $this->rootDir = $container->get('kernel')->getProjectDir();
    }

    public function getFunctions(): array
    {
        return array(
            new TwigFunction('lazy_css', array($this, 'lazyCss')),
            new TwigFunction('raw_include', array($this, 'rawInclude')),
        );
    }

    public function lazyCss(string $src): string
    {
        $name = preg_replace('/[0-9]+/', '', md5($src));
        return "<script type=\"text/javascript\">
                        /* First CSS File */
                        var $name = document.createElement('link');
                        $name.rel = 'stylesheet';
                        $name.href = '$src';
                        $name.type = 'text/css';
                        var parent_$name = document.getElementsByTagName('link')[0];
                        parent_$name.parentNode.insertBefore($name, parent_$name);
                    </script>";
    }

    public function rawInclude(string $src): string
    {
        return file_get_contents($this->rootDir . '/' . $src);
    }

}