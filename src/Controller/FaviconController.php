<?php

namespace App\Controller;

use Identicon\Identicon;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FaviconController extends Controller
{
    public function __invoke(Request $request)
    {
        (new Identicon())->displayImage($request->getHost());
    }
}