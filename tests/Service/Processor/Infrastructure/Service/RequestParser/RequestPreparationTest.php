<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\RequestParser;

use App\Service\Processor\Infrastructure\Service\RequestParser\RequestPreparation;
use PHPUnit\Framework\TestCase;

class RequestPreparationTest extends TestCase
{
    public function testPrepare()
    {
        $slug = 'slug';

        $request = new \Symfony\Component\HttpFoundation\Request();
        $request->query->set('slug', $slug);
        $request->headers->set('User-Agent','google');

        $service = new RequestPreparation();
        $newRequest = $service->prepareFromHttpRequest($request);
        $this->assertEquals($newRequest->getSlug(), $slug);
        $this->assertEquals($newRequest->getLimit(), 12);
        $this->assertEquals($newRequest->getPage(), 1);
        $this->assertEquals($newRequest->getIsBot(), false);
        $this->assertEquals($newRequest->getScheme(), 'http');
    }

    public function botProvider()
    {
        yield ['ads'];
        yield ['google'];
        yield ['bing'];
        yield ['msn'];
        yield ['yandex'];
        yield ['baidu'];
        yield ['yahoo'];
        yield ['ro'];
        yield ['career'];
    }

    /** @dataProvider botProvider */
    public function testPrepareForBot($bot)
    {
        $slug = 'slug';

        $request = new \Symfony\Component\HttpFoundation\Request();
        $request->query->set('slug', $slug);
        $request->headers->set('User-Agent', "{$bot}bot");

        $service = new RequestPreparation();
        $newRequest = $service->prepareFromHttpRequest($request);
        $this->assertEquals($newRequest->getSlug(), $slug);
        $this->assertTrue($newRequest->getIsBot());
    }
}