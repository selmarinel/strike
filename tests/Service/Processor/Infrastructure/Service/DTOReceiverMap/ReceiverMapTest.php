<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\DTOReceiverMap;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\VO\Receiver\SetReceiverParametersInterface;
use App\Service\Processor\Infrastructure\Exception\Map\MapException;
use App\Service\Processor\Infrastructure\Service\DTOReceiverMap\ReceiverMap;
use App\Service\Processor\Infrastructure\VO\ReceiverParameters;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReceiverMapTest extends TestCase
{
    private $parameters;
    private $container;

    public function setUp()
    {
        $this->parameters = $this->createMock(ReceiverParameters::class);
        $this->container = $this->createMock(ContainerInterface::class);
    }

    public function testMap()
    {
        $dto = $this->createMock(DTOInterface::class);
        $type = Type::AUDIO_TRACK;
        $dto->method('getEntityType')->willReturn($type);
        $this->container
            ->method('getParameter')
            ->willReturn('');

        $service = new ReceiverMap($this->parameters, $this->container);

        $result = $service->map($dto);
        $this->assertEquals($result->getHost(), '');
        $this->assertEquals($result->getUsername(), '');
        $this->assertEquals($result->getPassword(), '');
    }

    public function tesInvalidMap()
    {
        $dto = $this->createMock(DTOInterface::class);
        $type = -100;
        $dto->method('getEntityType')->willReturn($type);

        $service = new ReceiverMap($this->parameters, $this->container);
        $this->expectException(MapException::class);
        $result = $service->map($dto);
    }
}