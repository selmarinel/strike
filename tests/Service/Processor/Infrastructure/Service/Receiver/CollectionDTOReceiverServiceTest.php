<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\Receiver;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\Collection\DTOCollectionInterface;
use App\Service\Processor\Domain\Service\CollectionParser\PrepareCollectionReceiveUrlInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Domain\Service\Receiver\Filler\CollectionFillerInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\DTO\Audio\Collection\Tracks;
use App\Service\Processor\Infrastructure\DTO\Audio\Track;
use App\Service\Processor\Infrastructure\Service\Receiver\CollectionDTOReceiverService;
use App\Service\Processor\Infrastructure\VO\ReceiverParameters;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class CollectionDTOReceiverServiceTest extends TestCase
{
    private $prepare;
    private $filler;

    public function setUp()
    {
        $this->prepare = $this->createMock(PrepareCollectionReceiveUrlInterface::class);
        $this->filler = $this->createMock(CollectionFillerInterface::class);
    }

    public function testReceive()
    {
        $hostname = 'hostname';

        $host = new Host();
        $host->setEntityType(Type::COLLECTION_AUDIO_TRACKS);
        $request = new Request();

        $dtoCollection = new Tracks();

        $children = new Track();
        $dtoCollection->appendChild($children);

        $this->prepare->method('prepare')->willReturn(true);
        $this->prepare->method('getParent')->willReturn($dtoCollection);
        $this->prepare->method('getChildren')->willReturn([$children]);

        $receiverParameters = new ReceiverParameters();
        $receiverParameters->setHost($hostname);
        $receiverParameters->setUsername('user');
        $receiverParameters->setPassword('password');

        $this->filler->method('fill')->willReturn($dtoCollection);

        $service = new CollectionDTOReceiverService(
            $this->prepare,
            $this->filler);

        $result = $service->receive($host, $request);
        $this->assertTrue($result instanceof DTOCollectionInterface);
        $this->assertTrue(count($result->getChildren()) == 1);
        $this->assertEquals($result->getEntityType(),Type::COLLECTION_AUDIO_TRACKS);

        $this->assertEquals($result->getChildren()[0]->getEntityType(),Type::AUDIO_TRACK);
    }
}