<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\Receiver;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\DTO\DTOInterface;
use App\Service\Processor\Domain\Service\DTOReceiverMap\ReceiveMapInterface;
use App\Service\Processor\Domain\Service\EntityParser\PrepareEntityReceiveUrlInterface;
use App\Service\Processor\Domain\Service\Receiver\Fetcher\FetcherInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\DTO\Audio\Artist;
use App\Service\Processor\Infrastructure\DTO\Audio\Track;
use App\Service\Processor\Infrastructure\Service\Receiver\EntityDTOReceiveService;
use App\Service\Processor\Infrastructure\VO\ReceiverParameters;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class EntityDTOReceiveServiceTest extends TestCase
{
    private $prepare;
    private $map;
    private $fetcher;

    public function setUp()
    {
        $this->prepare = $this->createMock(PrepareEntityReceiveUrlInterface::class);
        $this->map = $this->createMock(ReceiveMapInterface::class);
        $this->fetcher = $this->createMock(FetcherInterface::class);
    }

    public function testReceive()
    {
        $hostname = 'hostname';

        $host = new Host();
        $request = new Request();

        $dto = new Artist();
        $dto2 = new Track();

        $this->prepare->method('prepare')->willReturn([$dto, $dto2]);

        $receiverParameters = new ReceiverParameters();
        $receiverParameters->setHost($hostname);
        $receiverParameters->setUsername('user');
        $receiverParameters->setPassword('password');

        $this->fetcher->method('fetchMany')->willReturn([[],[]]);

        $this->map->method('map')->willReturn($receiverParameters);

        $service = new EntityDTOReceiveService(
            $this->prepare,
            $this->map,
            $this->fetcher);

        $result = $service->receive($host, $request);

        $this->assertTrue($result instanceof DTOInterface);
        $this->assertTrue(count($result->getChildren()) == 1);
        $this->assertEquals($result->getEntityType(),Type::AUDIO_ARTIST);
        $this->assertEquals($result->getChildren()[0]->getEntityType(), Type::AUDIO_TRACK);
    }
}