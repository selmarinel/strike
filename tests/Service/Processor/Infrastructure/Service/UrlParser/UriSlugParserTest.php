<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\UrlParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;
use App\Service\Processor\Infrastructure\Service\UrlParser\UriSlugParser;
use PHPUnit\Framework\TestCase;

class UriSlugParserTest extends TestCase
{
    public function testParse()
    {
        $service = new UriSlugParser();
        $data = [
            UriSlugParser::SLUG_TYPES => [
                [
                    UriSlugParser::PATTERN_FIELD => [],
                    UriSlugParser::STATIC_FIELD => false,
                    UriSlugParser::SLUG_TYPE => Type::AUDIO_TRACK
                ]
            ]
        ];
        $result = $service->parseSlug($data);

        $this->assertTrue(!empty($result));
        $this->assertEquals($result[0]->isStatic(),false);
        $this->assertEquals($result[0]->getSlugType(),Type::AUDIO_TRACK);
    }

    public function testWhenIncorrectData()
    {
        $service = new UriSlugParser();
        $this->expectException(HostSlugException::class);
        $service->parseSlug([]);
    }

    public function testWhenIncorrectDataSlugs()
    {
        $service = new UriSlugParser();
        $data = [
            UriSlugParser::SLUG_TYPES => [[]]
        ];
        $this->expectException(HostSlugException::class);
        $service->parseSlug($data);
    }
}