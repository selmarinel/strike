<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\UrlParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Domain\Service\Redirecter\HostRedirectServiceInterface;
use App\Service\Processor\Domain\Service\Redirecter\RedirectServiceInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Infrastructure\Exception\Host\HostNotFoundException;
use App\Service\Processor\Infrastructure\Exception\Host\HostSlugException;
use App\Service\Processor\Infrastructure\Exception\Slug\SlugNotFoundException;
use App\Service\Processor\Infrastructure\Service\Redirecter\RedirectedService;
use App\Service\Processor\Infrastructure\Service\UrlParser\UrlParserService;
use App\Service\Processor\Infrastructure\VO\Request;
use App\Service\Processor\Infrastructure\VO\Slug;
use PHPUnit\Framework\TestCase;

class UrlParserServiceTest extends TestCase
{
    private $hostRepository;
    private $uriSlugParser;
    private $redirectService;
    private $hostRedirectGeoService;

    public function setUp()
    {
        $this->hostRepository = $this->createMock(HostRepository::class);
        $this->uriSlugParser = $this->createMock(UriSlugParserInterface::class);
        $this->redirectService = $this->createMock(RedirectedService::class);
        $this->hostRedirectGeoService = $this->createMock(HostRedirectServiceInterface::class);
    }

    public function testParse()
    {
        $host = new Host();
        $hostId = 1;
        $hostname = 'name';
        $typeSlug = 'slug';
        $static = false;
        $host->setId($hostId);
        $host->setHostname($hostname);
        $host->setType(Type::HOST_TYPE_AUDIO);
        $host->setAbuseLevel(1);
        $host->setData([]);
        $this->hostRepository->method('findByHostName')->willReturn($host);
        $slug = new Slug();
        $slug->setSlugType(Type::AUDIO_TRACK);
        $slug->setStatic($static);
        $slug->setPattern([
            Slug::PATTERN_SLUG => $typeSlug,
            Slug::PATTERN_SEPARATOR => '/',
            Slug::PATTERN_ORDER => [
                UrlParserService::TYPE_SLUG,
                UrlParserService::ENTITY_SLUG,
                UrlParserService::ENTITY_ID
            ]
        ]);

        $this->uriSlugParser->method('parseSlug')->willReturn([$slug]);
        $request = new Request();
        $request->setIsBot(false);
        $request->setSlug('slug/some/42');
        $service = new UrlParserService($this->hostRepository, $this->uriSlugParser, $this->redirectService,$this->hostRedirectGeoService);
        $hostDao = $service->processRequest($request);
        $this->assertEquals($hostDao->getTypeSlug(), $typeSlug);
        $this->assertEquals($hostDao->getEntityType(), Type::AUDIO_TRACK);
        $this->assertEquals($hostDao->getHostname(), $hostname);
        $this->assertEquals($hostDao->isStatic(), $static);
        $this->assertEquals($hostDao->getHostId(), $hostId);
    }

    public function testParseWithoutHost()
    {
        $this->hostRepository->method('findByHostName')->willReturn(null);
        $request = new Request();
        $service = new UrlParserService($this->hostRepository, $this->uriSlugParser, $this->redirectService,$this->hostRedirectGeoService);
        $this->expectException(HostNotFoundException::class);
        $service->processRequest($request);
    }

    public function testParseWithoutSlugs()
    {
        $host = new Host();
        $hostId = 1;
        $hostname = 'name';

        $host->setId($hostId);
        $host->setHostname($hostname);
        $host->setType(Type::HOST_TYPE_AUDIO);
        $host->setAbuseLevel(1);
        $host->setData([]);
        $this->hostRepository->method('findByHostName')->willReturn($host);
        $this->uriSlugParser->method('parseSlug')->willReturn([]);

        $request = new Request();
        $service = new UrlParserService($this->hostRepository, $this->uriSlugParser, $this->redirectService,$this->hostRedirectGeoService);
        $this->expectException(HostSlugException::class);
        $service->processRequest($request);
    }

    public function testNotFoundSlug()
    {
        $host = new Host();
        $hostId = 1;
        $hostname = 'name';
        $typeSlug = 'slug';
        $static = false;
        $host->setId($hostId);
        $host->setHostname($hostname);
        $host->setType(Type::HOST_TYPE_AUDIO);
        $host->setAbuseLevel(1);
        $host->setData([]);
        $this->hostRepository->method('findByHostName')->willReturn($host);
        $slug = new Slug();
        $slug->setSlugType(Type::AUDIO_TRACK);
        $slug->setStatic($static);
        $slug->setPattern([
            Slug::PATTERN_SLUG => 'shrimp',
            Slug::PATTERN_SEPARATOR => '/',
            Slug::PATTERN_ORDER => [
                UrlParserService::TYPE_SLUG,
                UrlParserService::ENTITY_SLUG,
                UrlParserService::ENTITY_ID
            ]
        ]);

        $this->uriSlugParser->method('parseSlug')->willReturn([$slug]);
        $request = new Request();
        $request->setIsBot(false);
        $request->setSlug('slug/some/42');
        $service = new UrlParserService($this->hostRepository, $this->uriSlugParser, $this->redirectService,$this->hostRedirectGeoService);
        $this->expectException(SlugNotFoundException::class);
        $service->processRequest($request);
    }
}