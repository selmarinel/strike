<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityNestedTree;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityNestedTreeRepository;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Service\EntityParser\EntityChildrenPreparation;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class EntityChildrenPreparationTest extends TestCase
{
    private $entityNestedTreeRepository;
    private $entityToHostRepository;
    private $preparation;

    public function setUp()
    {
        $this->entityNestedTreeRepository = $this->createMock(EntityNestedTreeRepository::class);
        $this->entityToHostRepository = $this->createMock(EntityToHostRepository::class);
        $this->preparation = $this->createMock(EntityPreparationInterface::class);
    }

    public function testPrepare()
    {
        $host = new Host();
        $entity = new Entity();
        $request = new Request();
        $childId = 1;

        $node = new EntityNestedTree();
        $childNode = new EntityNestedTree();
        $childEntity = new \App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity();
        $childEntity->setId($childId);
        $childNode->setEntity($childEntity);

        $this->entityNestedTreeRepository->method('findByEntityId')->willReturn($node);
        $this->entityNestedTreeRepository->method('getChildrenEntities')->willReturn([$childNode]);
        $this->entityNestedTreeRepository->method('getChildrenCount')->willReturn(1);

        $entityToHost = new EntityToHost();

        $this->entityToHostRepository->method('findByHostIdAndEntitiesId')->willReturn([$entityToHost]);

        $entityDto = new Entity();
        $name = 'name';
        $eSlug = 'e_slug';
        $eId = 1;
        $eSource = 42;
        $entityDto->setName($name);
        $entityDto->setEntitySlug($eSlug);
        $entityDto->setEntityId($eId);
        $entityDto->setSourceId($eSource);

        $this->preparation->method('prepareEntityFromEntityToHost')->willReturn($entityDto);

        $service = new EntityChildrenPreparation(
            $this->entityNestedTreeRepository,
            $this->entityToHostRepository,
            $this->preparation);

        $result = $service->prepare($host, $entity, $request);
        $this->assertTrue(is_array($result));
        $this->assertEquals($result[0]->getName(), $name);
        $this->assertEquals($result[0]->getEntityId(), $eId);
        $this->assertEquals($result[0]->getSourceId(), $eSource);
        $this->assertEquals($result[0]->getEntitySlug(), $eSlug);
    }

    public function testFindNext()
    {
        $service = new EntityChildrenPreparation(
            $this->entityNestedTreeRepository,
            $this->entityToHostRepository,
            $this->preparation);
        $node = new EntityNestedTree();
        $entity = new Entity();
        $service->setNode($node);
        $entityToHost = new EntityToHost();
        $this->entityNestedTreeRepository->method('getNext')->willReturn([1 => $entityToHost]);
        $this->preparation->method('prepareEntityFromEntityToHost')->willReturn($entity);
        $next = $service->findNext();

        $this->assertEquals($next,$entity);
    }

    public function testFindPrev()
    {
        $service = new EntityChildrenPreparation(
            $this->entityNestedTreeRepository,
            $this->entityToHostRepository,
            $this->preparation);
        $node = new EntityNestedTree();
        $entity = new Entity();
        $service->setNode($node);
        $entityToHost = new EntityToHost();
        $this->entityNestedTreeRepository->method('getPrev')->willReturn([1 => $entityToHost]);
        $this->preparation->method('prepareEntityFromEntityToHost')->willReturn($entity);
        $next = $service->findPrev();

        $this->assertEquals($next,$entity);
    }
}