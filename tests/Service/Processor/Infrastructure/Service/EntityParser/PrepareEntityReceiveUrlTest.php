<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\Service\EntityParser\EntityParserInterface;
use App\Service\Processor\Domain\Service\EntityParser\Relation\RelationSquashInterface;
use App\Service\Processor\Domain\Service\EntityParser\Tag\TagChildrenPreparationInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Service\EntityParser\PrepareEntityReceiveUrl;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class PrepareEntityReceiveUrlTest extends TestCase
{
    private $entityParserService;
    private $encryption;
    private $tagChildren;
    private $squash;

    public function setUp()
    {
        $this->entityParserService = $this->createMock(EntityParserInterface::class);
        $this->encryption = $this->createMock(EncryptionInterface::class);
        $this->tagChildren = $this->createMock(TagChildrenPreparationInterface::class);
        $this->squash = $this->createMock(RelationSquashInterface::class);
    }

    public function testPrepare()
    {
        $entity = new Entity();
        $entity->setEntityType(Type::AUDIO_TRACK);
        $entity->setLink('link');
        $entity->setSourceId(42);
        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);

        $this->entityParserService->method('parse')->willReturn($entityToHost);

        $eh = new Entity();
        $ehLink = "eh_link";
        $ehId = 13;
        $ehType = Type::AUDIO_ARTIST;
        $eh->setEntityType($ehType);
        $eh->setLink($ehLink);
        $eh->setSourceId($ehId);

        $this->entityParserService->method('getChildren')->willReturn([$eh]);

        $host = new Host();
        $request = new Request();
        $service = new PrepareEntityReceiveUrl(
            $this->entityParserService,
            $this->tagChildren,
            $this->squash,
            $this->encryption);
        $result = $service->prepare($host, $request);

        $this->assertEquals($result[0]->getSourceId(), 42);
        $this->assertEquals($result[0]->getEntityType(), Type::AUDIO_TRACK);
        $this->assertEquals($result[0]->getLink(), 'link');

        $this->assertEquals($result[1]->getLink(),$ehLink);
        $this->assertEquals($result[1]->getEntityType(),$ehType);
        $this->assertEquals($result[1]->getSourceId(),$ehId);
    }

    public function testNotFoundDTO()
    {
        $host = new Host();
        $request = new Request();
        $service = new PrepareEntityReceiveUrl(
            $this->entityParserService,
            $this->tagChildren,
            $this->squash,
            $this->encryption);
        $this->expectException(EntityNotFoundException::class);
        $service->prepare($host, $request);
    }
}