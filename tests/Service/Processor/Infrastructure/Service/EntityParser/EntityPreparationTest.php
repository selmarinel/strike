<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\Processor\Infrastructure\Service\EntityParser\EntityPreparation;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use PHPUnit\Framework\TestCase;

class EntityPreparationTest extends TestCase
{
    public function testPreparation()
    {
        $entity = new Entity();
        $name = 'name';
        $entityId = 1;
        $sourceId = 42;
        $type = Type::AUDIO_TRACK;
        $slug = 'slug';

        $entity->setName($name);
        $entity->setSourceId($sourceId);
        $entity->setId($entityId);
        $entity->setEntityType($type);

        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);
        $entityToHost->setEntitySlug($slug);

        $et = $this->createMock(EntityHostToEntityHostRepository::class);

        $service = new EntityPreparation($et);

        $eh = $service->prepareEntityFromEntityToHost($entityToHost);

        $this->assertEquals($eh->getEntitySlug(), $slug);
        $this->assertEquals($eh->getName(), $name);
        $this->assertEquals($eh->getEntityId(), $entityId);
        $this->assertEquals($eh->getSourceId(), $sourceId);
        $this->assertEquals($eh->getEntityType(), $type);

    }
}