<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Domain\Service\EntityParser\EntityChildrenPreparationInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Service\EntityParser\EntityParserService;
use App\Service\Processor\Infrastructure\Service\TagParser\TagParserService;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class EntityParserServiceTest extends TestCase
{
    private $entityToHostRepository;
    private $entityLinkGeneratorInterface;
    private $entityChildrenPreparationInterface;
    private $tagParser;

    public function setUp()
    {
        $this->entityToHostRepository = $this->createMock(EntityToHostRepository::class);
        $this->entityLinkGeneratorInterface = $this->createMock(EntityLinkGeneratorInterface::class);
        $this->entityChildrenPreparationInterface = $this->createMock(EntityChildrenPreparationInterface::class);
        $this->tagParser = $this->createMock(TagParserService::class);
    }

    public function testParse()
    {
        $hostname = 'hostname';
        $host = new Host();
        $host->setHostname($hostname);
        $name = 'name';
        $entityId = 1;
        $sourceId = 42;
        $type = Type::AUDIO_TRACK;

        $host->setEntityType($type);
        $host->setHostId(1);
        $host->setEntityId($entityId);

        $slug = 'slug';
        $host->setEntitySlug($slug);

        $entity = new Entity();
        $entity->setName($name);
        $entity->setId($entityId);
        $entity->setSourceId($sourceId);
        $entity->setEntityType($type);

        $entityToHost = new EntityToHost();
        $entityToHost->setId(1);
        $entityToHost->setEntity($entity);
        $entityToHost->setEntitySlug($slug);

        $this->entityToHostRepository
            ->method('findHostOrParentByHostIdAndEntityIdWithParentId')
            ->willReturn($entityToHost);

        $service = new EntityParserService(
            $this->entityToHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->entityChildrenPreparationInterface,
            $this->tagParser
        );

        $result = $service->parse($host);

        $this->assertEquals($result->getSlug(), $slug);
        $this->assertEquals($result->getHost(), $host);
        $this->assertEquals($result->getEntity()->getEntityType(), $type);
        $this->assertEquals($result->getEntity()->getEntityId(), $entityId);
        $this->assertEquals($result->getEntity()->getName(), $name);
        $this->assertEquals($result->getEntity()->getSourceId(), $sourceId);
    }

    public function testNotFountEntityToHost()
    {
        $host = new Host();
        $host->setEntityId(1);
        $host->setHostId(1);

        $this->entityToHostRepository->method('findHostOrParentByHostIdAndEntityIdWithParentId')->willReturn(null);
        $service = new EntityParserService(
            $this->entityToHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->entityChildrenPreparationInterface,
            $this->tagParser
        );

        $this->expectException(EntityNotFoundException::class);
        $service->parse($host);
    }

    public function testIncorrectHostAndEntityTypes()
    {
        $hostname = 'hostname';
        $host = new Host();
        $host->setHostname($hostname);
        $name = 'name';
        $entityId = 1;
        $sourceId = 42;
        $type = Type::AUDIO_TRACK;
        $hostEntityType = Type::AUDIO_ARTIST;

        $host->setHostId(1);
        $host->setEntityId(1);
        $host->setEntityType($hostEntityType);

        $slug = 'slug';
        $host->setEntitySlug($slug);

        $entity = new Entity();
        $entity->setName($name);
        $entity->setId($entityId);
        $entity->setSourceId($sourceId);
        $entity->setEntityType($type);

        $entityToHost = new EntityToHost();
        $entityToHost->setId(1);
        $entityToHost->setEntity($entity);
        $entityToHost->setEntitySlug($slug);

        $this->entityToHostRepository
            ->method('findHostOrParentByHostIdAndEntityIdWithParentId')
            ->willReturn($entityToHost);

        $service = new EntityParserService(
            $this->entityToHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->entityChildrenPreparationInterface,
            $this->tagParser
        );

        $this->expectException(EntityNotFoundException::class);
        $service->parse($host);
    }

    public function testInvalidValid()
    {
        $hostname = 'hostname';
        $host = new Host();
        $host->setHostname($hostname);
        $name = 'name';
        $entityId = 1;
        $sourceId = 42;
        $type = Type::AUDIO_TRACK;

        $host->setEntityType($type);
        $host->setHostId(1);
        $host->setEntityId($entityId);

        $slug = 'slug';
        $host->setEntitySlug($slug);

        $entity = new Entity();
        $entity->setName($name);
        $entity->setId($entityId);
        $entity->setSourceId($sourceId);
        $entity->setEntityType($type);

        $entityToHost = new EntityToHost();
        $entityToHost->setId(1);
        $entityToHost->setEntity($entity);
        $entityToHost->setEntitySlug('another_slug');

        $this->entityToHostRepository
            ->method('findHostOrParentByHostIdAndEntityIdWithParentId')
            ->willReturn($entityToHost);

        $service = new EntityParserService(
            $this->entityToHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->entityChildrenPreparationInterface,
            $this->tagParser
        );
        $this->expectException(EntityNotFoundException::class);
        $service->parse($host);
    }

    public function testGetChildren()
    {
        $host = new Host();
        $entity = new \App\Service\Processor\Infrastructure\DAO\Entity();
        $request = new Request();

        $service = new EntityParserService(
            $this->entityToHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->entityChildrenPreparationInterface,
            $this->tagParser
        );

        $childEntity = new \App\Service\Processor\Infrastructure\DAO\Entity();
        $childEntity->setEntityId(1);
        $childEntity->setEntitySlug('slug');
        $childEntity->setEntityType(Type::AUDIO_TRACK);
        $childEntity->setSourceId(42);
        $childEntity->setName('name');

        $this->entityChildrenPreparationInterface->method('prepare')->willReturn([$childEntity]);

        $children = $service->getChildren($host, $entity, $request);

        $this->assertEquals($children[0]->getEntityType(), Type::AUDIO_TRACK);
        $this->assertEquals($children[0]->getSourceId(), 42);
        $this->assertEquals($children[0]->getEntityId(), 1);
        $this->assertEquals($children[0]->getName(), 'name');
        $this->assertEquals($children[0]->getEntitySlug(), 'slug');
    }
}