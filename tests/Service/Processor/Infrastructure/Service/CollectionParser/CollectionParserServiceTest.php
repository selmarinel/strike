<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\CollectionParser;


use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\EntityToHostRepository;
use App\Service\Processor\Domain\Service\EntityLinkGenerator\EntityLinkGeneratorInterface;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Service\CollectionParser\CollectionParserService;
use App\Service\Processor\Infrastructure\Service\TagParser\TagParserService;
use App\Service\Processor\Infrastructure\VO\Request;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Entity\EntityHostToEntityHost;
use App\Service\StaticEntityProcessor\Infrastructure\Database\Repository\EntityHostToEntityHostRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NativeQuery;
use PHPUnit\Framework\TestCase;
use App\Service\Processor\Infrastructure\DAO\EntityToHost as DAO;


class CollectionParserServiceTesthuynya
//    extends TestCase
{
    private $entityToHostRepository;
    private $entityHostToEntityHostRepository;
    private $entityLinkGeneratorInterface;
    private $tagParser;
    private $registry;

    public function setUp()
    {
        $this->entityToHostRepository = $this->createMock(EntityToHostRepository::class);
        $this->entityHostToEntityHostRepository = $this->createMock(EntityHostToEntityHostRepository::class);
        $this->entityLinkGeneratorInterface = $this->createMock(EntityLinkGeneratorInterface::class);
        $this->tagParser = $this->createMock(TagParserService::class);
        $this->registry = $this->createMock(ManagerRegistry::class);

        $manager = $this->createMock(EntityManagerInterface::class);

        $nativeQuery = $this->createMock(NativeQuery::class);
        $nativeQuery->method('getResult')->willReturn([]);
        $manager->method('createNativeQuery')->willReturn($nativeQuery);
        $this->registry->method('getManager')->willReturn($manager);
    }

    public function testParse()
    {
        $entityType = 1;
        $entityId = 1;
        $relatedId = 1;
        $sourceId = 42;
        $slug = 'slug';
        $host = new Host();
        $host->setEntityType($entityType);

        $entity = new Entity();
        $entity->setId($entityId);
        $entity->setEntityType($entityType);
        $entity->setSourceId($sourceId);


        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);
        $entityToHost->setId($relatedId);
        $entityToHost->setEntitySlug($slug);

        $this->entityToHostRepository
            ->method('findByHostOrParentHostIdAndTypeSlug')
            ->willReturn($entityToHost);


        $service = new CollectionParserService(
            $this->entityToHostRepository,
            $this->entityHostToEntityHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->tagParser,
            $this->registry
        );

        $result = $service->parse($host);
        $this->assertTrue($result instanceof DAO);
        $this->assertEquals($result->getSlug(), $slug);
        $this->assertEquals($result->getHost(), $host);
    }

    public function testWhenNotEntityToHost()
    {
        $this->entityToHostRepository
            ->method('findByHostOrParentHostIdAndTypeSlug')
            ->willReturn(null);

        $host = new Host();
        $service = new CollectionParserService(
            $this->entityToHostRepository,
            $this->entityHostToEntityHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->tagParser,
            $this->registry
        );

        $this->expectException(EntityNotFoundException::class);
        $service->parse($host);
    }

    public function testWhenInvalidSlug()
    {
        $entityType = 1;
        $hostEntityType = 2;
        $entityId = 1;
        $relatedId = 1;
        $sourceId = 42;
        $slug = 'slug';
        $host = new Host();
        $host->setEntityType($hostEntityType);

        $entity = new Entity();
        $entity->setId($entityId);
        $entity->setEntityType($entityType);
        $entity->setSourceId($sourceId);


        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);
        $entityToHost->setId($relatedId);
        $entityToHost->setEntitySlug($slug);

        $this->entityToHostRepository
            ->method('findByHostOrParentHostIdAndTypeSlug')
            ->willReturn($entityToHost);


        $service = new CollectionParserService(
            $this->entityToHostRepository,
            $this->entityHostToEntityHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->tagParser,
            $this->registry
        );

        $this->expectException(EntityNotFoundException::class);
        $result = $service->parse($host);
    }

    public function testEmptyCollection()
    {
        $entityToHost = new \App\Service\Processor\Infrastructure\DAO\EntityToHost();
        $entity = new \App\Service\Processor\Infrastructure\DAO\Entity();
        $host = new Host();
        $entityToHost->setEntity($entity);
        $entityToHost->setHost($host);

        $request = new Request();

        $this->entityHostToEntityHostRepository->method('findByMainId')->willReturn([]);

        $service = new CollectionParserService(
            $this->entityToHostRepository,
            $this->entityHostToEntityHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->tagParser,
            $this->registry
        );

        $result = $service->getCollection($entityToHost, $request);

        $this->assertTrue(empty($result));
    }

    public function testSomeCollection()
    {
        $entityToHostModel = new EntityHostToEntityHost();

        $name = 'name';
        $sourceId = 42;
        $entityType = 1;
        $id = 1;
        $slug = 'slug';

        $entity = new Entity();
        $entity->setName($name);
        $entity->setSourceId($sourceId);
        $entity->setEntityType($entityType);
        $entity->setId($id);
        $entityToHost = new EntityToHost();
        $entityToHost->setEntitySlug($slug);
        $entityToHost->setEntity($entity);
        $entityToHostModel->setSubordinate($entityToHost);

        $collection = [$entityToHostModel];

        $entityToHost = new \App\Service\Processor\Infrastructure\DAO\EntityToHost();
        $entity = new \App\Service\Processor\Infrastructure\DAO\Entity();
        $host = new Host();
        $entityToHost->setEntity($entity);
        $entityToHost->setHost($host);

        $request = new Request();

        $this->entityHostToEntityHostRepository->method('findByMainId')->willReturn($collection);

        $service = new CollectionParserService(
            $this->entityToHostRepository,
            $this->entityHostToEntityHostRepository,
            $this->entityLinkGeneratorInterface,
            $this->tagParser,
            $this->registry
        );

        $result = $service->getCollection($entityToHost, $request);

        $this->assertFalse(empty($result));

        $this->assertEquals($result[0]->getEntityType(), $entityType);
        $this->assertEquals($result[0]->getName(), $name);
        $this->assertEquals($result[0]->getSourceId(), $sourceId);
        $this->assertEquals($result[0]->getEntitySlug(), $slug);
        $this->assertTrue(is_string($result[0]->getLink()));
        $this->assertEquals($result[0]->getEntityId(), $id);
    }

}