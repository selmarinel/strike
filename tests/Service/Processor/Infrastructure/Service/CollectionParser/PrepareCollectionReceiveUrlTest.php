<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\CollectionParser;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\Processor\Domain\Service\CollectionParser\CollectionParserInterface;
use App\Service\Processor\Domain\VO\Encryption\EncryptionInterface;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\EntityToHost;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\EntityToHost\EntityNotFoundException;
use App\Service\Processor\Infrastructure\Service\CollectionParser\PrepareCollectionReceiveUrl;
use App\Service\Processor\Infrastructure\VO\Request;
use PHPUnit\Framework\TestCase;

class PrepareCollectionReceiveUrlTest extends TestCase
{
    private $collectionParserInterface;
    private $encryptionInterface;

    public function setUp()
    {
        $this->collectionParserInterface = $this->createMock(CollectionParserInterface::class);
        $this->encryptionInterface = $this->createMock(EncryptionInterface::class);
    }

    public function testPrepareRoot()
    {
        $service = new PrepareCollectionReceiveUrl($this->collectionParserInterface, $this->encryptionInterface);
        $host = new Host();
        $hostname = md5(time());
        $host->setHostname($hostname);
        $service->prepareRoot($host);

        $parent = $service->getParent();
        $this->assertEquals($parent->getName(), 'Главная');
        $this->assertEquals($parent->getLink(), "http://$hostname");
    }

    public function testPrepareEmptyCollection()
    {
        $service = new PrepareCollectionReceiveUrl($this->collectionParserInterface, $this->encryptionInterface);
        $entityToHost = new EntityToHost();
        $name = 'name';
        $entity = new Entity();
        $entity->setName($name);
        $entity->setEntityType(Type::COLLECTION_AUDIO_TRACKS);
        $entityToHost->setEntity($entity);

        $this->collectionParserInterface->method('parse')->willReturn($entityToHost);
        $this->collectionParserInterface->method('getCollection')->willReturn([]);
        $request = new Request();
        $host = new Host();
        $hostname = md5(time());
        $host->setHostname($hostname);

        $service->prepare($host, $request);
        $parent = $service->getParent();

        $this->assertEquals($parent->getName(), $name);
        $this->assertTrue(empty($service->getChildren()));
    }

    public function testPrepareItemsCollection()
    {
        $service = new PrepareCollectionReceiveUrl($this->collectionParserInterface, $this->encryptionInterface);
        $entityToHost = new EntityToHost();
        $name = 'name';
        $entity = new Entity();
        $entity->setName($name);
        $entity->setEntityType(Type::COLLECTION_AUDIO_TRACKS);
        $entityToHost->setEntity($entity);

        $child = new Entity();
        $child->setEntityType(Type::AUDIO_TRACK);
        $child->setSourceId(42);

        $collection = [$child];

        $this->collectionParserInterface->method('parse')->willReturn($entityToHost);
        $this->collectionParserInterface->method('getCollection')->willReturn($collection);
        $request = new Request();
        $host = new Host();
        $hostname = md5(time());
        $host->setHostname($hostname);

        $service->prepare($host, $request);
        $parent = $service->getParent();
        $children = $service->getChildren();
        $this->assertEquals($parent->getName(), $name);
        $this->assertFalse(empty($children));
        $this->assertEquals($children[0]->getSourceId(), 42);
        $this->assertEquals($children[0]->getEntityType(), Type::AUDIO_TRACK);
    }

    public function testInvalidDto()
    {
        $service = new PrepareCollectionReceiveUrl($this->collectionParserInterface, $this->encryptionInterface);
        $entityToHost = new EntityToHost();
        $name = 'name';
        $entity = new Entity();
        $entity->setName($name);
        $entity->setEntityType(-100);
        $entityToHost->setEntity($entity);

        $this->collectionParserInterface->method('parse')->willReturn($entityToHost);

        $request = new Request();
        $host = new Host();
        $hostname = md5(time());
        $host->setHostname($hostname);
        $this->expectException(EntityNotFoundException::class);
        $service->prepare($host, $request);

    }
}