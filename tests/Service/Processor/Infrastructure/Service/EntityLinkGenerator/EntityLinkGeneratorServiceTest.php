<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityLinkGenerator;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\Processor\Infrastructure\DAO\Entity;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\Exception\Host\HostInvalidConfigurationException;
use App\Service\Processor\Infrastructure\Service\EntityLinkGenerator\EntityLinkGeneratorService;
use App\Service\Processor\Infrastructure\Service\UrlParser\UrlParserService;
use App\Service\Processor\Infrastructure\VO\Slug;
use PHPUnit\Framework\TestCase;

class EntityLinkGeneratorServiceTest extends TestCase
{
    public function testGenerateLinkDynamic()
    {
        $entity = new Entity();
        $hostname = md5(time());
        $entityId = 1;
        $entityType = Type::AUDIO_TRACK;
        $entitySlug = 'entity_slug';
        $typeSlug = 'type';
        $host = new Host();
        $host->setHostname($hostname);
        $host->setStatic(false);
        $host->setEntityId($entityId);
        $host->setEntityType($entityType);
        $host->setEntitySlug($entitySlug);
        $host->setTypeSlug($typeSlug);

        $slug = new Slug();
        $slug->setSlugType(Type::AUDIO_TRACK);
        $slug->setStatic(false);
        $slug->setPattern(
            [
                SlugTypeDTO::PATTERN_SLUG => $typeSlug,
                SlugTypeDTO::PATTERN_SEPARATOR => '/',
                SlugTypeDTO::PATTERN_ORDER => [
                    UrlParserService::TYPE_SLUG,
                    UrlParserService::ENTITY_SLUG,
                    UrlParserService::ENTITY_ID,
                ]
            ]
        );
        $slugData = [Type::AUDIO_TRACK => $slug];

        $host->setSlugData($slugData);
        $entity->setEntityType($entityType);
        $entity->setEntitySlug($entitySlug);
        $entity->setEntityId($entityId);

        $service = new EntityLinkGeneratorService();
        $link = $service->generateLink($entity, $host);
        $this->assertTrue(is_string($link));
        $this->assertEquals($link, "http://$hostname/$typeSlug/$entitySlug/$entityId");
    }

    public function testGenerateLinkStatic()
    {
        $entity = new Entity();
        $hostname = md5(time());
        $entityType = Type::COLLECTION_AUDIO_TRACKS;
        $typeSlug = 'type';

        $host = new Host();
        $host->setHostname($hostname);
        $host->setStatic(true);
        $host->setEntityType($entityType);
        $host->setTypeSlug($typeSlug);

        $slug = new Slug();
        $slug->setSlugType(Type::COLLECTION_AUDIO_TRACKS);
        $slug->setStatic(false);
        $slug->setPattern(
            [
                SlugTypeDTO::PATTERN_SLUG => $typeSlug,
            ]
        );
        $slugData = [Type::COLLECTION_AUDIO_TRACKS => $slug];

        $host->setSlugData($slugData);

        $service = new EntityLinkGeneratorService();
        $link = $service->generateLink($entity, $host);
        $this->assertTrue(is_string($link));
        $this->assertEquals($link, "http://$hostname/$typeSlug");
    }

    public function testWhileNotConfig()
    {
        $entity = new Entity();
        $hostname = md5(time());
        $entityType = Type::COLLECTION_AUDIO_TRACKS;
        $typeSlug = 'type';

        $host = new Host();
        $host->setHostname($hostname);
        $host->setStatic(true);
        $host->setEntityType($entityType);
        $host->setTypeSlug($typeSlug);

        $host->setSlugData([]);

        $service = new EntityLinkGeneratorService();
        $this->expectException(HostInvalidConfigurationException::class);
        $link = $service->generateLink($entity, $host);
    }
}