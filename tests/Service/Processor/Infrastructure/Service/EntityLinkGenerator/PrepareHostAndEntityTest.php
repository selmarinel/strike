<?php

namespace App\Tests\Service\Processor\Infrastructure\Service\EntityLinkGenerator;


use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Entity;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\EntityToHost;
use App\Service\EntityProcessor\Infrastructure\Database\Entities\Host;
use App\Service\EntityProcessor\Infrastructure\DTO\Host\SlugTypeDTO;
use App\Service\Processor\Domain\Service\EntityParser\EntityPreparationInterface;
use App\Service\Processor\Domain\Service\UrilParser\UriSlugParserInterface;
use App\Service\Processor\Infrastructure\Service\EntityLinkGenerator\PrepareHostAndEntity;
use App\Service\Processor\Infrastructure\VO\Slug;
use PHPUnit\Framework\TestCase;

class PrepareHostAndEntityTest extends TestCase
{
    private $uriSlugParser;
    private $entityPreparation;

    public function setUp()
    {
        $this->uriSlugParser = $this->createMock(UriSlugParserInterface::class);
        $this->entityPreparation = $this->createMock(EntityPreparationInterface::class);
    }

    public function testPreparation()
    {
        $hostname = md5(time());
        $name = 'name';
        $id = 1;
        $parentId = 42;
        $entityType = Type::AUDIO_TRACK;

        $entity = new Entity();
        $entity->setName($name);
        $entity->setId($id);
        $entity->setEntityType($entityType);
        $host = new Host();
        $host->setHostname($hostname);
        $parent = new Host();
        $parent->setId($parentId);
        $host->setParent($parent);
        $host->setId($id);
        $host->setData([]);

        $this->uriSlugParser->method('parseSlug')->willReturn([]);

        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);
        $entityToHost->setHost($host);
        $entityToHost->setEntitySlug('slug');

        $service = new PrepareHostAndEntity($this->uriSlugParser, $this->entityPreparation);
        $service->prepareFromEntityToHost($entityToHost);
        $hdao = $service->getHostDAO();
        $this->assertEquals($hdao->getHostId(), $id);
        $this->assertEquals($hdao->getHostname(), $hostname);
        $this->assertEquals($hdao->getParentId(), $parentId);
    }

    public function testPreparationWithSlugs()
    {
        $hostname = md5(time());
        $name = 'name';
        $id = 1;
        $parentId = 42;
        $entityType = Type::AUDIO_TRACK;

        $entity = new Entity();
        $entity->setName($name);
        $entity->setId($id);
        $entity->setEntityType($entityType);
        $host = new Host();
        $host->setHostname($hostname);
        $parent = new Host();
        $parent->setId($parentId);
        $host->setParent($parent);
        $host->setId($id);
        $host->setData([]);

        $slug = new Slug();
        $slug->setSlugType(Type::AUDIO_TRACK);
        $slug->setStatic(false);
        $slug->setPattern(
            [
                SlugTypeDTO::PATTERN_SLUG => 'slug',
                SlugTypeDTO::PATTERN_SEPARATOR => '/',
                SlugTypeDTO::PATTERN_ORDER => []
            ]
        );

        $slugs = [Type::AUDIO_TRACK => $slug];
        $this->uriSlugParser->method('parseSlug')->willReturn($slugs);
        $entityToHost = new EntityToHost();
        $entityToHost->setEntity($entity);
        $entityToHost->setHost($host);
        $entityToHost->setEntitySlug('slug');

        $entityDAO = new \App\Service\Processor\Infrastructure\DAO\Entity();
        $this->entityPreparation->method('prepareEntityFromEntityToHost')->willReturn($entityDAO);
        $entityDAO->setName($name);
        $entityDAO->setEntitySlug('slug');
        $entityDAO->setEntityType($entityType);
        $entityDAO->setEntityId($id);

        $service = new PrepareHostAndEntity($this->uriSlugParser, $this->entityPreparation);
        $service->prepareFromEntityToHost($entityToHost);
        $hdao = $service->getHostDAO();
        $this->assertEquals($hdao->getHostId(), $id);
        $this->assertEquals($hdao->getHostname(), $hostname);
        $this->assertEquals($hdao->getParentId(), $parentId);

        $this->assertEquals($hdao->isStatic(), false);
        $this->assertEquals($hdao->getEntityType(), $entityType);
        $this->assertEquals($hdao->getTypeSlug(), 'slug');


    }
}