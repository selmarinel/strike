<?php

namespace App\Tests\Service\SEO\Infrastructure\Service;


use App\Service\SEO\Infrastructure\Service\RobotsService;
use PHPUnit\Framework\TestCase;

class RobotsServiceTest extends TestCase
{
    public function testCorrectWork()
    {
        $string = md5(time());

        $service = new RobotsService();

        $result = $service->generate($string);
        $this->assertEquals($result,"Host: $string");
    }
}