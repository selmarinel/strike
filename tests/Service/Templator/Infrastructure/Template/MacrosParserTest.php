<?php

namespace App\Tests\Service\Templator\Infrastructure\Template;


use App\Service\Base\Infrastructure\Repository\Templator\MacrosRepository;
use App\Service\PattedDecorator\Infrastructure\Database\Entity\Macros;
use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Processor\Infrastructure\DTO\Audio\Artist;
use App\Service\Templator\Infrastructure\Template\MacrosParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MacrosParserTest extends TestCase
{
    private $macrosRepository;
    private $container;

    public function setUp()
    {
        $this->macrosRepository = $this->createMock(MacrosRepository::class);
        $this->container = $this->createMock(ContainerInterface::class);
    }

    public function testGetMacros()
    {
        $host = new Host();
        $dto = new Artist();

        $seoData = [
            'some' => 'some',
            'data' => 'data'
        ];

        $macros = new Macros();
        $macros->setSeoData($seoData);

        $this->macrosRepository->method('findByHostIdAndEntityType')->willReturn($macros);

        $service = new MacrosParser($this->macrosRepository, $this->container);
        $result = $service->getMacros($host, $dto);
        $this->assertArrayHasKey('some', $result->getData());
        $this->assertArrayHasKey('data', $result->getData());
    }

    public function testRun()
    {
        $host = new Host();
        $data = [
            'a' => 'b'
        ];

        $content = 'abc[[a]]';
        $service = new MacrosParser($this->macrosRepository, $this->container);
        $service->run($content, $host, $data);

        $this->assertEquals($content, 'abcb');
    }

    public function testRunFork()
    {
        $host = new Host();
        $data = [
            'a' => '[!c|d!]'
        ];

        $content = 'abc[[a]]';
        $service = new MacrosParser($this->macrosRepository, $this->container);
        $service->run($content, $host, $data);

        $this->assertTrue($content == 'abcc' || $content == 'abcd');
    }

    public function testRunThree()
    {
        $host = new Host();
        $data = [
            'a' => '[#:#c|d#]'
        ];

        $content = 'abc[[a]]';
        $service = new MacrosParser($this->macrosRepository, $this->container);
        $service->run($content, $host, $data);


        $this->assertTrue($content == 'abcc:d' || $content == 'abcd:c');
    }

}