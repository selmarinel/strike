<?php

namespace App\Tests\Service\Templator\Infrastructure\Widgets;


use App\Service\Processor\Infrastructure\DAO\Host;
use App\Service\Templator\Infrastructure\Widgets\WidgetParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

class WidgetParserTest extends TestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->createMock(ContainerInterface::class);
    }

    public function testParse()
    {
        $content = 'abc[%a%]';
        $host = new Host();
        $environment = $this->createMock(Environment::class);
        $data = [
            'a' => ''
        ];

        $service = new WidgetParser($this->container);
        $service->run($content, $host, $environment, $data);

        $this->assertEquals($content, 'abc');
    }
}