<?php

namespace App\Tests\Service\Templator\Infrastructure;


use App\Service\Templator\Infrastructure\TemplateLocalization;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class TemplateLocalizationTest extends TestCase
{
    public function testFunctionality()
    {
        $kernel = $this->createMock(KernelInterface::class);
        $kernel->method('getRootDir')->willReturn(__DIR__ . '/../../..');

        $localization = new TemplateLocalization($kernel);

        $result = $localization->localize();

        $this->assertTrue(!empty($result));
        $this->assertArrayHasKey('audio',$result);
        $this->assertArrayHasKey('video',$result);

    }
}