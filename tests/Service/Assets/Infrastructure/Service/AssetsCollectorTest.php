<?php

namespace App\Tests\Service\Assets\Infrastructure\Service;


use App\Service\Assets\Domain\VO\AssetsHostVOInterface;
use App\Service\Assets\Infrastructure\Exceptions\FileNotFound;
use App\Service\Assets\Infrastructure\Service\AssetsCollector;
use App\Service\Assets\Infrastructure\VO\ResponseVO;
use App\Service\Base\Infrastructure\Entity\Type;
use App\Service\EntityProcessor\Infrastructure\Database\Repositories\HostRepository;
use App\Service\Processor\Infrastructure\DAO\Host;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class AssetsCollectorTest extends TestCase
{
    private $hostRepository;

    private $kernel;

    public function setUp()
    {
        $this->hostRepository = $this->createMock(HostRepository::class);
        $this->kernel = $this->createMock(KernelInterface::class);
    }

    public function testGetDataSuccessful()
    {
        $hostType = Type::HOST_TYPE_AUDIO;
        $templateDriver = 'base';
        $hostname = md5(time());
        $host = new Host();
        $host->setHostType($hostType);
        $host->setTemplateDriver($templateDriver);
        $host->setHostname($hostname);

        $path = 'css';
        $name = 'index';
        $ext = 'css';

        $this->kernel->method('getRootDir')->willReturn(__DIR__ ."/../../../../../src");

        $assets = $this->createMock(AssetsHostVOInterface::class);
        $assets->method('getHost')->willReturn($host);

        $assets->method('getFileFolder')->willReturn($path);
        $assets->method('getFileName')->willReturn($name);
        $assets->method('getFileFormat')->willReturn($ext);

        $service = new AssetsCollector($this->hostRepository, $this->kernel);
        $response = $service->getAssetsData($assets);

        $filePath = "/internal_file/templates/driver/{$host->getHostTypeDriver()}/{$templateDriver}/$path/$name.$ext";

        $this->assertTrue($response instanceof ResponseVO);
        $this->assertEquals($response->getRedirect() , $filePath);
        $this->assertEquals($response->getHost(),$hostname);
    }

    public function testFileNotFound()
    {
        $service = new AssetsCollector($this->hostRepository, $this->kernel);
        $assets = $this->createMock(AssetsHostVOInterface::class);
        $this->expectException(FileNotFound::class);
        $response = $service->getAssetsData($assets);
    }

    public function testAnotherEnv()
    {
        putenv('APP_ENV=dev');

        $hostType = Type::HOST_TYPE_AUDIO;
        $templateDriver = 'base';
        $hostname = md5(time());
        $host = new Host();
        $host->setHostType($hostType);
        $host->setTemplateDriver($templateDriver);
        $host->setHostname($hostname);

        $path = 'css';
        $name = 'index';
        $ext = 'css';

        $this->kernel->method('getRootDir')->willReturn(__DIR__ ."/../../../../../src");

        $assets = $this->createMock(AssetsHostVOInterface::class);
        $assets->method('getHost')->willReturn($host);

        $assets->method('getFileFolder')->willReturn($path);
        $assets->method('getFileName')->willReturn($name);
        $assets->method('getFileFormat')->willReturn($ext);

        $service = new AssetsCollector($this->hostRepository, $this->kernel);
        $response = $service->getAssetsData($assets);

        $this->assertTrue($response instanceof ResponseVO);
        $this->assertNotEquals($response->getRawAsset(),'');

        putenv('APP_ENV=test');
    }
}