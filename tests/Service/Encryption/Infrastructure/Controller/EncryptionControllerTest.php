<?php

namespace App\Tests\Service\Encryption\Infrastructure\Controller;


use App\Service\Encryption\Context\Controller\EncryptionController;
use App\Service\Encryption\Domain\Handle\ImageDecryptServiceInterface;
use App\Service\Encryption\Domain\Service\DecryptionInterface;
use App\Service\Encryption\Domain\VO\ImageRequestVOInterface;
use App\Service\Encryption\Domain\VO\ResponseVOInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EncryptionControllerTest
//    extends TestCase
{
    public function testCorrectHeaders()
    {
        $host = 'example.test';
        $path = '/some_path';
        $controller = new EncryptionController();

        $request = $this->createMock(Request::class);
        $request->method('getHost')->willReturn($host);

        $decryption = $this->createMock(DecryptionInterface::class);
        $vo = $this->createMock(ImageRequestVOInterface::class);
        $image = $this->createMock(ImageDecryptServiceInterface::class);
        $response = $this->createMock(ResponseVOInterface::class);
        $image->method('makeDecrypt')->willReturn($response);

        $response = $controller->images($request, $decryption, $vo, $image);

        $this->assertTrue($response instanceof JsonResponse);
//        $this->assertTrue($response->headers->get('X-Accel-Host') === $host);
//        $this->assertTrue($response->headers->get('X-Accel-Redirect') === $path);

    }
}