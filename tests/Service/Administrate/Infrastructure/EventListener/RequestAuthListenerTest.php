<?php

namespace App\Tests\Service\Administrate\Infrastructure\EventListener;


use App\Service\Administrate\Infrastructure\EventListener\RequestAuthListener;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestAuthListenerTest extends TestCase
{
    public function testOptionsMethod()
    {
        $event = $this->createMock(GetResponseEvent::class);
        $request = $this->createMock(Request::class);
        $request->method('isMethod')->with('OPTIONS')->willReturn(true);
        $event->method('getRequest')->willReturn($request);

        $listener = new RequestAuthListener();

        $response = $listener->onKernelRequest($event);
        $this->assertTrue(true);

    }
}