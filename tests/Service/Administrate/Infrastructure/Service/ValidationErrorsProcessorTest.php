<?php

namespace App\Tests\Service\Administrate\Infrastructure\Service;


use App\Service\Administrate\Infrastructure\Service\ValidationErrorsProcessor;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationErrorsProcessorTest extends TestCase
{
    public function testProcessValidationWhileNoValidationErrors()
    {
        $constraintViolationList = $this->createMock(ConstraintViolationListInterface::class);

        $constraintViolationList->method('count')->willReturn(0);

        $validationService = new ValidationErrorsProcessor();
        $validationService->process($constraintViolationList);

        $this->assertFalse($validationService->hasErrors());
        $this->assertTrue(empty($validationService->getErrors()));
    }

    public function testProcessValidationWhileExistsValidationErrors()
    {
        $propertyPath = 'path';
        $errorMessage = 'message';

        $constraintViolation = $this->createMock(ConstraintViolation::class);
        $constraintViolation->method('getPropertyPath')->willReturn($propertyPath);
        $constraintViolation->method('getMessage')->willReturn($errorMessage);
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolationList->set(0,$constraintViolation);

        $validationService = new ValidationErrorsProcessor();
        $validationService->process($constraintViolationList);

        $this->assertTrue($validationService->hasErrors());
        $this->assertArrayHasKey($propertyPath,$validationService->getErrors());
    }
}